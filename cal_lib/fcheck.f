cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine fcheck(y,a,gx,iter,nck,ig)

c   functions:
c      check the first order optimization condition.

c   input arguments:
c      y : basic variables in the standard space.
c      a : unit normal vector in the y space.
c      gx : value of the performance function
c      iter : iteration number.

c   output arguments:
c      nck : convergence flag.
c            = -1 , convergence is not achieved.
c            = 1 , convergence is achieved.

c   calls: cdot, cerror.

c   called by: form.

c   revision: july 27 1987 by p.-l. liu.
c   last revision: march 1991 by y. zhang

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine fcheck(y,a,gx,iter,nck,ig)

      implicit   none

      include   'chck.h'
      include   'optm.h'
      include   'prob.h'

      integer (kind=4) :: iter,nck,ig
      real    (kind=8) :: gx 
      real    (kind=8) :: y(nry),a(nry)

      integer (kind=4) :: i,ir,iy
      real    (kind=8) :: bratio
      real    (kind=8) :: dya
      real    (kind=8) :: err
      real    (kind=8) :: t1,tm,tmp,tt
      real    (kind=8) :: ya0, ya,ymax,yratio, yt,yy

      real    (kind=8) :: cdot 
      save

c---  check convergence for non-convex function
c         check (beta_i-beta_i+1)/beta_i+1

        if(nidt.eq.2) then
        ya=cdot(y,y,1,1,nry)
          if (iter.eq.1) then
            ya0 = ya
            return
          endif
          if(ya.ne.0.0d0) then
          dya=abs((ya-ya0)/ya)
          else
          dya=abs(ya0)
          endif
          if (dya .le.tol ) then
            nck=1
            return
          endif
          ya0 = ya
          go to 100
        endif

c---  check fisrt-order optimization condition, i.e. check if y=beta*a.

      ya=cdot(y,a,1,1,nry)
c      commented because iop is for polak-he
c      if(iop.eq.6) ya=cc
      iy=0
      ymax=0.0d0
      yratio=0.0d0
      yt=dsqrt(cdot(y,y,1,1,nry))
      if(iter.eq.1) then
        bratio=1
        ya0=yt
       else
        if(dabs(yt).ge.0.1d0) then
          bratio=dabs(yt-ya0)/dabs(yt)
         else
          bratio=dabs(yt-ya0)
        endif
        ya0=yt
      endif
      t1=0.0d0
      do 30,i=1,nry
         yy=dabs(y(i)-ya*a(i))
         t1=t1+yy**2
         ymax=ymax+yy**2
         if(dabs(y(i)).ge.1d-10) then
           err=dabs(1.0d0-dabs(a(i)*ya/y(i)))
          else
           err=yy
         endif
         if(err.gt.yratio) then
           yratio=err
           ir=i
         endif
30    continue
      ymax=dsqrt(ymax)
      tm=dabs(gx)
      tmp=cdot(y,y,1,1,nry)
      if(tmp.gt.0.0d0) then
      tt=1-dabs(ya)/dsqrt(tmp)
      if(tt.ge.tm) tm=tt
      tm=max(tm,dsqrt(t1)/dsqrt(tmp))
      endif
c1000 format(a,i4,a,5(e11.4,1x),i3)
c1001 format(a,2(2e11.4,1x,i4,1x))
c     tm=max(yratio,tm)
c     if(int(iter/50)*50.eq.iter) then
c       write(*,1000)'iter ', iter,' 1-cos ',tt,gx,yt,yratio,tm
c     endif
      if(tm.le.tol) then
c        write(*,1000)'iter ', iter,'  1-cos ',tt,gx,yt
c        write(*,1001) 'ratio-of-y ',yratio,y(ir),ir,ymax,tm
         nck=1
      end if

c---  check iterations number

100   if(iter.gt.ni1) then
         call cerror(21,ig,0.,' ')
         nck=-1
      end if

      end subroutine fcheck
