c      subroutine cdir0

c changed by k-fujimura 09/2002

      subroutine cdir0(ifs,wq,nd,isampl)

      implicit   none

      include   'blkrel1.h'
      include   'cuts.h'
      include   'prob.h'
      include   'file.h'
      include   'line.h'
      include   'simu.h'

      integer    n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n15,n17
      integer    n18,n19,n20,n21,n22,n25,n26,n27,n29,n30,n31,n32,n33
      integer    n34,n35,  nr,nc,nd, isampl, ifs
      real*8     wq

      save

      call locate('tf  ',n1,nr,nc)
      call locate('par ',n2,nr,nc)
      call locate('bnd ',n3,nr,nc)
      call locate('tp  ',n4,nr,nc)
      call locate('ids ',n5,nr,nc)
      call locate('ib  ',n6,nr,nc)
      call locate('igt ',n7,nr,nc)
      call locate('lgp ',n8,nr,nc)
      call locate('izx ',n9,nr,nc)
      call locate('ipa ',n10,nr,nc)
      call locate('mc  ',n11,nr,nc)
      call define('x1  ',n12,nrx,1)
      call define('y1  ',n13,nry,1)
      call define('z1  ',n15,1,nry)
      call locate('sg  ',n17,nr,nc)
      call locate('ex  ',n18,nr,nc)
      call define('rr  ',n19,3,ngfc)
      call define('y2  ',n20,1,nry)
      call defini('id  ',n21,1,ngfc+2)
      call define('rot ',n22,1,ngfc+2)
      call lodefr('gv  ',n25,1,ngf)
      call defini('ir  ',n26,1,ngfc)
      call lodefr('beta',n27,1,ngf)
      call lodefr('alph',n29,nry,ngf)
      call locate('fltf',n30,nr,nc)
      call lodefr('gtor',n31,1,ngf)
      call locate('igfx',n32,nr,nc)
c
      call locate('xlin',n33,nr,nc)
      call locate('yo  ',n34,nr,nc)
      call locate('ixz ',n35,nr,nc)

      if(ifs.eq.0) then
         call dirs0(ia(n1),ia(n2),ia(n3),ia(n4),ia(n5),ia(n6),ia(n7),
     *      ia(n8),ia(n9),ia(n10),ia(n11),ia(n12),ia(n13),
     *      ia(n15),ia(n17),ia(n18),ia(n19),ia(n20),ia(n21),ia(n22),
     *      ia(n25),ia(n26),ia(n27),ia(n29),ia(n30),ia(n31),ia(n32))
      else
        call dirsimp(ia(n1),ia(n2),ia(n3),ia(n4),ia(n5),ia(n6),ia(n7),
     *      ia(n8),ia(n9),ia(n10),ia(n11),ia(n12),ia(n13),
     *      ia(n15),ia(n17),ia(n18),ia(n19),ia(n20),ia(n21),ia(n22),
     *      ia(n25),ia(n26),ia(n27),ia(n29),ia(n30),ia(n31),ia(n32),
     *      ia(n33),ia(n34),ia(n35),wq,nd,isampl)
      endif

      call delete('x1  ')
      call delete('y1  ')
      call delete('z1  ')
      call delete('y2  ')
      call delete('rr  ')
      call delete('id  ')
      call delete('rot ')
c     call delete('gv  ')
      call delete('ir  ')
      return

      end subroutine cdir0
