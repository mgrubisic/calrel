cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      double precision function cdot(p,q,np,nq,n)
c
c   functions:
c      compute the inner product of two row vectors.
c
c   input arguments:
c      p,q : two arbitrary vectors.
c      np : the declared dimension of p in the calling program.
c      nq : the declared dimension of q in the calling program.
c      n : the length of p and q.
c
c   output arguments:
c      cdot : the inner product of p and q.
c
c   called by: fcheck, fnewy1, fnewy2, fnewy3, fdirct.
c
c   last revision: sept. 18 1986 by p.-l. liu.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      real*8 function cdot(p,q,np,nq,n)

      implicit   none

      integer    np,nq,n, i
      real*8     p(np,1),q(nq,1)

      cdot = 0.0d0
      do i = 1,n
        cdot = p(i,1)*q(i,1) + cdot
      end do ! i

      end function cdot
