ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine sphbre(n,a,bt,pf)
c
c   functions:
c     use hohenbichler asymptotic formula to calculate failure probability.
c     this program is used in the case of principal curvatures .
c
c   input arguments:
c     a   : principal curvature matrix.
c     n   : =nrx-1.
c     bt  : reliability index.
c
c   output argument:
c     pf  : failure probability.
c
c   calls: dnorm.
c
c   called by: socf , sopf.
c
c   last revision: oct. 23, 1986 by h.-z. lin.
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine sphbre(n,a,bt,pf)

      implicit   none

      integer (kind=4) :: n
      real    (kind=8) :: bt,pf 
      real    (kind=8) :: a(n)

      integer (kind=4) :: i
      real    (kind=8) :: c,dab, p,p1, pai, z

      if(bt.le.0.d0) then
      do i = 1,n
        a(i) = -1.d0*a(i)
      end do ! i
      endif
      dab = abs(bt)
      call dnorm(-dab,p)
      pai = acos(-1.d0)
      p1 = exp(-bt*bt/2.d0)/sqrt(2.d0*pai)
      p1 = p1/p
      z = 1.d0
      do i = 1,n
        c = 1.d0+2.d0*p1*a(i)
        z = z*c
      end do ! i
      z = abs(z)
      pf = p/sqrt(z)
      if(bt.le.0.d0) then
        pf = 1.d0-pf
        do i = 1,n
          a(i) = -1.d0*a(i)
        end do ! i
      endif

      end subroutine sphbre
