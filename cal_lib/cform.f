      subroutine cform

      implicit   none

      include   'blkchr.h'
      include   'blkrel1.h'
      include   'file.h'
      include   'flag.h'
      include   'line.h'
      include   'optm.h'
      include   'prob.h'

      character  tim(3)*7
      integer    time(3)

      integer    ini,ist,ifragil, kk
      integer    n0,n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15
      integer    n16,n17,n18,n19,n20,n21,n22,n23,n24,n25,n26,n27,n28,n29
      integer    n30,n31,n32,n33,n37,n38,n39,n45,n47,n49,n50,n51,n52
      integer    nr,nc, npr
      real*8     fend,finc,fstart, tdefault
      real*4     tarray(2), runtime

      save

      write(not,1010)

      npr = 0
      call freei('R',npr,1)
      ini = 0
      call freei('I',ini,1)
      ist = 0
      call freei('T',ist,1)
      igf = 0
      call freei('F',igf,1)

      if(icl.eq.1.and.igf.eq.0) igf = 1
      write(not,1020) npr,ini,ist
      call locate('xn  ',n0,nr,nc)
      call locate('ex  ',n1,nr,nc)
      call locate('sg  ',n2,nr,nc)
      call locate('par ',n3,nr,nc)
      call locate('x0  ',n4,nr,nc)
      call locate('bnd ',n5,nr,nc)
      call locate('tp  ',n6,nr,nc)
      call locate('tf  ',n7,nr,nc)
      call locate('igt ',n8,nr,nc)
      call locate('lgp ',n9,nr,nc)
      call locate('ids ',n10,nr,nc)
      call locate('ipa ',n11,nr,nc)
      call locate('ib  ',n12,nr,nc)
      call locate('izx ',n13,nr,nc)
      call locate('ixz ',n14,nr,nc)
      call lodefh('fltf',n15,1,ngf)
      call lodefr('beta',n16,1,ngf)
      call lodefr('xlin',n17,nrx,ngf)
      call lodefr('alph',n18,nry,ngf)
      call lodefr('gamma',n19,nry,ngf)
      call lodefr('det ',n20,nry,ngf)
      call lodefr('eta ',n21,nry,ngf)
      call lodefr('sdgy',n22,1,ngf)
      call lodefr('x   ',n23,1,nrx)
      call lodefr('y   ',n24,1,nry)
      call lodefr('a   ',n25,1,nry)
      call lodefr('gam ',n26,1,nry)
      call lodefr('t1  ',n27,1,nry)
      call lodefr('t2  ',n28,1,nry)
      call lodefr('t3  ',n29,1,nrx)
      call lodefr('gtor',n30,1,ngf)
      call locate('igfx',n31,nr,nc)

c.... add the followings for gradient based curvature fitting method
      call lodefr('yo  ',n32,nry,ngf)
      call lodefr('ao  ',n33,nry,ngf)
      call lodefr('yoo0',n37,nry,ngf)
      call lodefr('yoo1',n38,nry,ngf)
      call lodefr('yoo2',n39,nry,ngf)
      call lodefr('bbet',n45,nry,ngf)
      call lodefr('curv',n47,nry-1,ngf)
      call lodefr('ylin',n49,nry,ngf)
      call lodefr('aa1 ',n50,nry,ngf)
      call lodefr('yy1 ',n51,nry,ngf)
      call lodefi('iste',n52,nry-1,ngf)

c      runtime = dtime(tarray)
      ifragil = 0
      call freei('G',ifragil,1)
      fstart = 0.0d0
      fend = 0.0d0
      finc = 0.0d0
      if(ifragil.ne.0) then
        if(ifragil.le.0.or.ifragil.gt.ntp) then
          write(not,'(1h ,'' !! fatal error !!'')')
          write(not,'(1h ,'' input error for fragility parameter !!'')')
          write(not,'(1h ,'' there is no '',i5,'' th parameter'')')
     *      ifragil
          stop
        endif
        call cfragil(ifragil,ia(n6),tdefault)
        fstart = tdefault
        call freer('1',fstart,1)
        fend = tdefault
        call freer('2',fend,1)
        finc = 0.0d0
        call freer('3',finc,1)
      endif

      call form(nd(1),ia(n1),ia(n2),ia(n3),ia(n4),ia(n5),ia(n6),ia(n7)
     *         ,ia(n8),ia(n9),ia(n10),ia(n11),ia(n12),ia(n13),ia(n14),
     *          ia(n15),ia(n16),ia(n17),ia(n18),ia(n19),ia(n20),ia(n21),
     *          ia(n22),ia(n23),ia(n24),ia(n25),ia(n26),ia(n27),ia(n28),
     *          ia(n29),ia(n30),ia(n31),ia(n32),ia(n33),ia(n37),ia(n38),
     *          ia(n39),ia(n45),ia(n47),ia(n49),ia(n50),ia(n51),ia(n52),
     *          npr,ini,ist,ifragil,fstart,fend,finc)

      call etime(tarray, runtime)
      tim(1) = 'hours'
      time(1) = int(runtime/60/60)
      tim(2) = 'minutes'
      time(2) = int((runtime-time(1)*60*60)/60)
      tim(3) = 'seconds'
      time(3) = int(runtime-time(1)*60*60-time(2)*60)
      if(time(1).ne.0) then
        kk = 1
      else
        if(time(2).ne.0) then
          kk = 2
        else
          kk = 3
        endif
      endif
c      write(*,2000) 'running time on form  = ',((time(i),tim(i)),i=kk,3)
c2000 format(a,3(1x,i2,1x,a))
      return

 1010 format(//' >>>> FIRST-ORDER RELIABILITY ANALYSIS <<<<'/)
 1020 format(
     & ' Print interval ..........................npr = ',i7/
     & '   npr<0 ..........no first order results are printed'/
     & '   npr = 0 ........print the final step of FORM results'/
     & '   npr>0 ........print the results of every npr steps'/
     & ' Initialization flag .....................ini = ',i7/
     & '   ini = 0 .......................start from mean point'/
     & '   ini = 1 ..........start from point specified by user'/
     & '   ini = -1 ....start from previous linearization point'/
     & ' Restart flag ............................ist = ',i7/
     & '   ist = 0 .......................analyze a new problem'/
     & '   ist = 1 .............continue an unconverged problem')
c2100  format(/' Form executing time '/
c    &  '  user time = ',e14.6,5x,' system time=',e14.6)

      end subroutine cform

      subroutine cfragil(ifragil,tp,tdefault)

      implicit   none

      integer*4  ifragil
      real*8     tp(*),tdefault

      save

      tdefault = tp(ifragil)

      end subroutine cfragil
