      subroutine dnormi (y,p,ier)

      implicit   none

      integer (kind=4) :: ier
      real    (kind=8) :: p,y

c                           specifications for local variables

      real    (kind=8) :: eps,g0,g1,g2,g3,h0,h1,h2,a,w,wi,sn,sd
      real    (kind=8) :: sigma,sqrt2,x,xinf

      data       xinf/1.7014d+38/
c     data       sqrt2/1.414214d0/
      data       eps/.59605d-07/
      data       g0/.1851159d-3/,g1/-.2028152d-2/
      data       g2/-.1498384d0/,g3/.1078639d-1/
      data       h0/.9952975d-1/,h1/.5211733d0/
      data       h2/-.6888301d-1/

c                           first executable statement

      sqrt2 = dsqrt(2.0d0)
      ier = 0
      if (p .gt. 0.0d0 .and. p .lt. 1.0d0) go to 5
      sigma = dsign(1.d0,p)
      y = sigma * xinf
      if(p.lt.0.0d0.or.p.gt.1.0d0) ier=129
      go to 9000
    5 if(p.le.eps) go to 10
      x = 1.0 -(p + p)
      call merfi (x,y,ier)
      y = -sqrt2 * y
      go to 9005
c                           p too small, compute y directly
   10 a = p+p
c     w = sqrt(-alog(a+(a-a*a)))
      w = dsqrt(-dlog(a+(a-a*a)))
c                           use a rational function in 1./w
      wi = 1./w
      sn = ((g3*wi+g2)*wi+g1)*wi
      sd = ((wi+h2)*wi+h1)*wi+h0
      y = w + w*(g0+sn/sd)
      y = -y*sqrt2
      go to 9005
 9000 continue
      call uertst(ier,'dnormi')
 9005 return

      end subroutine dnormi
