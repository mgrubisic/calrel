      subroutine goutc(t,ids,ipa,xn,ixz)

      implicit   none

      include   'file.h'
      include   'prob.h'

      character (len=72) :: out
      character (len=11) :: tt
      character (len=4)  :: xn(nrx)

      real    (kind=8) :: t(6,nrx)
      integer (kind=4) :: ids(nrx),ipa(4,nrx),ixz(nrx)

      integer (kind=4) :: i,j, kk, l
      save

      data tt/'           '/
        do 30 i=1,nrx
          out=' '
          if(ids(i).ne.7.and.ids(i).le.50) then
             write(out(1:71),2030) xn(i),(t(j,i),j=1,4)
          if(ixz(i).eq.0) then
              out(17:27)=tt
              out(39:49)=tt
              go to 99
          endif
             if(ipa(1,i).ne.0) then
              out(6:16)=tt
              out(28:38)=tt
             endif
             if(ipa(2,i).ne.0) then
              out(17:27)=tt
              out(39:49)=tt
             endif
          else
            write(out(1:71),2040) xn(i),(t(j,i),j=3,6)
            kk=28
            do 20 j=1,4
            l=kk+(j-1)*11
   20       if(ipa(j,i).ne.0) out(l:l+10)=tt
          end if
   99     write(not,'(a)') out
   30     continue
 2030 format(1x,a4,1p,4e11.3)
 2040 format(1x,a4,22x,1p,4e11.3)

      end subroutine goutc
