cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine pnet(beta,alph,rhom,prob,next,irep,rho0)
c
c   functions:
c      compute pnet failure probability.
c
c   input arguments:
c     rhom : modal correlation matrix.
c     prob : bounds of joint modal failure probabilities.
c         upper triangle - upperbounds.
c         lower triangle - lowerbounds.
c         diagonal - individual mode probabilities:
c     next : an array indicating the element which is next to this
c            element in magnitude.
c
c
c   calls: cmprin.
c
c   called by: main.
c
c   last revision: jan. 8, 1990 by hong-zong lin
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine pnet(beta,alph,rhom,prob,next,irep,id,igfx,rho0)

      implicit   none

      include   'bond.h'
      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'prob.h'

      integer (kind=4) :: next(ngfc),id(ngfc),igfx(ngf,3)
      integer (kind=4) :: irep(ngfc)
      real    (kind=8) :: rho0
      real    (kind=8) :: rhom(ngfc,ngfc),prob(ngfc,ngfc)
      real    (kind=8) :: beta(ngf),alph(nry,ngf)

      integer (kind=4) :: i,ii,ip1, ier, j,jj,k,ki,kj, ngf1
      real    (kind=8) :: bt,btij, pf, p1,p2, pnetp, r, ynet
      save

      if(icl.ne.2) return
      ngf1=ngfc-1
      if(itt.eq.1) go to 231
      call bsort(beta,next,gmin,gmax,igfx)
      do 20 i=1,ngfc
      bt=-beta(igfx(i,2))
      call dnorm(bt,pf)
   20 prob(i,i)=pf
      do 10 i=1,ngf1
      ki=igfx(i,2)
      ip1=i+1
      rhom(i,i)=1.d0
      do 10 j=ip1,ngfc
      kj=igfx(j,2)
      rhom(i,j)=0.d0
      do 12 k=1,nry
   12 rhom(i,j)=rhom(i,j)+alph(k,ki)*alph(k,kj)
   10 rhom(j,i)=rhom(i,j)
      rhom(ngfc,ngfc)=1.d0
c
c     compute bounds on joint modal failure probabilities
c     upperbounds store in upper trigangle of prob(i,j)
c     lowerbounds store in lower triangle of prob(i,j)
c     individual mode probabilities are stored along the diagonal
c
      do 30 i=1,ngf1
      ki=igfx(i,2)
      ip1 =i+1
      do 30 j=ip1,ngfc
      kj=igfx(j,2)
      r=sqrt(1.d0-rhom(i,j)*rhom(i,j))
      if(r.le.0.1e-12) then
      p1=0.5d0*prob(j,j)
      p2=0.5d0*prob(i,i)
      go to 146
      endif
      btij=-(beta(ki)-beta(kj)*rhom(i,j))/r
      call dnorm(btij,p1)
      p1=p1*prob(j,j)
      btij=-(beta(kj)-beta(ki)*rhom(i,j))/r
      call dnorm(btij,p2)
      p2=p2*prob(i,i)
  146 if(rhom(i,j).lt.0.d0) go to 31
      prob(i,j)=p1+p2
      if(abs(rhom(i,j)).le.0.1e-12)prob(i,j)=p1
      prob(j,i)=p1
      if(p2.gt.p1)prob(j,i)=p2
      go to 30
   31 prob(i,j)=p1
      if(p2.lt.p1) prob(i,j)=p2
      prob(j,i)=0.d0
   30 continue
  231 do 50 i=1,ngfc
  50  irep(i)=1
      ii=gmin
      pnetp=0.d0
      do 90 i=1,ngf1
      if(irep(ii).eq.0)go to 11
      ip1=i+1
      jj=ii
      do 80 j=ip1,ngfc
      jj=next(jj)
      if(rhom(ii,jj).gt.rho0) irep(jj)=0
  80  continue
      pnetp=pnetp+prob(ii,ii)
  11  ii=next(ii)
  90  continue
      if(irep(ii).ne.0)pnetp=pnetp+prob(ii,ii)
      if(itt.eq.1) go to 100
      write(not,1010)
      call cmprin(igfx(1,3),rhom,ngfc,ngfc,ngfc,1,0)
      write(not,1020)
      call cmprin(igfx(1,3),prob,ngfc,ngfc,ngfc,2,0)
 100  if(pnetp.gt.1.d0) pnetp=1.d0
      call dnormi(ynet,pnetp,ier)
      write(not,1030) -ynet,pnetp
      ii=gmin
      j=0
      do 70 i=1,ngfc
      if(irep(ii).eq.1)then
      j=j+1
      id(j)=igfx(ii,3)
      endif
  70  ii=next(ii)
      write(not,1040)(id(i),i=1,j)
 1010 format(' Modal correlation coefficient matrix')
 1020 format(' Lower\upper bound joint-modal probabilities')
 1030 format(/
     *' Generalized reliability index .....betag=',1p,1e11.3/
     *' Failure probability ..................pf=',1p,1e11.3/
     *' Representative modes:')
 1040 format(3x,15i5)

      end subroutine pnet
