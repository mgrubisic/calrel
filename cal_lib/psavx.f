      subroutine psavx

      implicit   none

      include   'blkrel1.h'
      include   'cdata.h'

      integer (kind=4) :: nn,n0,n1,n2,n3,n4,n5,n6,n7,n8,n9
      common /mdata/      nn,n0,n1,n2,n3,n4,n5,n6,n7,n8,n9
      integer (kind=4) :: n10,n11,n12,n13,m1
      common /mdata/      n10,n11,n12,n13,m1

      integer (kind=4) :: n11a,n11b,iaa      ,n14,n15
      common /mdat2/      n11a,n11b,iaa(2,26),n14,n15

      integer (kind=4) :: ndf,ndm,nen1,nst
      common /sdata/      ndf,ndm,nen1,nst

      call savx(ia(n8),ia(n14),ndm,ndf,numnp)

      end subroutine psavx
