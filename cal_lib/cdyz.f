      subroutine cdyz(tf,dxz,ns,nge,ngec,indx)

      implicit   none

      integer    ns,nge,ngec,indx
      real*8     tf(nge,nge),dxz(nge)

      integer    i,j,k,ken
      if(indx.eq.0) then

        do i = ns,nge
          do j = 1,ns-1
            tf(i,j) = tf(i,j)*dxz(j)
          end do ! j
        end do ! i

      else

        do j = 1,ns-1
          do i = ns,nge
            dxz(i) = 0.d0
            ken = ngec
            if(i.le.ngec) ken = i
            do k = ns,ken
              dxz(i) = dxz(i) + tf(i,k)*tf(k,j)
            end do ! k
          end do ! i
          do k = ns,nge
            tf(k,j) = dxz(k)
          end do ! k
        end do ! j

      endif

      end subroutine cdyz
