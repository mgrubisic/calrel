      subroutine goutp(t,pbu,xn,stp,sdp,ids,ipa,ixz,isv)

      implicit   none

      include   'file.h'
      include   'prob.h'

      character (len=4) ::xn(nrx)

      integer (kind=4) :: isv
      real    (kind=8) :: pbu
      real    (kind=8) :: t(6,nrx)
      real    (kind=8) :: stp(4,nrx),sdp(ntp)
      integer (kind=4) :: ids(nrx),ipa(4,nrx),ixz(nrx)

      integer (kind=4) :: i,j
      save

       if(isv.eq.2) then
       write(not,2070)
       go to 110
       endif
       write(not,2010)
        write(not,2020)
        do 12 j=1,nrx
          if(ids(j).ne.7.and.ids(j).le.50) then
            do 32 i=1,4
 32         t(i,j)=stp(i,j)/pbu
          else
            do 35 i=3,6
 35         t(i,j)=stp(i-2,j)/pbu
          end if
   12   continue
         call goutc(t,ids,ipa,xn,ixz)
        write(not,2050)
        do 14 j=1,nrx
          if(ids(j).ne.7.and.ids(j).le.50) then
            do 132 i=1,4
 132        t(i,j)=stp(i,j)
          else
            do 135 i=3,6
 135        t(i,j)=stp(i-2,j)
          end if
   14   continue
         call goutc(t,ids,ipa,xn,ixz)
      if(ntp.eq.0.or.isv.eq.1) then
      write(not,2080)
      return
      else
      write(not,2060)
      endif
  110 continue
      write(not,2095)
        do 140 i=1,ntp
  140     write(not,2090) i,sdp(i)/pbu,sdp(i)
      write(not,2080)

 2010 format(/' Sensitivity with respect to distribution',
     &    ' parameters')
 2020 format(' d(betag)/d(parameter) :'/
     &       1x,'var',5x,'mean',6x,'std dev',5x,'par 1',6x,
     &       'par 2',6x,'par 3',6x,'par 4')

 2050 format(/' d(pf1)/d(parameter) :'/
     &       1x,'var',5x,'mean',6x,'std dev',5x,'par 1',6x,
     &       'par 2',6x,'par 3',6x,'par 4')
 2080 format(1x,70('-'))
 2060 format(//' Sensitivity with respect to limit-state',
     &       ' function parameters')
 2070 format(/' Sensitivity with respect to limit-state',
     &       ' function parameters')
 2090 format(i3,8x,1pe11.3,14x,e11.3)
 2095 format(1x,'par',3x,'d(betag)/d(parameter)',
     &       5x,'d(pf1)/d(parameter)')

      end subroutine goutp
