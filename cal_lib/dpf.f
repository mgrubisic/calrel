      subroutine dpf(m1,id,rot,xp)

      implicit   none

      include   'cuts.h'
      include   'prob.h'

      integer (kind=4) :: m1
      real    (kind=8) :: xp 
      integer (kind=4) :: id(ngfc+2)
      real    (kind=8) :: rot(ngfc+2)

      integer (kind=4) :: ii,jj, in1, ier, idex,idex1
      real    (kind=8) :: cs,cs1,cs2
      real    (kind=8) :: p,p1,p2
      real    (kind=8) :: rn

c.....compute failure probability for this directinal v vector.

      xp = 0.d0
      rn = real(nry)
      idex = 1
      cs2 = 0.d0
      do ii = 1,m1-1
        idex = ii+1
        if(id(ii).ne.0) then
          cs2 = rot(idex)
        else
          if(ii.eq.1) go to 1741
          cs2 = cs2*cs2
          call dchis(cs2,rn,p2,ier)
          xp = xp+p2
c                   print *,'cs2,p2',cs2,p2
          go to 1741
        endif
      end do ! ii
      if(id(m1).ne.0) then
        xp = xp+1.d0
      else
        cs2 = cs2*cs2
        call dchis(cs2,rn,p2,ier)
        xp = xp+p2
      endif
      return
 1741 if(idex.eq.m1) go to 109
  199 do ii = idex,m1-1
        if(id(ii).ne.0) then
          cs1 = rot(ii)
          in1 = ii+1
          cs2 = rot(in1)
          if(in1.eq.m1) go to 299
          do jj = in1,m1-1
            if(id(jj).ne.0) then
              idex1 = jj+1
              cs2 = rot(idex1)
            else
              idex1 = jj+1
              cs1 = cs1*cs1
              cs2 = cs2*cs2
              call dchis(cs1,rn,p1,ier)
              call dchis(cs2,rn,p2,ier)
              xp = p2-p1+xp
              go to 141
            endif
          end do ! jj
          go to 299
        else
          idex1 = ii+1
        endif
      end do ! ii
 141  if(idex1.ne.m1) then
        idex = idex1
        go to 199
      endif
      go to 109
 299  cs1 = cs1*cs1
      call dchis(cs1,rn,p1,ier)
      if(id(m1).ne.0) then
        xp = xp+1.d0-p1
c       print *,'cs1,1-p1 = ',dsqrt(cs1),1.d0-p1
        return
      else
        cs2 = cs2*cs2
        call dchis(cs2,rn,p2,ier)
        xp = p2-p1+xp
c       print *,'cs1,cs2,p2-p1 = ',cs1,cs2,p2-p1
        return
      endif
 109  if(id(m1).ne.0) then
        cs = rot(m1)*rot(m1)
        call dchis(cs,rn,p,ier)
        xp = xp+1.d0-p
      endif

      end subroutine dpf
