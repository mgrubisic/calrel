cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine icon(name,iname)
c
c   functions:
c      convert characters to integer data.
c
c   input arguments:
c      name : name of the array.
c
c   output arguments:
c      iname : integer data corresponding to name.
c
c   called by: chgdim, defin, delete, locate.
c
c   last revision: by wilson
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine icon(name,iname)

      implicit   none

      character (len=1) ::  name(4)
      integer (kind=4)  :: iname(4), i

      do i = 1,4
        iname(i) = ichar( name(i) )
      end do ! i

      end subroutine icon
