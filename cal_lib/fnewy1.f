cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine fnewy1(x,y,z,a,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,
c                        ib,izx,sdgy,bt,gx)
c
c   functions:
c      compute new x and y using rf method.
c
c   input arguments:
c      x : current x.
c      y : current y.
c      a : unit normal vector in the y space.
c      tf : the transformation matrix from z to y, i.e. y = tf * z.
c      ex : mean of x.
c      sg : standard deviation of x.
c      par : distribution parameters of x.
c      bnd : lower and upper bound of x.
c      igt : group types.
c      lgp : location of the first element of a group in y.
c      ids : distribution numbers of x.
c      ipa : #'s of the associated rv of distribution parameters.
c             n-th element of ipa = 0 means parameter n is constant.
c      ib : code of distribution bounds of x.
c           = 0 , no bounds
c           = 1 , bounded from below.
c           = 2 , bounded from above.
c           = 3 , both sides bounded.
c      izx : indicators for z to x.
c      sdgy : |dg/dy|.
c      gx : g(x).
c      ig : failure mode number.
c
c   output arguments:
c      x : new x.
c      y : new y.
c      z : new z.
c      bt : new reliability index.
c
c   calls: cdot, cytox.
c
c   called by: form.
c
c   last revision: july 27 1987 by p.-l. liu.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine fnewy1(x,y,z,a,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,
     *                  ib,izx,sdgy,bt,gx)

      implicit   none

      include   'optm.h'
      include   'prob.h'

      real    (kind=8) :: sdgy,bt,gx
      integer (kind=4) :: ids(nrx),ipa(4,nrx)
      integer (kind=4) :: ib(nrx),igt(nig),lgp(3,nig),izx(nry)
      real    (kind=8) :: x(nrx),y(nry),z(nry)
      real    (kind=8) :: a(nry),tf(*),ex(nrx),sg(nrx)
      real    (kind=8) :: par(4,nrx),bnd(2,nrx)

      integer (kind=4) :: i
      real    (kind=8) :: s, s1
      real    (kind=8) :: t

      real    (kind=8) :: cdot
      save

c---  use hl-rf method to compute new y, step size = op1.
c
      s=gx/sdgy
      s1=cdot(y,a,1,1,nry)
      s=s+s1
      do 20 i=1,nry
         y(i)=s*a(i)
   20 continue
c
c---  transform y to x and compute new beta.
c
      call cytox(x,y,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      t=cdot(y,a,1,1,nry)
      bt=dsqrt(cdot(y,y,1,1,nry))
      if(t.lt.0.d0) bt=-bt

      end subroutine fnewy1
