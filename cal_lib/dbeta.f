      subroutine dbeta  (x,a,b,p,ier)

      implicit   none

!     specifications for arguments
      
      integer (kind=4) :: ier
      real    (kind=8) :: x,a,b,p

!     specifications for local variables

      real    (kind=8) :: aa,bb,temp
      real    (kind=8) :: ps,px,y,p1,da,xint,cnt,wh,xb,db,c,eps,eps1
      real    (kind=8) :: aleps,tot,pq,d4,one,zero
      integer (kind=4) :: int1,ib

!     machine precision

      data             eps/1.d-6/

!     smallest positive number safely representable

      data             eps1/1.d-78/

!     natural log of eps1

      data             aleps/-179.6016d0/
      data             one/1.0d0/, zero/0.0d0/

!     check ranges of the arguments


!     first executable statement
      y = x
      if ((x .le. 1.0) .and. (x .ge. 0.0)) go to 5
      ier = 129
      go to 9000
    5 if ((a .gt. 0.0) .and. (b .gt. 0.0)) go to 10
      ier = 130
      go to 9000
   10 ier = 0
      aa = a
      bb = b
      if (x .gt. 0.5) go to 15
      int1 = 0
      go to 20
!                           switch arguments for more efficient
!                           use of the power series
   15 int1 = 1
      temp = aa
      aa = bb
      bb = temp
      y = one-y
   20 if (x .ne. 0. .and. x .ne. 1.) go to 25
!                           special case - x is 0. or 1.
      p = 0.e0
      go to 60
   25 temp = int(bb)
      ps = bb-temp
      if (bb .eq. temp) ps = one
      da = aa
      db = bb
      px = da*log(y)
      d4 = log(da)
      pq = log_gamma(da+db)
      p1 = log_gamma(da)
      c = log_gamma(db)
      xb = px+log_gamma(ps+da)-log_gamma(ps)-d4-p1
!                           scaling
      ib   = int(xb/aleps)  !WIP: guess to truncate not round
      xint = zero
!                           first term of a decreasing series
!                           will underflow
      if (ib .ne. 0) go to 35
      xint = exp(xb) * 1.d10
      cnt = xint*da
!                           cnt will equal exp(temp)*(1.d0-ps)i*
!                           p*y**i/factorial(i)
      wh = zero
   30 wh = wh+one
      cnt = cnt*(wh-ps)*y/wh
      xb = cnt/(da+wh)
      xint = xint+xb
      if (xb/eps .gt. xint) go to 30
      xint = xint * 1.d-10
   35 tot = zero
      if (db .le. one) go to 55
      xb = px+db*log(one-y)+pq-p1-log(db)-c
!                           scaling
      temp = xb/aleps
      temp = int(temp)
      if(temp.lt.0.0e0) temp = 0.0e0
      c = one/(one-y)
      ps = temp
      cnt = exp(xb-ps*aleps)
      ps = db
      wh = db
   40 wh = wh-one
      if (wh .le. zero) go to 55
      px = (ps*c)/(da+wh)
      if (px .gt. one) go to 45
      if (cnt/eps .le. tot .or. cnt .le. eps1/px) go to 55
   45 cnt = cnt*px
      if (cnt .le. one) go to 50
!                           rescale
      temp = temp - 1.0e0
      cnt = cnt*eps1
   50 ps = wh
      if (temp .eq. 0.0e0) tot = tot+cnt
      go to 40
   55 p = tot+xint
   60 if (int1 .ne. 0) p = 1.-p
      go to 9005
 9000 continue
      call uertst(ier,'dbeta')
 9005 return

      end subroutine dbeta
