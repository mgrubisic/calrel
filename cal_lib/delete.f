cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine delete(name)
c
c   functions:
c      delete an array from storage.
c
c   input arguments:
c      name : name of the array.
c
c   calls: icon, ifind.
c
c   called by: cfosm, csosm, cboun, csens, cdir1, cdir2, cmont.
c
c   last revision: by wilson
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine delete(name)

      implicit   none

      include   'blkrel1.h'
      include   'dbsy.h'
      include   'file.h'

      character (len=1) :: name(4)

      integer (kind=4) :: i,ii, j,k, na, nsize, nnxt
      integer (kind=4) :: iname(4)

      integer (kind=4) :: ifind

      save

c-----delete array from storage ---------------------
      call icon(name,iname)
      i = ifind(iname,0)
      if(i.eq.0) go to 900
      nsize = ia(i+8)
c-----reduce number of arrays -----------------------
      next = next - nsize
      numa = numa - 1
      na   = ia(i+7)
      if (na.eq.next) go to 800
c-----compact storage -------------------------------
      ii   = na + nsize
      nnxt = next - 1
      do j = na,nnxt
        ia(j) = ia(ii)
        ii = ii + 1
      end do ! j
c-----compact and update directory ------------------
  800 na   = i - idir
      idir = idir + 10
      if (na.eq.0) go to 900
      na = na/10
      do k=1,na
        ii = i + 9
        do j = 1,10
          ia(ii) = ia(ii-10)
          ii     = ii - 1
        end do ! j
        if(ia(i+7).le.0) go to 860
        if(ia(i+9).eq.0) ia(i+7) = ia(i+7) - nsize
  860   i = i - 10
      end do ! k
c----------------------------------------------------
  900 return

      end subroutine delete
