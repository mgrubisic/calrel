ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     subroutine snewg(ur,rg,bt,c,n)
c
c     function: determinate the saddle point.
c
c     last version: nov. 1989 by h.z. lin
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine snewg(ur,rg,bt,c,n)

      implicit   none

      integer (kind=4) :: n
      real    (kind=8) :: ur,rg,bt
      real    (kind=8) :: c(n)

      integer (kind=4) :: i
      real    (kind=8) :: u2
      rg=0.d0
      u2=2.d0*ur
      do 10 i=1,n
      rg=rg+c(i)/(1.d0-c(i)*u2)
   10 continue
      rg=rg+ur+bt-1.d0/ur

      end subroutine snewg
