      subroutine gcom1(t,x,y,ex,sg,dgx,tf0,tf1,par,bnd,ids,ipa,
     *                 igt,izx,ixz,dadp,dxz,nge,nges,j0,j1,ii)

      implicit   none

      include   'prob.h'

      integer (kind=4) :: nge,nges, j0,j1, ii
      integer (kind=4) :: ipa(4,nrx),igt(nig),izx(nry),ixz(nrx)
      integer (kind=4) :: ids(nrx)
      real    (kind=8) :: x(nrx),y(nry),ex(nrx),sg(nrx),dgx(nrx)
      real    (kind=8) :: t(6,nrx),tf0(nge,nge),par(4,nrx),bnd(2,nrx)
      real    (kind=8) :: dadp(nry,4,nrx),tf1(nge,nge),dxz(nrx),p(4)

      integer (kind=4) :: i,j,k, ic,id, kl,kp
      real    (kind=8) :: ex0,sg0,pp,rp
c       do 390 j=1,nrx
c 390   t(6,j)=x(j)
c     use finite difference method.
c       print *,'j0=',j0,'j1=',j1
        do 395 j=j0,j1
        i=izx(j)
        do 899 kp=1,4
  899   p(kp)=par(kp,i)
        ex0=ex(i)
        sg0=sg(i)
        do 994 k=1,4
        if(ids(i).ne.7.or.ids(i).le.50) then
        go to (200,300,400,400) k
  200   pp=ex(i)
        if(dabs(pp).le.0.1e-8) then
        rp=0.001d0
        else
        rp=0.001d0*pp
        endif
        ex(i)=pp+rp
        id=ids(i)
        go to 399
  300   pp=sg(i)
        if(dabs(pp).le.0.1e-8) then
        rp=0.001d0
        else
        rp=0.001d0*pp
        endif
        sg(i)=pp+rp
        id=ids(i)
        go to 399
  400   if(ids(i).eq.1) then
        do 435 kl=1,nry
        dadp(kl,3,i)=dadp(kl,1,i)
  435   dadp(kl,4,i)=dadp(kl,2,i)
        go to 395
        endif
        pp=par(k-2,i)
        if(dabs(pp).le.0.1e-8) then
        rp=0.001d0
        else
        rp=0.001d0*pp
        endif
        par(k-2,i)=pp+rp
        id=-ids(i)
        go to 399
        endif
        pp=par(k,i)
        if(dabs(pp).le.0.1e-8) then
        rp=0.001d0
        else
        rp=0.001d0*pp
        endif
        par(k,i)=pp+rp
        id=-ids(i)
  399   call cparam(ex(i),sg(i),par(1,i),id)
          if(j.lt.j0+nges) then
          call cdxz(x,dgx(j0),par,ids,izx(j0),nges)
c         print *,'dxz=',(dgx(ka),ka=j0,j0+nges-1)
          do 92 ic=nges+1,nge
          tf1(ic,ic)=1.d0/tf0(ic,ic)
  92      continue
c         print *,'tf1='
c         call mprint(tf1,nge,nge,1)
          call fgam(tf1,dgx(j0),nges,nge)
          do 90 ic=j0,j0+nges-1
 90       dgx(ic)=(dgx(ic)-dxz(ic))/rp
          else
          call cdyx(x,y(j0),ex,sg,par,bnd,tf1,ids,ipa,izx(j0),
     *            ixz,nges,nge,j0,igt(ii),0)
          call fgam(tf1,dxz(j0),nges,nge)
            if(nges.ne.0) then
            do 94 ic=j0,j0+nges-1
 94         dgx(ic)=0.d0
            endif
          endif
c          print *,'inverse tf, ie, dxz= for j=',j,'k=',k
c         call mprint(tf1,nge,nge,1)
        call gdgyp(t,tf1,tf0,dgx(j0),dadp,nge,nges,rp,j0
     *             ,i,k)
        do 940 kp=1,4
  940   par(kp,i)=p(kp)
        ex(i)=ex0
        sg(i)=sg0
  994   continue
  395   continue
c       do 380 ki=1,nrx
c 380   x(ki)=t(6,ki)

       end subroutine gcom1
