      subroutine symsol(xkk,u,nn,mm)

      implicit   none

      integer (kind=4) :: nn,mm
      real    (kind=8) :: u(nn),xkk(nn,nn)

      integer (kind=4) :: i,j,k,l,n, ka
      real    (kind=8) :: c 
c     reduce stiffness matrix

      do n = 1,mm-1
        do l = n+1,mm
          c = xkk(l,n)/xkk(n,n)
          do k = n+1,mm
            xkk(l,k) = xkk(l,k)-c*xkk(n,k)
          end do ! k
          xkk(l,n) = c
        end do ! l
      end do ! n

c     reduce load vector

      do n = 1,mm-1
        do l = n+1,mm
          u(l) = u(l)-xkk(l,n)*u(n)
        end do ! l
      end do ! n

c     back substitution

      do k = mm,1,-1
        do j = mm,k,-1
          if(j.eq.k) then
            u(k) = u(k)/xkk(k,k)
          else
            u(k) = u(k)-xkk(k,j)*u(j)
          endif
        end do ! j
      end do ! k

      do i = mm+1,nn
        u(i) = 0.d0
      end do ! i
      print *,'u = ',(u(ka),ka = 1,nn)

      end subroutine symsol
