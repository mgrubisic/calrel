      subroutine grbimo(xn,xlin,beta,sdgy,tp,ex,sg,par,igt,lgp,ipa,
     * izx,ixz,ids,tf,alph,next,prob,rhom,ibx,t,tj,ddpi,drip,ti,bdp,
     * dadi,drid,bnd,ilub,stp1,sdp1,sdxz,stf,igg,igfx,isv)

      implicit   none

      include   'bond.h'
      include   'cuts.h'
      include   'file.h'
      include   'prob.h'

      character (len=4) :: xn(nrx)

      integer (kind=4) :: isv
      real    (kind=8) :: xlin(nrx,ngf),t(6,nrx),tp(*)
      integer (kind=4) :: ids(nrx),next(ngfc),ibx(ngfc-1,2),igfx(ngf,3)
      real    (kind=8) :: beta(ngf),sdgy(ngf),alph(nry,ngf)
      real    (kind=8) :: prob(ngfc,ngfc),drip(4,nrx),drid(ntp)
      real    (kind=8) :: rhom(ngfc,ngfc),ddpi(nry,4,nrx*ngfc)
      real    (kind=8) :: bdp(ntp,ngfc),stf(ntge,ngf),dadi(nry,ntp*ngfc)
      real    (kind=8) :: ti(4,nrx),tj(4,nrx),bnd(2,nrx),tf(ntf)
      real    (kind=8) :: par(4,nrx)
      integer (kind=4) :: igt(nig),ixz(nrx),izx(nry)
      integer (kind=4) :: ipa(4,nrx),lgp(3,nig)
      real    (kind=8) :: ex(nrx),sg(nrx),sdxz(nrx,3*ngf)
      integer (kind=4) :: ilub(ngfc-1,ngfc-1),igg(ngfc*2)
      real    (kind=8) :: stp1(4,nrx),sdp1(ntp)

      integer (kind=4) :: i,i0,i1,i9,igd,ii,im,imo,index, ier
      integer (kind=4) :: j0,j1,j9,jj
      integer (kind=4) :: k,ki,ki0,kii,kj,kj0,kjj
      real    (kind=8) :: pai,pbi,pbj,pbu, ro2, yu2
      save

      pai = acos(-1.d0)
      call dnormi(yu2,pu2,ier)
      pbu = -0.39894228d0*exp(-yu2*yu2*0.5d0)
      ii = gmin
      do 260 i = 2,ngfc
      ii = next(ii)
      kii = igfx(ii,2)
      pbi = exp(-beta(kii)*beta(kii)*0.5d0)/sqrt(2.d0*pai)
      jj = ibx(i-1,1)
      if(jj.eq.0) go to 260
      kjj = igfx(jj,2)
      if(ii.lt.jj)then
      i9 = jj
      j9 = ii
      else
      i9 = ii
      j9 = jj
      endif
      if(ilub(i9-1,j9).eq.0) go to 260
      if(ilub(i9-1,j9).eq.1) then
      index = 1
      if(ii.gt.jj) index = 2
      else
      index = 2
      if(ii.gt.jj) index = 1
      endif
      pbj = exp(-beta(kjj)*beta(kjj)*0.5d0)/sqrt(2.d0*pai)
!     compute  the system sensitivity w. r. t. distribution paprmeters for
!     relax bimodal bound method.
!     compute d(btij)/d(par), d(pfij)/d(par).
      ro2 = sqrt(1.d0-rhom(ii,jj)*rhom(ii,jj))
      if(isv.eq.2) go to 220
!     read d(bti)/d(par) and d(btj)/d(par).
      call ginp(ti,tj,ids,kii,kjj)
!     compute d(ro ij)/d(par)
      do i1 = 1,nrx
        do j1 = 1,4
          drip(j1,i1) = 0.d0
        end do ! j1
      end do ! i1
      imo = 0
      do 920 im = 1,2
      if(imo.eq.0) then
      i0 = ii
      j0 = jj
      imo = 1
      else
      i0 = jj
      j0 = ii
      endif
      ki0 = igfx(i0,2)
      kj0 = igfx(j0,2)
      igd = (i0-1)*nrx
      if(igg(i0).eq.0) then
      call fsave(stf,sdxz,tf,t(1,1),igt,lgp,ki0,1,t(1,2),t(1,3))
      call gdapar(xlin(1,ki0),sdxz(1,ki0+2*ngf),ex,sg,alph(1,ki0)
     *,sdgy(ki0),sdxz(1,ki0+ngf),t,stf(1,ki0),par,igt,lgp,ids,ipa,
     * izx,ixz,ddpi(1,1,igd+1),tf,bnd,sdxz(1,ki0),igfx(i0,3))
       igg(i0) = 1
      endif
      do 290 ki = 1,nrx
      do 290 kj = 1,4
      do 290 k = 1,nry
  290 drip(kj,ki) = drip(kj,ki)+alph(k,kj0)*ddpi(k,kj,ki+igd)
  920 continue
!     compute d(pfij)/d(par)
      if(index.eq.1) then
      call gdfijp(ti,tj,drip,beta(kii),beta(kjj),rhom(ii,jj),
     *     ro2,pbi,prob(ii,ii),stp1)
      else
      call gdfijp(tj,ti,drip,beta(kjj),beta(kii),rhom(ii,jj),
     *     ro2,pbj,prob(jj,jj),stp1)
      endif
!     compute d(a)/d(par) when par is the deterministic parameters.
  220 if(ntp.eq.0.or.isv.eq.1) go to 250
      do 230 ki = 1,ntp
  230 drid(ki) = 0.d0
      imo = 0
      do 930 im = 1,2
      if(imo.eq.0) then
      i0 = ii
      j0 = jj
      imo = 1
      else
      i0 = jj
      j0 = ii
      endif
      ki0 = igfx(i0,2)
      kj0 = igfx(j0,2)
      igd = (i0-1)*ntp
      if(igg(i0+ngfc).eq.0) then
      call fsave(stf,sdxz,tf,t(1,1),igt,lgp,ki0,1,t(1,2),t(1,3))
      call gdadtp(xlin(1,ki0),sdxz(1,ki0+2*ngf),tp,ex,sg,alph(1,ki0)
     * ,sdgy(ki0),tf,par,bnd,igt,lgp,ids,ipa,izx,ixz,t,dadi(1,igd+1),
     *  sdxz(1,ki0),igfx(i0,3))
      igg(i0+ngfc) = 1
      endif
      do 430 ki = 1,ntp
      do 430 k = 1,nry
  430 drid(ki) = drid(ki)+alph(k,kj0)*dadi(k,ki+igd)
  930 continue
!     compute d(pfij)/d(par)
      if(index.eq.1) then
      call gdfijd(bdp(1,ii),bdp(1,jj),drid,beta(kii),beta(kjj),
     *rhom(ii,jj),ro2,pbi,prob(ii,ii),sdp1)
      else
      call gdfijd(bdp(1,jj),bdp(1,ii),drid,beta(kjj),beta(kii),
     *rhom(ii,jj),ro2,pbj,prob(jj,jj),sdp1)
      endif
 250  continue
  260 continue
!     output the result
      write(not,4020)
      call goutp(t,pbu,xn,stp1,sdp1,ids,ipa,ixz,isv)
 4020 format(///' sensitivity measures based on relax bimodal',
     *    ' upper bound of pf1',
     *       /,1x,70('-'))

       end subroutine grbimo
