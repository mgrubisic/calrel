ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine snews0(s,u,bt,c,n,rgg)
c
c     function: compute the integrand for the semi-pricipal curvature
c
c     last version: aug. 1990, by h.-z. lin
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine snews0(s,us,bt,c,n,zz)

      implicit   none

      integer (kind=4) :: n
      real    (kind=8) :: s, us, bt
      real    (kind=8) :: c(2,n)

      integer (kind=4) :: i
      complex (kind=8) ::  z,zz,z2,zn,zp,zd

      z=dcmplx(us,s)
      zz=bt+z
      zz=zz*zz/2.
      zz= exp(zz)/z
      z2=2.d0*z
      zd=(1.d0,0.d0)
      do i=1,n
        zn=1.d0-z2*c(1,i)
        zp=1.d0-z2*c(2,i)
        zd=zd*(1.d0/sqrt(zn)+1.d0/sqrt(zp))/2.d0
      end do ! i
      zz=zz*zd

      end subroutine snews0
