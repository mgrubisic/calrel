      subroutine cdir1(ifs,wq,nd,isampl)

      implicit   none

      include   'blkrel1.h'
      include   'prob.h'
      include   'cuts.h'

      integer    ifs,isampl
      real*8     wq

      integer    n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14
      integer    nr,nc,nd

      save

      call locate('beta',n1,nr,nc)
      call locate('alph',n2,nr,nc)
      call locate('mc  ',n3,nr,nc)
      call define('x8  ',n4,1,nry)
      call define('y5  ',n5,1,nry)
      call define('rr  ',n6,1,ngfc)
      call define('a2  ',n7,2,ngfc)
      call define('rot ',n8,1,2+ngfc)
      call defini('ind ',n9,1,ngfc)
      call defini('id  ',n10,1,ngfc+2)
      call defini('ig  ',n11,1,ngfc)
      call locate('igfx',n12,nr,nc)
c
      call locate('xlin',n13,nr,nc)
      call locate('yo  ',n14,nr,nc)
c
      if(ifs.eq.1) then
      call dirs1(ia(n1),ia(n2),ia(n3),ia(n4),ia(n5),ia(n6),
     *     ia(n7),ia(n8),ia(n9),ia(n10),ia(n11),ia(n12))
      else
      call dirsi1(ia(n1),ia(n2),ia(n3),ia(n4),ia(n5),ia(n6),
     *     ia(n7),ia(n8),ia(n9),ia(n10),ia(n11),ia(n12),
     *     ia(n13),ia(n14),wq,nd,isampl)
      endif
      call delete('x8  ')
      call delete('y5  ')
      call delete('rr  ')
      call delete('a2  ')
      call delete('rot ')
      call delete('ind ')
      call delete('id  ')
      call delete('ig  ')

      end subroutine cdir1
