cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine cboun1(x,bnd,ib,sg)
c
c
c   input arguments:
c      x : x computed by inverse transformation from y.
c      ib : code of distribution bounds of x.
c            =  0 , no bounds
c            =  1 , bounded from below.
c            =  2 , bounded from above.
c            =  3 , both sides bounded.
c      bnd : lower and upper bound of x.
c
c   output arguments:
c      x : updated x.
c
c   called by: cztox.
c
c   last revision: april 20, 1988 by h. z. lin.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cboun1(x2,bnd,ib,sg,pr,f2)

      implicit   none

      integer    ib
      real*8     x2, sg,pr,f2
      real*8     bnd(2)

      go to (60,30,40,50) ib + 1

   30 continue
      if(x2.le.bnd(1)) then
      x2 = (x2 + sg + bnd(1))*0.5d0
      go to 30
      else
      return
      endif
   40 continue
      if(x2.ge.bnd(2)) then
      x2 = (x2 + bnd(2) - sg)*0.5d0
      go to 40
      else
      return
      endif
   50 continue
      if(x2.le.bnd(1)) then
   55    x2 = (x2 + sg + bnd(1))*0.5d0
         if(x2.le.bnd(1)) go to 55
      else if(x2.ge.bnd(2)) then
   56    x2 = (x2 + bnd(2) - sg)*0.5d0
         if(x2.ge.bnd(2)) go to 56
      end if
      return
  60  if(pr.gt.f2) then
      x2 = x2 + sg
      else
      x2 = x2 - sg
      endif

      end subroutine cboun1
