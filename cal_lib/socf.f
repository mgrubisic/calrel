ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine socf(tf,xlin,alph,sg,ex,par,bnd,ib,tp,ids,srt,y1,y0,z,
c    *           c,ddg,u,beta,sdgy,igt,lgp,izx,ipa,fltf,iso,itg,icu)
c
c     functions:
c       compute failure probabilities by means of curvature fitting
c       method. (only for the single component of performanace function)
c
c     input arguments:
c       tf  : the transformation matrix from z to y, y=tf * z.
c       xlin   : mostly likely failure point in x.
c       alph   : unit normal vector at design point in the y space.
c       sg  : standard deviation of x.
c       par : distribution parameters of x.
c       bnd : lower and upper bound of x.
c       tp  : deterministic parameters.
c       ids : distribution number of x.
c       ib  : code of distribution bounds of x.
c       beta  : reliability index.
c       sdgy: |dg/dy|
c       itg : eq. 2 , use hb and ei compute pf.
c             ne. 2 , use hb formula only.
c       iso : second order scheme to use.
c             =1 point-fitting method.
c             =2 curvature-fitting method.
c             =3 both of the above.
c
c     output arguments:
c      srt  : rotational transformation matrix, y=rt * y' , last col. in
c             rt is vector a.
c
c     calls: sgs, scurvt, seigen, sphbre, shbrei, dnormi, sptved, cmprin
c
c     called by: main.
c
c     last revision: jan. 8, 1990 by h.-z. lin.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine socf(tf,xlin,alph,sg,ex,par,bnd,ib,tp,ids,srt,
     *                y1,y0,z,c,ddg,u,beta,sdgy,
c    *          igt,lgp,izx,ipa,fltf,x,dgx,rt1,iso,itg,icu)
     *          igt,lgp,izx,ipa,fltf,x,fltc,igfx,iso,itg)

      implicit    none

      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'prob.h'

c     real tarray(2)

      logical          :: fltf(ngf),fltc(ngf)

      integer (kind=4) :: iso, itg
      integer (kind=4) :: izx(nry),lgp(3,nig),ipa(4,nrx),igfx(ngf,3)
      integer (kind=4) :: ib(nrx),igt(nig),ids(nrx)
      real    (kind=8) :: tf(ntf),xlin(nrx,ngf),z(nry)
      real    (kind=8) :: tp(*),c(nry-1),srt(nry,nry,ngf)
      real    (kind=8) :: ddg(nry-1,nry-1),u(nry-1,nry-1),bnd(2,nrx)
      real    (kind=8) :: y0(nry),sg(nrx),ex(nrx),par(4,nrx),y1(nry)
      real    (kind=8) :: alph(nry,ngf),beta(ngf),x(nrx),sdgy(ngf)

      integer (kind=4) :: ig2,ig4, igk, ier
      integer (kind=4) :: in, ip, ir
      integer (kind=4) :: jj
      integer (kind=4) :: k, kj
      integer (kind=4) :: lm, ln, lp
      real    (kind=8) :: betag1, betag2
      real    (kind=8) :: pf, pf1,pf2,pf3,pf4
      real    (kind=8) :: sa 

      save

      if(icl.eq.1) call finim(igfx,igf)
      do 200 igk=1,ngfc
      ig2=igfx(igk,2)
      if(.not.fltf(ig2)) call cerror(5,0,0.,' ')
            if(fltc(ig2)) go to 200
      fltc(ig2)=.true.
      ig4=igfx(igk,3)
      write(not,2000) ig4
      write(not,2010)
      if(abs(beta(ig2)).gt.8.0) then
        betag1=beta(ig2)
        betag2=betag1
        call dnorm(-betag1,pf1)
        pf2=pf1
        go to 160
      endif
c-----orthonormalize the direction cosine
      if(iso.ne.2) go to 30
      do 101 k=1,nry
 101  z(k)=alph(k,ig2)
      kj=0
      do 10 k=nry,1,-1
      if(dabs(z(k)).le.0.1e-9) then
      kj=kj+1
      else
      go to 20
      endif
   10 continue
   20 continue
      call sgs(srt(1,1,ig2),z,nry,y0,kj)
c-----compute the rotated curvature matrix at design point
   30 continue
c     if(icu.eq.0) then
      call scurvt(ddg,xlin(1,ig2),z,tp,srt(1,1,ig2),y1,y0,tf,ids,
     * par,beta(ig2),bnd,sg,ib,sdgy(ig2),ex,ig4,igt,lgp,izx,ipa,x)
c     else
c     call scurvc(ddg,xlin(1,ig2),z,tp,srt(1,1,ig2),y1,y0,tf,ids,
c    * par,bnd,sg,ib,sdgy(ig2),ex,igt,lgp,izx,ipa,dgx,rt1,ig4)
c     endif
c.....compute the failure probability for the case of pricipal curvature matrix
c-----check if sum. of abs. values of column ( or row) is equql to zero.
c     if yes, shift these columns and rows to the n-th column and row in
c     order to make eigenvalue program working.
      if(nry.eq.2) then
      c(1)=ddg(1,1)
      write(not,2020)
      ddg(1,1)=2.d0*ddg(1,1)
      call cmprin(ib,ddg,1,1,1,1,1)
      if(itg.ne.2) go to 150
      go to 140
      endif
      if(itg.ne.2) go to 150
      jj=0
      do 50 lp=1,nry-1
      sa=0.d0
      do 40 ln=1,nry-1
   40 sa=sa+dabs(ddg(lp,ln))
      if(sa.ge.1.e-10) then
      c(lp)=1.d0
      else
      jj=jj+1
      c(lp)=0.d0
      endif
   50 continue
      if(jj.eq.0) go to 111
      in=0
      do 110 ir=1,nry-1
      if(ir.ge.nry-in) go to 111
      if(c(ir).ge.0.1) go to 110
      in=in+1
   60 if(c(nry-in).gt.0.1) then
      do 70 ip=1,nry-1
      u(ir,ip)=ddg(nry-in,ip)
   70 u(nry-in,ip)=ddg(ir,ip)
      do 80 ip=1,nry-1
      ddg(ir,ip)=u(ir,ip)
   80 ddg(nry-in,ip)=u(nry-in,ip)
      do 90 ip=1,nry-1
      u(ip,ir)=ddg(ip,nry-in)
   90 u(ip,nry-in)=ddg(ip,ir)
      do 100 ip=1,nry-1
      ddg(ip,ir)=u(ip,ir)
  100 ddg(ip,nry-in)=u(ip,nry-in)
      go to 110
      else
      in=in+1
      if(in.lt.jj) go to 60
      endif
  110 continue
  111 continue
      do 120 ln=1,nry-jj-1
      do 120 lm=1,nry-jj-1
  120 u(ln,lm)=2.d0*ddg(ln,lm)
c     call cmprin(ib,ddg,nry-1,nry-1,nry-1,1,1)
c-----compute principal curvatures.
      call seigen(u,nry-1,jj)
c     call cmprin(ib,u,nry-1,nry-1,nry-1,1,1)
      do 130 lm=1,nry-1
      if(lm.le.nry-jj-1) then
      c(lm)=u(lm,lm)
      else
      c(lm)=0.d0
      endif
  130 continue
      write(not,2020)
      call cmprin(ib,c,1,nry-1,1,2,1)
      do 971 lm=1,nry-1
  971 c(lm)=0.5d0*c(lm)
c-----compute failure prob. and generated reliability index.
c-----improved breitung formula
  140 call sphbre(nry-1,c,beta(ig2),pf1)
      pf3=1.d0-pf1
      call dnormi(betag1,pf3,ier)
c---  Tvedt double integral formula
c     call sptved(nry-1,c,beta(ig2),pf2)
      call sptvee(nry-1,c,beta(ig2),pf2)
      pf4=1.d0-pf2
      call dnormi(betag2,pf4,ier)
      go to 160
  150 write(not,2030)
c.....compute the failure probability for the case of full curvature matrix.
      call shbrei(nry-1,ddg,beta(ig2),pf1,u)
      pf=1.d0-pf1
      call dnormi(betag1,pf,ier)
      if(betag1.le.-1.5) then
      write(not,2045) betag1,pf
      else
      write(not,2040) betag1,pf1
      endif
      go to 200
  160 write(not,2030)
      if(betag1.le.-1.5.or.betag2.le.-1.5) then
      write(not,2055) betag1,betag2,pf3,pf4
      else
      write(not,2050) betag1,betag2,pf1,pf2
      endif
      write(not,2010)
  200 continue
c---  output formats
 2000 format(/' Limit-state function',i5)
 2010 format(1x,77('-'))
 2020 format(/' Main curvatures in (n-1)x(n-1) space')
 2030 format(39x,'Improved Breitung',6x,7hTvedt's,' EI')
 2040 format(' Generalized reliability index betag =',5x,f9.6,13x,'----'
     */,' probability                     Pf2 =',1pe14.7,13x,'----')
 2045 format(' Generalized reliability index betag =',5x,f9.6,13x,'----'
     */,' probability                     Pf2 =',' 1- ',1pe14.7,13x,
     *'----')
 2050 format(' Generalized reliability index betag =',5x,f9.6,11x,f9.6,
     */,' probability                     Pf2 =',1pe14.7,6x,1pe14.7)
 2055 format(' Generalized reliability index betag =',5x,f9.6,11x,f9.6,
     */,' probability                     Pf2 =',' 1- ',1pe14.7,2x,
     *' 1- ',1pe14.7)
c     run= etime(tarray)

      end subroutine socf
