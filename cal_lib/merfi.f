      subroutine merfi (p,y,ier)

c                specifications for arguments

      real*8     p,y
      integer    ier

c                specifications for local variables

      real*8     a,b,x,z,w,wi,sn,sd,f,z2,rinfm,a1,a2,a3,b0,b1,
     *           b2,b3,c0,c1,c2,c3,d0,d1,d2,e0,e1,e2,e3,f0,f1,
     *           f2,g0,g1,g2,g3,h0,h1,h2,sigma

      data       a1/-.5751703d0/,a2/-1.896513d0/,a3/-.5496261d-1/
      data       b0/-.113773d0/,b1/-3.293474d0/,b2/-2.374996d0/
      data       b3/-1.187515d0/
      data       c0/-.1146666d0/,c1/-.1314774d0/,c2/-.2368201d0/
      data       c3/.5073975d-1/
      data       d0/-44.27977d0/,d1/21.98546d0/,d2/-7.586103d0/
      data       e0/-.5668422d-1/,e1/.3937021d0/,e2/-.3166501d0/
      data       e3/.6208963d-1/
      data       f0/-6.266786d0/,f1/4.666263d0/,f2/-2.962883d0/
      data       g0/.1851159d-3/,g1/-.2028152d-2/
      data       g2/-.1498384d0/,g3/.1078639d-1/
      data       h0/.9952975d-1/,h1/.5211733d0/
      data       h2/-.6888301d-1/
      data       rinfm/1.7014d+38/

c                first executable statement

      ier = 0
      x = p
      sigma = dsign(1.d0,x)
c                           test for invalid argument
      if (.not.(x.gt.-1. .and. x.lt.1.)) go to 30
      z = dabs(x)
      if (z.le. .85) go to 20
      a = 1.-z
      b = z
c                           reduced argument is in (.85,1.),
c                              obtain the transformed variable
c   5 w = sqrt(-alog(a+a*b))
      w = dsqrt(-dlog(a+a*b))
      if (w.lt.2.5) go to 15
      if (w.lt.4.) go to 10
c                           w greater than 4., approx. f by a
c                              rational function in 1./w
      wi = 1./w
      sn = ((g3*wi+g2)*wi+g1)*wi
      sd = ((wi+h2)*wi+h1)*wi+h0
      f = w + w*(g0+sn/sd)
      go to 25
c                           w between 2.5 and 4., approx. f
c                              by a rational function in w
   10 sn = ((e3*w+e2)*w+e1)*w
      sd = ((w+f2)*w+f1)*w+f0
      f = w + w*(e0+sn/sd)
      go to 25
c                           w between 1.13222 and 2.5, approx.
c                              f by a rational function in w
   15 sn = ((c3*w+c2)*w+c1)*w
      sd = ((w+d2)*w+d1)*w+d0
      f = w + w*(c0+sn/sd)
      go to 25
c                           z between 0. and .85, approx. f
c                              by a rational function in z
   20 z2 = z*z
      f = z+z*(b0+a1*z2/(b1+z2+a2/(b2+z2+a3/(b3+z2))))
c                           form the solution by mult. f by
c                              the proper sign
   25 y = sigma*f
      ier = 0
      go to 9005
c                           error exit. set solution to plus
c                              (or minus) infinity
   30 ier = 129
      y = sigma * rinfm

      call uertst(ier,'merfi ')

 9005 return
      end
