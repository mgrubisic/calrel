c$Id:$
      subroutine cdirs

      implicit   none

      include   'file.h'
      include   'flag.h'
      include   'flcg.h'
      include   'line.h'
      include   'simu.h'

      integer    icen,ifs, nd
      real*8     wq

      save

      nsm = 10000
      call gseed(stp)
      call freer('p',stp,1)
      call freei('m',nsm,1)
      npr = max0(nsm/20,1)
      call freei('r',npr,1)
      cov = 0.05d0
      call freer('v',cov,1)
      if(igf.eq.0) then
      call freei('f',igf,1)
      if(icl.eq.1.and.igf.eq.0) igf = 1
      endif
      ist = 0
      call freei('t',ist,1)
      ifs = 1
      call freei('s',ifs,1)
      wq = 0.5d0
      call freer('q',wq,1)
      nd = 0
      call freei('g',nd,1)
      icen = 0
      call freei('n',icen,1)

c
      if(ifs.eq.0.or.ifs.eq.-1) then
        if(ist.eq.0) then
          if(ifs.eq.0) then
          write(not,1110)
          else
          write(not,1111)
          endif
        else
          if(ifs.eq.0) then
          write(not,1120)
          else
          write(not,1121)
          endif
        endif
        write(not,1030) npr,nsm,cov,stp
        if(icl.eq.1) write(not,2000) igf
          call cdir0(ifs,wq,nd,icen)
      else if(ifs.eq.1.or.ifs.eq.-2) then
        if(flc(3)) call cerror(5,0,0.,' ')
        if(ist.eq.0) then
          write(not,1010)
        else
          write(not,1020)
        endif
        write(not,1030) npr,nsm,cov,stp
        if(icl.eq.1) write(not,2000) igf
        call cdir1(ifs,wq,nd,icen)
      else
        if(flc(3)) call cerror(5,0,0.,' ')
        if(flc(5)) call cerror(7,0,0.,' ')
        if(ist.eq.0) then
          write(not,1040)
        else
          write(not,1050)
        endif
        write(not,1030) npr,nsm,cov,stp
       if(icl.eq.1) write(not,2000) igf
        call cdir2
      endif

 1010 format(//' >>>> FIRST-ORDER DIRECTIONAL SIMULATION <<<<'/)
 1020 format(//' >>>> FIRST-ORDER DIRECTIONAL SIMULATION',
     &         ' -- RESTART <<<<'/)
 1030 format(
     & '  Print interval ........................ npr=',i8/
     & '  Number of simulationts ................ nsm=',i8/
     & '  Threshold for coef. of variation ... cov=',1p,e11.3/
     & '  Random seed ........................ stp=',f11.0)
 1040 format(//' >>>> SECOND-ORDER DIRECTIONAL SIMULATION <<<<'/)
 1050 format(//' >>>> SECOND-ORDER DIRECTIONAL SIMULATION',
     &         ' -- RESTARt <<<<'/)
 1110 format(//' >>>> DIRECTIONAL SIMULATION <<<<'/)
 1111 format(//' >>>> DIRECTIONAL IMPORTANCE SIMULATION <<<<'/)
 1120 format(//' >>>> DIRECTIONAL SIMULATION -- RESTART <<<<'/)
 1121 format(//' >>>> DIRECTIONAL IMPORATNCE SIM .-- RESTART <<<<'/)
 2000 format(/' Limit-state function ',i5,/,1x,70('-'))

      end subroutine cdirs
