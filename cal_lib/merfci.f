      subroutine merfci (p,y,ier)

c                specifications for arguments

      real*8     p,y
      integer    ier

c                specifications for local variables

      real*8     eps,g0,g1,g2,g3,h0,h1,h2,w,wi,sn,sd
      real*8     sigma,x,xinf

      data       xinf/1.7014d+38/
      data       eps/.59605d-07/
      data       g0/.1851159d-3/,g1/-.2028152d-2/
      data       g2/-.1498384d0/,g3/.1078639d-1/
      data       h0/.9952975d-1/,h1/.5211733d0/
      data       h2/-.6888301d-1/

c                first executable statement

      ier = 0

c                error check on input argument

      if(p .gt. 0.0 .and. p .lt. 2.0) go to 5
      ier = 129
      sigma = dsign(1.d0,-p)
      y = sigma * xinf
      go to 9000
    5 if(p.le.eps) go to 10
      x = 1.0 - p
      call merfi (x,y,ier)
      go to 9005
c                           p too small, compute y directly
   10 w = dsqrt(-dlog(p+(p-p*p)))
c                           w greater than 4., approx. f by a
c                              rational function in 1./w
      wi = 1./w
      sn = ((g3*wi+g2)*wi+g1)*wi
      sd = ((wi+h2)*wi+h1)*wi+h0
      y = w + w*(g0+sn/sd)
      go to 9005
 9000 continue
      call uertst(ier,'merfci')
 9005 return
      end
