      subroutine gdfijd(ddp1,ddp2,drid,bt1,bt2,ro12,ro2,pb1,pf1,sdp)

      implicit   none

      include   'prob.h'

      real    (kind=8) :: bt1,bt2,ro12,ro2,pb1,pf1
      real    (kind=8) :: ddp1(ntp),ddp2(ntp),drid(ntp),sdp(ntp)
!
      integer (kind=4) :: i
      real    (kind=8) :: a1,a2, b1,b2, bt21, pb21,pf21

!     compute bt2|1
      bt21 = (bt2-bt1*ro12)/ro2

!     compute prob(-bt2|1)
      call dnorm(-bt21,pf21)

!     compute pb21
      pb21 = 0.39894228d0*exp(-bt21*bt21*0.5d0)

!     compute d(bt2|1)/d(par)
      b1 = (bt2-ro12*bt1)*ro12/(ro2*ro2)
      a1 = -pb1*pf21
      a2 = -pf1*pb21
      do i = 1,ntp
        b2 = ddp2(i)-drid(i)*bt1-ro12*ddp1(i)+b1*drid(i)
        sdp(i) = sdp(i)-a1*ddp1(i)-a2*b2/ro2
      end do ! i

      end subroutine gdfijd
