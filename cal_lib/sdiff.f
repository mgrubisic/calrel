cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     function sdiff(x,z,tp,rt,y1,y0,tf,ids,par,bnd,sg,ex,ib,ig,
c    *                 igt,lgp,izx,ipa)
c
c   functions:
c     compute the g value in the finite difference formula in order to
c     compute curvature at design point for c. f. method.
c
c   input arguments:
c     x   : the design point in x space.
c     tp  : deterministic parameters in performance function.
c     rt  : rotation transformation matrix, y=rt * y'.
c     tf  : the transformation matrix from z to y, y=tf * z.
c     ids: distribution number in x.
c     par : distribution parameters of x.
c     bnd : lower and upper bounds of x.
c     sg  : standard deviation of x.
c     ib  : code of distribution bounds of x.
c     ds  : parameters in full distribution function.
c
c   calls: cytox, ugfun.
c
c   called by: scurvt.
c
c   last revision: aug. 25 1987, by h.-z. lin.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      real*8 function sdiff(x,z,tp,rt,y1,y0,tf,ids,par,bnd,sg,ex,ib,ig,
     *                      igt,lgp,izx,ipa)

      implicit    none

      include   'grad.h'
      include   'prob.h'

      integer (kind=4) :: ig
      real    (kind=8) :: x(nrx),z(nry)
      real    (kind=8) :: tp(*),rt(nry,nry),y1(nry),y0(nry),tf(ntf)
      integer (kind=4) :: ids(nrx),ib(nrx)
      real    (kind=8) :: par(4,nrx),bnd(2,nrx),sg(nrx),ex(nrx)
      integer (kind=4) :: igt(nig),lgp(3,nig),izx(nry),ipa(4,nrx)

      integer (kind=4) :: i,k
      save
c
c.... from rotated standard space to standard space
c
      do i=1,nry
        y0(i)=0.d0
        do k=1,nry
          y0(i)=y0(i)+rt(i,k)*y1(k)
        end do ! k
      end do !

c.... from standard space to original space
c
      call cytox(x,y0,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
c.... compute g value
      flgf=.true.
      flgr=.false.
      call ugfun(sdiff,x,tp,ig)

      end function sdiff
