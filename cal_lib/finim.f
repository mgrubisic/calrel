      subroutine finim(igfx,igf)

      implicit   none

      include   'cuts.h'
      include   'prob.h'

      integer (kind=4) :: igf
      integer (kind=4) :: igfx(ngf,3)

      integer (kind=4) :: i,ike 

      igfx(1,3)=igf
      if(imc.eq.1) then
        igfx(1,1)=igf
        igfx(1,2)=1
      else
        ike=0
        do 398 i=1,imc
          if(igfx(i,1).eq.igf) then
          ike=1
          go to 390
          endif
  398   continue
  390     if(ike.eq.1) then
          igfx(1,2)=i
          ngfc=1
          else
          imc=imc+1
          igfx(imc,1)=igf
          igfx(1,2)=imc
          endif
      endif

      end subroutine finim
