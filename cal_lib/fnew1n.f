!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!      subroutine fnew1n(x,y,z,a,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,
!                        ib,izx,sdgy,bt,gx,bbeta)
!
!   functions:
!      compute new x and y using rf method and shift of origin.
!
!   input arguments:
!      x : current x.
!      y : current y.
!      a : unit normal vector in the y space.
!      tf : the transformation matrix from z to y, i.e. y  =  tf * z.
!      ex : mean of x.
!      sg : standard deviation of x.
!      par : distribution parameters of x.
!      bnd : lower and upper bound of x.
!      igt : group types.
!      lgp : location of the first element of a group in y.
!      ids : distribution numbers of x.
!      ipa : #'s of the associated rv of distribution parameters.
!             n-th element of ipa  =  0 means parameter n is constant.
!      ib : code of distribution bounds of x.
!            =  0 , no bounds
!            =  1 , bounded from below.
!            =  2 , bounded from above.
!            =  3 , both sides bounded.
!      izx : indicators for z to x.
!      sdgy : |dg/dy|.
!      gx : g(x).
!      ig : failure mode number.
!
!   output arguments:
!      x : new x.
!      y : new y.
!      z : new z.
!      bt : new reliability index.
!
!   calls: cdot, cytox.
!
!   called by: form.
!
!   written by mario de stefano      august-october 1990
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine fnew1n(x,y,z,a,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,
     *                  ib,izx,sdgy,bt,gx,bbeta)

      implicit   none

      include   'optm.h'
      include   'prob.h'

      real    (kind=8) :: sdgy,bt,gx
      integer (kind=4) :: ib(nrx),igt(nig),lgp(3,nig),izx(nry)
      integer (kind=4) :: ids(nrx),ipa(4,nrx)
      real    (kind=8) :: x(nrx),y(nry),z(nry)
      real    (kind=8) :: a(nry),tf(*),ex(nrx),sg(nrx)
      real    (kind=8) :: par(4,nrx),bnd(2,nrx),bbeta(nry)

      integer (kind=4) :: i
      real    (kind=8) :: s,ysum
      real    (kind=8) :: t
      real    (kind=8) :: ynew,yold
      real    (kind=8) :: cdot
      save

!---  use hl-rf method to compute new y, step size  =  op1.

      s = gx/sdgy
      do i = 1,nry
        s = s+y(i)*a(i)
      end do ! i
      do i = 1,nry
        yold  =  y(i)
        ynew  =  s*a(i)
        y(i)  =  yold + (ynew - yold)*1.
      end do ! i
      ysum = 0.d0
      do i = 1,nry
        ysum = ysum+a(i)*bbeta(i)
      end do ! i
      do i = 1,nry
        y(i) = y(i)-ysum*a(i)+bbeta(i)
      end do ! i

!---  transform y to x and compute new beta.

      call cytox(x,y,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      t = cdot(y,a,1,1,nry)
      bt = sqrt(cdot(y,y,1,1,nry))
      if(t.lt.0.d0) bt = -bt

      end subroutine fnew1n
