      subroutine upper

      include   'line.h'

      character*1 sp,sc

      save

      data sp/' '/,sc/':'/

c---  check end of line and convert to upper case ---

      isp = ichar(sp)
      do i=1,llen
        if(line(i).ne.sc) go to 75
        line(i) = sp
        llen = i
        go to 85
   75   nn = ichar( line(i) )
        if(nn.gt.96) line(i)= char( nn - isp )
      end do ! i

c---  remove blanks at end of line ------------------

   85 if(line(llen).ne.sp) go to 95
      if(llen.eq.1) go to 95
      llen = llen - 1
      go to 85

c---  exit

   95 continue

      end
