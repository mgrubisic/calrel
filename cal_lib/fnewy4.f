      subroutine fnewy4(x,y,a,d,t2,t3,gam,tf,ex,sg,par,bnd,tp,b1,b2,t,
     *     igt,lgp,ids,ipa,ib,izx,ixz,sdgy,bt,iter,gx,ig,nry1,gdgx)

      implicit   none

      include   'crel.h'
      include   'flag.h'
c     include   'file.h'
      include   'grad.h'
      include   'para.h'
      include   'prob.h'
      include   'optm.h'

      real    (kind=8) :: qf4,dqf4 
      external            qf4,dqf4

      integer (kind=4) :: iter,ig,nry1
      real    (kind=8) :: sdgy,bt,gx
      real    (kind=8) :: x(nrx),y(nry)
      real    (kind=8) :: a(nry),d(nry1),t2(nry),t3(nrx),gam(nry)
      real    (kind=8) :: tf(ntf),ex(nrx),sg(nrx)
      real    (kind=8) :: par(4,nrx),bnd(2,nrx),tp(*)
      integer (kind=4) :: ids(nrx),ib(nrx),igt(nig),lgp(3,nig)
      real    (kind=8) :: gdgx(nrx)
      integer (kind=4) :: izx(nry),ixz(nrx),ipa(4,nrx)
      real    (kind=8) :: b1(nry1,nry1),b2(nry1,nry1),t(nry)

      integer (kind=4) :: i,j
      real    (kind=8) :: drl,pbp,pe,pt
      real    (kind=8) :: q,rl,s
      real    (kind=8) :: tt,theta 

      real    (kind=8) :: cdot
      save

c---  if iter  =  2, initialize b, otherwise update b.

      flgf = .true.
      flgr = .false.
      if(iter.gt.2) go to 30
      do 20 i = 1,nry
      do 10 j = 1,nry
      b1(j,i) = 0.
   10 b2(j,i) = 0.
      b1(i,i) = 1.
      b2(i,i) = 1.
   20 t(i) = y(i)
      u = 0.

c---  solve min 0.5<d>[b]{d}+<y>{d}  s.t. <dg>{d}+g = 0 for direction of search

   30 do 40 i = 1,nry
      b1(nry1,i) = -a(i)*sdgy
      b1(i,nry1) = b1(nry1,i)
      b2(nry1,i) = b1(nry1,i)
      b2(i,nry1) = b1(nry1,i)
   40 d(i) = -y(i)
      b1(nry1,nry1) = 0.
      b2(nry1,nry1) = 0.
      d(nry1) = -gx
c     call symsol(b2,d,nry1,nry1)
      call cbsolx(b2,d,nry1,nry1,nry1)
c     write(not,'(''a'',1p,7e11.4)') a,sdgy
c     write(not,'(''y'',1p,7e11.4)') y,gx
c     print *,'b1 = '
c     call mprint(b1,nry1,nry1,1)
c     write(not,'(''d'',1p,7e11.4)') d
c     if(cdot(d,d,1,1,nry).lt.tol) return
      rl = d(nry1)
      drl = dabs(rl)
      u = dmax1(drl,(u+drl)/2.)
c----------------------------------------------------------------------
c     perform line search to minimize q = 0.5*<y>{y}+u*|g|
c----------------------------------------------------------------------
      call flinsc(x,y,a,d,t2,t3,tf,ex,sg,par,bnd,tp,igt,lgp,ids,
     *           ipa,ib,izx,gx,sdgy,ig,qf4,dqf4,q)
c     write(not,'(''y'',1p,7e11.4)') y
c---  update b
c     if(ifeap.eq.0) go to 50
      if(igr.ne.0) flgr = .true.
      call cytox(x,y,t3,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call ugfun(gx,x,tp,ig)
c  50 continue
      call fdirct(x,y,tp,ex,sg,t3,gam,a,tf,par,bnd,igt,lgp,ids,
     *            ipa,izx,ixz,sdgy,gdgx,ig,0)
c     write(not,'(''a'',1p,7e11.4)') a,sdgy
      bt = dsqrt(cdot(y,y,1,1,nry))
      s = cdot(y,a,1,1,nry)
      bt = sign(bt,s)
c---  t2 = [b]{p}
      do 90 i = 1,nry
   90 t2(i) = cdot(b1(i,1),d,nry1,1,nry)*alpha
      pbp = alpha*cdot(d,t2,1,1,nry)
c     write(not,'(''bp,pbp'',6x,1p,5e11.4)') t2,pbp
c---  t3 = (y+lambda*dg)new-(y+lambda*dg)old
      do 100 i = 1,nry
      tt = t(i)
      t(i) = y(i)-rl*sdgy*a(i)
  100 t3(i) = t(i)-tt
      pt = cdot(d,t3,1,1,nry)*alpha
c     write(not,'(''y,py'',8x,1p,5e11.4)') t3,pt
      if(pt.ge.0.2d0*pbp) then
        theta = 1.0
      else
        theta = 0.8*pbp/(pbp-pt)
      endif
c---  compute eta = theta*t3+(1-theta)*bp
      do 110 i = 1,nry
  110 t3(i) = theta*t3(i)+(1.d0-theta)*t2(i)
      pe = alpha*cdot(d,t3,1,1,nry)
c     write(not,'(''eta,peta'',4x,1p,5e11.4)') t3,pe
      do 120 j = 1,nry
      do 120 i = 1,j
  120 b1(i,j) = b1(i,j)-t2(i)*t2(j)/pbp+t3(i)*t3(j)/pe
      do 130 j = 1,nry
      do 130 i = j,nry
      b1(i,j) = b1(j,i)
      b2(i,j) = b1(i,j)
  130 b2(j,i) = b1(j,i)

      end subroutine fnewy4
