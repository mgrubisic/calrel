      subroutine mdgam (x,p,prob,ier)

      implicit   none

!                specifications for arguments
      integer (kind=4) ::  i,ier
      real    (kind=8) ::  x,p,prob
!                specifications for local variables
      real    (kind=8) ::  v(6),v1(6),pnlg,cnt,ycnt,xmin,ax,big,cut
      real    (kind=8) ::  y,z,ratio,reduc
      equivalence (v(3),v1(1))

      data       xmin/-87.49823d0/

!                test  x and      p
!                first executable statement

      prob = 0.0d0
      if (x  .ge. 0.0d0) go to 5
      ier = 129
      go to 9000
    5 if (p .gt. 0.0d0) go to 10
      ier = 130
      go to 9000
   10 ier = 0
      if (x  .eq. 0.0d0) go to 9005
!                           define log-gamma and initialize
      pnlg = log_gamma(p)
      cnt = p * log(x)
      ycnt = x + pnlg
      if ((cnt-ycnt) .gt. xmin) go to 15
      ax = 0.0d0
      go to 20
   15 ax = exp(cnt-ycnt)
   20 big = 1.d35
      cut = 1.d-8
!                           choose algorithmic method
      if ((x  .le. 1.0d0) .or. (x  .lt. p )) go to 40
!                           continued fraction expansion
      y = 1.0d0 - p
      z = x  + y + 1.0d0
      cnt = 0.0d0
      v(1) = 1.0d0
      v(2) = x
      v(3) = x + 1.0d0
      v(4) = z * x
      prob = v(3)/v(4)
   25 cnt = cnt + 1.0d0
      y = y + 1.0d0
      z = z + 2.0d0
      ycnt = y * cnt
      v(5) = v1(1) * z - v(1) * ycnt
      v(6) = v1(2) * z - v(2) * ycnt
      if (v(6) .eq. 0.0d0) go to 50
      ratio = v(5)/v(6)
      reduc = dabs(prob-ratio)
      if (reduc .gt. cut) go to 30
      if (reduc .le. ratio*cut) go to 35
   30 prob = ratio
      go to 50
   35 prob = 1.0d0 - prob * ax
      go to 9005
!                           series expansion
   40 ratio =  p
      cnt = 1.0d0
      prob = 1.0d0
   45 ratio = ratio + 1.0d0
      cnt = cnt *  x/ratio
      prob = prob + cnt
      if (cnt .gt. cut) go to 45
      prob = prob * ax/p
      go to 9005
   50 do 55 i=1,4
       v(i) = v1(i)
   55 continue
      if (dabs(v(5)) .lt. big) go to 25
!                           scale terms down to prevent overflow
      do 60 i=1,4
       v(i) = v(i)/big
   60 continue
      go to 25
 9000 continue
      call uertst (ier,'mdgam ')
 9005 return

      end subroutine mdgam
