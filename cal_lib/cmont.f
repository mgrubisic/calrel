      subroutine cmont

      implicit   none

      include   'blkrel1.h'
      include   'counts.h'
      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'flcg.h'
      include   'line.h'
      include   'prob.h'
      include   'simu.h'

      integer    n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n15
      integer    n16,n17,n18,n19,n20,n21,n22,n23,n24,n25,n26
      integer    nr,nc,nd
      integer    icen,iimp,isol
      real*8     dist,rad,rdev

      save

      neval = 0
      nsm = 10000
      call freei('m',nsm,1)
      call gseed(stp)
      call freer('p',stp,1)
      igf = 0
      call freei('f',igf,1)
      if(icl.eq.1.and.igf.eq.0) igf = 1
      npr = max0(nsm/20,1)
      call freei('r',npr,1)
      cov = 0.05d0
      call freer('v',cov,1)
      rad = 0.d0
      call freer('d',rad,1)
      ist = 0
      call freei('t',ist,1)
      iimp = 0
      call freei('e',iimp,1)
      isol = 0
      call freei('l',isol,1)
      icen = 0
      call freei('n',icen,1)
      nd = 0
      call freei('g',nd,1)
c
c
c
c     ----- importance sampling -----
c
      if(iimp.ne.0) then
c
c       ---- read data for inportance sampling ------
c       rdev : standard deviation for sampling density
c              > 0.0 stddev itself (default = 1.0d0)
c              < 0.0 the point to fit the pdf in number of sigma
c
c       dist : minimum distance to distinguish 2 design points
c              in number of sigma
c              default = 1.0d0
c
        rdev = 1.0d0
        dist = 1.0d0
      endif
c
      call locate('tf  ',n1,nr,nc)
      call locate('par ',n2,nr,nc)
      call locate('bnd ',n3,nr,nc)
      call locate('tp  ',n4,nr,nc)
      call locate('ids ',n5,nr,nc)
      call locate('ib  ',n6,nr,nc)
      call locate('igt ',n7,nr,nc)
      call locate('lgp ',n8,nr,nc)
      call locate('izx ',n9,nr,nc)
      call locate('ipa ',n10,nr,nc)
      call locate('mc  ',n11,nr,nc)
      call define('x1  ',n12,nrx,1)
      call define('y1  ',n13,nry,1)
      call define('z1  ',n15,1,nry)
      call define('igx ',n16,1,ngfc)
      call locate('sg  ',n17,nr,nc)
      call locate('ex  ',n18,nr,nc)
      call locate('igfx',n19,nr,nc)

c********** added by k.f 07/2002 for importance sampling *****
c     ----- locate design point coordinate -----
      if(iimp.ne.0) then
      if(flc(3)) call cerror(5,0,0.d0,' ')
      call locate('xlin',n20,nr,nc)
      call locate('yo  ',n21,nr,nc)
      call locate('beta',n22,nr,nc)
      call lodefr('gtor',n23,nr,nc)
      call locate('ixz ',n24,nr,nc)
      call locate('alph',n25,nr,nc)
      call locate('fltf',n26,nr,nc)
      endif
c
      if(ist.eq.0) then
        write(not,1010)
      else
        write(not,1020)
      endif
      write(not,1030) npr,nsm,cov,stp
      if(icl.eq.1) write(not,2000) igf
c
      if(iimp.eq.0) then
      call nmont(ia(n1),ia(n2),ia(n3),ia(n4),ia(n5),ia(n6),ia(n7),
     *           ia(n8),ia(n9),ia(n10),ia(n11),ia(n12),ia(n13),
     *           ia(n15),ia(n16),ia(n17),ia(n18),ia(n19),rad)
      elseif(iimp.eq.1) then
c********** added by k.f 07/2002 for importance sampling *****
c     ----- importance sampling subroutine -----
      call imont(ia(n1),ia(n2),ia(n3),ia(n4),ia(n5),ia(n6),ia(n7),
     *           ia(n8),ia(n9),ia(n10),ia(n11),ia(n12),ia(n13),
     *           ia(n15),ia(n16),ia(n17),ia(n18),ia(n19),ia(n20),
     *           ia(n21),ia(n22),ia(n24),ia(n25),ia(n26),
     *           rad,rdev,dist,nd,icen)
c      subroutine imont(tf,par,bnd,tp,ids,ib,igt,lgp,izx,ipa,
c     *                 mc,x1,y1,z,igx,sg,ex,igfx,xdes,ydes,btdes,
c     *                 ixz,alph,fltf,beta,rdev,dist,ndesign,isampl)
c********** added by k.f 07/2002 for importance sampling *****
c     ----- importance sampling by orthgonal plane ( koo heongong ) -----
      elseif(iimp.eq.2) then
      call kmont(ia(n1),ia(n2),ia(n3),ia(n4),ia(n5),ia(n6),ia(n7),
     *           ia(n8),ia(n9),ia(n10),ia(n11),ia(n12),ia(n13),
     *           ia(n15),ia(n16),ia(n17),ia(n18),ia(n19),ia(n20),
     *           ia(n21),ia(n22),ia(n23),ia(n24),ia(n25),ia(n26),
     *           rdev,dist,isol,nd,icen)
c      else
c********** added by k.f 09/2002 for search based sampling (under construction)*
c      call smont(ia(n1),ia(n2),ia(n3),ia(n4),ia(n5),ia(n6),ia(n7),
c     *           ia(n8),ia(n9),ia(n10),ia(n11),ia(n12),ia(n13),
c     *           ia(n15),ia(n16),ia(n17),ia(n18),ia(n19),ia(n20),
c     *           ia(n21),neach,ncmax)
c
      endif
c     run = etime(tarray)
c     print *,'run time = ',run
      call delete('x1  ')
      call delete('y1  ')
      call delete('z1  ')
      call delete('igx ')

 1010 format(//' >>>> MONTE CARLO SIMULATION <<<<'/)
 1020 format(//' >>>> MONTE CARLO SIMULATION -- RESTART <<<<'/)
 1030 format(
     & '  Print interval ......................... npr=',i8/
     & '  Number of simulations .................. nsm=',i8/
     & '  Threshold for coef. of variation .... cov=',e11.4/
     & '  Random seed ......................... stp=',f11.0)
 2000 format(/' Limit-state function ',i5,/,1x,70('-'))
 
      end subroutine cmont
