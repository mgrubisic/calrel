!     ******************************************************************
!     subroutine fnew3n
!
!     compute new x and new y with gradient projection and
!     shift of origin
!
!     written by mario de stefano    august-october 1990
!     ******************************************************************
      subroutine fnew3n(x,y,a,d,yt,z,gam,tf,ex,sg,par,bnd,tp,igt,lgp,
     *                ids,ipa,ib,izx,ixz,sdgy,bt,iter,gx,ig,gdgx,bbeta)

      implicit   none

      include   'flag.h'
      include   'grad.h'
      include   'prob.h'
      include   'optm.h'

      integer (kind=4) :: iter,ig
      real    (kind=8) :: sdgy,bt,gx
      integer (kind=4) :: ids(nrx),ib(nrx),igt(nig),lgp(3,nig)
      integer (kind=4) :: ixz(nrx),ipa(4,nrx),izx(nry)
      real    (kind=8) :: x(nrx),y(nry)
      real    (kind=8) :: a(nry),d(nry),yt(nry),z(nry),gam(nry)
      real    (kind=8) :: tf(*),ex(nrx),sg(nrx)
      real    (kind=8) :: par(4,nrx),bnd(2,nrx),tp(*)
      real    (kind=8) :: gdgx(nrx),bbeta(nry)

      integer (kind=4) :: i,ii,iii, k
      real    (kind=8) :: alfa,bt1,gx0
      real    (kind=8) :: s0,s1,ss, ysum 
      real    (kind=8) :: ya

      real    (kind = 8) :: cdot
      save

      flgf = .true.
      flgr = .false.
      if(igr.ne.0) flgr = .true.

!---  compute new y using gradient projection method.

!----------------------------------------------------------------------
!     perform line search.
!----------------------------------------------------------------------
!---  move along gradient projection direction

      if(iter.eq.2) bt = 100.d0
      alfa = 1.d0
      ya = cdot(y,a,1,1,nry)
      do i = 1,nry
        d(i) = y(i)-ya*a(i)
      end do ! i
      ysum = 0.0d0
      do i = 1,nry
        ysum = ysum+a(i)*bbeta(i)
      end do ! i
      do i = 1,nry
        d(i) = d(i)-ysum*a(i)+bbeta(i)
      end do ! i
      iii = 0
 40   do i = 1,nry
        yt(i) = y(i)-alfa*d(i)
      end do ! i

!--- if y is not on the failure surface, do newton-type correction.

      call cytox(x,yt,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call ugfun(gx0,x,tp,ig)
      call fdirct(x,yt,tp,ex,sg,z,gam,a,tf,par,bnd,igt,lgp,
     &                   ids,ipa,izx,ixz,sdgy,gdgx,ig,0)
      if(abs(gx0).le.op2) go to 100
      ii = 1
      s0 = 0.0d0
 60   s1 = gx0/sdgy+s0
      if(abs(s1).gt.op3) then
         k = int(s1/op3) ! WIP: truncate not round with nint
         s1 = s1-op3*k
      end if
      ss = s1-s0
      do i = 1,nry
        yt(i) = yt(i)+ss*a(i)
      end do ! i
      call cytox(x,yt,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call ugfun(gx,x,tp,ig)
      call fdirct(x,yt,tp,ex,sg,z,gam,a,tf,par,bnd,igt,lgp,ids,
     &            ipa,izx,ixz,sdgy,gdgx,ig,0)
      if(abs(gx).le.op2) go to 100
      s0 = s1
      gx0 = gx
      ii = ii+1
      if(ii.le.ni2) go to 60

!---  check if the new beta is less than the old one.
!---  if new beta does not decrease, repeat line search.

 100  alfa = alfa*op1
      bt1 = dsqrt(cdot(yt,yt,1,1,nry))
      iii = iii+1
      if(dabs(bt1).gt.dabs(bt).and.iii.lt.ni2) go to 40
      do i = 1,nry
        y(i) = yt(i)
      end do ! i

!---  update beta.

      ss = cdot(y,a,1,1,nry)
      bt = bt1
      if(ss.lt.0.d0) bt = -bt

      end subroutine fnew3n
