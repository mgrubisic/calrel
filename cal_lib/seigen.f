ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine seigen(a,n,jj)
c
c   functions:
c     use jacobian method to find principal curvatures for the c. f. method.
c
c   input arguments:
c     a   : curvature matrix at the design point in the y space.
c     n   : =nrx-1
c     jj  : number of columns which sum of abs(entries)=0.
c
c   output argument:
c     a   : diagonal curvature matrix which diagonal entries are princial
c	    curvatures.
c
c   called by: socf.
c
c   last revision: oct. 23, 1986 by h.-z. lin.
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine seigen(a,n,jj)

      implicit   none

      integer (kind=4) :: n, jj
      real    (kind=8) :: a(n,n)

      integer (kind=4) :: i,j,k, kk
      real    (kind=8) :: x, x1,x2, xx1
      real    (kind=8) :: aa, alfa, beta

      do 15 kk=1,100
      aa=0.d0
      do 55 i=1,n-jj
      do 55 j=1,n-jj
      if(i.ne.j) then
      aa=aa+a(i,j)*a(i,j)
      endif
  55  continue
      if(abs(aa).le.10.e-20) go to 105
      do 10 i=1,n-1-jj
      do 10 j=i+1,n-jj
      if(abs(a(i,j)).le.10.e-12) go to 10
      x1=-(a(i,i)-a(j,j))/(2.d0*a(i,j))
      x2=x1*x1+1.d0
      if(x1.ge.0.d0) then
      x=x1+dsqrt(x2)
      else
      x=x1-dsqrt(x2)
      endif
      xx1=x*x+1.d0
      x2=dsqrt(xx1)
      alfa=x/x2
      beta=1.d0/x2
      do 20 k=1,n
      if(i.ne.k.and.j.ne.k) then
      x1=a(i,k)
      x2=a(j,k)
      a(i,k)=alfa*x1-beta*x2
      a(j,k)=alfa*x2+beta*x1
      a(k,i)=a(i,k)
      a(k,j)=a(j,k)
      endif
  20  continue
      x1=a(i,i)
      x2=a(j,j)
      a(i,i)=alfa**2*x1+beta**2*x2-2.d0*alfa*beta*a(i,j)
      a(j,j)=alfa**2*x2+beta**2*x1+2.d0*alfa*beta*a(i,j)
      a(i,j)=0.d0
      a(j,i)=0.d0
  10  continue
  15  continue
      if(kk.eq.100) then
      print 123
  123 format(' iteration terminated without convergence in the eigenvalu
     *e problem after 100 cyclic jacobians')
      endif
  105 continue

      end subroutine seigen
