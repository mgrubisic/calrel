      subroutine gdfijp(dbp1,dbp2,drip,bt1,bt2,ro12,
     *                  ro2,pb1,pf1,stp)

      implicit   none

      include   'prob.h'

      real    (kind=8) :: bt1,bt2, ro12,ro2, pb1,pf1 
      real    (kind=8) :: dbp1(4,nrx),dbp2(4,nrx),drip(4,nrx),stp(4,nrx)

      integer (kind=4) :: i,j
      real    (kind=8) :: a1,a2,b1,b2, bt21, pb21,pf21

!     compute bt2|1
      bt21 = (bt2-bt1*ro12)/ro2

!     compute prob(-bt2|1)
      call dnorm(-bt21,pf21)

!     compute pb21
      pb21 = 0.39894228d0*exp(-bt21*bt21*0.5d0)

!     compute d(bt2|1)/d(par)
      b1 = (bt2-ro12*bt1)*ro12/(ro2*ro2)
      a1 = -pb1*pf21
      a2 = -pf1*pb21
      do i = 1,nrx
        do j = 1,4
          b2 = dbp2(j,i)-drip(j,i)*bt1-ro12*dbp1(j,i)+b1*drip(j,i)
          stp(j,i) = stp(j,i)-a1*dbp1(j,i)-a2*b2/ro2
        end do ! j
      end do ! i

      end subroutine gdfijp
