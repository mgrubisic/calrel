cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine cdyx(x,y,ex,sg,par,bnd,dyx,ids,ipa,izx,ixz,nges,nge,j0,
c                     igt,ngec)
c
c   functions:
c      compute dy/dx of types 3 and 4 groups by central difference.
c
c   input arguments:
c      x : basic random variables in the original space.
c      ex : mean values of x.
c      sg : standard deviations of x.
c      ids : distribution numbers of x.
c      ipa : #'s of the associated rv of distribution parameters.
c             n-th element of ipa  =  0 means parameter n is constant.
c      izx : indicators for z to x.
c      nge : number of z's in the group.
c      igt : group type.
c
c   output arguments:
c      dyx : dy/dx.
c
c   calls: cdxz, cdzpar, cdyxu.
c
c   called by: fdirct.
c
c   last revision: june 9 1989 by h.-z. lin.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cdyx(x,y,ex,sg,par,bnd,dyx,ids,ipa,izx,ixz,nges,
     *          nge,j0,igt,ngec)

      implicit   none

      include   'prob.h'

      integer    nges,nge, j0,igt,ngec
      integer    ids(nrx),ipa(4,nrx),izx(nge),ixz(nrx)
      real*8     x(nrx),y(nge),ex(nrx),sg(nrx),par(4,nrx),dyx(nge,nge)
      real*8     s(6),bnd(2,nrx)

      integer    i,j,k,l, ii
      integer    n0,ns,ns1,nj1
      real*8     dxy

      save

      n0 = nge-nges
      if(igt.eq.4) then
        ns = 1
        ns1 = ngec
        nj1 = n0
      else
        ns = nges+1
        ns1 = nge
        nj1 = nge
      endif

c---- compute dy/dx.

      do 40 i = ns,ns1
        do j = 1,nj1
          dyx(i,j) = 0.
        end do ! j
        k = izx(i)
        if(igt.eq.4.and.i.gt.n0) go to 667
        if(ids(k).ge.50) go to 667
        call cdxz(x,dxy,par,ids,izx(i),1)
        dyx(i,i) = 1.d0/dxy
 667    if(ids(k).le.50) then
          if(ipa(1,k).eq.0.and.ipa(2,k).eq.0.and.ipa(3,k).eq.0.and.
     *       ipa(4,k).eq.0) go to 40
          call cdzpar(x,y(i),ex(k),sg(k),par(1,k),ids(k),s,k)
          do l = 1,2
            ii = ixz(iabs(ipa(l,k)))-j0+1
            if(igt.eq.4.and.ii.gt.n0) go to 20
            if(ipa(l,k).gt.0) then
              if(ixz(ipa(l,k)).eq.0) go to 20
              dyx(i,ii) = s(l)
            elseif(ipa(l,k).lt.0) then
              if(ixz(-ipa(l,k)).eq.0) go to 20
              dyx(i,ii) = s(l+2)
            endif
   20       continue
          end do ! l
          if(ids(k).ne.7) go to 40
          do l = 3,4
            ii = ixz(ipa(l,k))-j0+1
            if(igt.eq.4.and.ii.gt.n0) go to 30
   30       if(ipa(l,k).ne.0) dyx(i,ii) = s(l+2)
          end do ! l
        else
          call cdyxu(x,ex,sg,par,bnd,dyx,ids,ipa,izx,nge,i,igt,n0)
        endif
   40 continue

      end subroutine cdyx
