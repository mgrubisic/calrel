cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine dirs0(tf,par,bnd,tp,ids,ib,igt,lgp,izx,ipa,mc,x1,y1,
c    *             yy,z1,sg,ex,rr,y2,id,rot,gv,ir,beta)
c
c     functions:
c       compute the failure probability by directional simulation method
c       and using true failure surface.
c
c     inpute arguments:
c       tf   : transformation matrix from z to y, y=tf * z.
c       bnd  : lower and upper bounds of x.
c       par  : distribution parameter of x.
c       tp   : deterministic parameters.
c       ids : distribution number of x.
c       ib   : codes of distribution bounds of x.
c       mc   : aray of minimun cut sets.
c       ist  : index for the option of restart of simulation.
c       nsm  : number of simulations.
c       cov  : criteria of coefficient of variation.
c       ncs  : number of minimum cut sets.
c       ntl  : total number of elements in the minimum cut sets.
c       igt  : group type
c       lgp  : location of the first element of a group in y.
c       izx  : a vector associated with z indicating the no. of the
c              corresponding x.
c       ipa  : #'s of the associated rv of distribution parameters.
c              n-th element = 0 means parameter n is constant.
c
c    working aray:
c      x,y,z,ex,sg
c
c    calls: mdris, cytox, dsolv0, dscum0.
c
c    called by : cdir0.
c
c    last version: dec. 24, 1989 by h.-z. lin.
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine dirs0(tf,par,bnd,tp,ids,ib,igt,lgp,izx,ipa,mc,x1,y1,
     *          z1,sg,ex,rr,y2,id,rot,gv,ir,beta,alph,fltf,gtor,igfx)

      implicit   none

      include   'bond.h'
      include   'counts.h'
      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'prob.h'
      include   'simu.h'

      logical fltf(ngf)
      real    (kind=8) :: random

      real    (kind=8) :: tf(ntf),x1(nrx),y1(nry),z1(nry),bnd(2,nrx)
      real    (kind=8) :: par(4,nrx),tp(*),rr(3,ngfc)
      integer (kind=4) :: ids(nrx),ib(nrx),mc(ntl,2)
      real    (kind=8) :: sg(nrx),ex(nrx),y2(nry)
      integer (kind=4) :: ipa(4,nrx),izx(nry),lgp(3,nig),igt(nig)
      real    (kind=8) :: rot(ngfc+2),gv(ngf),beta(ngf)
      integer (kind=4) :: id(ngfc+2),ir(ngfc),igfx(ngf,3)
      real    (kind=8) :: alph(nry,ngf),gtor(ngf)

      integer (kind=4) :: i,i1,i2,i4, ic,ics
      integer (kind=4) :: iw, ii,ij, imm,iopt, id0,idg1, ier,iexact
      integer (kind=4) :: j,j4
      integer (kind=4) :: kj,kpl
      integer (kind=4) :: m,m1,mm, mcend,mcst
      integer (kind=4) :: n,n1,neval0,nexact
      real    (kind=8) :: axp, btg
      real    (kind=8) :: cs,cum,cvar
      real    (kind=8) :: dev
      real    (kind=8) :: exact
      real    (kind=8) :: p2, pf,pff 
      real    (kind=8) :: r,r1,rn,rnm, root
      real    (kind=8) :: sp,sy 
      real    (kind=8) :: xp,xp1 

      save

c     ----- set x as their mean  in case they are deterministic -----

      neval = 0
      do iw = 1,nrx
        x1(iw) = ex(iw)
      end do ! iw
c
      rn    = real(nry)
      mcst  = 1
      mcend = ntl
      if(icl.eq.2) then
        if(mcend.ne.2*ngfc) mcend = 2*ngfc
      endif
      if(icl.eq.1) then
        call finim(igfx,igf)
        mcst    = 1
        mcend   = 2
        mc(1,1) = igf
        mc(1,2) = 1
        mc(2,1) = 0
        mc(2,2) = 0
      endif
c
      if(ist.ne.0) then
        read(ns0,rec=4,err=10) cum,xp1,root,id0,i1
        id(1) = id0
        n1    = i1
        go to 30
   10   call cerror(41,0,0.,' ')
      endif
c
      kpl = 0
      do i = 1,ngfc
        i2 = igfx(i,2)
        i4 = igfx(i,3)
        if(.not.fltf(i2)) then
          root     = 8.0d0
          kpl      = 1
          gtor(i2) = 0.0001d0
        endif
      end do ! i
c
      if(kpl.eq.0) call droot(beta,alph,mc,y1,z1,ir,root,gv,igfx,
     *                1,mcst,mcend)
c
      i1  = 0
      cum = 0.d0
      xp1 = 0.d0
      n1  = 0
c
c.... check the origin is in the failure domain or not.
      do j = 1,nry
        y1(j) = 0.d0
      end do ! j
      call cytox(x1,y1,z1,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      do j = 1,ngfc
        j4 = igfx(j,3)
        call ugfun(gv(j),x1,tp,j4)
        if(gv(j).lt.0) then
          ir(j) = 0
        else
          ir(j) = 1
        endif
      end do ! j
      imm  = 1
      idg1 = 1
      do i = mcst,mcend
        if(mc(i,1).ne.0) then
          ic = mc(i,2)
          if(mc(i,1).lt.0) then
            imm = imm*ir(ic)
          else
            imm = imm*(1-ir(ic))
          endif
        else
          ics  = 1-imm
          idg1 = idg1*ics
          imm  = 1
        endif
      end do ! i
      idg1 = 1-idg1
      if(idg1.eq.1) then
        id(1) = 1
      else
        id(1) = 0
      endif
c
   30 n  = npr
      n1 = n+n1
      i1 = i1+1
      xp = 0.d0
      write(not,2010)
c
c.... perform monte carlo simulation
c------- added by k-fujim -----
      exact  = 0.0d0
      iexact = 0
      nexact = 0
c------- added by k-fujim -----
      iopt = 1
      do j=i1,nsm
        axp = 0.d0
        sy  = 0.d0
        do m = 1,nry
          call gguw(stp,1,iopt,random)
          iopt = 0
          r    = random
          call dnormi(y1(m),r,ier)
        end do ! m
        do m = 1,nry
          sy = sy+y1(m)*y1(m)
        end do ! m
        sy = sqrt(sy)
        do m = 1,nry
          y1(m) = y1(m)/sy
        end do ! m

c.....  use performance function to determine r

        do kj = 1,2
          if(kj.eq.2) then
            do m = 1,nry
              y1(m) = -y1(m)
            end do ! m
          endif
          call dsolv0(tp,y1,x1,z1,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,
     *                izx,rr,y2,gv,ir,root,gtor,igfx)
c.....    count total number of roots.
          m1     = 1
          rot(1) = 0.d0
          do i = 1,ngfc
            if(ir(i).ne.0) then
              do ij = 1,ir(i)
                m1      = m1+1
                rot(m1) = rr(ij,i)
              end do ! ij
            endif
          end do ! i
c.....    arrange m1 roots in orderings.
          if(m1.le.1) then
            if(id(1).eq.1) axp=axp+1.d0
            go to 190
          endif
          mm = m1-1
 100      do ii = 1,mm
            if(abs(rot(ii)).gt.abs(rot(ii+1))) then
              r1        = rot(ii)
              rot(ii)   = rot(ii+1)
              rot(ii+1) = r1
            endif
          end do ! ii
          if(mm.gt.1) then
            mm = mm-1
            go to 100
          endif
c.....    determine the ranges of the roots in the failure sets.
          if(icl.le.2) then
            if(id(1).eq.0) then
              cs = rot(2)*rot(2)
              call dchis(cs,rn,p2,ier)
              axp = axp+1.d0-p2
            else
              cs = rot(m1)*rot(m1)
              call dchis(cs,rn,p2,ier)
              axp = axp+p2
            endif
            go to 190
          endif
c       ----- general system -----
          rot(m1+1) = root
          call dscum0(y1,y2,x1,z1,tf,tp,ex,sg,par,bnd,igt,lgp,ids,
     *             ipa,ib,izx,rot,id,m1,mc,xp,ir,igfx,mcst,mcend)
          axp = axp+xp
 190      continue
        end do ! kj
        axp = axp*0.5d0
        cum = cum+axp
        xp1 = xp1+axp*axp
c------ added by k-fujim -----
        if(iexact.ne.0) then
          nexact = nexact+1
          exact  = exact+cum/real(j)
        endif
c------ added by k-fujim -----
c
        if(j.ge.n1) then
          rnm = real(j)
          pf  = cum/rnm
c-----------old --------------------
c         sp=cum-2.d0*pf*cum+rnm*pf*pf
c          sp=xp1-2.d0*pf*cum+rnm*pf*pf
c-----------old --------------------
          sp = xp1-cum*cum/rnm
          sp = abs(sp/(rnm*(rnm-1)))
c-----------new --------------------
          rnm = rnm*(rnm-1.d0)
          pff = 1.d0-pf
          if(sp.gt.0.d0) then
c -----------new ----------------------
            dev = sqrt(sp)
            if(iexact.ne.0) goto 60
            cvar = dev/pf
c -----------old ----------------------
c            dev  = sqrt(sp/rnm)
c            cvar = dev/pf
c -----------old ----------------------
            call dnormi(btg,pff,ier)
            write(not,2020) j,pf,btg,cvar
            if(cvar.le.cov) then
              iexact = 1
              neval0 = neval
              n1     = n1+npr
              goto 50
            endif
          else
            write(not,2030) j
          endif
          n1 = n+n1
        endif
   50   continue
      end do ! j
 60   id0 = id(1)

      write(ns0,rec=4) cum,xp1,root,id0,min(j,nsm)

c---  output formats

 2010 format('     trials',8x,'pf-mean',5x,'betag-mean',4x,
     *       'coef of var')

 2020 format(i11,1p,e15.5,e15.5,e15.5)

 2030 format(i12,5x,'  -----',9x,'  ------',9x,'   ------')

      end subroutine dirs0
