cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine chgdim(name,nr,nc)
c
c   functions:
c      update the dimension of an array.
c
c   input arguments:
c      name : name of the array.
c      nr : number of rows.
c      nc : number of columns.
c
c   calls: icon, cstop, ifind.
c
c   called by: cinpu, input.
c
c   last revision: july 29 1987 by p.-l. liu.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine chgdim(name,nr,nc)

      implicit   none

      include   'blkrel1.h'
      include   'dbsy.h'

      character  name(4)*1
      integer    nr,nc, iname(4)

      integer    i,j,k, ii, na
      integer    nds,next0, nsize,nsize0

      integer    ifind

      save

c-----modify the dimension of an array---------------

      call icon(name,iname)
      i = ifind(iname,0)
      if(i.eq.0) go to 900
      np = ia(i+6)
      nsize = 2*( 1 + (nr*nc*ip(np) -1)/(ip(1)*2) )
      nsize0 = ia(i+8)
      ia(i+8) = nsize
      ia(i+4) = nr
      ia(i+5) = nc
      nds = nsize0 - nsize
      next0 = next
      next = next - nds
      na = ia(i+7)
      if (nsize.eq.nsize0) return

c-----shift storage ---------------------------------

      if(nsize.lt.nsize0) then
        ii = na + nsize0
        do j = na+nsize, next-1
          ia(j) = ia(ii)
          ii = ii + 1
        end do ! j
      else
        ii = next0 - 1
        do j = next-1, na+nsize, -1
          ia(j) = ia(ii)
          ii = ii - 1
        end do ! j
      endif

c-----update directory -----------------------------

      na = i - idir
      if (na.eq.0) go to 900
      na = na/10
      do 860 k=1,na
      i=i-10
      if(ia(i+7).le.0) go to 860
  860 if(ia(i+9).eq.0) ia(i+7) = ia(i+7) - nds

c----------------------------------------------------

  900 return

      end subroutine chgdim
