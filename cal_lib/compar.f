cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c      logical function compar(a,b)
c
c   functions:
c      check if the first 4 characters of a and b are identical.
c
c   input arguments:
c      a,b : two character strings.
c
c   output arguments:
c      compar : result of the comparision.
c               = .true., a and b are identical.
c               = .false., a and b are different.
c
c   calls: ichar.
c
c   called by: cexect, input.
c
c   last revision: july 31 1987 by p.-l. liu.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      logical function compar(a,b)

      implicit     none

      character        :: a(4)*1,b(4)*1
      integer (kind=4) :: i, ia,ib, inc

      data inc/32/

c     logical function to determine a match between alphanumeric data

      compar = .false.

c     compare for a match

      do i = 1,4
         ia = ichar(a(i))
         ib = ichar(b(i))
c        test all permutations of characters for a match
         if(ia.ne.ib .and. ia+inc.ne.ib .and. ia.ne.ib+inc ) return
      end do ! i
      compar = .true.

      end function compar
