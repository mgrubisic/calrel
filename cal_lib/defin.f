cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine defin(name,na,nr,nc)

c   functions:
c      define and reserve storage for array.

c   input arguments:
c      name : name of the array.
c      nr : number of rows.
c      nc : number of columns.

c   output arguments:
c      na : initial location of array.

c   calls: icon, cstop.

c   called by: define, defini.

c   last revision: by wilson

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine defin(name,na,nr,nc)

      implicit   none

      include   'blkrel1.h'
      include   'dbsy.h'
      include   'file.h'

      character (len=1) :: name(4)
      integer (kind=4)  :: na,nr,nc, nsize
      integer (kind=4)  :: i
      real    (kind=8)  :: r

      save

!         Bytes:  int real char
      data    ip /  4,   8,   1 /

c----------------------------------------------------
c where name = name of array - 4 characters maximum
c       na = location of array if in blank common
c       nr = number of rows
c       nc = number of columns
c       mtot = end of directory
c       numa = number of arrays in data base
c       next = next available storage location
c       idir = start of directory in blank common
c       ip   = number of characters contained in data type
c       lenr = number of characters in physical record
c       np = type of data
c          = 1 integer data
c          = 2 real data
c          = 3 character data
c------directory definition for core or sequential files
c      idir(1,n) = name of array  - iname (4 char.)
c      idir(5,n) = number of rows    - nr
c      idir(6,n) = number of columns - nc
c      idir(7,n) = type of data      - np
c      idir(8,n) = incore address    - na
c      idir(9,n) = size of array
c      idir(10,n) = 0 if in core storage
c----------------------------------------------------
c---- initialization of data base for first array --

      if (numa.le.0) then
        idir = mtot
        next = 1
      endif

c---- set storage for next array -------------------

      nsize = 2*( 1 + (nr*nc*ip(np) -1)/(ip(1)*2) )
      na = next
      next = next + nsize

c-----set up new directory --------------------------

      numa = numa + 1
      idir = idir - 10
      i = idir

c-----check storage limits --------------------------

      if(i.ge.next) then
        call icon(name,ia(i))
        ia(i+4) = nr
        ia(i+5) = nc
        ia(i+6) = np
        ia(i+7) = na
        ia(i+8) = nsize
        ia(i+9) = 0
c-----Error in storage
      else
        i = next - i + mtot - 1
        call cerror(1,i,r,name)
        call cstop
      endif

c----------------------------------------------------
      end subroutine defin
