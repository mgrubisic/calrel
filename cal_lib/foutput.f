      subroutine foutput(ixz,xn,x,y,a,gam,det,eta,ids,iter,bt,npr,
     *                   nck,gx,iout,pf)

      implicit   none

      include   'file.h'
      include   'flag.h'
      include   'prob.h'

      integer (kind=4) :: iter,npr,nck,iout
      real    (kind=8) :: bt,gx,pf
      character (len=4) :: xn(nrx)

      real    (kind=8) :: x(nrx),y(nry)
      real    (kind=8) :: a(nry),gam(nry),det(nry),eta(nrx)
      integer (kind=4) :: ids(nrx),ixz(nrx)

      integer (kind=4) :: i,j

      save

      if(iout.eq.1) then
        if(npr.le.0) return
        if(mod(iter-1,npr).ne.0) return
        write(not,2001)
        write(not,2002) iter,bt,gx
        do i=1,nrx
          j=ixz(i)
          if(j.eq.0) then
            write(not,2003) xn(i),x(i)
          else
            write(not,2003) xn(i),x(i),y(j),a(j)
          endif
        end do ! i
        return
      elseif(iout.eq.2) then
        if(nck.eq.1) then
          call dnorm(-bt,pf)
          write(not,2004)
          if(bt.le.-1.5) then
            write(not,2050) iter,gx,bt,1.d0-pf
          else
            write(not,2005) iter,gx,bt,pf
          endif
!         print *,'bt=',bt,'pf=',pf
          write(not,2006)
          do i=1,nrx
            j=ixz(i)
            if(j.eq.0) then
              write(not,2007) xn(i),x(i)
            else
              if(ids(i).le.50 .and. ids(i).ne.7) then
                write(not,2003) xn(i),x(i),y(j),a(j),gam(j),
     &                          det(j),eta(j)
              else
                write(not,2003) xn(i),x(i),y(j),a(j),gam(j)
              end if
            endif
          end do ! i
          write(not,2004)
        else
          write(not,2001)
          write(not,2002) iter,bt,gx
          do i = 1,nrx
            j = ixz(i)
            write(not,2003) xn(i),x(i),y(j),a(j)
          end do ! i
          write(not,2001)
        end if
      endif

!---  output formats

 2001 format(1x,47('-'))

 2002 format(' Iteration number .............iter=',i11/
     &       ' Reliability index ............beta=',f11.4/
     &       ' Value of limit-state function.g(x)=',1p,1e11.3/
     &       ' var       linearization point       unit normal'/
     &       '             x             u            alpha')

 2003 format(1x,a4,1p,1e13.3,1p,1e14.3,0p,4f11.4)

 2004 format(1x,77('-'))
 2005 format(' Iteration number ..............iter=',i10/
     &       ' Value of limit-state function..g(x)=',1pe14.4/
     &       ' Reliability index .............beta=',0pf14.6/
     &       ' Probability ....................pf1=',1pe14.7)
 2050 format(' Iteration number ..............iter=',i10/
     &       ' Value of limit-state function..g(x)=',1pe14.4/
     &       ' Reliability index .............beta=',0pf14.6/
     &       ' Probability ................1 - pf1=',1pe14.7)
 2006 format(' var          design point                    ',
     &       ' sensitivity vectors'/'            x*            u*',
     &       '          alpha      gamma      delta        eta')
 2007 format(1x,a,3x,1pe10.3,28x,'    0.    ')

      end subroutine foutput
