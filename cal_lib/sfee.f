!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!     subroutine sfee(b,u,bt,c,n,km)
!
!     c: main curvatures.

      real*8 function sfee(b,u,bt,c,n,km)

      implicit    none

      integer (kind=4) :: n, km
      real    (kind=8) :: b, u, bt
      real    (kind=8) :: c(n)
      complex (kind=8) :: z,zz,z4

      integer (kind=4) :: i,j
      real    (kind=8) :: pi
      real    (kind=8) :: r
      real    (kind=8) :: x1,x2, xz
      real    (kind=8) :: cu, ubt

      pi = acos(-1.0d0)
      xz = 0.d0
      do 400 i = 1,km
      r = real(i)
      z = cmplx(u,b*r)
      zz = bt+z/2.d0
      zz = zz*z
!     z4 = cexp(zz)
      z4 =  exp(zz)
!     print *,'z',z
      z4 = z4/z
      z = 2.d0*z
      do 420 j = 1,n
      zz = 1.d0-z*c(j)
!     print *,'zz = ',zz
!     z4 = z4/csqrt(zz)
      z4 = z4/sqrt(zz)
  420 continue
      xz = xz+real(z4)
  400 continue
      ubt = u/2.d0+bt
      ubt = ubt*u
!     print *,'u = ',u
      ubt = (exp(ubt))/u
      x1 = 2.d0*u
      do 430 j = 1,n
      cu = 1.d0-c(j)*x1
!     print *,'cu = ',cu
      ubt = ubt/sqrt(cu)
 430  continue
      ubt = ubt/2.d0
      r = xz+ubt
      r = r*b/pi
!     sfee = -r
!     print *,'b = ',b
      x2 = 2.0d0*pi*u/b
      x2 = 1.0d0-exp(x2)
!     print *,'x2 = ',x2
      x2 = 1.d0-1.d0/x2
      sfee = x2-r

      end function sfee
