c---  locate the initial and end positions of an input item.

      subroutine isep(a,is,ie,sep)

      implicit   none

      character (len=1) :: a(80),sep
      logical           :: fl
      integer (kind=4)  :: is,ie

      integer (kind=4)  :: i

      save

      fl=.false.
      if(sep.eq.',') fl=.true.
      do 10 i=is,80
      if(a(i).ne.' '.and.a(i).ne.'	') then
        if(a(i).ne.',') then
          go to 20
        else
          if(fl) then
            is=i
            sep=','
            ie=0
            return
          endif
          fl=.true.
        endif
      endif
   10 continue
      i=81
   20 is=i
      do 30 ie=is,80
      if(a(ie).eq.' '.or.a(ie).eq.'	'.or.a(ie).eq.',') go to 40
   30 continue
      ie=81
   40 ie=ie-1
      sep=' '

      end subroutine isep
