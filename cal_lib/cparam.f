!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!      subroutine cparam(ex,sg,par,ids)
!
!   functions:
!      compute distribution parameters from mean and std. if ids > 0
!      or compute mean and std. from distribution parameters if ids < 0
!
!   input arguments:
!      ex : mean vector of x (if ids > 0).
!      sg : standard deviations of x (if ids > 0).
!      par : distribution parameters of x (if ids < 0).
!      ids : distribution numbers of x.
!
!   output arguments:
!      ex : mean vector of x (if ids < 0).
!      sg : standard deviations of x (if ids < 0).
!      par : distribution parameters of x (if ids > 0).
!
!   called by: input.
!
!   last revision: may 18, 1989 by hong-zong lin.
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cparam(ex,sg,par,ids)

      implicit   none

      integer (kind=4) :: ids
      real    (kind=8) :: ex, sg
      real    (kind=8) :: par(4)

      real    (kind=8) :: ba, c,cov, d,dd, s
      real    (kind=8) :: g1,g2, gm1,gm2
      real    (kind=8) :: qr
      real    (kind=8) :: vy
      real    (kind=8) :: x1,x2, xk, y2

      if(ids.eq.0) return
      if(ids.lt.0) go to 1000
!
!---  if ids > 0, calculate par(1) and par(2) in terms of ex and sg.
!
      if(ids.gt.50) return
      go to (10,20,30,40,50,60,70,500,500,500,110,120,130,140),ids
   10 par(1) = ex
      par(2) = sg
      return
   20 d = sg/ex
      d = 1.0d0+d*d
      par(1) = log(ex/sqrt(d))
      par(2) = sqrt(log(d))
      return
   30 d = ex/sg
      par(1) = d/sg
      par(2) = d*d
      return
   40 par(1) = 1./sg
      par(2) = ex-sg
      return
   50 par(1) = 1.526399745d0*sg
      par(2) = ex-1.913058383d0*sg
      return
   60 d = 1.7320500807*sg
      par(1) = ex-d
      par(2) = ex+d
      return
   70 ba = par(4)-par(3)
      c = (ex-par(3))/ba
      d = sg/ba
      dd = d*d
      s = c-c*c-dd
      par(1) = c*s/dd
      par(2) = (1.-c)*s/dd
      return
  110 par(2) = 1.28254983/sg
      par(1) = ex-0.5772/par(2)
      return
  120 par(2) = 1.28254983/sg
      par(1) = ex+0.5772/par(2)
      return
  130 cov = sg/ex
      xk = 1.5
  131 xk = xk+1.0
      x1 = 1.-1./xk
      x2 = 1.-2./xk
      gm1 = gamma(x1)
      gm2 = gamma(x2)
      vy = sqrt(gm2/gm1/gm1-1.0)
      if(cov-vy) 131,134,132
  132 xk = xk-0.1
      x1 = 1.-1./xk
      x2 = 1.-2./xk
      gm1 = gamma(x1)
      gm2 = gamma(x2)
      vy = sqrt(gm2/gm1/gm1-1.0)
      if(cov-vy) 133,134,132
  133 xk = xk+0.01
      x1 = 1.-1./xk
      x2 = 1.-2./xk
      gm1 = gamma(x1)
      gm2 = gamma(x2)
      vy = sqrt(gm2/gm1/gm1-1.0)
      if(cov-vy) 133,134,134
  134 par(1) = ex/gm1
      par(2) = xk
      return
  140 cov = sg/ex
      xk = 0.0
  141 xk = xk+1.
      x1 = 1.+1./xk
      x2 = 1.+2./xk
      gm1 = gamma(x1)
      gm2 = gamma(x2)
      vy = sqrt(gm2/gm1/gm1-1.)
      if(cov-vy) 141,144,142
  142 xk = xk-0.1
      x1 = 1.+1./xk
      x2 = 1.+2./xk
      gm1 = gamma(x1)
      gm2 = gamma(x2)
      vy = sqrt(gm2/gm1/gm1-1.)
      if(cov-vy) 143,144,142
  143 xk = xk+0.01
      x1 = 1.+1./xk
      x2 = 1.+2./xk
      gm1 = gamma(x1)
      gm2 = gamma(x2)
      vy = sqrt(gm2/gm1/gm1-1.)
      if(cov-vy) 143,144,144
  144 par(1) = ex/gm1
      par(2) = xk
  500 return
!
!---  if ids < 0, calculate ex and sg in terms of par(1) and par(2).
!
!1000 par(1) = ex
!     par(2) = sg
 1000 continue
      if(-ids.gt.50) return
      go to (1010,1020,1030,1040,1050,1060,1070,1500,1500,1500,1110,
     *       1120,1130,1140),-ids
 1010 ex = par(1)
      sg = par(2)
      return
 1020 y2 = par(2)*par(2)
      ex = exp(par(1)+y2/2.)
      sg = ex*sqrt(exp(y2)-1.)
      return
 1030 ex = par(2)/par(1)
      sg = sqrt(par(2))/par(1)
      return
 1040 sg = 1./par(1)
      ex = sg+par(2)
      return
 1050 ex = 1.253314137d0*par(1)+par(2)
      sg = 0.655136377d0*par(1)
      return
 1060 ex = (par(1)+par(2))/2.
      sg = (par(2)-par(1))/3.464101614d0
      return
 1070 qr = par(1)+par(2)
      ba = par(4)-par(3)
      ex = par(3)+par(1)*ba/qr
      sg = ba/qr*sqrt(par(1)*par(2)/(qr+1.))
      return
 1110 ex = par(1)+0.5772d0/par(2)
      sg = 1.28254983/par(2)
      return
 1120 ex = par(1)-0.5772d0/par(2)
      sg = 1.28254983/par(2)
      return
 1130 g1 = gamma(1.-1./par(2))
      g2 = gamma(1.-2./par(2))
      ex = par(1)*g1
      sg = par(1)*sqrt(g2-g1*g1)
      return
 1140 g1 = gamma(1.+1./par(2))
      g2 = gamma(1.+2./par(2))
      ex = par(1)*g1
      sg = par(1)*sqrt(g2-g1*g1)
 1500 return

      end subroutine cparam
