cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine sptves(n,c,bt,pb)
c
c   functions:
c     use Tvedt's exact integral formula to calculate failure probability.
c     this program is used in the case of semi-principal curvatures.
c
c   input arguments:
c     n   : nrx-1
c     c   : principal curvatures.
c     bt  : reliability index.
c
c   output argument:
c     pb  : failure probability.
c
c   calls: snewg,snewg0,snewg1
c
c   called by: sopf.
c
c   last revision: aug. 13, 1990, by h.-z. lin.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine sptves(n,c,bt,pb)

      implicit   none

      integer (kind=4) :: n
      real    (kind=8) :: bt,pb
      real    (kind=8) :: c(2,n)

      complex (kind=8) :: zz,zz0

      integer (kind=4) :: i, indx
      integer (kind=4) :: kj, km,km1
      real    (kind=8) :: aa, akk,akp
      real    (kind=8) :: b, cri, dt
      real    (kind=8) :: err
      real    (kind=8) :: p2, pdf 
      real    (kind=8) :: rr, rg, rg1,rg2,rg3, rgl,rgu
      real    (kind=8) :: s,s1
      real    (kind=8) :: u1,u2,u3, ul,us,uu, xh
c     determine the range of saddle point.
      km1=5
      xh=0.5d0
      indx=1
      km=km1
      do 210 i=1,n
      if(dabs(c(1,i)).ge.1.e-8.and.dabs(c(2,i)).ge.1.e-8) go to 230
 210  continue
c     first order result
      call dnorm(-bt,pb)
      return
 230  akk=0.d0
      do 220 i=1,n
      if(c(1,i).lt.c(2,i)) then
      if(c(1,i).lt.akk) akk=c(1,i)
      else
      if(c(2,i).lt.akk) akk=c(2,i)
      endif
 220  continue
c     set the range of the saddle point.
  190 uu=-0.1d-5
      if(indx.eq.1) go to 720
      akk=0.d0
      do 240 i=1,n
      if(c(1,i).lt.c(2,i)) then
      if(c(1,i).lt.akk) akk=c(1,i)
      else
      if(c(2,i).lt.akk) akk=c(2,i)
      endif
 240  continue
        if(dabs(akk).le.0.1d-8) then
        ul=-1000.d0
        else
        ul=0.5d0/akk+0.1d-5
        endif
      go to 740
c---  there are (or all are) negative main curvatures
720     if(dabs(akk).le.0.1d-8) then
        ul=-1000.d0
        else
        ul=0.5d0/akk+0.1d-5
        endif
c
c     use secant method to determine the saddle point.
 740  u1=uu
      u2=ul
      call snewgs(u1,rg1,bt,c,n)
c     print *,'u1=',u1,'rg1=',rg1
      rgu=rg1
      call snewgs(u2,rg2,bt,c,n)
c     print *,'u2=',u2,'rg2=',rg2
      rgl=rg2
  301 u3=u1-rg1*(u2-u1)/(rg2-rg1)
c     print *,'uu=',uu,'ul=',ul
      if(u3.gt.uu.or.u3.lt.ul) u3=(uu+ul)/2.
c     print *,'u3=',u3
      call snewgs(u3,rg3,bt,c,n)
      if(rg3*rgu.gt.0.) then
      uu=u3
      rgu=rg3
      else
      ul=u3
      rgl=rg3
      endif
c     print *,'rg3=',rg3
      if(dabs(rg3).le.0.1e-7) then
      us=u3
c--   compute the second derivative term
      dt=us*0.001
      call snewgs(us+dt,rg1,bt,c,n)
c     print *,'us+dt=',us+dt,'rg1=',rg1
      call snewgs(us-dt,rg2,bt,c,n)
c     print *,'us-dt=',us-dt,'rg2=',rg2
      rg=4.*dt/(rg1-rg2)
      b=dabs(rg)
      b=dsqrt(b)
c     print *,'b=',b,'us=',us
        if(indx.eq.1.and.b.lt.0.5) then
        bt=-bt
        do 143 kj=1,n
        c(1,kj)=-c(1,kj)
 143    c(2,kj)=-c(2,kj)
        indx=-1
        go to 190
        endif
      b=xh*b
      else
      u1=u2
      u2=u3
      rg1=rg2
      rg2=rg3
c     print *,'rg2=',rg2,'rg1=',rg1,'rg2-rg1=',rg2-rg1
      go to 301
      endif
      s1=real(km)
c     compute the truncation error
  49  s=b*s1
c     print *,'b*h=',s
      if(s.le.1.) then
      rr=2.d0
      else
      rr=1.d0/s
      endif
      call snews0(s,us,bt,c,n,zz)
      err=abs(zz)*rr
c     print *,'err=',err
      call dnorm(-bt,cri)
      cri=cri*0.00001
      if(err.ge.cri) then
      s1=s1+4.d0
      go to 49
      endif
      km=int(s1)
c
c     use trapezoid rule to estimate the integration ( 12 intervals).
c     set the step length equal 1.
c     print *,'np=',km
      s=0.
      call snews0(s,us,bt,c,n,zz0)
      zz0=zz0/2.
c     print *,'first term=',zz0
      do 400 i=1,km
      s=i*b
      call snews0(s,us,bt,c,n,zz)
c     print *,'i=',i,'s=',s
c     print *,'zz=',zz
      zz0=zz0+zz
c     print *,'zz0=',zz0
 400  continue
      zz0=zz0*b
      aa=2.d0/3.1415927410126d0
      aa=sqrt(aa)
c     print *,'aa=',aa
c     zz=(0.,-1.)
c     print *,'final=',aa*zz0
      zz=aa*zz0
      aa=-bt*bt/2.
      p2=6.2831854820251
      akp=real(zz)
c     zz0=(0.,-1)
c     akp1=zz0*zz
      pdf=exp(aa)/sqrt(p2)
c     print *,'re(zz)=',akp
c     print *,'pdf=',pdf
      pb=-pdf*akp
c     print *,'pb=',pb
c     print *,'akp1=',akp1
c     print *,'real=',pdf*akp1
      if(indx.eq.-1) then
      pb=1.d0-pb
      bt=-bt
      do 145 kj=1,n
      c(1,kj)=-c(1,kj)
 145  c(2,kj)=-c(2,kj)
      endif

      end subroutine sptves
