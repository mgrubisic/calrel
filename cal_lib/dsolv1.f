ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine dsolv1(ngf,bt,alph,y,nry,rr,ind,a2,root)
c
c     functions:
c       solve the roots of the intersection of first order failure
c        surface and simulated vectors.
c
c     input arguments:
c       ngf   : number of performance functions.
c       bt   : reliability indexes.
c       alph: unit vectors of design points.
c       y    : simulated vector.
c       nry   : no. of variables.
c       root  : threshold of root in root's finding procedure.
c
c     output arguments:
c       rr   : roots of intersection of 2nd order surface and simulated
c              vector.
c       ind  : index of how many roots for each mode, (1-4)
c       a2   : coef. of 1st order terms in the 2nd order surface.
c
c     called by: dirs1
c
c     last version: july 30, 1987 by h.-z. lin
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine dsolv1(bt,alph,y,rr,ind,a2,igfx,root)

      implicit   none

      include   'cuts.h'
c     include   'file.h'
      include   'prob.h'

      real    (kind=8) :: root
      integer (kind=4) :: igfx(ngf,3),ind(ngf)
      real    (kind=8) :: y(nry),bt(ngf),rr(ngf),a2(ngf),alph(nry,ngf)

      integer (kind=4) :: i
      integer (kind=4) :: k,k2
      real    (kind=8) :: sy
      do 100 k=1,ngfc
      k2=igfx(k,2)
      sy=0.d0
      do 20 i=1,nry
 20   sy=sy+y(i)*alph(i,k2)
      if(dabs(sy).le.0.1e-4) then
      ind(i)=0
      go to 100
      endif
      rr(k)=bt(k2)/sy
      if(dabs(rr(k)).ge.root) then
      ind(k)=0
      else
      ind(k)=1
      endif
      a2(k)=-sy
c     write(not,200) y(1),y(2),alph(1,k2),alph(2,k2),sy,rr(k)
c200  format(' y= ',2e14.7,' a= ',2e14.7, 'sy= ',e14.7,' rr=',e14.7)
 100  continue

      end subroutine dsolv1
