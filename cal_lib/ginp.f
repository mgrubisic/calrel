      subroutine ginp(ti,tj,ids,ii,jj)

      implicit   none

      include   'cuts.h'
      include   'file.h'
      include   'prob.h'

      integer (kind=4) :: ii,jj
      real    (kind=8) :: ti(4,nrx),tj(4,nrx),t(6)
      integer (kind=4) :: ids(nrx)

      integer (kind=4) :: i1,j1
      integer (kind=4) :: k,k3
      integer (kind=4) :: nrec
      save

c     read d(bti)/d(par) and d(btj)/d(par).
      k3=1
      do 251 k=1,ngf
      if(k3.gt.2) go to 255
        if(ii.eq.k.or.jj.eq.k) then
        k3=k3+1
      nrec=(k-1)*nrx
      do 210 i1=1,nrx
      read(nsv,rec=nrec+i1) (t(j1), j1=1,6)
         if(ii.eq.k) then
          if(ids(i1).ne.7.and.ids(i1).le.50) then
          do 294 j1=1,4
  294     ti(j1,i1)=t(j1)
          else
          do 293 j1=3,6
  293     ti(j1-2,i1)=t(j1)
          endif
         else
          if(ids(i1).ne.7.and.ids(i1).le.50) then
          do 213 j1=1,4
  213     tj(j1,i1)=t(j1)
          else
          do 215 j1=3,6
  215     tj(j1-2,i1)=t(j1)
          endif
         endif
  210 continue
        endif
  251 continue
  255 continue

      end subroutine ginp
