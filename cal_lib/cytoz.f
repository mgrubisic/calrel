cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine cytoz(tf,y,z,nge,nges)
c
c   functions:
c      transform y in a type 2 group to z.
c
c   input arguments:
c      tf : the transformation matrix from z to y, i.e. y = tf * z.
c      y : basic variables in the standard space.
c      nge : number of variables in this group.
c      nges : number of variables to be transformed.
c
c   output arguments:
c      z : basic variables in the correlated normal space.
c
c   called by: cytox.
c
c   last revision: june 26 1987 by p.-l. liu.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cytoz(tf,y,z,nge,nges)

      implicit   none

      integer (kind=4) :: nge,nges
      real    (kind=8) :: tf(nge,nge),y(nge),z(nge)

      integer (kind=4) :: i,j

      save

c     print *,'y=',(y(ki),ki=1,nge)
c     call mprint(tf,nge,nge,1)
      z(1)=y(1)/tf(1,1)
      if(nges.eq.1) return
      do i=2,nges
        z(i)=y(i)
        do j=1,i-1
          z(i)=z(i)-tf(i,j)*z(j)
        end do ! j
        z(i)=z(i)/tf(i,i)
      end do ! i

      end subroutine cytoz
