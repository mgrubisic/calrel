c     ********************************************************************
c     subroutine fstart
c
c     written by mario de stefano             august-october 1990
c     ********************************************************************
      subroutine fstart(y,ydp,epc,cosa,ystar,nry,icont,adp,see)

      implicit   none

      real    (kind=8) :: random

      integer (kind=4) :: nry,icont
      real    (kind=8) :: epc, see
      real    (kind=8) :: y(nry),cosa(nry,nry-1),ystar(nry,nry-1)
      real    (kind=8) :: ydp(nry),adp(nry)

      integer (kind=4) :: i,j, iopt
      real    (kind=8) :: ysum

      real    (kind=8) :: cdot
c     set parameters for the subroutine drand
c     call gseed(dseed)
      iopt = 1
      do i = 1,nry
        call gguw(see,1,iopt,random)
        iopt = 0
        y(i) = (random-0.5d0)*2
      end do ! i
      ysum = sqrt(cdot(y,y,1,1,nry))
      do i = 1,nry
        y(i) = y(i)/ysum*epc+ydp(i)
      end do ! i
      call project(y,cosa,ystar,nry,icont)
      do i = 1,nry
         y(i) = y(i)-ydp(i)
      end do ! i
      ysum = cdot(y,y,1,1,nry)
      do i = 1,nry
        y(i) = y(i)*epc/sqrt(ysum)+ydp(i)
      end do ! i

      end subroutine fstart
