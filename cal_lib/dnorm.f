      subroutine dnorm(y,p)

      implicit   none

      real    (kind=8) :: p,y
      real    (kind=8) :: sqr1d2

      sqr1d2 = sqrt(0.5d0)
      p      = 0.5d0 * derfc(-y*sqr1d2)

!     Bound p between 1.e-10 and 1.0 - 1.e-10

      p = max(min(p,1.0d0-1.0d-10),1.0d-10)

      end subroutine dnorm
