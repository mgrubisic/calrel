      subroutine freetm

      implicit   none

      include   'file.h'
      include   'line.h'

      character (len=1) :: sp,bs,sc,c

      integer (kind=4) :: i,k 
      save

      data sp/' '/,sc/':'/,c/'c'/
      data bs/'\'/

c-----read line of free field data ------------------

      do i=1,160
        line(i) = sp
      end do ! i

      i    = 1
      llen = 80
  50  continue
      read (nin,1000,err=100,end=200) (line(k),k=i,llen)

c-----check for additional line ---------------------

      do k=i,llen
        if(line(k).ne.bs) go to 80
        i = k
        llen= k+79
        if(llen.gt.160) llen = 160
        go to 50
   80   continue
      end do ! k

      call upper
      go to 900

c-----error in read ---------------------------------

  100 write(not,2001)
      stop

  200 write(not,2002)
      stop

  900 continue

c----------------------------------------------------

 1000 format (80a1)
 2001 format(/,' * Error in reading input line *',/)
 2002 format(/,' * End of file in reading input line *',/)

      end
