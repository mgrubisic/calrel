      subroutine dscum0(y1,y2,x1,z1,tf,tp,ex,sg,par,bnd,igt,lgp,
     *           ids,ipa,ib,izx,rot,id,m1,mc,xp,ir,igfx,mcst,mcend)

      implicit   none

      include   'cuts.h'
      include   'prob.h'

      integer (kind=4) :: m1,mcst,mcend
      real    (kind=8) :: xp
      real    (kind=8) :: tf(ntf),x1(nrx),y1(nry),z1(nry),bnd(2,nrx)
      real    (kind=8) :: rot(ngfc+2),par(4,nrx),tp(*),sg(nrx)
      integer (kind=4) :: ids(nrx),ib(nrx)
      real    (kind=8) :: ex(nrx)
      integer (kind=4) :: id(ngfc+2),ipa(4,nrx),izx(nry)
      integer (kind=4) :: lgp(3,nig),igt(nig)
      real    (kind=8) :: y2(nry)
      integer (kind=4) :: mc(ntl,2),ir(ngfc),igfx(ngf,3)

      integer (kind=4) :: i,ii,i4,imm, ic,ics,idg
      real    (kind=8) :: diff
      real    (kind=8) :: gx
      real    (kind=8) :: r,rn
      xp=0.d0
      rn=real(nry)
c     print *,'m1=',m1
      if(m1.le.1) return
      do 120 ii=2,m1
      diff=rot(ii+1)-rot(ii)
      if(dabs(diff).le.0.1e-3) go to 120
      r=(rot(ii)+rot(ii+1))/2.d0
      do 30 i=1,nry
  30  y2(i)=r*y1(i)
      call cytox(x1,y2,z1,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      do 1101 i=1,ngfc
      i4=igfx(i,3)
      call ugfun(gx,x1,tp,i4)
      if(gx.le.0.d0) then
        ir(i)=0
      else
        ir(i)=1
      endif
c     print *,'r=',r,'gx=',gx
 1101 continue
      imm=1
      idg=1
      do 100 i=mcst,mcend
      if(mc(i,1).ne.0) then
        ic=mc(i,2)
        if(mc(i,1).lt.0) then
          imm=imm*ir(ic)
        else
          imm=imm*(1-ir(ic))
        endif
      else
        ics=1-imm
        idg=idg*ics
        imm=1
      endif
  100 continue
      idg=1-idg
      if(idg.eq.1) then
        id(ii)=1
      else
        id(ii)=0
      endif
 120  continue

c     print *,'id=',(id(ka),ka=1,m1)

c.....compute the failure probability for this directinal v vector.

      call dpf(m1,id,rot,xp)

      end subroutine dscum0
