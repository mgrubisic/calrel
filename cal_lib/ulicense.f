      subroutine ulicense(isw)

      implicit   none

      logical       userflag, uname
      common /ulic/ userflag, uname

      include   'file.h'

      integer    isw

      save

!     Set flags

      if(isw.eq.0) then
        userflag = .true.    ! Activate license

        uname    = .true.    ! For U.C. Berkeley
!       uname    = .false.   ! For External license
      else
        write(not,2000)
      endif

!     Format (Add Name of university)

 2000 format(
     & 5x,'*                                                        *',/
     & 5x,'*   This version of CalREL is for exclusive use by:      *',/
     & 5x,'*               Students and Faculty at                  *',/
     & 5x,'*                  <name> University                     *',/
     & 5x,'*                                                        *',/
     & 5x,'*   This software is protected by copyright laws.        *',/
     & 5x,'*   Unauthorized use and distribution are prohibited     *',/
     & 5x,'*   by law and will be prosecuted.                       *',/
     & 5x,'*                                                        *',/
     & 5x,'*========================================================*' )

      end
