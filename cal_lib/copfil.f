      subroutine copfil

      implicit   none

      include   'blkrel1.h'
      include   'cuts.h'
      include   'dbsy.h'
      include   'file.h'
      include   'filebound.h'
      include   'flag.h'
      include   'line.h'
      include   'optm.h'
      include   'prob.h'

      logical       userflag, uname
      common /ulic/ userflag, uname

      logical    exst,  linp,lout
      character  chch*1, filnam*200, filout*200
      integer    in,io, status
      real*8     r

      integer (kind=4) :: i, ifound
      integer (kind=4) :: lrec
      integer (kind=4) :: nchar, nchar2

      save

c     Set logical unit numbers for I/O

      nin =  9  ! Standard File Input
      not = 10  ! Standard File Output
      nia = 11
      nco = 12
      nsv = 14
      ns0 = 15

c     Look for command inputs

      linp = .false.
      lout = .false.

c     Open input file

      filnam = ' '
      filout = ' '
      call get_command_argument(1,filnam,in,status)
      if(status.eq.0) then
        inquire(file= filnam(1:in), exist = exst)
        if(exst) then
          open(nin,file=filnam(1:in),form='formatted')
         else
          write(*,'(/a)') ' Specified filename does not exist'
          stop
        endif
        linp = .true.
      else  
        go to 100
      endif

      call get_command_argument(2,filout,io,status)
      if(status.eq.0) then
        open(not,file=filout(1:io),form='formatted',status='unknown')
        lout = .true.
      endif

 100  if(.not.linp) then
        write(*,'(/a$)') ' Specify Input Filename:' 
        read (*,'(a)')  filnam
        in = index(filnam,' ') - 1
  
        if(filnam(1:1).eq.'O' .or.  filnam(1:1).eq.'o') then
          write(*,*) ' ERROR: File name may not begin with O'
          stop
        endif
        inquire(file= filnam(1:in), exist = exst)
        if(exst) then
          open(nin,file=filnam(1:in),form='formatted',status='unknown')
        else
          write(*,'(/a)') ' Specified file does not exist, reinput'
          stop
        endif
      endif

c     Open output file

      if(.not.lout) then
        filout      = filnam
        filout(1:1) = 'O'
        io = index(filout,' ') - 1
        open(not,file=filout(1:io),form='formatted',status='unknown')
      endif

      do i = 1,200
       filnam2(i:i) = ' '
       filnam3(i:i) = ' '
      end do ! i
      nchar  = 0
      nchar2 = 0
      ifound = 0

  150 continue
      nchar=nchar+1
      read(filnam(nchar:nchar),'(a1)',end=159) chch
      if(chch.eq.'.'.or.chch.eq.' ') then
        ifound = 1
        goto 159
      endif
      write(filnam2(nchar:nchar),'(a1)') chch
      write(filnam3(nchar:nchar),'(a1)') chch
      nchar2 = nchar2 + 1
      goto 150

  159 continue

      write(filnam2(nchar2+1:nchar2+4),'(a4)') '.bnd'
      write(filnam3(nchar2+1:nchar2+4),'(a4)') '.pbs'

c     Set direct access length to 6 real*8 words

      inquire(iolength=lrec) (r,i=1,6)
      open(nsv,file='rel.nsv',form='unformatted',status='unknown',
     &     access='direct',recl=lrec)

      end subroutine copfil
