      subroutine title

      include   'file.h'

      logical       userflag, uname
      common /ulic/ userflag, uname

      save

      write(not,2000)

      if(uname) then
        write(not,2001)
      else
        call ulicense(1)
      endif

      write(not,2002)

 2000 format(//
     & 5x,'**********************************************************'/
     & 5x,'*    U n i v e r s i t y   o f   C a l i f o r n i a     *'/
     & 5x,'*    Department of Civil & Environmental Engineering     *'/
     & 5x,'*                                                        *'/
     & 5x,'*                   C  a  l  r  e  l                     *'/
     & 5x,'*               Cal-reliability program                  *'/
     & 5x,'*                     developed by                       *'/
     & 5x,'*       P.-L. Liu, H.-Z. Lin and A. Der Kiureghian       *'/
     & 5x,'*                                                        *'/
     & 5x,'*             Last revision : July 2017                  *'/
     & 5x,'*                                                        *'/
     & 5x,'*              Copyright (c) 1989-2017                   *'/
     & 5x,'*        Regents of the University of California         *'/
     & 5x,'*                All rights reserved                     *'/
     & 5x,'**********************************************************')

 2001 format(
     & 5x,'*                                                        *'/
     & 5x,'*  This version of Calrel is for the exclusive use of    *'/
     & 5x,'*  students and faculty at the University of California, *'/
     & 5x,'*  Berkeley, USA. Unauthorized use is prohibited by law. *'/
     & 5x,'*                                                        *'/
     & 5x,'**********************************************************')

 2002 format(//10x,'R U N N I N G     C A L R E L     N O W ')

      end
