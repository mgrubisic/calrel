cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine cdgx(x,sg,dgx,tp,ig,ids,indx)
c
c   functions:
c      compute dg/dx by a central difference scheme.
c
c   input arguments:
c      x : basic random variables in the original space.
c      sg : standard deviations of x.
c      tp : deterministic parameters in performance function.
c      ig : performance function number.
c      ids: distribution numbers of x.
c      indx: indx.eq.0 compute dg/dx for ids.ne.0
c            indx.ne.0 compute dg/dx for ids.eq.0
c
c   output arguments:
c      dgx : dg/dx.
c
c   calls: ugfun.
c
c   called by: fdirct, fsenvd, gdadtp
c
c   last revision: dec. 12 1989 by h.-z. lin.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cdgx(x,sg,dgx,tp,ig,ids,indx)

      implicit   none

c     include   'file.h'
      include   'grad.h'
      include   'hong.h'
      include   'prob.h'
      include   'test.h'

      real*8     x(nrx),dgx(nrx),sg(nrx),tp(*)
      integer    ids(nrx)

      integer    i, ig, indx
      real*8     fm,fp, h,h2, r, xi

      save

      addk = 1
      indd = 1
      r = 0.0001d0
      flgf = .true.
      flgr = .false.
      do 10 i = 1,nrx
        if(i.eq.1) then
          inkk = -1
        else
          inkk = 1
        endif
        xi = x(i)
        if(indx.eq.0) then
          if(ids(i).eq.0) go to 10
          h = sg(i)*r
        else
          if(ids(i).ne.0) go to 10
          h = xi*r*10.d0
        endif
        if(h.eq.0.) h = r
        h2   = h*2.d0
        x(i) = xi+h
        call ugfun(fp,x,tp,ig)
        x(i) = xi-h
        call ugfun(fm,x,tp,ig)
        dgx(i) = (fp-fm)/h2
        x(i) = xi
  10  continue
      indd = 0
      addk = 0

      end subroutine cdgx
