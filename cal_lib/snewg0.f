      subroutine snewg0(s,u,bt,c,n,rgg)

      implicit   none

      integer (kind=4) :: n
      real    (kind=8) :: s, u, bt, rgg
      real    (kind=8) :: c(n)

      integer (kind=4) :: i
      real    (kind=8) :: u2,r1,r2,s2,rg, pp

      u2 = u*2.d0
      s2 = s*s
      rg = 1.d0
      do i = 1,n
        r1 = 1.d0-u2*c(i)
        rg = rg*(r1*r1+c(i)*c(i)*s2)
      end do ! i
      r2 = u*u/2.d0+bt*u
      r2 = exp(r2)
      call dnorm(-s,pp)
      rgg = 0.79788456d0*r2*pp/sqrt(rg)

      end subroutine snewg0
