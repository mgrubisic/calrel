!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!      subroutine gsenc(xn,xlin,beta,sdgy,tp,t,ids,ipa,fltf,isv)
!
!   functions:
!      perform sensitivity analysis on beta and pf1 with
!      respect to distribution parameters and deterministic
!      parameters for each component.
!
!   input arguments:
!      x : names of x.
!      xlin : basic random variables in the original space.
!      t : temporary array.
!      ids : distribution numbers of x.
!      ipa : #'s of the associated rv of distribution parameters.
!             n-th element of ipa = 0 means parameter n is constant.
!      tp : deterministic parameters in performance function.
!      beta : new reliability index.
!      sdgy : |dg/dy|.
!
!   calls: ugfun.
!
!   called by: csens.
!
!   last revision: dec 10, 1989 by h.-z. lin.
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine gsenc(xn,xlin,beta,sdgy,tp,t,ids,ipa,fltf,ixz,
     *                 igfx,isv)

      implicit   none

      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'grad.h'
      include   'prob.h'

      logical            :: fltf(ngf)
      character (len=72) :: tout
      character (len=11) :: tt
      character (len=4 ) :: xn(nrx)

      integer (kind=4) :: isv 
      real    (kind=8) :: xlin(nrx,ngf),t(6,nrx)
      integer (kind=4) :: ids(nrx),ipa(4,nrx)
      real    (kind=8) :: tp(*),beta(ngf),sdgy(ngf)
      integer (kind=4) :: ixz(nrx),igfx(ngf,3)

      integer (kind=4) :: i,j,k,l, itst, k2,k4,kk, nrec
      real    (kind=8) :: dbtp, gm,gp, pb, r, tpi 
      save

      data tt/'           '/
!
      itst = 2
      flgf = .false.
      flgr = .false.
!
!---  output sensitivity measures of distribution parameters.
!
      if(icl.eq.1.and.igf.gt.0) call finim(igfx,igf)
      if(isv.eq.2) go to 100
      write(not,2010)
      do 70 k = 1,ngfc
        k2 = igfx(k,2)
        if(.not.fltf(k2)) call cerror(5,0,0.,' ')
        nrec = (k2-1)*nrx
        do 10 i = 1,nrx
        read(nsv,rec = nrec+i) (t(j,i),j=1,6)
  10    continue
        write(not,2020) igfx(k,3)
        do 30 i = 1,nrx
          tout = ' '
          if(ids(i).ne.7.and.ids(i).le.50) then
             write(tout(1:71),2030) xn(i),(t(j,i),j = 1,4)
          if(ixz(i).eq.0) then
              tout(17:27) = tt
              tout(39:49) = tt
              go to 99
          endif
             if(ipa(1,i).ne.0) then
              tout(6:16) = tt
              tout(28:38) = tt
!             tout(39:49) = tt
             endif
             if(ipa(2,i).ne.0) then
              tout(17:27) = tt
!             tout(28:38) = tt
              tout(39:49) = tt
             endif
          else
            write(tout(1:71),2040) xn(i),(t(j,i),j = 3,6)
            kk = 28
            do 20 j = 1,4
            l = kk+(j-1)*11
   20       if(ipa(j,i).ne.0) tout(l:l+10) = tt
          end if
   99     write(not,'(a)') tout
   30     continue
        write(not,2050)
        pb = -0.39894228d0*exp(-beta(k2)*beta(k2)*0.5d0)
        do 60 i = 1,nrx
!         if(ixz(i).eq.0) go to 60
          do 40 j = 1,6
   40       t(j,i) = t(j,i)*pb
          tout = ' '
          if(ids(i).ne.7.and.ids(i).le.50) then
            write(tout(1:71),2030) xn(i),(t(j,i),j = 1,4)
            if(ixz(i).eq.0) then
              tout(17:27) = tt
              tout(39:49) = tt
              go to 199
            endif
            if(ipa(1,i).ne.0) then
              tout(6:16) = tt
              tout(28:38) = tt
!             tout(39:49) = tt
            endif
            if(ipa(2,i).ne.0) then
              tout(17:27) = tt
!             tout(28:38) = tt
              tout(39:49) = tt
            endif
          else
            write(tout(1:71),2040) xn(i),(t(j,i),j = 3,6)
            kk = 28
            do 50 j = 1,4
            l = kk+(j-1)*11
   50       if(ipa(j,i).ne.0) tout(l:l+10) = tt
          end if
  199     write(not,'(a)') tout
   60   continue
        write(not,2060)
   70 continue
!
!---  perform sensitvity analysis on tp.
!
  100 if(ntp.eq.0.or.isv.eq.1) return
      write(not,2070)
      do 130 k = 1,ngfc
        k2 = igfx(k,2)
        k4 = igfx(k,3)
        pb = -0.39894228d0*exp(-beta(k2)*beta(k2)*0.5d0)
        write(not,2080) k4
        do 120 i = 1,ntp
          r = 0.001d0*tp(i)
          if(r.eq.0.d0) r = 0.001d0
          tpi = tp(i)
          tp(i) = tpi+r
          call ugfun(gp,xlin(1,k2),tp,k4)
          tp(i) = tpi-r
          call ugfun(gm,xlin(1,k2),tp,k4)
          dbtp = (gp-gm)/2.d0/r/sdgy(k2)
          tp(i) = tpi
          write(not,2090) i,dbtp,dbtp*pb
  120     continue
        write(not,2060)
  130   continue

!---  output formats.

 2010 format(/' Sensitivity with respect to distribution',
     &       ' parameters'/)
 2020 format(' Limit-state function',i5/1x,70('-')/
     &       ' d(beta)/d(parameter) :'/
     &       1x,'var',5x,'mean',6x,'std dev',5x,'par 1',6x,
     &       'par 2',6x,'par 3',6x,'par 4')
 2030 format(1x,a4,1p,4e11.3)
 2040 format(1x,a4,22x,1p,4e11.3)
 2050 format(/' d(pf1)/d(parameter) :'/
     &       1x,'var',5x,'mean',6x,'std dev',5x,'par 1',6x,
     &       'par 2',6x,'par 3',6x,'par 4')
 2060 format(1x,70('-'))
 2070 format(/' Sensitivity with respect to limit-state',
     & ' function parameters'/)
 2080 format(' Limit-state function',i5/1x,70('-')/
     &   1x,'par',3x,'d(beta)/d(parameter)',5x,'d(pf1)/d(parameter)')
 2090 format(i3,8x,1pe11.3,14x,e11.3)

      end subroutine gsenc
