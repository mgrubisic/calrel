cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine inpro(ex,sg,par,rox,ro,nds,ids,izx,ngx,nge,igt)
c
c   functions:
c      read and print input data.
c
c   input arguments:
c      ex : mean vector of x.
c      sg : standard deviations of x.
c      par : distribution parameters of x.
c      nds : distribution numbers of the members in the group.
c      ids : distribution numbers of x.
c      izx : indicators for z to x.
c      nge : number of random variables in the group.
c      ngx : number of basic variables in the group.
c
c   output arguments:
c      rox : correlation matrix of a group of x.
c      ro : correlation matrix of z.
c
c   calls: infree, cmprin, itraro, ibcbdp, cinver.
c
c   called by: input.
c
c   last revision: jan. 9 1990 by h.-z. lin.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine inpro(ex,sg,par,ro,nds,ids,izx,ngx,nge,igt,
     *                 igx,rox)

      implicit   none

      include   'file.h'
      include   'prob.h'

      integer (kind=4) :: igt, nge,ngx
      real    (kind=8) :: ex(nrx),sg(nrx),par(4,nrx)
      real    (kind=8) :: rox(ngx,ngx),ro(nge,nge)
      integer (kind=4) :: nds(ngx),ids(nrx),izx(nge),igx(ngx)

      integer (kind=4) :: i,j,k,l, j1s, k1

      save
 
c---  read correlation matrix.
      rox(1,1)=1.0
      do i=2,ngx
        read(nin,*,err=70) (rox(i,j),j=1,i-1)
        rox(i,i)=1.d0
      end do ! i
      do j=2,ngx
        do i=1,j-1
          rox(i,j)=rox(j,i)
        end do ! i
      end do ! j
      write(not,2010)
      call cmprin(igx,rox,ngx,ngx,ngx,1,1)
c---  store the sub-correlation matrix in the global matrix.
      l=1
      do 50 j=1,ngx
      if(nds(j).eq.0) go to 50
      k=1
      do 40 i=1,ngx
      if(nds(i).eq.0) go to 40
      ro(k,l)=rox(i,j)
      k=k+1
   40 continue
      l=l+1
   50 continue
c---  transform correlation matrix from x space to z space.
      if(k.eq.2) return
      k1=k-1
      call itraro(ex,sg,par,ro,ids,izx,nge,k1)
c     write(not,2020)
c     call cmprin(igx,ro,nge,nge,nge,1,1)
c---  compute transformation matrix from z to y.
c---  cholesky decomposition.
      call icdecp(ro,k1,k1,nge)
c---  tf=inverse of ro
      call cinver(ro,k1,nge)
c     print *,'cinver'
c     call cmprin(igx,ro,nge,nge,nge,1,1)
      if(igt.eq.4) then
      j1s=nge-k1
      do 445 i=1,k1
      do 445 j=1,i
 445  rox(i,j)=ro(i,j)
      do 448 i=1,k1
      do 448 j=1,i
 448  ro(j1s+i,j1s+j)=rox(i,j)
      if(k1.ge.nge) return
      do 449 j=1,j1s
      do 449 i=j,nge
 449  ro(i,j)=0.
      do 549 j=1,j1s
 549  ro(j,j)=1.d0
      else
      if(k1.ge.nge) return
      do 60 i=k1+1,nge
      do 60 j=1,i
   60 ro(i,j)=0.
      endif
c     call cmprin(igx,ro,nge,nge,nge,1,1)
      return
c---  input error.
   70 call cerror(14,0,0.,' ')

c---  output formats.

 2010 format(/' correlation coefficient matrix in original space:')
c2020 format(/' correlation coefficient matrix in normal space:')

      end subroutine inpro
