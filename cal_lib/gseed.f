      subroutine gseed(dseed)
c
c     generating a random initial seed for random number generator
c     by y. zhang 1991.

c     portability
c      machine : ibm/pc, unix/linux
c      fortran : F90/95, gfortran

      implicit   none

      real    (kind=8) :: yr,mon,day,hr,min,sec
      real    (kind=8) :: dseed

      character (len=10) :: ctime
      character (len=8 ) :: cdate
      integer (kind=4) :: ihr,imin,i100th,imon,iday

c---- Fortran 90/95 time and date

      ihr  = 0
      imin = 0
      sec  = 0.0d0
      call date_and_time(time = ctime)
      read(ctime(1:2),'(i2)') ihr
      read(ctime(3:4),'(i2)') imin
      read(ctime(5:10),'(0p,1f6.2)') sec
      min = dble(imin)
      hr  = dble(ihr)

      call date_and_time(date = cdate)
      read(cdate(1:4),'(i4)') i100th
      read(cdate(5:6),'(i2)') imon
      read(cdate(7:8),'(i2)') iday
      yr  = i100th
      mon = imon
      day = iday

c     Set return seed

      dseed = sec + min*1.0d2 + hr*1.0d4 + day*1.0d6 + mon*1.0d8 + yr

      end subroutine gseed
