      subroutine cstop

      implicit   none

      include   'blkrel1.h'
      include   'blkchr.h'
      include   'bond.h'
      include   'crel.h'
      include   'cuts.h'
      include   'dbsy.h'
      include   'file.h'
      include   'flag.h'
      include   'flcg.h'
      include   'fsav.h'
      include   'optm.h'
      include   'prob.h'
      include   'simu.h'

      integer    i

      integer    nps     ! rlt: Cannot find in program

      save

      nps   = 0

      if(ifeapo.ne.0) call pstop

c     Write data files

      open(nia,file='rel.nia',form='unformatted',status='unknown')
      rewind(nia)
      write(nia) mtot,np,(ia(i),i=1,next),(ia(i),i=idir,mtot)
      close(nia, status='keep')
!     write(99,'(5i16)') mtot,np,next,idir
!     write(99,'(5i16)') (ia(i),i=1,next)
!     write(99,'(5i16)') (ia(i),i=idir,mtot)

      open(nco,file='rel.nco',form='unformatted',status='unknown')
      rewind(nco)
      write(nco) icl,igr,igf,ngf,nig,nrx,ntp,nry,ntf,ncs,ntl,imc,
     &           ngfc,ntge,numa,next,idir,ip,iop,ni1,ni2,tol,
     &           op1,op2,op3,nsm,nps,cov,flc,itt,gx,ig0,iter,des0
      write(nco) nd
      close(nco, status='keep')

c     Close all open files

      write(not,'(/a)') ' >>>> STOP CALREL <<<<'
      close(nin, status='keep')
      close(not, status='keep')
      close(nsv, status='keep')
      close(ns0, status='keep')

c     Stop program

      stop
      end
