!     ********************************************************************
!     subroutine fcosine
!
!     last version: dec. 12, 1990 by hong-zong lin
!     ********************************************************************
      subroutine fcosine(yooo,yoo,yo,y,cosa,
     1                  yses,cosan,cosao,icont,nses)

      implicit   none

      include   'optm.h'
      include   'prob.h'

      integer (kind=4) :: icont,nses
      real    (kind=8) :: yo(nry),y(nry),cosao(nry),cosan(nry)
      real    (kind=8) :: cosa(nry,nry-1),yoo(nry),yooo(nry),yses(nry)

      integer (kind=4) :: i,j,ij
      real    (kind=8) :: asumc, asumco
      real    (kind=8) :: sees
      real    (kind=8) :: ysum,sumc,sumco,sumdi,sumn,sumo,sumoo,sumop
!--cosao, cosaop(cosan)

!     if(icont.eq.2) print *,'y = ',(y(kk),kk=1,nry)
!     if(icont.eq.2) print *,'yo = ',(yo(kk),kk=1,nry)
      sumco = 0.d0
      sumo = 0.d0
      do i = 1,nry
        cosao(i) = yo(i)-yoo(i)
        sumo = sumo+cosao(i)*cosao(i)
      end do ! i
      do j = 1,nry
        cosao(j) = cosao(j)/dsqrt(sumo)
      end do ! j
      sumop = 0.d0
      do i = 1,nry
        cosan(i) = yoo(i)-yooo(i)
        sumop = sumop+cosan(i)*cosan(i)
      end do ! i
      do j = 1,nry
        cosan(j) = cosan(j)/dsqrt(sumop)
      end do ! j
      do i = 1,nry
        sumco = sumco+cosao(i)*cosan(i)
      end do ! i
!     if(icont.eq.2) print *,'sumco = ',sumco
!--cosan, cosao
      sumc = 0.d0
      sumn = 0.d0
      do i = 1,nry
        cosan(i) = y(i)-yo(i)
        sumn = sumn+cosan(i)*cosan(i)
      end do ! i
      do j = 1,nry
        cosan(j) = cosan(j)/dsqrt(sumn)
      end do ! j
      do i = 1,nry
        sumc = sumc+cosan(i)*cosao(i)
      end do ! i
!     if(icont.eq.2) print *,'sumn = ',sumn,'sumc=',sumc
      asumc = acos(sumc)
      asumco = acos(sumco)
      sees = (asumc-asumco)/(asumc+asumco)
      if (abs(sumc).gt.(1.d0-.01d0).or.abs(sees).gt.0.01d0) then
!--cosan
        do ij = 1,nry
          cosa(ij,icont) = cosan(ij)
        end do ! ij
        nses = 0
      else
!--cosadi(cosan), cosaoo(cosao)
        sumdi = 0.d0
        do i = 1,nry
          cosan(i) = y(i)-yoo(i)
          sumdi = sumdi+cosan(i)*cosan(i)
        end do ! i
        do j = 1,nry
          cosan(j) = cosan(j)/sqrt(sumdi)
        end do ! i
        sumoo = 0.d0
        do i = 1,nry
          cosao(i) = yo(i)-yooo(i)
          sumoo = sumoo+cosao(i)*cosao(i)
        end do ! i
        do i = 1,nry
          cosao(i) = cosao(i)/sqrt(sumoo)
        end do ! i
        ysum = 0.0d0
        do i = 1,nry
          cosa(i,icont) = -cosan(i)+cosao(i)
          ysum = ysum+cosa(i,icont)*cosa(i,icont)
        end do ! i
!       if(icont.eq.2) print *,'sum = ',sum
        do i = 1,nry
          cosa(i,icont) = cosa(i,icont)/sqrt(ysum)
          yses(i) = y(i)-tol*cosa(i,icont)
        end do ! i
        nses = 1
      endif

      end subroutine fcosine
