      subroutine cdir2

      implicit   none

      include   'blkrel1.h'
      include   'prob.h'
      include   'cuts.h'


      integer    n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15
      integer    n16,n17
      integer    nr,nc

      save

      call locate('beta',n1,nr,nc)
      call locate('srt ',n2,nr,nc)
      call locate('cur ',n3,nr,nc)
      call locate('mc  ',n4,nr,nc)
      call define('x5  ',n5,1,nry)
      call define('y5  ',n6,2,nry)
      call define('yy  ',n7,1,nry)
      call define('rr  ',n8,4,ngfc)
      call define('a1  ',n9,ngfc,2)
      call define('a2  ',n10,ngfc,2)
      call define('rot ',n11,1,2+ngfc)
      call defini('ind ',n12,ngfc,2)
      call defini('id  ',n13,1,2+ngfc)
      call defini('ig  ',n14,1,ngfc)
      call locate('btg1',n15,nr,nc)
      call locate('alph',n16,nr,nc)
      call locate('igfx',n17,nr,nc)
      call dirs2(ia(n1),ia(n2),ia(n3),ia(n4),ia(n5),ia(n6),ia(n7),
     *           ia(n8),ia(n9),ia(n10),ia(n11),ia(n12),
     *           ia(n13),ia(n14),ia(n15),ia(n16),ia(n17))
      call delete('x5  ')
      call delete('y5  ')
      call delete('yy  ')
      call delete('rr  ')
      call delete('a1  ')
      call delete('a2  ')
      call delete('rot ')
      call delete('ind ')
      call delete('id  ')
      call delete('ig  ')
c     call delete('btg1')
c     call delete('cur ')

      end subroutine cdir2
