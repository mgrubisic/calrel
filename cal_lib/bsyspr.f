!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!     subroutine bsyspr(rhom,pro,prbr,pl1,pl2,pl3)
!
!   functions:
!     print results of system reliability analysis.
!
!   input arguments:
!     rhom : modal correlation matrix.
!     pro : bounds of joint modal failure probabilities.
!         upper triangle - upperbounds.
!         lower t2riangle - lowerbounds.
!         diagonal - individual mode probabilities:
!     prbr: joint modal failure probability calculated by numerical integration.
!     pl1,pu1 : unimodal bounds of system failure probability.
!     pl2,pu2 : relax bimodal bounds of system failure probability.
!     pl3,pu3 : bimodal bounds of system failure probability.
!     ibt  : = 1, unimodal bound only.
!            = 2, relax bimodal bound only
!            = 3, bimodal bound only.
!            = 0, unimodal, relax bimodal and bimodal bounds.
!
!   calls: cmprin, dnormi.
!
!   called by: bfos.
!
!   last revision: aug. 30 1987 by p.-l. liu.
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine bsyspr(rhom,pro,prbr,pl1,pl2,pl3,igfx)

      implicit   none

      include   'bond.h'
      include   'cuts.h'
      include   'prob.h'
      include   'file.h'

      real    (kind=8) :: pro(ngfc,ngfc),rhom(ngfc,ngfc),prbr(ngfc,ngfc)
      integer (kind=4) :: igfx(ngfc)

      integer (kind=4) :: ier
      real    (kind=8) :: pl1,     pl2,     pl3
      real    (kind=8) :: yl1,yu1, yl2,yu2, yl3,yu3

      save

      pl1 = min(pl1,1.0d0)
      pu1 = min(pu1,1.0d0)
      pl2 = min(pl2,1.0d0)
      pu2 = min(pu2,1.0d0)
      pl3 = min(pl3,1.0d0)
      pu3 = min(pu3,1.0d0)

      if(ibt.eq.0) go to 60
! WIP: 2 options for ibt = 0
      go to (10,20,40) ibt

   10 call dnormi(yl1,pl1,ier)
      call dnormi(yu1,pu1,ier)
      write(not,1030)pl1,pu1,-yl1,-yu1
      return

   20 if(itt.eq.1) go to 30
      write(not,1010)
      call cmprin(igfx,rhom,ngfc,ngfc,ngfc,1,0)
      write(not,1020)
      call cmprin(igfx,pro,ngfc,ngfc,ngfc,2,0)

   30 call dnormi(yl2,pl2,ier)
      call dnormi(yu2,pu2,ier)
      write(not,1040) pl2,pu2,-yl2,-yu2
      return

   40 if(itt.eq.1) go to 50
      write(not,1010)
      call cmprin(igfx,rhom,ngfc,ngfc,ngfc,1,0)

   50 write(not,1050)
      call cmprin(igfx,prbr,ngfc,ngfc,ngfc,2,0)
      call dnormi(yl3,pl3,ier)
      call dnormi(yu3,pu3,ier)
      write(not,1060) pl3,pu3,-yl3,-yu3
      return

   60 continue
      if(itt.eq.1) go to 70
      write(not,1010)
      call cmprin(igfx,rhom,ngfc,ngfc,ngfc,1,0)
      write(not,1020)
      call cmprin(igfx,pro,ngfc,ngfc,ngfc,2,0)

   70 write(not,1050)
      call cmprin(igfx,prbr,ngfc,ngfc,ngfc,2,0)
      call dnormi(yl1,pl1,ier)
      call dnormi(yu1,pu1,ier)
      call dnormi(yl2,pl2,ier)
      call dnormi(yu2,pu2,ier)
      call dnormi(yl3,pl3,ier)
      call dnormi(yu3,pu3,ier)
      write(not,1030) pl1,pu1,-yl1,-yu1
      write(not,1040) pl2,pu2,-yl2,-yu2
      write(not,1060) pl3,pu3,-yl3,-yu3

!     Formats

 1010 format(/' modal correlation coefficient matrix')
 1020 format(/' lower\\upper bound joint-modal probabilities')
 1030 format(/
     &' unimodal bounds:',24x,'lower bound',7x,'upper bound'/
     &'   probability ......................',1pe15.5,' , ',1pe15.5/
     &'   generalized reliability index.....',1pe15.5,' , ',1pe15.5)
 1040 format(/
     &' relax bimodal bounds:',19x,'lower bound',7x,'upper bound'/
     &'   probability ......................',1pe15.5,' , ',1pe15.5/
     &'   generalized reliability index.....',1pe15.5,' , ',1pe15.5)
 1050 format(5x/1x,'joint-modal probabilities by numerical',
     &             ' integration')
 1060 format(/
     &' bimodal bounds:',25x,'lower bound',7x,'upper bound'/
     &'   probability ......................',1pe15.5,' , ',1pe15.5/
     &'   generalized reliability index.....',1pe15.5,' , ',1pe15.5)

      end
