      subroutine project1(y,cosa,ystar,nry,icont)

      implicit   none

      integer (kind=4) :: nry, icont
      real    (kind=8) :: y(nry),cosa(nry,nry-1),ystar(nry,nry-1)

      integer (kind=4) :: i,j,k
      real    (kind=8) :: cdot, ysum

      ysum = cdot(y,ystar(1,1),1,1,nry)/cdot(ystar(1,1),
     &                                       ystar(1,1),1,1,nry)
      do i = 1,nry
        y(i) = y(i)-ysum*ystar(i,1)
      end do ! i
      if(icont.eq.1) return
      do i = 1,icont-1
         ysum = 0.d0
         do k = 1,nry
           ysum = ysum+cosa(k,i)*y(k)
         end do ! k
         do j = 1,nry
           y(j) = y(j)-ysum*cosa(j,i)
         end do ! j
      end do ! i

      end subroutine project1
