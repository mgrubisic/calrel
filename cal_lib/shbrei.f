ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine shbrei(n,a,bt,pf,d)
c
c   functions:
c     use hohenbichler asymptotic formula to calculate failure probability.
c     this program is used in the case of determine form of curvature matrix.
c
c   input arguments:
c     a   : curvature matrix.
c     n   : =nry-1.
c     bt  : reliability index.
c     d   : working matrix.
c
c   output argument:
c     pf  : failure probability.
c
c   calls: dnorm,sudecp.
c
c   called by: socf
c
c   last revision: jan. 19, 1987 by h.-z. lin.
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine shbrei(n,a,bt,pf,d)

      implicit   none

      integer (kind=4) :: n
      real    (kind=8) :: bt,pf
      real    (kind=8) :: a(n,n),d(n,n)

      integer (kind=4) :: i,j, n1
      real    (kind=8) :: b,pai,p1,p2
      real    (kind=8) :: c,p,r,rc

      b = abs(bt)
      pai = acos(-1.d0)
      p1 = exp(-bt*bt/2.d0)/sqrt(2.d0*pai)
      call dnorm(b,p)
      p2 = 1.d0-p
      p1 = p1/p2
      if(bt.le.0.d0) then
        rc = -1.d0
      else
        rc = 1.d0
      endif
      r = 2.d0*p1*rc
      n1 = n
      do i = 1,n1
        do j = 1,n1
          d(i,j) = a(i,j)*r
        end do ! j
        d(i,i) = d(i,i) + 1.d0
      end do ! i
      call sudecp(d,n1,n1,n1,n)
      c = 1.d0
      do i = 1,n1
        c = c*d(i,i)
      end do ! i
      c = 1.d0/sqrt(c)
      pf = (1.d0-p)*c
      if(bt.le.0.d0) then
        pf = 1.d0 - pf
      endif

      end subroutine shbrei
