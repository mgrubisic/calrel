      subroutine  rddata(i,xx,nn)

      implicit    none

      include    'line.h'

      integer (kind=4) :: i,nn
      real    (kind=8) :: xx

      character        :: blk*1,comma*1,i0*1,dot*1,neg*1,eqs*1
      integer (kind=4) :: is
      real    (kind=8) :: y, xn

      save

      data blk/' '/,comma/','/,i0/'0'/,dot/'.'/
     .    ,neg/'-'/,eqs/'='/

c-----convert string to real floating point number --
      if(line(i+1).eq.eqs) i=i+1
      y = 0.0d0
      is = 1
      xx = 0.0d0
      if(line(i+1).ne.neg) go to 267
      is = -1
      i = i + 1
  267 if(line(i+1).ne.blk) go to 270
      i = i + 1
      if (i.gt.llen) go to 300
      go to 267
  270 i = i + 1
      if (i.gt.llen) go to 300
      if ((line(i).eq.blk).and.(line(i+1).eq.blk)) go to 270
      nn = ichar( line(i) ) - ichar( i0 )
      xn = isign(nn,is)
      if (line(i).ne.dot) go to 275
      y = 1.0
      go to 270
  275 if (line(i).eq.blk) go to 300
      if (line(i).eq.comma) go to 300
      if ((nn.lt.0).or.(nn.gt.9)) go to 300
      if (y.eq.0) go to 280
      y = y/10.
      xn = xn*y
      xx = xx + xn
      go to 270
  280 xx = 10.*xx + xn
      go to 270
  300 return

      end subroutine  rddata
