cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine inline(par,x0,igt,ids,ipa,xn,nv)

c   functions:
c      input the distribution information of a variable.

c   output arguments:
c      par : distribution parameters of x.
c      x0 : initial values of x.
c      igt : group type.
c      ids : distribution numbers of x.
c      ipa : #'s of the associated rv of distribution parameters.
c             n-th element of ipa  =  0 means parameter n is constant.
c      xn : variable name.
c      nv : variable number.

c   calls: infree, cerror.

c   called by: input.

c   last revision: june 21 1989 by h.-z. lin.

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine inline(par,x0,igt,ids,ipa,xn,nv)

      implicit   none

      include   'file.h'
      include   'prob.h'

      integer (kind=4) :: nv, ids,igt
      real    (kind=8) :: x0
      real    (kind=8) :: par(4)
      integer (kind=4) :: ipa(4),is(8),ie(8)

      character (len=4 ) :: xn(nrx)
      character (len=80) :: a
      character (len=1 ) :: sep

      integer (kind=4) :: i

      save

      sep = ','
      read(nin,'(a)',end = 100) a
      do i = 1,7
        is(i) = 5
        ie(i) = 4
      end do ! i
      do 20 i = 1,7
      call isep(a,is(i),ie(i),sep)
      is(i+1) = max0(is(i),ie(i))+1
      if(is(i+1).gt.80) go to 30
   20 continue
   30 do 40 i = 1,4
      par(i) = 0.
   40 ipa(i) = 0
      read(a(is(1):ie(1)),'(i80)',err = 110) nv
      ids = 0
      if(is(2).le.ie(2)) read(a(is(2):ie(2)),'(i80)',err = 110) ids
      if(igt.ge.3) then
        do 70 i = 3,6
          if(is(i).le.ie(i)) then
            read(a(is(i):ie(i)),'(i80)',err = 60) ipa(i-2)
          endif
          go to 70
   60     ipa(i-2)  =  0
          if(is(i).le.ie(i)) read(a(is(i):ie(i)),'(f80.0)') par(i-2)
   70   continue
      else
        do 80 i = 3,6
        if(is(i).le.ie(i)) read(a(is(i):ie(i)),'(f80.0)',err = 110)
     *                          par(i-2)
   80   continue
      endif
      x0 = 0.d0
      if(is(7).le.ie(7)) read(a(is(7):ie(7)),'(f80.0)',err = 110) x0
      xn(nv) = a(1:4)
      if(ids.lt.0) then
        ipa(1) = -ipa(1)
        ipa(2) = -ipa(2)
        ipa(3) = -ipa(3)
        ipa(4) = -ipa(4)
      endif
      return
  100 call cerror(12,0,0.,' ')
  110 call cerror(15,0,0.,a)

      end subroutine inline
