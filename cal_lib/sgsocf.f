cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine sgsocf(ex,sg,par,bnd,tp,tf,igt,lgp,ids,ipa,ib,izx,
c                      ixz,beta,alpha,x,y,a,gam,t1,t2,t3,ist)
c
c   functions:
c      control the flow of solution procedures to compute first order
c      failure probability and relability index.
c
c   input arguments:
c      ex : mean vector of x.
c      sg : standard deviations of x.
c      par : distribution parameters of x.
c      bnd : lower and upper bound of x.
c      tp : deterministic parameters in performance function.
c      tf : the transformation matrix from z to y, i.e. y = tf * z.
c      igt : group types.
c      lgp : location of the first element of a group in y.
c      ids : distribution numbers of x.
c      ipa : #'s of the associated rv of distribution parameters.
c             n-th element of ipa = 0 means parameter n is constant.
c      ib : code of distribution bounds of x.
c           = 0 , no bounds
c           = 1 , bounded from below.
c           = 2 , bounded from above.
c           = 3 , both sides bounded.
c      izx : indicators for z to x.
c      ixz : indicators for x to z.
c      ist : restart flag.
c            ist = 1 , restart problem.
c      igf : for icl=1, perform form for performance function igf.
c
c   output arguments:
c      beta : reliability indices for all failure modes.
c      alpha : unit normal in y space for all failure modes.
c      adgx  : dgx in x space for all failure modes
c      x : basic random variables in the original space.
c      y : basic variables in the standard space.
c      a : unit normal vector in the y space.
c      gam : unit normal vector in the z space.
c      t1 : temporary array.
c      t2 : temporary array.
c      t3 : temporary array.
c
c   calls: fdirct, fxtoy, fnewy1, fnewy2, fnewy3, fcheck, fsenvd.
c
c   called by: csocf
c
c   last version: dec. 15, 1990 by hong-zong lin
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine sgsocf(tf,alpha,sg,ex,par,bnd,ib,tp,ids,t1,t2,t3,
     *          curv,beta,igt,lgp,izx,ipa,x,igfx,ixz,y,a,gam,yo,ao,
     *          cosa,ystar,yoo,yooo,yoooo,yses,ases,xses,
     *          bbeta,istep,ylin,gdgx,igss,a1,y1,sed,
     *          ist,kreq,see)

      implicit   none

      include   'blkrel1.h'
      include   'bsocf.h'
      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'fsav.h'
      include   'optm.h'
      include   'prob.h'
      include   'test.h'

c     parameter (net=50,nrt=3,npt=33,npf=18)
c     real tarray(2)

      integer (kind=4) :: ist,kreq
      real    (kind=8) :: see
      integer (kind=4) :: ids(nrx),ipa(4,nrx),igfx(ngf,3),ib(nrx)
      integer (kind=4) :: igt(nig),lgp(3,nig),izx(nry),ixz(nrx)
      integer (kind=4) :: igss(ngf)
      real    (kind=8) :: tf(*),x(nrx),a(nry)
      real    (kind=8) :: y(nry),t1(nry),t2(nry),t3(nrx),gam(nry),tp(*)
      real    (kind=8) :: ex(nrx),sg(nrx),par(4,nrx),bnd(2,nrx)
      real    (kind=8) :: alpha(nry,ngf),gdgx(nrx)
      real    (kind=8) :: beta(ngf),ylin(nry,ngf)
c     dimension xxt(4),yyt(4)
c***
      integer (kind=4) :: istep(nry-1,ngf)
      real    (kind=8) :: yo(nry,ngf),ao(nry,ngf),cosa(nry,nry-1,ngf)
      real    (kind=8) :: ystar(nry,nry-1,ngf),sed(ngf)
      real    (kind=8) :: yoo(nry,ngf),yooo(nry,ngf),yoooo(nry,ngf)
      real    (kind=8) :: yses(nry),a1(nry,ngf),y1(nry,ngf)
      real    (kind=8) :: ases(nry),xses(nrx),bbeta(nry,ngf)
      real    (kind=8) :: curv(nry-1,ngf)

      integer (kind=4) :: i, igk, ik,kk, icont
      integer (kind=4) :: ig2, ig4, ier
      integer (kind=4) :: nck, nses, ncur, nbk
      real    (kind=8) :: bta1,bta2, bdp, beff, bbe
      real    (kind=8) :: palfa, pf1,pf2,pf3,pf4
      real    (kind=8) :: sdgy,seps,sydp,sbbeta, seed
      save
c     ****************************************************************
c     values for epc, kreq
c     ****************************************************************
c*
      addk=0
c
c---  define b (approx. hessian) and q (gradient) of lagrangian
c---  solution phase.
      if(ist.eq.0) then
      do 1100 i=1,ngf
 1100 sed(i)=see
      endif
      do 100 igk=1,ngfc
      icont=1
c     ncount=0
      nbk=0
      ig4=igfx(igk,3)
      ig2=igfx(igk,2)
      if(ist.ne.0) then
      if(ig4.ne.igf) go to 100
      icont=igss(ig2)
      endif
      seed=sed(ig2)
      iter=istep(icont,ig2)
      do 2015 ik=1,nry
      y(ik)=y1(ik,ig2)
 2015 a(ik)=a1(ik,ig2)
      bdp=beta(ig2)
      write(not,2000) ig4
      write(not,2010)
      write (not,9438)
c---------------------------------------------------------------------
c
c     ***************************************************************
c     ***************************************************************
c     computation of additional curvatures
c     ***************************************************************
c     ***************************************************************
c
c
c
 4010 nck=0
      nses=0
c     *************** copy y design to y star ***********************
      do 4025 i=1,nry
 4025 ystar(i,icont,ig2)=ylin(i,ig2)
      if (icont.ge.kreq) then
      istep(kreq,ig2)=iter
      do 6346 ik=1,igss(ig2)
 6346 write (not,9428)ik,curv(ik,ig2),istep(ik,ig2)
      if(ist.ne.0) write(not,9427)
 9427 format(2x,32('-'))
      do 6347 ik=igss(ig2)+1,kreq
 6347 write (not,9428)ik,curv(ik,ig2),istep(ik,ig2)
 9428 format(i5,3x,1p,e11.3,8x,i5)
      igss(ig2)=icont
      do 5727 i=1,kreq
 5727 gdgx(i)=curv(i,ig2)/2.d0
      write(not,2030)
      call sphbre(kreq,gdgx,bdp,pf1)
      pf3=1.d0-pf1
      call dnormi(bta1,pf3,ier)
      call sptvee(kreq,gdgx,bdp,pf2)
      write(not,2030)
      pf4=1.d0-pf2
      call dnormi(bta2,pf4,ier)
      if(bta1.le.-1.5.or.bta2.le.-1.5) then
      write(not,2045) bta1,bta2,pf3,pf4
      else
      write(not,2040) bta1,bta2,pf1,pf2
      endif
      go to 105
      endif
c     ******* detect case beta*cur less than epb   ******************
      if(bdp*dabs(curv(icont,ig2)).lt.epb) then
      istep(icont,ig2)=iter
      do 1846 i=1,igss(ig2)
 1846 write (not,9428) i,curv(i,ig2),istep(i,ig2)
      if(ist.ne.0) write(not,9427)
      do 1845 i=igss(ig2)+1,icont
 1845 write (not,9428) i,curv(i,ig2),istep(i,ig2)
      igss(ig2)=icont
      do 5725 i=1,icont
 5725 gdgx(i)=curv(i,ig2)/2.d0
      write(not,2030)
      call sphbre(icont,gdgx,bdp,pf1)
      pf3=1.d0-pf1
      call dnormi(bta1,pf3,ier)
      call sptvee(icont,gdgx,bdp,pf2)
      pf4=1.d0-pf2
      call dnormi(bta2,pf4,ier)
      if(bta1.le.-1.5.or.bta2.le.-1.5) then
      write(not,2045) bta1,bta2,pf3,pf4
      else
      write(not,2040) bta1,bta2,pf1,pf2
      endif
      go to 105
      endif
c     *************compute cosines of orthogonal plane **************
      call fcosine (yooo(1,ig2),yoo(1,ig2),
     * yo(1,ig2),y,cosa(1,1,ig2),yses,t1,t2,icont,nses)
c    **************** compute curvature in case of seesaw behaviour
      if (nses.eq.1) then
      call cytox(xses,yses,t1,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call ugfun(gx,xses,tp,ig4)
      call fdirct(xses,yses,tp,ex,sg,t3,gam,ases,tf,par,bnd,igt,lgp,ids,
     *            ipa,izx,ixz,sdgy,gdgx,ig4,0)
      call fcurvat(yses,ases,y,a,curv(icont,ig2),ig4,tp,ncur)
      endif
      istep(icont,ig2)=iter
c     write (not,5003)
c     write (not,5004) icont,iter,curv(icont,ig2)
      icont=icont+1
      iter=istep(icont,ig2)
c     ************** find new starting point ***********************
 1530 call fstart(y,ylin(1,ig2),epc,cosa(1,1,ig2),ystar(1,1,ig2),
     *            nry,icont,alpha(1,ig2),see)
      call cytox(x,y,t1,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
c---  compute dg/dy and unit normal vectors in y space.

      call ugfun(gx,x,tp,ig4)
      call fdirct(x,y,tp,ex,sg,t3,gam,a,tf,par,bnd,igt,lgp,ids,
     *            ipa,izx,ixz,sdgy,gdgx,ig4,0)
c5003 format('  curv. #        iter. #              curvature  ')
 3000 continue
c     *********** speed up convergence *****************************
      if (iter.eq.2) then
      seps=0.d0
      do 1943 i=1,nry
 1943 seps=seps+(y(i)-ylin(i,ig2))**2
      seps=dsqrt(seps)
      endif
      if (iter.eq.3) then
      sydp=0.d0
      bbe=0.d0
      do 2765 i=1,nry
      bbe=bbe+bbeta(i,ig2)*alpha(i,ig2)
 2765 sydp=sydp+(ylin(i,ig2)-y(i))**2
      sydp=dsqrt(sydp)
      palfa=sydp/seps
      sbbeta=0.d0
      do 4658 i=1,nry
      bbeta(i,ig2)=(epa/palfa*bbe-(epa/palfa-1.d0)*bdp)*alpha(i,ig2)
 4658 sbbeta=sbbeta+bbeta(i,ig2)*bbeta(i,ig2)
      sbbeta=dsqrt(sbbeta)
      bbe=(epa/palfa*bbe-(epa/palfa-1.d0)*bdp)
c     write (not,3054)
c3054 format (/,'  value of b (translation of origin) ' )
c     write (not,*) bbe
      iter=iter+1
      goto 1530
      endif
      iter=iter+1
c     ****** storage of y and a **************************************
      do 2050 ik=1,nry
      yoooo(ik,ig2)=yooo(ik,ig2)
      yooo(ik,ig2)=yoo(ik,ig2)
      yoo(ik,ig2)=yo(ik,ig2)
      yo(ik,ig2)=y(ik)
 2050 ao(ik,ig2)=a(ik)
c     ************ find new y and new x ******************************
      if (nbk.eq.1.and.(iop.eq.2.or.iop.eq.5)) then
      call fnew1n(x,y,t1,a,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,
     *            izx,sdgy,beta(ig2),gx,bbeta(1,ig2))
      go to 260
      endif
      if (nbk.eq.1.and.iop.eq.3) then
      call fnew3n(x,y,a,t1,t2,t3,gam,tf,ex,sg,par,bnd,tp,igt,lgp,
     *ids,ipa,ib,izx,ixz,sdgy,beta(ig2),iter,gx,ig4,gdgx,
     *bbeta(1,ig2))
      go to 260
      endif
      go to (151,152,153,154,154), iop
 151  call fnewy1(x,y,t1,a,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,
     *            izx,sdgy,beta(ig2),gx)
      go to 260
  152 call fnewy2(x,y,a,t1,t2,t3,gam,tf,ex,sg,par,bnd,tp,igt,lgp,
     *ids,ipa,ib,izx,ixz,sdgy,beta(ig2),iter,gx,ig4,
     *gdgx,des0)
      go to 260
  153 call fnewy3(x,y,a,t1,t2,t3,gam,tf,ex,sg,par,bnd,tp,igt,lgp,
     *ids,ipa,ib,izx,ixz,sdgy,beta(ig2),iter,gx,ig4,gdgx)
      go to 260
  154 call fnewy5(x,y,a,t1,t2,t3,gam,tf,ex,sg,par,bnd,tp,igt,lgp,
     *   ids,ipa,ib,izx,ixz,sdgy,beta(ig2),iter,gx,ig4,gdgx)
c     ************  project new y point *******************************
 260  call project(y,cosa(1,1,ig2),ystar(1,1,ig2),nry,icont)
      call cytox(x,y,t1,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
c
c---  compute dg/dy, unit normal vectors in y space.

c     if(iop.eq.1) then
      call ugfun(gx,x,tp,ig4)
      call fdirct(x,y,tp,ex,sg,t3,gam,a,tf,par,bnd,igt,lgp,
     *          ids,ipa,izx,ixz,sdgy,gdgx,ig4,0)
c     endif
c     *******************  compute curvatures ************************
      call fcurvat(yo(1,ig2),ao(1,ig2),y,a,curv(icont,ig2),
     *             ig4,tp,ncur)
c     write (not,5003)
c     write (not,5004) icont,iter,curv(icont,ig2)
c5004 format (5x,i4,11x,i4,20x,f15.10)
      if (ncur.eq.1) then
      nck=1
      goto 1208
      endif
      if (iter.gt.3) then
      bbe=0.d0
      do 9673 i=1,nry
 9673 bbe=bbe+bbeta(i,ig2)*alpha(i,ig2)
      beff=bdp-bbe
c     write (not,8329) beff*dabs(curv(icont,ig2))
      if (dabs(curv(icont,ig2)).lt.(10.d-10)) goto 1208
c8329 format ( ' value of beff*cur = ',f20.10)
      bbe=bdp-epa/dabs(curv(icont,ig2))
      do 9704 i=1,nry
 9704 bbeta(i,ig2)=bbe*alpha(i,ig2)
      endif
c
c---  check convergence.
c
      call fchekn(y,yo(1,ig2),a,gx,iter,nck,ig4)
      if(nck.eq.0) go to 3000
      if(iter.le.3) then
      nck=0
      goto 3000
      endif
 1208 continue
c---------------------------------------------------------------------
c     end of iteration
c--------------------------------------------------------------------
      go to 4010
  105 beta(ig2)=bdp
      do 554 kk=1,nry
      y1(kk,ig2)=y(kk)
 554  a1(kk,ig2)=a(kk)
      sed(ig2)=seed
  100 continue
 9438 format (/,2x,' Principal curvature        # steps ' )
 2000 format(/' Limit-state function ',i5)
 2010 format(1x,77('-'))
 2030 format(/39x,'Improved Breitung',6x,7hTvedt's,' EI')
 2040 format(' Generalized reliability index betag =',5x,f9.6,11x,f9.6,
     & /,' probability                     Pf2 =',1pe14.7,6x,1pe14.7)
 2045 format(' Generalized reliability index betag =',5x,f9.6,11x,f9.6,
     & /,' probability                     Pf2 =',' 1- ',1pe14.7,6x,
     & ' 1- ',1pe14.7)
      write(not,2010)

      end subroutine sgsocf
