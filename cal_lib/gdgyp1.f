      subroutine gdgyp1(dgyp,t,tf,izx,nges,nge,j0,indx)

      implicit   none

      include   'prob.h'

      integer (kind=4) :: nges,nge, j0, indx
      real    (kind=8) :: dgyp(nry,4,nrx),t(6,nge),tf(nge,nge)
      integer (kind=4) :: izx(nges)

      integer (kind=4) :: i,j,k, iz, kk, ne,ns

      real    (kind=8) :: cdot

        if(indx.eq.0) then
        ns=1
        ne=nges
        else
        ns=nge-nges+1
        ne=nge
        endif
      do 10 i=ns,ne
      iz=izx(i)
c     print *,'dgyp(',iz,')='
      do 20 j=1,4
      dgyp(j0+i-1,j,iz)=t(j,i)/tf(i,i)
      do 20 k=i-1,1,-1
      kk=j0+k-1
      dgyp(kk,j,iz)=-cdot(dgyp(kk+1,j,iz),tf(k+1,k),1,1,i-k)/tf(k,k)
 20   continue
c     call mprint(dgyp(1,1,iz),nry,4,1)
 10   continue

      end subroutine gdgyp1
