c - -----------------------------------------------------------------
c      subroutine cbsolx(a,b,m,n,mm)
c
c   functions:
c      Solve constant-band simultaneous equations.
c
c   input arguments:
c      a : symmetric banded matrix.
c      b : right-hand-side vector
c      m : band width of a.
c      n : dimension of a.
c      mm  : declared row number of a in the calling routine.
c
c   output arguments:
c      b : solution of ax = b
c
c   calls: cerror.
c
c   called by: fnewy4.
c
c   last revision: sept. 18 1986 by p. - l. liu.
c
c - -----------------------------------------------------------------
      subroutine cbsolx(a,b,m,n,mm)

      implicit   none

      include   'file.h'

      integer    m,n,mm
      real*8     a(mm,*),b(*)

      integer    i,j,k, ia, j1,ja,jm,jm1, n2
      real*8     aij

c---  Decomposition a = l*d*u

      ja = 1
      do j = 1,n
        j1  = j - 1
        jm  = max(j-m,1)
        jm1 = jm + 1
        ia  = jm + 1
        do i = jm1,j1
          do k = jm,i-1
            a(i,ja) = a(i,ja) - a(k,ia)*a(k,ja)
          end do ! k
          ia = ia + 1
        end do ! i
        ia = jm
        do i = jm,j1
          aij     = a(i,ja)/a(i,ia)
          a(j,ja) = a(j,ja) - aij*a(i,ja)
          a(i,ja) = aij
          ia      = ia + 1
        end do ! i
        if(a(j,ja).eq.0.0d0) then
          write(not,2000)
          call cstop
          return
        endif
        ja = ja + 1
      end do ! j

c---  Forward substitution

      do i = 1,n
        do k = max(i-m,1),i-1
          b(i) = b(i) - a(k,i)*b(k)
        end do ! k
      end do ! i

c---  Divide by diagonal

      do i = 1,n
        b(i) = b(i)/a(i,i)
      end do ! i

c---  Backsubstitution

      ia = n + 1
      n2 = n + 2
      do j = 2,n
        i  = n2 - j
        ia = ia - 1
        do k = max(i-m,1),i-1
          b(k) = b(k) - a(k,ia)*b(i)
        end do ! k
      end do ! j

c---  Format

 2000 format(' ERROR:',
     &       ' The quasi-hessian matrix is not positive definite.')

      end subroutine cbsolx
