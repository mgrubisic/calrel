      subroutine csent(isv)

      implicit   none

      include   'blkchr.h'
      include   'blkrel1.h'
      include   'bond.h'
      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'line.h'
      include   'prob.h'

      integer (kind=4) :: isv
      integer (kind=4) :: nr,nc
      integer (kind=4) :: n0 , n1, n2, n3, n4, n5, n6, n7, n8, n9
      integer (kind=4) :: n10,n11,n12,n13,n14,n15,n16,n17,n18,n19
      integer (kind=4) :: n20,n22,n23,n26,n28,n29
      integer (kind=4) :: n30,n31,n32,n33,n35
      integer (kind=4) :: n44,n45,n46,n47,n48,n49
      integer (kind=4) :: n50,n51,n52

c     real tarray(2)

      save

      call locate('xn  ',n0,nr,nc)
      call locate('xlin',n1,nr,nc)
      call locate('beta',n2,nr,nc)
      call locate('sdgy',n3,nr,nc)
      call locate('tp  ',n4,nr,nc)
      call locate('ex  ',n5,nr,nc)
      call locate('sg  ',n6,nr,nc)
      call locate('par ',n7,nr,nc)
      call locate('igt ',n8,nr,nc)
      call locate('lgp ',n9,nr,nc)
      call locate('ipa ',n10,nr,nc)
      call locate('izx ',n11,nr,nc)
      call locate('ixz ',n12,nr,nc)
      call locate('ids ',n13,nr,nc)
      call locate('tf  ',n14,nr,nc)
      call locate('alph',n15,nr,nc)
      call locate('next',n16,nr,nc)
      call locate('prob',n17,nr,nc)
      call locate('prbr',n18,nr,nc)
      call locate('rhom',n19,nr,nc)
      call locate('ibx ',n20,nr,nc)
c     call locate('ib  ',n21,nr,nc)
      call define('t   ',n22,6,nrx)
      call define('tj  ',n23,4,nrx)
      call define('ddpi',n26,nry,4*nrx*ngfc)
      call define('drip',n28,4,nrx)
      call define('stp ',n29,4,nrx)
      call define('sdp ',n30,ntp,1)
      call define('ti  ',n31,4,nrx)
      call define('bdp ',n32,ntp,ngfc)
      call define('dadi',n33,nry,ntp*ngfc)
      call define('drid',n35,ntp,1)
      call locate('bnd ',n44,nr,nc)
      call locate('ilub',n45,nr,nc)
      call define('stp1',n46,4,nrx)
      call define('sdp1',n47,ntp,1)
      call locate('fltf',n48,nr,nc)
      call locate('sdxz',n49,nr,nc)
      call locate('stf ',n50,nr,nc)
      call defini('igg ',n51,ngfc*2,1)
      call locate('igfx',n52,nr,nc)
c     compute the system sensitivity for unimodal bounds.
      call gunimo(nd(1),ia(n1),ia(n2),ia(n3),ia(n4),ia(n13),ia(n22),
     *        ia(n29),ia(n30),ia(n32),ia(n46),ia(n47),ia(n48),ia(n10),
     *        ia(n12),ia(n52),isv)
c     run=etime(tarray)
c     print *,'run time=',run
      if(ibt.eq.0) go to 200
        go to (100,200,300) ibt
  100 go to 40
c
c     compute system sensitivity for relax bimodal bounds
  200 call grbimo(nd(1),ia(n1),ia(n2),ia(n3),ia(n4),ia(n5),ia(n6),
     *ia(n7),ia(n8),ia(n9),ia(n10),ia(n11),ia(n12),ia(n13),ia(n14),
     *ia(n15),ia(n16),ia(n17),ia(n19),ia(n20),ia(n22),ia(n23),
     *ia(n26),ia(n28),ia(n31),ia(n32),ia(n33),ia(n35),
     *ia(n44),ia(n45),ia(n46),ia(n47),ia(n49),ia(n50),ia(n51),
     *ia(n52),isv)
c     run=etime(tarray)
c     print *,'run time=',run
  300 if(ibt.eq.2) go to 40
      call gsens(nd(1),ia(n1),ia(n2),ia(n3),ia(n4),ia(n5),ia(n6),
     *ia(n7),ia(n8),ia(n9),ia(n10),ia(n11),ia(n12),ia(n13),ia(n14),
     *ia(n15),ia(n16),ia(n18),ia(n19),ia(n20),ia(n22),ia(n23),
     *ia(n26),ia(n28),ia(n29),ia(n30),ia(n31),ia(n32),ia(n33),ia(n35),
     *ia(n44),ia(n49),ia(n50),ia(n51),ia(n52),isv)
c     run=etime(tarray)
c     print *,'run time=',run
 40   call delete('t   ')
      call delete('ti  ')
      call delete('tj  ')
      call delete('ddpi')
      call delete('drip')
      call delete('stp ')
      call delete('sdp ')
      call delete('bdp ')
      call delete('dadi')
      call delete('drid')
      call delete('y   ')
      call delete('ilub')
      call delete('ibx ')
c     call delete('prob')
      call delete('prbr')
      call delete('stp1')
      call delete('sdp1')
c     call delete('sdxz')
c     call delete('stf ')
      call delete('igg ')
      return
      end subroutine csent
