cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine cbound(x,bnd,ib)
c
c   functions:
c      check if x falls into the singular domain. if it is,
c      bring it back to its sample space.
c
c   input arguments:
c      x : x computed by inverse transformation from y.
c      ib : code of distribution bounds of x.
c            =  0 , no bounds
c            =  1 , bounded from below.
c            =  2 , bounded from above.
c            =  3 , both sides bounded.
c      bnd : lower and upper bound of x.
c
c   output arguments:
c      x : updated x.
c
c   called by: cztox.
c
c   last revision: mar. 24 1987 by p.-l. liu.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cbound(x,bnd,ib)

      implicit   none

      integer    ib
      real*8     x
      real*8     bnd(2)

      go to (60,30,40,50) ib+1

   30 if(x.lt.bnd(1)) x = bnd(1)
      return

   40 if(x.gt.bnd(2)) x = bnd(2)
      return

   50 if(x.lt.bnd(1)) then
         x = bnd(1)
      else if(x.gt.bnd(2)) then
         x = bnd(2)
      end if

   60 return

      end subroutine cbound
