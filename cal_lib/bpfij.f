!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!     subroutine bpfij(pij,bti,btj,blij)

!   functions:
!     use gaussian quadrature formula to calculate failure probability pfij

!   input arguments:
!     bti : reliability index of transition state i.
!     btj : reliability index of transition state j.
!     blij :cos vij.

!   output:
!     pij : failure probability pfij.

!     called by : bounds

!     calls: dnorm.

!        revision: july 20, 1989 by h.-z. lin.
!   last revision: feb  07, 1995 by c.-c. li
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine bpfij(pij,bti,btj,blij)

      implicit   none

!-----Gauss quadrature method .
!     Use 10 points Gauss quadrature

      real    (kind=8) :: pij,bti,btj,blij

      integer (kind=4) :: i
      real    (kind=8) :: a1,a2,aa,bb, p1,pai,pfi,pfj, s6, theta,theta1
      real    (kind=8) :: x(10),w(10)

!------ x(i) : abscissas of  quadrature

      dat    a (x(i),i = 1,10) /
     1 -0.97390652851717172d0 ,
     2 -0.86506336668898451d0 ,
     3 -0.67940956829902440d0 ,
     4 -0.43339539412924719d0 ,
     5 -0.14887433898163121d0 ,
     6  0.14887433898163121d0 ,
     7  0.43339539412924719d0 ,
     8  0.67940956829902440d0 ,
     9  0.86506336668898451d0 ,
     *  0.97390652851717172d0 /

!----- w(i) : weights  quadrature

      data         (w(i),i = 1,10) /
     1   0.06667134430868813759d0  ,
     2   0.14945134915058059314d0  ,
     3   0.21908636251598204399d0  ,
     4   0.26926671930999635509d0  ,
     5   0.29552422471475287017d0  ,
     6   0.29552422471475287017d0  ,
     7   0.26926671930999635509d0  ,
     8   0.21908636251598204399d0  ,
     9   0.14945134915058059314d0  ,
     *   0.06667134430868813759d0  /

      pai = acos(-1.0d0)
      if(blij.lt.-1.0d0) then
         print *,'warning: in subroutine bpfij, blij < -1.0'
         print *,'blij+1 = ',blij+1
         blij = -1.0d0
      endif
      if(blij.gt.1.0d0) then
        print *,'WARNING: In subroutine bpfij, blij > 1.0'
        print *,'blij-1 = ',blij-1
        blij = 1.0d0
      endif
      p1  = asin(blij)
      pij = 0.0d0
      if(bti*btj.gt.0.0d0) then
        aa = (bti - btj)**2
        bb = 4.0d0*bti*btj
        do i = 1,10
          theta = (x(i) + 1.0d0)*p1*0.5d0
          theta1 = theta*0.5d0 - pai*0.25d0  ! atan(1.0d0)
          s6 = 0.5d0/pai*exp(-0.5d0*(aa+bb*sin(theta1)**2)
     &        /cos(theta)**2)
          pij = pij + s6*w(i)
        end do ! i
      else
        aa = (bti+btj)**2
        bb = -4.0d0*bti*btj
        do i = 1,10
          theta  = (x(i) + 1.0d0)*p1*0.5d0
          theta1 = theta*0.5d0 - atan(1.0d0)
          s6 = 0.5d0/pai*exp(-0.5d0*(aa+bb*cos(theta1)**2)
     &       /cos(theta)**2)
          pij = pij + s6*w(i)
        end do ! i
      endif
      a1 = -bti
      a2 = -btj
      call dnorm(a1,pfi)
      call dnorm(a2,pfj)

      pij = max(0.0d0,pij*p1*0.5d0 + pfi*pfj)

      end
