      subroutine gunimo(xn,xlin,beta,sdgy,tp,ids,t,stp,sdp,bdp,stp1,
     *                  sdp1,fltf,ipa,ixz,igfx,isv)

      implicit   none

      include   'bond.h'
      include   'cuts.h'
      include   'file.h'
      include   'prob.h'

      logical fltf(ngf)

      character (len=4) :: xn(nrx)

      integer (kind=4) :: isv
      real    (kind=8) :: xlin(nrx,ngf),t(6,nrx),tp(*)
      real    (kind=8) :: beta(ngf),sdgy(ngf),stp(4,nrx),sdp(ntp)
      real    (kind=8) :: bdp(ntp,ngfc),stp1(4,nrx),sdp1(ntp)
      integer (kind=4) :: ids(nrx),igfx(ngf,3),ipa(4,nrx),ixz(nrx)

      integer (kind=4) :: i,j,k, k2,k4, ier
      integer (kind=4) :: nrec
      real    (kind=8) :: dbtp, gm,gp
      real    (kind=8) :: pb,pbu, r, tpi, yu1 
      save

      do k = 1,nrx
        do j = 1,4
          stp(j,k) = 0.
        end do ! j
      end do ! k
      if(ibt.le.1) then
      write(not,2010)
      call dnormi(yu1,pu1,ier)
      pbu = -0.39894228d0*exp(-yu1*yu1*0.5d0)
      endif
      if(isv.eq.2) go to 110
      do k = 1,ngfc
        k2 = igfx(k,2)
        if(.not.fltf(k2)) call cerror(5,0,0.,' ')
        nrec = (k2-1)*nrx
        do i = 1,nrx
          read(nsv,rec = nrec+i) (t(j,i),j=1,6)
        end do ! i
        pb = -0.39894228d0*exp(-beta(k2)*beta(k2)*0.5d0)
        do j = 1,nrx
          if(ids(j).ne.7.and.ids(j).le.50) then
            do i = 1,4
              stp(i,j) = stp(i,j)+t(i,j)*pb
            end do ! i
          else
            do i = 3,6
              stp(i-2,j) = stp(i-2,j)+t(i,j)*pb
            end do ! i
          end if
        end do ! i
      end do ! k
      do i = 1,nrx
        do j = 1,4
          stp1(j,i) = stp(j,i)
          end do ! j
      end do ! i

!---  perform system sensitvity analysis on tp for the unimodal bounds.
  110 if(ntp.eq.0.or.isv.eq.1) go to 500
      do k = 1,ngfc
         k2 = igfx(k,2)
         k4 = igfx(k,3)
        pb = -0.39894228d0*exp(-beta(k2)*beta(k2)*0.5d0)
        do i = 1,ntp
          r = 0.001d0*tp(i)
          if(r.eq.0.d0) r = 0.001d0
          tpi = tp(i)
          tp(i) = tpi+r
          call ugfun(gp,xlin(1,k2),tp,k4)
         tp(i) = tpi-r
          call ugfun(gm,xlin(1,k2),tp,k4)
          dbtp = (gp-gm)/2.d0/r/sdgy(k2)
         tp(i) = tpi
          bdp(i,k) = dbtp
          sdp(i) = sdp(i)+dbtp*pb
        end do ! i
      end do ! k
      do i = 1,ntp
        sdp1(i) = sdp(i)
      end do ! i
 500  if(ibt.le.1) call goutp(t,pbu,xn,stp,sdp,ids,ipa,ixz,isv)

 2010 format(///' sensitivity measures based on unimodal upper',
     *     ' bound of pf1',/
     * 1x,70('-'))

      end subroutine gunimo
