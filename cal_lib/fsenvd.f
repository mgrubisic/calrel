cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine fsenvd(a,tf,x,y,z,ex,sg,par,tp,det,eta,dgx,
c                        igt,lgp,ids,izx,sdgy,ig)
c
c   functions:
c      perform sensitivity analysis on beta and pf1 with
c      respect to distribution parameters.
c
c   input arguments:
c      tf : the transformation matrix from z to y, i.e. y = tf * z.
c      x : basic random variables in the original space.
c      y : basic variables in the standard space.
c      z : basic variables in the correlated normal space.
c      ex : mean vector of x.
c      sg : standard deviations of x.
c      par : distribution parameters of x.
c      tp : transition function parameters.
c      det : normalized sg*d(beta)/d(ex)
c      eta : normalized sg*d(beta)/d(sg)
c      ids : distribution numbers of x.
c      izx : indicators for z to x.
c
c   calls: cdot, cdzpar.
c
c   called by: form.
c
c   last revision: june 21, 1989 by h.-z. lin
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine fsenvd(a,tf,x,y,z,ex,sg,par,tp,det,eta,dgx,igt,lgp,
     *                  ids,izx,sdgy,ig,ig4)

      implicit   none

      include   'file.h'
      include   'flag.h'
      include   'prob.h'


      integer (kind=4) :: ig,ig4
      real    (kind=8) :: sdgy
      integer (kind=4) :: ids(nrx),igt(nig),lgp(3,nig),izx(nry)
      real    (kind=8) :: tf(ntf),x(nrx),y(nry),z(nry),det(nry),tp(*)
      real    (kind=8) :: eta(nry),ex(nrx),sg(nrx),par(4,nrx),dgx(nrx)
      real    (kind=8) :: db(6),a(nry)

      integer (kind=4) :: i,ii
      integer (kind=4) :: j,j0,j1,j1s,j1s0
      integer (kind=4) :: k
      integer (kind=4) :: nn,nge,nges,nrec

      real    (kind=8) :: t

      real    (kind=8) :: cdot 
      save

c
c---  perform sensitivity analysis on distribution parameters.
c
      nn=1
      do 100 ii=1,nig
        j0=lgp(1,ii)
        if(ii.eq.nig) then
        j1=nry
        else
        j1=lgp(1,ii+1)-1
        endif
        nge=j1-j0+1
        if(nge.le.0) go to 100
c---  for type 1 groups:
        if(igt(ii).eq.1) then
          do 20 i=j0,j1
            j=izx(i)
            t=a(i)
            call cdzpar(x,y(i),ex(j),sg(j),par(1,j),ids(j),db,j)
            do 10 k=1,6
   10         db(k)=db(k)*t
            det(i)=db(1)*sg(j)
            eta(i)=db(2)*sg(j)
            nrec=(ig-1)*nrx+j
            write(nsv,rec=nrec) db
   20       continue
c---  for type 2 groups:
        else if(igt(ii).eq.2) then
          call cytoz(tf(nn),y(j0),z(j0),nge,nge)
          do 40 i=j0,j1
            j=izx(i)
            t=cdot(a(j0),tf(nn+(i-j0)*nge),1,1,nge)
            call cdzpar(x,z(i),ex(j),sg(j),par(1,j),ids(j),db,j)
            do 30 k=1,6
   30         db(k)=db(k)*t
            det(i)=db(1)*sg(j)
            eta(i)=db(2)*sg(j)
            nrec=(ig-1)*nrx+j
            write(nsv,rec=nrec) db
   40       continue
c---  for type 3 groups:
        else if(igt(ii).eq.3) then
      j1s=lgp(2,ii)
      nges=j1s-j0+1
          if(j1s.lt.j0) go to 70
          call cytoz(tf(nn),y(j0),z(j0),nge,nges)
        j1s0=j1s+1
          do 60 i=j0,j1s
            j=izx(i)
        t=cdot(a(i),tf(nn+(i-j0)*nge+i-j0),1,1,j1s0-i)
c           t=cdot(y(j0),tf(nn+(i-j0)*nge),1,1,nges)/bt
            call cdzpar(x,z(i),ex(j),sg(j),par(1,j),ids(j),db,j)
            do 50 k=1,6
   50         db(k)=db(k)*t
            det(i)=db(1)*sg(j)
            eta(i)=db(2)*sg(j)
            nrec=(ig-1)*nrx+j
            write(nsv,rec=nrec) db
   60       continue
   70     if(j1s.eq.j1) go to 150
          do 90 i=j1s+1,j1
            j=izx(i)
            t=a(i)
            call cdzpar(x,y(i),ex(j),sg(j),par(1,j),ids(j),db,j)
            do 80 k=1,6
   80         db(k)=db(k)*t
            det(i)=db(1)*sg(j)
            eta(i)=db(2)*sg(j)
            nrec=(ig-1)*nrx+j
            write(nsv,rec=nrec) db
   90       continue
c       else
c---  type 4 group
c     j1s=lgp(2,ii)
c     nges=j1-j1s+1
c     if(j1s.le.j0) go to 470
c     do 460 i=j0,j1s-1
c     j=izx(i)
c     t=a(i)
c           call cdzpar(x,y(i),ex(j),sg(j),par(1,j),ids(j),db,j)
c           do 480 k=1,6
c 480         db(k)=db(k)*t
c           det(i)=db(1)*sg(j)
c           eta(i)=db(2)*sg(j)
c           nrec=(ig-1)*nrx+j
c           write(nsv,rec=nrec) db
c 460       continue
c 470   if( j1s.gt.j1) go to 100
c       do 490 i=j1s,j1
c           j=izx(i)
c           t=cdot(a(i),tf(nn+(i-j0)*nge+i-j0),1,1,j1-i+1)
c           call cdzpar(x,z(i),ex(j),sg(j),par(1,j),ids(j),db,j)
c           do 450 k=1,6
c 450         db(k)=db(k)*t
c           det(i)=db(1)*sg(j)
c           eta(i)=db(2)*sg(j)
c           nrec=(ig-1)*nrx+j
c           write(nsv,rec=nrec) db
c 490       continue
        endif
  150   nn=nn+nge*nge
  100   continue
c---  compute sensitivity measures for non-random variables.
      if(nrx.eq.nry) return
      db(2)=0.d0
      db(4)=0.d0
      db(5)=0.d0
      db(6)=0.d0
      if(igr.eq.0) then
        call cdgx(x,sg,dgx,tp,ig4,ids,1)
      else
        call udgx(dgx,x,tp,ig4)
      endif
      do 110 j=1,nrx
        if(ids(j).ne.0) go to 110
        db(1)=dgx(j)/sdgy
        db(3)=db(1)
        nrec=(ig-1)*nrx+j
        write(nsv,rec=nrec) db
  110   continue

      end subroutine fsenvd
