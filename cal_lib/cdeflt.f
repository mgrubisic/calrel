      subroutine cdeflt(fltf,fltc,fltp)

      implicit   none

      include   'blkrel1.h'
      include   'cuts.h'
      include   'flag.h'
      include   'prob.h'
      include   'mdat4.h'
      include   'optm.h'

      logical fltf(ngf),fltc(ngf),fltp(ngf)

      integer (kind=4) :: i, n1

      save

      iop = 1
      ni1 = 100
      ni2 = 4
      tol = 1.d-3
      op1 = 1.d0
      op2 = 0.
      op3 = 0.
      icl = 1
      igr = 0
      ncs = ngf
      ntl = ncs+ncs
      ntl = 500
      imc = 0
      ngfc = ngf
      call lodefi('mc  ',n1,1,2*ntl)
      do 10 i = 1,ncs
      ia(n1+i*2-2) = i
      ia(n1+i*2-2+ntl) = i
      ia(n1+i*2-1+ntl) = 0
   10 ia(n1+i*2-1) = 0
      call lodefi('nes ',nes0,2,ngf)
      do 20 i = 0,2*ngf-1
   20 ia(nes0+i) = 0
c     call lodefi('igfx',n2,ngf,4)
c     print *,'n2 = ',n2
      do 30 i = 1,ngf
      fltc(i) = .false.
      fltp(i) = .false.
   30 fltf(i) = .false.
c     do 40 i = 1,4*ngf-1
c  40 ia(n2-1+i) = 0

      end subroutine cdeflt
