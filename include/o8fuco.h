      include 'o8para.h'
      logical val,gconst,llow,lup
      integer gunit
      common/o8gri/val(0:nresm),gconst(0:nresm),gunit(3,0:nresm),
     &             llow(nx),lup(nx)
      integer n,nr,nres,nh,ng
      common/o8dim/n,nh,ng,nr,nres
      double precision epsmac,tolmac,deldif
      common/o8mpar/epsmac,tolmac,deldif
      character*40 name
      common/o8id/name
      double precision epsdif
      common/o8der/epsdif
      logical intakt,inx,std,te0,te1,te2,te3,singul
      logical ident,eqres,silent,analyt,cold
      common/o8stpa/intakt,inx,std,te0,te1,te2,te3,singul,
     &        ident,eqres,silent,analyt,cold
      integer icf,icgf,cfincr,cres,cgres
      common/o8cnt/
     &      icf,icgf,cfincr,cres(nresm),cgres(nresm)
      logical ffuerr,cfuerr(nresm)
      common/o8err/ffuerr,cfuerr

