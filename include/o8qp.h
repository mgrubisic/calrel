      integer nqpmax,mqpmax,maxlen
      parameter ( nqpmax=nx+nresm, mqpmax=2*nresm,
     &   maxlen=3*nqpmax*(nqpmax+1))
      double precision qpg , qpr ,  qpg0 , qpgf , qpqgf , qpc
      double precision qpw , qpqc , qpqs
      double precision tiny , tiny2 , qpgf0
      integer qpbind , qpact , qpalis , qpqi1, qpqi2 , applis , nqp ,
     &        mqp , len , qpterm
      common / o8qpma / qpg(nqpmax,mqpmax), qpr(nqpmax,nqpmax) ,
     &                 qpc(mqpmax), qpqc(maxlen) , qpqs(maxlen) ,
     &                 qpg0(mqpmax) , qpw(nqpmax) , qpgf(nqpmax) ,
     &                 qpgf0(nqpmax) , qpqgf(nqpmax) , tiny , tiny2
      common /o8qpin /   applis , len , nqp , mqp , qpterm ,
     &                 qpbind(mqpmax) , qpact(mqpmax) , qpalis(mqpmax) ,
     &                 qpqi1(maxlen) , qpqi2(maxlen)
      integer newcal,qpstp
      common/o8qpct/newcal,qpstp

