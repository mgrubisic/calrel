c**** if bloc=.true. then it is assumed that functionevaluation takes place
c**** at once in an external routine and
c**** that user_eval has been called before calling for evaluation of functions
c**** the latter then simply consists in copying data from common o8fuext
c**** to donlp2's own data
c**** user_eval must set valid=true, if functionvalues are valid for the
c**** current xtr
c**** corr is set to true by donlp2, if the initial x does not satisfy
c**** the bound constraints. x is modified in this case
c**** difftype=1,2,3 numerical differentiation by the ordinary forward
c**** differences, by central differences or by richardson-extrapolation
c**** of order 6, requiring n, 2n , 6n additional function evaluations
c**** respectively
c**** epsfcn is the assumed precision of the function evaluation, to be
c**** set by the user
c**** taubnd: amount by which bound constraints may be violated during
c**** finite differencing, set by the user
      logical bloc,valid,corr
      integer difftype
      double precision xtr(nx),xsc(nx),fu(0:nresm),fugrad(nx,0:nresm),
     &                 fud(0:nresm,1:6),epsfcn,taubnd
      common/o8fuext/xtr,xsc,fu,fugrad,fud,epsfcn,taubnd
      common/o8fupar/bloc,valid,corr,difftype
