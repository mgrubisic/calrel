      subroutine lpprep(not,ncomp,nlevel,ntotal,nry,icomp,ibtable,
     *                  beta,alph,cost,mtdat,igfx,ngf,rhsin,
     *                  ptabin,ind,imode)

      implicit   none

      character*8,allocatable::moji(:)
      real*8,allocatable::val(:)
      integer*4 not,ncomp,nlevel,ntotal,nry,icomp(*),ngf
      integer*4 ibtable(nlevel,*),mtdat,igfx(ngf,3)
      real*8 beta(*),alph(nry,*),cost(*),rhsin(*)
      real*8 bt,pf,ro,cdot,ptabin(*)
      integer*4 k,i,j,ii,jj,nalloc,nw,js,je,ind,imode
      integer*4 nlevw,nlevt,kk
      character*8 dump,head
c
      if(imode.eq.1) then
        nlevw=ncomp
        write(not,'(1h ,''  '')')
        write(not,'(1h ,'' --- uni-component probability ---'')')
      elseif(imode.eq.2) then
        nlevw=ncomp*(ncomp+1)/2
        write(not,'(1h ,''  '')')
        write(not,'(1h ,'' --- bi -component probability ---'')')
      else
        nlevw=ncomp*(ncomp+1)/2+ncomp*(ncomp-1)*(ncomp-2)/6
        write(not,'(1h ,''  '')')
        write(not,'(1h ,'' --- tri -component probability ---'')')
      endif
      nlevt=nlevel
c
      rewind mtdat
      write(mtdat,'(''name          lpbounds'')')
      write(mtdat,'(''rows'')')
      write(mtdat,'('' n  cost'')')
      do i=1,nlevw+1
      if(i.le.9) then
      write(mtdat,'('' e  lim'',i1)') i
      elseif(i.le.99) then
      write(mtdat,'('' e  lim'',i2)') i
      elseif(i.le.999) then
      write(mtdat,'('' e  lim'',i3)') i
      elseif(i.le.9999) then
      write(mtdat,'('' e  lim'',i4)') i
      elseif(i.le.99999) then
      write(mtdat,'('' e  lim'',i5)') i
      elseif(i.le.999999) then
      write(mtdat,'('' e  lim'',i6)') i
      else
      write(not,'(1h ,'' too much constraint '')')
      stop
      endif
      enddo
      write(mtdat,'(''columns'')')
      nalloc=max(nlevw+1,ntotal)
      allocate(moji(nalloc))
      allocate(val(nalloc))
      do i=1,ntotal
        nw=0
        if(cost(i).ne.0) then
          nw=nw+1
          moji(nw)='cost    '
        endif
        do j=1,nlevw
          if(ibtable(j,i).ne.0) then
            nw=nw+1
            dump='lim     '
            if(j.le.9) then
              write(dump(4:4),'(i1)')j
            elseif(j.le.99) then
              write(dump(4:5),'(i2)')j
            elseif(j.le.999) then
              write(dump(4:6),'(i3)')j
            elseif(j.le.9999) then
              write(dump(4:7),'(i4)')j
            elseif(j.le.99999) then
              write(dump(4:7),'(i5)')j
            elseif(j.le.999999) then
              write(dump(4:7),'(i6)')j
            elseif(j.le.9999999) then
              write(dump(4:7),'(i7)')j
            elseif(j.le.99999999) then
              write(dump(4:7),'(i8)')j
            endif
            moji(nw)=dump
          endif
        enddo
c       ----- total probabiliyt -----
        if(ibtable(nlevt,i).ne.0) then
          nw=nw+1
          dump='lim     '
          if(nlevw+1.le.9) then
            write(dump(4:4),'(i1)')nlevw+1
          elseif(nlevw+1.le.99) then
              write(dump(4:5),'(i2)')nlevw+1
          elseif(nlevw+1.le.999) then
              write(dump(4:6),'(i3)')nlevw+1
          elseif(nlevw+1.le.9999) then
              write(dump(4:7),'(i4)')nlevw+1
          elseif(nlevw+1.le.99999) then
              write(dump(4:7),'(i5)')nlevw+1
          elseif(nlevw+1.le.999999) then
              write(dump(4:7),'(i6)')nlevw+1
          elseif(nlevw+1.le.9999999) then
              write(dump(4:7),'(i7)')nlevw+1
          elseif(nlevw+1.le.99999999) then
              write(dump(4:7),'(i8)')nlevw+1
          elseif(nlevw+1.le.999999999) then
              write(dump(4:7),'(i9)')nlevw+1
          endif
          moji(nw)=dump
        endif
c
        head='   x    '
        if(i.le.9) then
          write(head(5:5),'(i1)') i
        elseif(i.le.99) then
          write(head(5:6),'(i2)') i
        elseif(i.le.999) then
          write(head(5:7),'(i3)') i
        elseif(i.le.9999) then
          write(head(5:8),'(i4)') i
        endif
        do j=1,nw,2
          js=j
          je=min(j+1,nw)
          write(mtdat,'(a8,6x,a8,1pe14.5,3x,a8,1pe14.5)')
     *     head,(moji(k),1.0d0,k=js,je)
        enddo
      enddo
c     ----- construct bounds for each event-----
      write(mtdat,'(''rhs'')')
c     ----- unimodal -----
      do i=1,ncomp
        if(ind.eq.0) then
          bt=-beta(icomp(i))
          call dnorm(bt,pf)
        else
          pf=ptabin(i)
        endif
        write(not,'(1h ,i5,10x,1pe15.5)') igfx(icomp(i),1),pf
        dump='lim     '
        if(i.le.9) then
          write(dump(4:4),'(i1)')i
        elseif(i.le.99) then
          write(dump(4:5),'(i2)')i
        elseif(i.le.999) then
          write(dump(4:6),'(i3)')i
        elseif(i.le.9999) then
          write(dump(4:7),'(i4)')i
        endif
        moji(i)=dump
        val(i)=pf
        rhsin(i)=pf
      enddo
      nw=ncomp
      if(imode.eq.1) goto 200
c     ----- bi modal -----
      do i=1,ncomp-1
        ii=icomp(i)
        do j=i+1,ncomp
           nw=nw+1
           jj=icomp(j)
           if(ind.eq.0) then
             ro=cdot(alph(1,ii),alph(1,jj),1,1,nry)
             call bpfij(pf,beta(ii),beta(jj),ro)
           else
             pf=ptabin(nw)
           endif
           dump='lim     '
           write(not,'(1h ,2i5,5x,1pe15.5)')igfx(ii,1),igfx(jj,1),pf
           if(nw.le.9) then
             write(dump(4:4),'(i1)')nw
           elseif(nw.le.99) then
             write(dump(4:5),'(i2)')nw
           elseif(nw.le.999) then
             write(dump(4:6),'(i3)')nw
           elseif(nw.le.9999) then
             write(dump(4:7),'(i4)')nw
           endif
           moji(nw)=dump
           val(nw)=pf
           rhsin(nw)=pf
        enddo
      enddo
      if(imode.eq.2) goto 200
c     ----- tri modal -----
      do i=1,ncomp-2
        ii=icomp(i)
        do j=i+1,ncomp-1
          jj=icomp(j)
          do k=j+1,ncomp
             kk=icomp(k)
             nw=nw+1
             if(ind.eq.0) then
c               ro=cdot(alph(1,ii),alph(1,jj),1,1,nry)
c               call bpfij(pf,beta(ii),beta(jj),ro)
             else
               pf=ptabin(nw)
             endif
             dump='lim     '
             write(not,'(1h ,3i5,1pe15.5)')igfx(ii,1),igfx(jj,1),
     *          igfx(kk,1),pf
             if(nw.le.9) then
               write(dump(4:4),'(i1)')nw
             elseif(nw.le.99) then
               write(dump(4:5),'(i2)')nw
             elseif(nw.le.999) then
               write(dump(4:6),'(i3)')nw
             elseif(nw.le.9999) then
               write(dump(4:7),'(i4)')nw
             endif
             moji(nw)=dump
             val(nw)=pf
             rhsin(nw)=pf
          enddo
        enddo
      enddo
c     ----- total probability -----
 200  continue
      nw=nw+1
      dump='lim     '
      if(nw.le.9) then
        write(dump(4:4),'(i1)')nw
      elseif(nw.le.99) then
        write(dump(4:5),'(i2)')nw
      elseif(nw.le.999) then
        write(dump(4:6),'(i3)')nw
      elseif(nw.le.9999) then
        write(dump(4:7),'(i4)')nw
      endif
      moji(nw)=dump
      val(nw)=1.0d0
      rhsin(nw)=1.0d0
      head='    rhs1'
      do j=1,nw,2
        js=j
        je=min(j+1,nw)
        write(mtdat,'(a8,6x,a8,1pe14.5,3x,a8,1pe14.5)')
     *   head,(moji(k),val(k),k=js,je)
      enddo
      write(mtdat,'(''bounds'')')
c      do i=1,ntotal
c      if(i.le.9) then
c      write(mtdat,'('' lo bnd1     x'',i1,7x,1pe14.5)') i,0.0d0
c      elseif(i.le.99) then
c      write(mtdat,'('' lo bnd1     x'',i2,6x,1pe14.5)') i,0.0d0
c      elseif(i.le.999) then
c      write(mtdat,'('' lo bnd1     x'',i3,5x,1pe14.5)') i,0.0d0
c      elseif(i.le.9999) then
c      write(mtdat,'('' lo bnd1     x'',i4,4x,1pe14.5)') i,0.0d0
c      else
c      write(not,'(1h ,'' too much parameter for lp '')')
c      stop
c      endif
c      enddo
      do i=1,ntotal
      if(i.le.9) then
      write(mtdat,'('' lo bnd1 x'',i1,7x,1pe14.5)') i,0.0d0
      elseif(i.le.99) then
      write(mtdat,'('' lo bnd1 x'',i2,6x,1pe14.5)') i,0.0d0
      elseif(i.le.999) then
      write(mtdat,'('' lo bnd1 x'',i3,5x,1pe14.5)') i,0.0d0
      elseif(i.le.9999) then
      write(mtdat,'('' lo bnd1 x'',i4,4x,1pe14.5)') i,0.0d0
      elseif(i.le.99999) then
      write(mtdat,'('' lo bnd1 x'',i5,3x,1pe14.5)') i,0.0d0
      elseif(i.le.999999) then
      write(mtdat,'('' lo bnd1 x'',i6,2x,1pe14.5)') i,0.0d0
      elseif(i.le.9999999) then
      write(mtdat,'('' lo bnd1 x'',i7,1x,1pe14.5)') i,0.0d0
      else
      write(not,'(1h ,'' too much parameter for lp '')')
      stop
      endif
      enddo
      write(mtdat,'(''enddata'')')

      deallocate(moji)
      deallocate(val)

      end subroutine lpprep
