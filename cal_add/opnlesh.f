c  compute the i-th equality constaint, value is hxi
      subroutine esh(i,x,hxi)

      implicit   none

      include 'o8fuco.h'
      include 'o8fint.h'
      double precision x(*),hxi
      integer i
      integer j
      save
      if ( bloc ) then
        if ( valid ) then
          if ( gunit(1,i) .ne. 1 ) cres(i)=cres(i)+1
          hxi=fu(i)
          return
        else
          stop 'donlp2: bloc-call with functoninformation invalid'
        endif
      else
        do j=1,n
          xtr(j)=x(j)*xsc(j)
        enddo
        call eh(i,xtr,hxi)
        return
      endif
      end subroutine esh
