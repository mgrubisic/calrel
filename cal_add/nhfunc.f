!***************************************************************
      subroutine nhfunc(ngfc,nry,weight,y1,stddev,amvec,ffunc,hfunc,
     *                  coef)
!***************************************************************
      implicit   none

      integer (kind=4) :: ngfc,nry
      real    (kind=8) :: weight(ngfc),y1(nry),amvec(nry,ngfc),coef
      real    (kind=8) :: stddev(*)
!
      real    (kind=8) :: ffunc,hfunc,yyy,pi,ysum
!     real    (kind=8) :: yy,fai,hsub
      integer (kind=4) :: i,j

      pi=sqrt(2.0d0*acos(-1.0d0))

!     -------------------------------------
!
!      ffunc=1.0d0
!      do i=1,nry
!        yy=-0.5d0*y1(i)*y1(i)
!        fai=exp(yy)/2.506628275d0
!        fai=exp(yy)/pi
!        ffunc=ffunc*fai
!      enddo

!      hfunc=0.0d0
!      do i=1,ngfc
!        hsub=1.0d0
!        do j=1,nry
!          yyy=(y1(j)-amvec(j,i))/stddev(i)
!          yy=-0.5d0*yyy*yyy
!          fai=exp(yy)/2.506628275d0/stddev(i)
!          fai=exp(yy)/pi/stddev(i)
!          hsub=hsub*fai
!        enddo
!        hfunc=hfunc+weight(i)*hsub
!      enddo
!      coef=ffunc/hfunc

      coef=0.0d0
      do i=1,ngfc
        ysum=0.0d0
        do j=1,nry
          yyy=(y1(j)-amvec(j,i))/stddev(i)
          ysum=ysum+yyy**2-y1(j)**2
        enddo
        ysum=-0.5d0*ysum
        coef=coef+weight(i)*exp(ysum)/(stddev(i)**(nry))
      enddo

      coef=1.0d0/coef

      end subroutine nhfunc
