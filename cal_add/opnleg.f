      subroutine eg(i,x,gxi,isfunc,idfunc,yyy,zzz,tf,tp,ex,sg,par,bnd,
     *              igt,lgp,ids,ipa,ib,izx)

      implicit   none

      include   'o8fuco.h'
      include   'nlpflag.h'

      integer i,isfunc(*)
      double precision x(*),gxi
      real*8 yyy(*),zzz(*),tf(*),ex(*),sg(*),par(4,*),bnd(2,*),tp(*)
      integer*4 idfunc(*),igt(*),lgp(3,*),ids(*),ipa(4,*),ib(*),izx(*)

c     ----- transform y into x -----

      call cytox(yyy,x,zzz,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call ugfun(gxi,yyy,tp,iabs(idfunc(i)))

      if(idfunc(i).gt.0) then
        gxi=-gxi
      endif

      if(iswnlp.ne.0) then
        gxi=gxi+zero(iabs(isfunc(i)))
      endif

      end subroutine eg
