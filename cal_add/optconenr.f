      subroutine conenr(n,m,pmat1,r,istrt1,ncor,icor,tol,iwork,
     *liwrk,work,lwrk,vec,ptnrr,picor,nparm,numgr,coef,ptnr,dist,nmaj,
     *nmin,jflag)
c
      implicit   none

c     include   'file.h'

      integer (kind=4) :: n,m, nparm,istrt1,ncor,liwrk,lwrk,numgr,nmaj
      integer (kind=4) :: nmin,jflag
      real    (kind=8) :: tol,dist
      real    (kind=8) :: pmat1(nparm+1,numgr),r(nparm+1)
      integer (kind=4) :: icor(nparm+1)
      real    (kind=8) :: coef(numgr),ptnr(nparm+1)
      real    (kind=8) :: vec(nparm+1),ptnrr(nparm+1)
      integer (kind=4) :: iwork(liwrk)
      real    (kind=8) :: picor(nparm+1,nparm+1),work(lwrk)

      integer (kind=4) :: i,ii,icoro, ihouse, itst1
      integer (kind=4) :: ilc01,ilc03,ilc04,ilc09,ilc23,ilc34
      integer (kind=4) :: j,jj,jmax,jmin
      integer (kind=4) :: kntsl
      integer (kind=4) :: l, limsl, ll
      integer (kind=4) :: mincf, mp1
      integer (kind=4) :: ncoro, ndm
      real    (kind=8) :: amax,amin
      real    (kind=8) :: cjj
      real    (kind=8) :: diff,dmax,dp,dsq
      real    (kind=8) :: four
      real    (kind=8) :: d1mach
      real    (kind=8) :: omt, one
      real    (kind=8) :: pdotj
      real    (kind=8) :: quot
      real    (kind=8) :: spcmn
      real    (kind=8) :: theta, tolel, tst, two,ten
      real    (kind=8) :: z1,z2,z3, zero 

      integer (kind=4) :: iloc
      real    (kind=8) :: dotprd
c given m n-dimensional vectors p(j) as the first m columns
c of the matrix pmat1 and an n-vector r, this subroutine returns in
c ptnr the nearest point to r in the cone of points summation(
c coef(j)*p(j)), where coef(j) .ge. 0.0 for j=1,...,m (unless jflag
c .gt. 0, which indicates failure).  the number of vectors p(j) in
c the final corral is returned in ncor with their indices in icor,
c the distance is returned in dist, the number of major cycles (i.e.
c adding a vector) is returned in nmaj, and the number of minor cycles
c (i.e. removing a vector) is returned in nmin.  if the user sets
c istrt1=0 the subroutne starts from scratch, but the user can set
c istrt1=1 and initially specify ncor, icor, and coef (noting that ncor
c must be .le. n, and if j does not occur in icor, then coef(j) should
c be set to 0.0.)
c
c set machine and precision dependent constants for conenr.
c     nwrit=i1mach(2)
      one=1.0d0
      zero=one-one
      two=one+one
      four=two+two
      ten=four+four+two
      spcmn=d1mach(3)
      tolel=ten*ten*spcmn
      z1=ten*tolel
      z2=ten*z1
      z3=ten*z1
c end of setting machine and precision dependent constants for conenr.
      ilc01=iloc(1,nparm,numgr)
      ilc03=iloc(3,nparm,numgr)
      ilc04=iloc(4,nparm,numgr)
      ilc09=iloc(9,nparm,numgr)
      ilc23=iloc(23,nparm,numgr)
      ilc34=iloc(34,nparm,numgr)
      kntsl=0
      limsl=100
      mp1=m+1
      ndm=n-1
      nmaj=0
      nmin=0
      jflag=0
      itst1=0
      ncoro=-1
      if(istrt1)100,100,1000
c
c here istrt1=0 so we start from scratch.  find the index jmax for
c which (p(j).r)/sqrt(p(j).p(j)) is maximized for p(j).p(j) .gt. z1.
  100 amax=zero
      jmax=0
      do 500 j=1,m
        do 200 i=1,n
          vec(i)=pmat1(i,j)
  200     continue
        pdotj=dotprd(n,vec,vec,nparm)
        if(pdotj-z1)500,500,300
  300   quot=dotprd(n,vec,r,nparm)/sqrt(pdotj)
        if(quot-amax)500,500,400
  400   amax=quot
        jmax=j
  500   continue
      if(jmax)600,600,550
c if amax is not siginficantly positive we proceed as if it were zero.
  550 if(amax*sqrt(ndm+one)-tolel)600,600,800
c
c here there were no vectors p(j) which have both length squared
c greater than z1 and angle with r significantly less than 90 degrees,
c and we set ncor=0, ptnr=the zero vector, coef=the zero vector, dist=
c the length of r, and we return.
  600 ncor=0
      do 700 i=1,n
        ptnr(i)=zero
  700   continue
      do 750 j=1,m
        coef(j)=zero
  750   continue
      dist=sqrt(dotprd(n,r,r,nparm))
      return
c
c here we found the ray closest to r and we complete the
c initialization by setting ncor=1, icor(1)=jmax, and coef(jmax)=1.0
c (with all other entries of coef equal to zero).
  800 ncor=1
      icor(1)=jmax
      do 900 i=1,m
        coef(i)=zero
  900   continue
      coef(jmax)=one
c
c
c set ptnr to the current nearest point.  first zero it out.
 1000 do 1050 i=1,n
        ptnr(i)=zero
 1050   continue
      if(ncor)1330,1330,1100
c here ncor .gt. 0 and we set ptnr=summation(coef(j)*p(j)).
 1100 do 1300 j=1,ncor
        jj=icor(j)
        cjj=coef(jj)
        do 1200 i=1,n
          ptnr(i)=ptnr(i)+cjj*pmat1(i,jj)
 1200     continue
 1300   continue
c
c put ptnr-r into ptnrr and compute the distance from ptnr to r.
 1330 do 1370 i=1,n
        ptnrr(i)=ptnr(i)-r(i)
 1370   continue
      dsq=dotprd(n,ptnrr,ptnrr,nparm)
      dist=sqrt(dsq)
c
c now check optimality.
c first see whether the hyperplane through ptnr perpendicular to
c ptnr-r passes through the origin.  if ncor=0 this will
c automatically be true since then ptnr is the origin.  if it is not
c true we go down to solve for a new nearest point in the subspace
c determined by the current icor.

      if(ncor)2100,2100,1470

 1470 tst=dotprd(n,ptnr,ptnrr,nparm)

      if(abs(tst)-z1)2100,4000,4000
c here the hyperplane roughly passes through the origin, and we
c check whether all p(j) vectors are roughly separated from r by it.
c put the minimum of (ptnr-r).(p(j)-r) in amin and the index where it
c is achieved in jmin.

 2100 do 2200 i=1,n
        vec(i)=pmat1(i,1)-r(i)
 2200   continue
      jmin=1
      amin=dotprd(n,ptnrr,vec,nparm)

      if(m-1)2700,2700,2300

 2300 do 2600 j=2,m
        do 2400 i=1,n
          vec(i)=pmat1(i,j)-r(i)
 2400     continue
        dp=dotprd(n,ptnrr,vec,nparm)
        if(dp-amin)2500,2600,2600
 2500   amin=dp
        jmin=j
 2600   continue
c
c for testing purposes compute the maximum of the squares of the
c lengths of the distances considered.
 2700 do 2800 i=1,n
        vec(i)=pmat1(i,jmin)-r(i)
 2800   continue
      dmax=dotprd(n,vec,vec,nparm)
      if(ncor)3300,3300,2900
 2900 do 3200 j=1,ncor
        jj=icor(j)
        do 3000 i=1,n
          vec(i)=pmat1(i,jj)-r(i)
 3000     continue
        dp=dotprd(n,vec,vec,nparm)
        if(dp-dmax)3200,3200,3100
 3100   dmax=dp
 3200   continue
c do the test.  if it is successful, then we have (approximate)
c optimality and we return.
 3300 if(amin-dsq+z1*dmax)3500,3400,3400
 3400 return
c
c here ptnr is not optimal.  as a check against blunders we make sure
c ncor .lt. n and jmin is not in icor.
 3500 if(ncor)3900,3900,3550
 3550 if(ncor-n)3600,3800,3800
 3600 do 3700 l=1,ncor
        if(jmin-icor(l))3700,3800,3700
 3700   continue
      go to 3900
c
c here we have blundered so we set jflag=1 as a warning, compute dist,
c and return.  first try from scratch if this has not been done.
 3800 if(istrt1+jflag)3870,3870,3830
 3830 jflag=-1
      kntsl=0
      go to 100
 3870 jflag=1
c     write(not,3880)
c3880 format(26h *****jflag is 1 in conenr)
      return
c
c here ptnr is not optimal, ncor .lt. n, and jmin is not in icor.
c we increment the major cycle counter and add p(jmin).
 3900 nmaj=nmaj+1
      ncor=ncor+1
      icor(ncor)=jmin
      coef(jmin)=zero
c
c check to see if we have solved the system below limsl times already,
c and if so, set jflag=6 as a warning and return (but
c try from scratch before giving up if this has not already been done).
 4000 if(kntsl-limsl)4080,4020,4020
 4020 if(istrt1+jflag)4060,4060,4040
 4040 jflag=-1
      kntsl=0
      go to 100
c
 4060 jflag=6
c     write(not,4070)
c4070 format(26h *****jflag is 6 in conenr)
      return
c
c check to see if ncor and the last element in icor are unchanged from the
c previous house call (ha ha), which indicates failure.  note that here we
c must have ncor .gt. 0.
 4080 if(ncor-ncoro)4130,4090,4130
 4090 if(icor(ncor)-icoro)4140,4100,4140
c
c here we have cycling and we set jflag=2 as a warning and return.  first
c try from scratch if this has not been done.
 4100 if(istrt1+jflag)4120,4120,4110
 4110 jflag=-1
      kntsl=0
      go to 100
c
 4120 jflag=2
      return
c
 4130 ncoro=ncor
 4140 icoro=icor(ncor)
      kntsl=kntsl+1
c
c now we solve the system picor*vec = r in the least squares
c sense for the coefficient vector vec (relative to
c icor) of the nearest point to r in the subspace spanned by
c p(icor(1)),...,p(icor(ncor)), where p(icor) is the n x ncor matrix
c whose columns are these vectors.
c now fill in picor and call house to compute vec.
      do 4300 j=1,ncor
        jj=icor(j)
        do 4200 i=1,n
          picor(i,j)=pmat1(i,jj)
 4200     continue
 4300   continue
c
      call house(n,ncor,picor,r,iwork(ilc23),nparm,work(ilc01),
     *work(ilc04),work(ilc09),work(ilc34),work(ilc03),vec,ihouse)
c
c if house fails (indicated by ihouse=1) we set jflag=3 as a
c warning and return.  first try from scratch if this has not been done.
      if(ihouse)5500,5500,5400
 5400 if(istrt1+jflag)5470,5470,5430
 5430 jflag=-1
      kntsl=0
      go to 100
c
 5470 jflag=3
      return
c
c check to see if all the coefficients in vec are .gt. z2, and if so,
c put vec into coef and go back to compute ptnr.  the coefficients in
c coef not corresponding to those in vec will remain equal to zero.
 5500 do 5600 i=1,ncor
        if(vec(i)-z2)5800,5800,5600
 5600   continue
      do 5700 i=1,ncor
        ii=icor(i)
        coef(ii)=vec(i)
 5700   continue
      go to 1000
c
c here some element of vec is .le. z2.  compute theta=min(1.0, min(
c coef(icor(i))/(coef(icor(i))-vec(i)): coef(icor(i))-vec(i) .gt. z3)).
 5800 theta=one
      do 6100 l=1,ncor
        ll=icor(l)
        diff=coef(ll)-vec(l)
        if(diff-z3)6100,6100,5900
 5900   quot=coef(ll)/diff
        if(quot-theta)6000,6100,6100
 6000   theta=quot
 6100   continue
c compute the new coef as (1.0-theta)*coef+theta*vec.
      omt=one-theta
      do 6200 l=1,ncor
        ll=icor(l)
        coef(ll)=omt*coef(ll)+theta*vec(l)
 6200   continue
c compute the index mincf (relative to icor) of the smallest element of
c coef and set all elements of coef which are .le. z2 to zero.
      mincf=0
      amin=z2
      do 6600 i=1,ncor
        ii=icor(i)
        if(coef(ii)-z2)6300,6300,6600
 6300   if(coef(ii)-amin)6400,6400,6500
 6400   amin=coef(ii)
        mincf=i
 6500   coef(ii)=zero
 6600   continue
c
      if(mincf)6640,6640,6800
c here mincf=0 and an unlikely blunder has occurred.  this must be due to
c roundoff error since in theory (new) coef(icor(i)) must be .le. z2
c for some i=1,...,ncor, which makes mincf .gt. 0 in the last loop.
c to see this, first note that for some ibar=1,...,ncor, vec(ibar) must
c be .le. z2 since otherwise we would not be here.  by its definition,
c theta must be .le. 1.0.  if theta = 1.0, then (new) coef(icor(ibar))
c = (1.0 - theta)*(old) coef(icor(ibar)) + theta*vec(ibar) = vec(ibar)
c .le. z2.  if on the other hand theta .lt. 1.0, then for some istar=1,
c ...,icor we have (old) coef(icor(istar)) - vec(istar) .ge. z3 and
c theta = (old) coef(icor(istar))/((old) coef(icor(istar)) - vec(istar)),
c so (new) coef(icor(istar)) = (1.0 - theta)*(old) coef(icor(istar)) +
c theta*vec(istar) = (-vec(istar)*(old) coef(icor(istar)) + (old)
c coef(icor(istar))*vec(istar))/((old) coef(icor(istar)) - vec(istar))
c = 0.0.  note that we have z2 .ge. 0.0 and z3 .gt. 0.0.
c to correct this blunder we set mincf = an index i for which (new)
c coef(icor(i)) is minimized and set coef(icor(i)) = 0.0.
 6640 do 6760 i=1,ncor
        ii=icor(i)
        if(i-1)6720,6720,6680
 6680   if(coef(ii)-amin)6720,6760,6760
 6720   amin=coef(ii)
        mincf=i
 6760   continue
      ii=icor(mincf)
      coef(ii)=zero
c
c increment the minor iteration counter nmin, remove icor(mincf),
c and decrement ncor.
 6800 nmin=nmin+1
      do 7000 l=1,ncor
        if(l-mincf)7000,7000,6900
 6900   icor(l-1)=icor(l)
 7000   continue
      ncor=ncor-1
c go back to compute ptnr.
      go to 1000
      end
