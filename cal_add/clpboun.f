      subroutine clpboun

      implicit   none

      include   'blkrel1.h'
      include   'bond.h'
      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'line.h'
      include   'prob.h'

      integer (kind=4),allocatable :: mcr(:),iflag(:)
      real    (kind=8),allocatable :: ptabin(:),ptabin2(:)
      integer (kind=4) :: iclr
      integer (kind=4) :: icheck
c
      integer (kind=4) :: ind,ncompr,ncsr,ntlr,j,nptab,nadd,i,inequality
      integer (kind=4) :: n1,nr,nc,n2,n3,n4,n5,imxx,maxset,ii,iclin
      integer (kind=4) :: incost,ibtnegative,imeth

      save

      imxx=350000
      call freei('y',imxx,1)
      if(imxx.eq.0) imxx=350000
      ibt=0
      ibtnegative=0
      call freei('t',ibt,1)
      if(ibt.eq.0) ibt=3
      if(ibt.lt.0) then
        ibt=-ibt
        ibtnegative=1
      endif
      ind=0
      call freei('d',ind,1)
      icheck=0
      call freei('k',icheck,1)
      maxset=1000
      call freei('x',maxset,1)
      if(maxset.eq.0) maxset=1000
      imeth=0
      call freei('h',imeth,1)
      incost=0
      call freei('c',incost,1)
c
      write(not,1000) ibt
      if(ibt.eq.3.and.ind.eq.0) then
      write(not,'(1h ,'' '')')
      write(not,'(1h '' tri-component bounds are available'',
     *               '' only for user input problem '')')
      write(not,'(1h '' up to bi-component bounds are calculated'')')
      if(ibt.eq.3) ibt=2
      endif
c
      if(ind.eq.0.and.icl.eq.1) then
      write(not,'(1h '' fatal error in lp bounds          '')')
      write(not,'(1h '' applicable only to system problems'')')
      stop
      endif
c     ----- user input problem -----
      inequality=0
      if(ind.ne.0) then
        read(nin,*) iclr,ncompr
        if(ncompr.lt.0) inequality=1
        ncompr=iabs(ncompr)
        if(iabs(iclr).ne.2.and.iabs(iclr).ne.3) then
        write(not,'(1h ,''  !! fatal error in lp bounds          !!'')')
        write(not,'(1h ,''  !! iclr must be -2 or 2 or 3         !!'')')
        stop
        endif
        allocate(iflag(ncompr))
        if(ncompr.le.2.and.ibt.eq.3) then
        write(not,'(1h ,''  !! fatal error in lp bounds          !!'')')
        write(not,'(1h ,''  !! 3 or more components are required !!'')')
        stop
        endif
        if(iclr.eq.3) then
          iclin=3
          read(nin,*) ncsr,ntlr
          ntlr=ncsr+ntlr
          allocate(mcr(ntlr))
          read(nin,*,err=89) (mcr(j),j=1,ntlr)
          do i=1,ncompr
            iflag(i)=0
          enddo
          do i=1,ntlr
            ii=iabs(mcr(i))
            if(ii.gt.ncompr) then
            write(not,'(1h ,''!! fatal error in lp bounds        !!'')')
            write(not,'(1h ,''!! component '',i5,''  !!'')')
            write(not,'(1h ,''!! larger than # of comps    !!'')')
            stop
            endif
            if(ii.ne.0) iflag(ii)=1
          enddo
          do i=1,ncompr
            if(iflag(i).eq.0) then
            write(not,'(1h ,''!! fatal error in lp bounds        !!'')')
            write(not,'(1h ,''!! component '',i5,'' is not used  !!'')')
     *          i
            write(not,'(1h ,''!! check the data                  !!'')')
            stop
            endif
          enddo
        elseif(iclr.eq.2) then
c       ----- siries -----
          iclin=2
          ntlr=2*ncompr
          allocate(mcr(ntlr))
          do i=1,ntlr,2
            mcr(i)=(i+1)/2
            mcr(i+1)=0
          enddo
        elseif(iclr.eq.-2) then
c       ----- parallel -----
          iclin=3
          ntlr=ncompr+1
          allocate(mcr(ntlr))
          do i=1,ntlr
            mcr(i)=i
          enddo
          mcr(ntlr)=0
        endif
        goto 88
   89   continue
        write(not,'(1h ,''!! wrong input in cutset in lp bounds !!'')')
        write(not,'(1h ,''!! check the data !!'')')
        stop
   88   continue
        if(ibt.eq.1) then
c       ----- unimodal bounds -----
          nptab=ncompr
        elseif(ibt.eq.2) then
c       ----- bimodal bounds -----
          nptab=(ncompr+1)*ncompr
          nptab=nptab/2
        elseif(ibt.eq.3.or.ibt.eq.0) then
c       ----- trimodal bounds -----
          nptab=(ncompr+1)*ncompr
          nptab=nptab/2
          nadd =(ncompr-1)*(ncompr-2)*ncompr
          nadd =nadd/6
          nptab=nptab+nadd
        endif
        allocate(ptabin(nptab))
        allocate(ptabin2(nptab))      
        read(nin,*,err=99) (ptabin(i),i=1,nptab)
c        do i=1,nptab
c          ptabin(i)=ptabin(i)*0.9999d0
c       enddo
        if(inequality.ne.0) then
        read(nin,*,err=99) (ptabin2(i),i=1,nptab)
c        do i=1,nptab
c          ptabin2(i)=ptabin2(i)*1.0001d0
c       enddo
        endif
        goto 98
   99   continue
        write(not,'(1h ,''!! wrong input for ptable in lp bounds !!'')')
        write(not,'(1h ,''!! check the data !!'')')
        stop
   98   continue
      else
        iclin=icl
      endif
      if(ind.eq.0) then
        call locate('beta',n1,nr,nc)
        call locate('alph',n2,nr,nc)
        call locate('igfx',n3,nr,nc)
        call locate('mc  ',n4,nr,nc)
        call locate('fltf',n5,nr,nc)
      else
        n1=1
        n2=1
        n3=1
        n4=1
        n5=1
      endif
      call lpboun(ia(n1),ia(n2),ia(n3),ia(n4),ia(n5),imxx,maxset,ibt,
     *           ind,imeth,icheck,iclin,ncompr,ntlr,mcr,nptab,ptabin,
     *           ptabin2,inequality,incost,ibtnegative)

 1000 format(//' >>>> first-order lp bounds for systems <<<<'//
     *' type of bounds ..........................ibt=',i7,/
     *'   ibt=1 .............................unimodal bounds'/
     *'   ibt=2 ............................ bimodal  bounds'/
     *'   ibt=3 ............................trimodal  bounds'/
     *'   ibt=0 ............................all of the above')

      if(ind.eq.1) then
        deallocate(mcr)
        deallocate(ptabin)
        deallocate(iflag)
      endif

      end subroutine clpboun
