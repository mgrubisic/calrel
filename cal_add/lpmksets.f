c******************************************************************
      subroutine lpmksets(not,ngf,igfx,ntotal,ncomp,icomp,
     *                    nlevel,ibtable,
     *                    ncset,nset,maxset,icset,cost,nonzero)
c*******************************************************************
c
c     construct boolean table
c
c     input    not  : output file
c              ngf  : # of limit state functions
c              igfx : limit state function id, etc.
c              ncomp: # of single components used in the system
c              icomp: seq id of single components used in the system
c
c     output   ibtable : boolean table (ncomp*(2**ncomp))
c
c********************************************************************
      implicit   none

      integer*4,allocatable::iflag(:)
      integer*4 ntotal,ilevel,nkind,nc,i,j,k,kk,l
      integer*4 not,ngf,igfx(ngf,3),ncomp,icomp(ncomp),maxset
      integer*4 nlevel,ibtable(nlevel,ntotal),ncset,nset(*),ii
      integer*4 icset(maxset,*),nonzero
      real*8 cost(*)
c
c
      allocate(iflag(ntotal))
c
      nonzero=0
      ilevel=0
c     ----- uni modal level -----
   10 continue
      ilevel=ilevel+1
      nkind=2**(ncomp-ilevel)
      nc=0
   20 continue
      do i=1,nkind
        nc=nc+1
        ibtable(ilevel,nc)=1
        nonzero=nonzero+1
      enddo
      do i=1,nkind
        nc=nc+1
        ibtable(ilevel,nc)=0
      enddo
      if(nc.lt.ntotal) goto 20
      if(ilevel.lt.ncomp) goto 10
c     ----- bi-modal level -----
      do i=1,ncomp-1
      do j=i+1,ncomp
        ilevel=ilevel+1
        do k=1,ntotal
          ibtable(ilevel,k)=ibtable(i,k)*ibtable(j,k)
          if(ibtable(ilevel,k).ne.0) then
            nonzero=nonzero+1
          endif
        enddo
      enddo
      enddo
c     ----- tri-modal level -----
      do i=1,ncomp-2
      do j=i+1,ncomp-1
      do k=j+1,ncomp
        ilevel=ilevel+1
        do l=1,ntotal
          ibtable(ilevel,l)=ibtable(i,l)*ibtable(j,l)*ibtable(k,l)
          if(ibtable(ilevel,k).ne.0) then
            nonzero=nonzero+1
          endif
        enddo
      enddo
      enddo
      enddo
c     ----- total probability level -----
      ilevel=ilevel+1
      do k=1,ntotal
        ibtable(ilevel,k)=1
        nonzero=nonzero+1
      enddo
c
c     ----- find cost function -----
c
      do i=1,ntotal
        cost(i)=0.0d0
      enddo
c
      do i=1,ncset
        do j=1,ntotal
          iflag(j)=1
        enddo
        do j=1,nset(i)
          ii=icset(j,i)
          do k=1,ntotal
            kk=ibtable(iabs(ii),k)
            if(ii.gt.0) then
              if(kk.eq.1) then
                iflag(k)=iflag(k)*1
              else
                iflag(k)=iflag(k)*0
              endif
            else
              if(kk.eq.1) then
                iflag(k)=iflag(k)*0
              else
                iflag(k)=iflag(k)*1
              endif
            endif
          enddo
        enddo
        do j=1,ntotal
          if(iflag(j).eq.1) then
            cost(j)=cost(j)+1.0d0
          endif
        enddo
      enddo
      do j=1,ntotal
        if(cost(j).ne.0.0d0) then
          cost(j)=1.0d0
        endif
      enddo
c
      if(not.ne.0) then
c      write(not,'(1h ,'' '')')
c      write(not,'(1h ,'' === boolean table ===== '')')
c      write(not,'(1h ,15x,100i2)') (i,i=1,ntotal)
c      do i=1,ncomp
c      write(not,'(1h ,i5,10x,100i2)')
c     *      igfx(icomp(i),1),(ibtable(i,j),j=1,ntotal)
c      enddo
c      ilevel=ncomp
c      do i=1,ncomp-1
c      do j=i+1,ncomp
c      ilevel=ilevel+1
c      write(not,'(1h ,2i5,5x,100i2)')
c     * igfx(icomp(i),1),igfx(icomp(j),1),(ibtable(ilevel,k),k=1,ntotal)
c      enddo
c      enddo
c      do i=1,ncomp-2
c      do j=i+1,ncomp-1
c      do l=j+1,ncomp
c      ilevel=ilevel+1
c      write(not,'(1h ,3i5,5x,100i2)')
c     * igfx(icomp(i),1),igfx(icomp(j),1),igfx(icomp(l),1),
c     *       (ibtable(ilevel,k),k=1,ntotal)
c      enddo
c      enddo
c      enddo
c      ilevel=ilevel+1
c      write(not,'(1h ,3hall,12x,100i2)')
c     * (ibtable(ilevel,k),k=1,ntotal)
      write(not,'(1h ,'' '')')
      write(not,'(1h ,'' === cost function ===== '')')
      write(not,'(1h ,15x,100i2)')
     * (int(cost(j)),j=1,ntotal)
      write(not,'(1h ,'' '')')
      endif
c
      deallocate(iflag)

      end subroutine lpmksets
