c***********************************************************
      subroutine nksecant(nry,nrx,ntf,nig,nrmx,ifunc,irr,idx1,idx2,idn,
     *                    igt,lgp,ids,ipa,ib,izx,tp,tf,ex,sg,par,bnd,
     *                    gtor,y0,alpha,beta,x1,y1w,z1w,
     *                    r1,r2,g1,g2,root,rr,indx,isflg,ifound,irpre,
     *                    neval)
c***********************************************************
c
c     find a solution between r1 and r2
c
c***********************************************************
      implicit   none

      include   'file.h'

      integer*4 nry,nrx,ntf,nig,irr,ifunc,idn,idx1,idx2,indx,ifound
      real*8 x1(nrx),y1w(nry),z1w(nry),y0(nry),alpha(nry)
      real*8 tf(ntf),ex(nrx),sg(nrx),par(4,nrx),bnd(2,nrx)
      integer*4 igt(nig),lgp(3,nig),ids(nrx),ipa(4,nrx),ib(nrx),izx(nry)
      integer*4 nrmx,isflg(nrmx),irpre,neval
      real*8 rr(nrmx),tp(*),r2,g2,g1,r1,root,gtor,beta
c
      integer*4 i,kp
      real*8 gg2,rr2,rb,gb,rh,r3,g3,r4
c
      ifound=0
      do i=1,nry
        y1w(i)=y0(i)+r2*alpha(i)
      enddo
      call cytox(x1,y1w,z1w,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call ugfun(g2,x1,tp,ifunc)
      neval=neval+1
      gg2=g2
      rr2=r2
      if(dabs(g2).le.gtor*1.0001d0) then
        irr=irr+1
        rr(irr)=r2
        g1=g2
        r1=r2
        idn=0
        isflg(irr)=0
        ifound=1
        return
      endif
c
      if(g2.lt.0.0d0) then
        idx2=1
      else
        idx2=0
      endif
c
      if(idn.eq.0) then
        g1=g2
        r1=r2
        idx1=idx2
        idn=1
        if(indx.eq.0) then
          if(idx2.eq.1) then
            isflg(irpre)= 1
          else
            isflg(irpre)=-1
          endif
        else
          if(idx2.eq.1) then
            isflg(irpre)=-1
          else
            isflg(irpre)= 1
          endif
        endif
        return
      endif
c
      if(idx1.eq.idx2) then
        g1=g2
        r1=r2
        return
      endif
c
      kp=2
      rb=r1
      gb=g1
      rh=r2
c
 201  continue
        r3=r1-g1*(r2-r1)/(g2-g1)
        if(indx.eq.0) then
c         ----- for positive region -----
          if(r3.lt.beta.or.r3.ge.root) then
            r3=(rb+rh)/2.d0
          endif
        else
c         ----- for negative -----
          if(r3.gt.beta.or.r3.lt.root) then
            r3=(rb+rh)/2.d0
          endif
        endif
        do i=1,nry
          y1w(i)=y0(i)+r3*alpha(i)
        enddo
        call cytox(x1,y1w,z1w,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
        kp=kp+1
        call ugfun(g3,x1,tp,ifunc)
        neval=neval+1
        if(g3*gb.lt.0.) then
          if(rh.gt.r3) rh=r3
        else
          if(rb.lt.r3) rb=r3
        endif
        if(dabs(g3).le.gtor*1.0001d0.or.kp.ge.100) then
          irr=irr+1
          if(indx.eq.0) then
c           ----- positive region -----
            if(idx1.eq.0) then
              isflg(irr)=1
            else
              isflg(irr)=-1
            endif
          else
c           ----- negative region -----
            if(idx1.eq.0) then
              isflg(irr)=-1
            else
              isflg(irr)= 1
            endif
          endif
          r4=r2-g2*(r3-r2)/(g3-g2)
          if(indx.eq.0) then
          if(r4.lt.rb.or.r4.gt.rh) r4=(rb+rh)/2.d0
          else
          if(r4.gt.rb.or.r4.lt.rh) r4=(rb+rh)/2.d0
          endif
          rr(irr)=r4
          g1=gg2
          r1=rr2
          idx1=idx2
          idn=1
          ifound=1
          if(kp.ge.100) write(not,309)
 309      format('  warning: root is not solved within 100 steps')
          return
        endif
        g1=g2
        r1=r2
        r2=r3
        g2=g3
      go to 201

      end subroutine nksecant
