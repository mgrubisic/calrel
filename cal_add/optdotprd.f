      real*8 function dotprd(lgth,vec1,vec2,nparm)

      implicit   none

      integer (kind=4) :: lgth,nparm, j
      real    (kind=8) :: vec1(nparm+1),vec2(nparm+1)

      real    (kind=8) :: dd

c this subprogram computes the dot product of vectors vec1
c and vec2 of length lgth.
c vec1 and vec2 do not appear in function iloc since they are used only
c as input names for this subprogram, and so they don't need to have
c space reserved for them in the array work.

      dd = vec1(1)*vec2(1)
      do j = 2,lgth
        dd = dd + vec1(j)*vec2(j)
      end do ! j
      dotprd = dd

      end function dotprd
c*****end of conmax package.
