cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine nsumpt(tp,x,mc,igx,idg)
c
c     functions:
c       determine whether the simulated point in monte carlo simulation
c       is in the failure region or not.
c
c     input arguments:
c       tp   : deterministic parameters.
c       x    : simulated point in the x space.
c       mc   : aray of minimun cut sets.
c
c     output arguments:
c       idg  : index to show the trial point is in the failure region or not.
c              =1, the point is in the failure region.
c              =0, the point is not in the failure region.
c
c     calls: gfun.
c
c     called by: nmont.
c
c     last version: july 30, 1987 by h.-z. lin.
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine nsumpt(tp,x,mc,igx,igfx,idg,mcst,mcend)

      implicit   none

      include   'cuts.h'
      include   'grad.h'
      include   'prob.h'

      real    (kind=8) :: x(nrx),tp(*)
      integer (kind=4) :: igx(ngfc),igfx(ngf,3),mc(ntl,2)

      integer (kind=4) :: idg, mcst,mcend

      integer (kind=4) :: i, ic,ig,ig4, imm
      real    (kind=8) :: gx
c     save

      flgf=.true.
      flgr=.false.
      do 10 ig=1,ngfc
      ig4=igfx(ig,3)
      call ugfun(gx,x,tp,ig4)
      if(gx.le.0.0d0) then
c     call mprint(x,1,nrx,1)
c     print *,'gx=',gx
        igx(ig)=0
      else
        igx(ig)=1
      endif
 10   continue
      idg=1
      imm=1
      do 100 i=mcst,mcend
      if(mc(i,1).ne.0) then
          ic=mc(i,2)
        if(mc(i,1).lt.0) then
          imm=imm*igx(ic)
        else
          imm=imm*(1-igx(ic))
        endif
      else
        idg=idg*(1-imm)
        imm=1
      endif
 100  continue
      idg=1-idg

      end subroutine nsumpt
