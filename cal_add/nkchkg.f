c*****************************************************
      subroutine nkchkg(nrmx,nr,isflg,rr,g0,gtor,pp)
c*****************************************************
      implicit   none

      integer*4 nrmx,nr
      integer*4 isflg(nrmx)
      real*8 rr(nrmx),g0,pp,gtor
c
      integer*4 i,ist,ied
      real*8 rst,red,p1,p2
c
      if(nr.eq.0) then
        if(g0.lt.0.0d0) then
          pp=1.0d0
        else
          pp=0.0d0
        endif
      elseif(nr.eq.1) then
        if(rr(1) .eq. 0.0d0) then
          pp=0.5d0
        else
          call dnorm(-rr(1),pp)
          if(isflg(1).eq.-1) then
            pp=1.0d0-pp
          elseif(isflg(1).eq.0) then
            if(rr(1).gt.0.0d0) then
              if(g0.lt.-gtor*1.0001d0) then
                pp=1.0d0-pp
              endif
            else
              if(g0.gt.gtor*1.0001d0) then
                pp=1.0d0-pp
              endif
            endif
          endif
        endif
      else
        pp=0.0d0
        call nkorder(nr,rr,isflg)
        do i=1,nr-1
          ist=isflg(i)
          ied=isflg(i+1)
          if( (ist.eq.0.and.ied.eq.-1) .or.
     *        (ist.eq.1.and.ied.eq.-1) .or.
     *        (ist.eq.1.and.ied.eq. 0) ) then
            rst=rr(i)
            red=rr(i+1)
            call dnorm(rst,p1)
            call dnorm(red,p2)
            pp=pp+p2-p1
          endif
        enddo
        if(isflg(1).eq.-1) then
           call dnorm(rr(1),p1)
           pp=pp+p1
        endif
        if(isflg(nr).eq.1) then
           call dnorm(-rr(nr),p1)
           pp=pp+p1
        endif
      endif

      end subroutine nkchkg
c******************************************
      subroutine nkorder(nr,rr,isflg)
c******************************************
      implicit   none

      integer*4 nr,isflg(*)
      real*8 rr(*)
c
      real*8 wrj
      integer*4 i,j,isj
c
      do i=1,nr-1
        do j=i+1,nr
          if(rr(j).lt.rr(i)) then
            wrj=rr(j)
            isj=isflg(j)
            rr(j)=rr(i)
            rr(i)=wrj
            isflg(j)=isflg(i)
            isflg(i)=isflg(j)
          endif
        enddo
      enddo

      end subroutine nkorder
