!******************************************************************
      subroutine sciseng(nset,ndro,not,npr,nsm,iopt,iflag,
     *                   isys,cov,ro,stp,y1,rrr,avec,bvec,pf,cvar,
     *                   nind,nda,amat,init,itst,stpsav,cum,cum2)
!******************************************************************
!
!     input nset            : number of limit state functions
!           ndro            : dimention of r matrix
!           ns0             : restart file id
!           not             : output file id
!           npr             : printout flag
!           nsm             : maximum number of simulations
!           ist             : restart flag
!           iopt            : r.v. geneartion flag
!           iflag           : =0 without lower bonud
!                             =1 with    lower bound
!           cov             : threshold for coefficient of variation
!           ro(nset,nset)   : correlation matrix between components
!           stp             : seed for r.n. generator
!           y1              : work area
!           avec            : lower bound for each compnent
!           bvec            : upper bound for each compnent
!                             (beta value for each comp.)
!     output  pf            : probability of failure
!
!**********************************************************************
      implicit   none

      real*8,allocatable::rovec(:)
      integer*4 nset,ndro,not,npr,nsm,iopt,iflag,isys
      integer*4 nind,nda,init
      real*8 ro(ndro,*),stp,y1(*),avec(*),bvec(*),cov,rrr(*)
      real*8 amat(nda,*),cvar,stpsav
!
      integer*4 nro,i1,n1,n,jjj,itst,ier,i,j
      real*8 cum,cum2,yyy,pf,sp,pff,dev,btg,rnm,ysum
!
!     ------ inverse of ro -----
!
      nro=nind*(nind+1)/2
      allocate(rovec(nro))
      call scroinv(nind,ndro,ro,rovec)
!
!     ----- for restart problem -----
!
      if(init.eq.1) then
        n1=itst
        i1=itst
        goto 30
      endif
!
      i1=0
      n1=0
      cum=0.0d0
      cum2=0.0d0
   30 n=npr
      n1=n+n1
      i1=i1+1
      write(not,'(1h ,'' '')')
      write(not,'(1h ,''============================= simulation'',
     *                '' ================================='')')
      write(not,2010)
!
!.... perform monte carlo simulation
!
      iopt = 1
      do 50 jjj=i1,i1+nsm-1
        itst=jjj
        call scgen(nind,stp,iopt,y1,rrr,yyy,rovec,bvec,avec,iflag)
        stpsav=stp
        if(nset.eq.nind) goto 25
        do i=1,nset-nind
          ysum=0.0d0
          do j=1,nind
            ysum=ysum+amat(i,j)*y1(j)
          enddo
          if(bvec(i+nind).lt.ysum) then
            yyy=0.0d0
            goto 25
          endif
        enddo
   25   continue
        iopt=0
        cum=cum+yyy
        cum2=cum2+yyy*yyy
        if(jjj.lt.n1) go to 50
        rnm=real(jjj)
        if(isys.eq.1) then
        pf=1-cum/rnm
        else
        pf=cum/rnm
        endif
        sp=cum2-cum*cum/rnm
        sp=sp/(rnm*(rnm-1))
        pff=1.d0-pf
        if(sp.gt.0.d0) then
          dev=sqrt(sp)
          cvar=dev/pf
          call dnormi(btg,pff,ier)
          write(not,2020) jjj,pf,btg,cvar
          if(jjj.ge.10.and.cvar.le.cov) then
            goto 60
          endif
        else
          write(not,2030) jjj,pf
!     *        jjj,pf,cvar,btg
        endif
        n1=n+n1
  50  continue
!
!  60  write(ns0,rec=1) cum,sbet,min(jjj,nsm)
  60  continue
!
 2010 format('      trials',13x,'pf-mean ',9x,'betag-mean ',4x,
     1       'coef of var of pf')
 2020 format(i12,1p,3x,1pe17.7,3x,1pe17.7,3x,1pe19.7,3x,1p4e17.7)
 2030 format(i12,1p,3x,1pe17.7,3x,'-----------------',3x,
     *       '-------------------')

      deallocate(rovec)

      end subroutine sciseng
