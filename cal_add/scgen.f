c******************************************************
      subroutine scgen(nset,stp,iopt,y1,rrr,yyy,rovec,bvec,avec,iflag)
c******************************************************
c
c     generate y1(k) conditioned by y(1)-y(k-1)
c
c********************************************************
      implicit   none

      integer*4 nset,iopt,iflag
      real*8 y1(*),rovec(*),avec(*),bvec(*),stp,yyy,rrr(*)
c
      real*8 phib,phia,pf,am,sm,zb,za
      integer*4 i,ier,k,nl
c     ----- step 1 generate of y1 -----
c     ------ generation of [0,1] uniform
      call gguw(stp,nset,iopt,rrr)
      call dnorm(bvec(1),phib)
      if(iflag.eq.0) then
        phia=0.0d0
      else
        call dnorm(avec(1),phia)
      endif
      pf=phib*rrr(1)+(1.0d0-rrr(1))*phia
      call dnormi(y1(1),pf,ier)
c
c     ----- step k generation of y(k) -----
c
      nl=0
      yyy=phib-phia
      do k=2,nset
c       ----- step.k-1 calcu mean -----
        am=0.0d0
        do i=1,k-1
          nl=nl+1
          am=am-rovec(nl)*y1(i)
        enddo
        nl=nl+1
        am=am/rovec(nl)
        sm=dsqrt(1.0d0/rovec(nl))
        zb=(bvec(k)-am)/sm
        call dnorm(zb,phib)
        if(iflag.eq.0) then
          phia=0.0d0
        else
          za=(avec(k)-am)/sm
          call dnorm(za,phia)
        endif
c       ------ generation of [0,1] uniform
        pf=phib*rrr(k)+(1.0d0-rrr(k))*phia
        call dnormi(y1(k),pf,ier)
        y1(k)=y1(k)*sm+am
        yyy=yyy*(phib-phia)
      enddo

      end subroutine scgen
