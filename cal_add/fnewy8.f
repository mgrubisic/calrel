ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine fnewy5(x,y,a,d,yt,dxz,gam,tf,ex,sg,par,bnd,tp,igt,lgp,
c                        ids,ipa,ib,izx,ixz,sdgy,bt,iter,gx,ig)
c
c   functions:
c      compute new x and y using improved rf method.
c      the descent function des = y*y/2 + op2*|gx| .
c      use armijo line search method.
c
c   input arguments:
c      x : current x.
c      y : current y.
c      a : unit normal vector in the y space.
c      d : direction of search.
c      yt : temporary y.
c      tf : the transformation matrix from z to y, i.e. y = tf * z.
c      ex : means of x.
c      sg : standard deviations of x.
c      par : distribution parameters of x.
c      bnd : lower and upper bound of x.
c      tp : deterministic parameters in performance function.
c      igt : group types.
c      lgp : location of the first element of a group in y.
c      ids : distribution numbers of x.
c      ipa : #'s of the associated rv of distribution parameters.
c             n-th element of ipa = 0 means parameter n is constant.
c      ib : code of distribution bounds of x.
c           = 0 , no bounds
c           = 1 , bounded from below.
c           = 2 , bounded from above.
c           = 3 , both sides bounded.
c      izx : indicators for z to x.
c      ixz : indicators for x to z.
c      sdgy : |dg/dy|.
c      iter : iteration number.
c      gx : g(x).
c      ig : failure mode number.
c
c   output arguments:
c      x : new x.
c      y : new y.
c      dxz : dx/dz.
c      gam : unit normal vector in the z space.
c      bt : new reliability index.
c
c   calls: ugfun, cdot, cytox, fdirct.
c
c   called by: form.
c
c   last revision: april 6, 1995 by c.-c. li
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine fnewy8(x,y,a,d,yt,dxz,gam,tf,ex,sg,par,bnd,tp,igt,lgp,
     *            ids,ipa,ib,izx,ixz,sdgy,bt,iter,gx,ig,gdgx)

      implicit   none

      include   'file.h'
      include   'flag.h'
      include   'grad.h'
      include   'prob.h'
      include   'optm.h'

      integer (kind=4) :: ig
      real    (kind=8) :: sdgy, bt, gx
      real    (kind=8) :: x(nrx),y(nry),a(nry),d(nry),yt(nry),dxz(nrx)
      real    (kind=8) :: tf(ntf),ex(nrx),sg(nrx),par(4,nrx),bnd(2,nrx)
      real    (kind=8) :: gam(nry),tp(*)
      integer (kind=4) :: ids(nrx),ib(nrx),igt(nig),lgp(3,nig)
      integer (kind=4) :: izx(nry),ixz(nrx),ipa(4,nrx),gdgx(nrx)

      integer (kind=4) :: i,iii, iter
      real    (kind=8) :: alpha
      real    (kind=8) :: fmerit
      real    (kind=8) :: g0
      real    (kind=8) :: s
      real    (kind=8) :: t, thetaf, tmp1

      real    (kind=8) :: cdot
c---  des = y*y/2 + op2*|gx|.
c
      flgf=.true.
      flgr=.false.
      if(igr.ne.0) flgr=.true.
      tmp1=cdot(y,y,1,1,nry)
      if(dabs(sdgy).eq.0.0d0) then
        print *,'warning norm of gradg is zero'
        stop
      endif
      s=gx/sdgy+cdot(y,a,1,1,nry)
c
c*****************************************************************
c
c     the algorithm below is transplanted from opensees
c     which was implemented by t.haukkas and j.o. royset
c
c******************************************************************
c
      if(iter.eq.1) then
        if(gx.gt.15.0d0.or.gx.lt.2.0d0) then
        write(not,'(1h ,'' warning: the start value of the limit'',
     *                  '' state function is outside'')')
        write(not,'(1h ,'' the ideal range for fastest convergence of'',
     *                  '' the polak-he algorithm'')')
        endif
      endif
c
c     ----- calculate direction -----
c
      call fnpolak(not,nry,gx,y,a,sdgy,d,thetaf)
c
c     ----- line search -----
c
      g0=gx
      alpha=1.0d0
c----------------------------------------------------------------------
c     perform armijo line search.
c----------------------------------------------------------------------
      do 60,iii=1,ni2
c        ----- trial point -----
         do 70 i=1,nry
            yt(i) = y(i) + alpha*d(i)
   70    continue
c---     compute new x and gx
         call cytox(x,yt,dxz,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
         call ugfun(gx,x,tp,ig)
       call fnmerit(nry,y,yt,g0,gx,fmerit)
       if(fmerit.le.alpha*thetaf*dabs(op2)) goto 90
       alpha=alpha*dabs(op1)
60    continue
      alpha=alpha/dabs(op1)
      print *,'warning: the number of iteration in armijo line search',
     &        ' method exceeds ni2'
      stop
90    do 100 i=1,nry
         y(i)=yt(i)
100   continue
      call cytox(x,y,dxz,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call ugfun(gx,x,tp,ig)
      call fdirct(x,y,tp,ex,sg,dxz,gam,a,tf,par,bnd,igt,lgp,ids,
     &            ipa,izx,ixz,sdgy,gdgx,ig,0)
      tmp1=cdot(y,y,1,1,nry)
      bt=dsqrt(tmp1)
      t=cdot(a,y,1,1,nry)
      if(t.lt.0.d0) bt=-bt

      end subroutine fnewy8
