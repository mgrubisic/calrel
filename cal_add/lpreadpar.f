      subroutine lpreadpar(
     x nam1,outlev,minmax,maxmn,namstr,objnam,rhsnam,bndnam,rngnam,
     x bigb,inslck)

      implicit   none

c common declarations

      real*8         tpiv1,tpiv2,tabs,trabs,lam,tfind,order,supdens
      common/lpfactor/ tpiv1,tpiv2,tabs,trabs,lam,tfind,order,supdens
c
      integer*4      psupn,ssupn,maxsnz
      common/lpsprnod/ psupn,ssupn,maxsnz
c
      real*8        tsdir,topt1,topt2,tfeas1,tfeas2,feas1,feas2,
     x              pinfs,dinfs,inftol
      integer*4     maxiter
      common/lptoler/ tsdir,topt1,topt2,tfeas1,tfeas2,feas1,feas2,
     x              pinfs,dinfs,inftol,maxiter

c
      real*8        tplus,tzer
      common/lpnumer/ tplus,tzer
c
      real*8        tresx,tresy
      integer*4     maxref
      common/lpitref/ tresx,tresy,maxref
c
      real*8        objnor,rhsnor,scdiff
      integer*4     scpass,scalmet
      common/lpascal/ objnor,rhsnor,scdiff,scpass,scalmet
c
      real*8        climit,ccorr
      common/lpcompl/ climit,ccorr
c
      real*8        ccstop,barset,bargrw,barmin
      integer*4     mincor,maxcor,inibar
      common/lppredp/ ccstop,barset,bargrw,barmin,mincor,maxcor,inibar
c
      real*8        target,tsmall,tlarge,center,corstp
      integer*4     mincc,maxcc
      common/lppredc/ target,tsmall,tlarge,center,corstp,mincc,maxcc
c
      real*8        palpha,dalpha
      common/lpparam/ palpha,dalpha
c
      real*8        tfixvar,tfixslack,slklim
      common/lpdrop/  tfixvar,tfixslack,slklim
c
      real*8        prmin,upmax,dumin
      integer*4     stamet,safmet,premet,regul
      common/lpinitv/ prmin,upmax,dumin,stamet,safmet,premet,regul
c
      real*8          varadd,slkadd,scfree
      common/lpmscal/ varadd,slkadd,scfree
c
      real*8           maxdense,densgap
      integer*4                         setlam,denslen
      common/lpsetden/ maxdense,densgap,setlam,denslen
c
      integer*4        loglog,lfile
      common/lplogprt/ loglog,lfile

      character*12 nam1
      character*40 namstr
      character*8 objnam,rhsnam,bndnam,rngnam
      real*8 bigb
c
      integer*4   outlev,minmax,maxmn,inslck

      integer (kind=4) :: ismethod1, ismethod2, ispasses1, ispasses2
!     integer (kind=4) :: maxsnzpinfs
c
      objnam(1:8)='object  '
      rhsnam(1:8)='rhs     '
      bndnam(1:8)='bnd     '
      rngnam(1:8)='rngnam  '
      bigb=1.0d+15   !bigbound
      inslck=0       !explslack
      loglog=3       !iterlog
      maxmn =250000  !maxmn
      varadd=1.0d-12 !varadd
      slkadd=1.0d+16 !slackadd
      tpiv1=1.0d-3   !tpiv1
      tpiv2=1.0d-8   !tpiv2
      tabs=1.0d-12   !tabs
      trabs=1.0d-15  !trabs
      lam=1.1d-5     !lam
      tfind=25.d0    !tfind
      order=2.0d0    !ordering
      psupn=4        !psuipnode
      maxsnz=99999999 !maxsnz
      ssupn=4        !ssupnode
      topt1=1.0d-8   !topt1
      topt2=1.0d-16  !topt2
      tfeas1=1.0d-7  !tfeas1
      tfeas2=1.0d-7  !tfeas2
      feas1=1.0d-2   !feas1
      feas2=1.0d-2   !feas2
      pinfs=1.0d-6   !pinfs
      dinfs=1.0d-6   !dinfs
      inftol=1.0d4   !inftol
      denslen=10     !denslen
      tplus=1.0d-10  !tplus
      tzer=1.0d-35   !tzer
      palpha=0.999d0 !prdare
      tresx=1.0d-9   !tresx
      tresy=1.0d-9   !tresy
      maxref=5       !maxref
      dalpha=0.999d0 !dudare
      ismethod1=2    !smethod1
      scalmet=ior(ismethod1,iand(scalmet,32512))
      ismethod2=0    !smethod2
      scalmet=ior(256*ismethod2,iand(scalmet,255))
      objnor=1.0d2   !objnorm
      rhsnor=0.0d0   !rhsnorm
      scdiff=1.0d0   !mindiff
      ispasses1=5    !spasses1
      scpass=ior(ispasses1,iand(scpass,32512))
      ispasses2=0    !spasses2
      scpass=ior(256*ispasses2,iand(scpass,255))
      ccstop=1.01d0  !stopcor
      barset=2.5d-1  !barset
      bargrw=1.0d2   !bargrow
      barmin=1.0d-10 !barmin
      mincor=1       !mincorr
      maxcor=1       !maxcorr
      inibar=0       !inibar
      target=9.0d-2  !target
      tsmall=2.0d-1  !tsmall
      tlarge=2.0d+1  !tlarge
      center=5.0d0   !center
      corstp=1.01d0  !corstop
      mincc=0        !minccorr
      maxcc=9        !maxccorr
      climit=1.0d0   !complimit
      ccorr=1.0d-5   !comppar
      tfixvar=1.0d-16 !tfixvar
      tfixslack=1.0d-16 !tfixslack
      slklim=1.0d-16 !dslacklim
      prmin=100.0d0  !prmin
      upmax=50000.d0 !upmax
      dumin=100.d0   !dumin
      stamet=2       !smethod
      safmet=-3      !safemet
      regul=0        !regularize
      premet=511     !presolv
      maxiter=99     !maxiter
      maxdense=0.15d0 !maxdense
      densgap=3.0d0   !densgap
      setlam=0        !setlam
      outlev=1        !output
      minmax=1        !minmax
      scfree=1.0d-6   !scfree
      tsdir=1.0d-16   !tsdir
      supdens=300.d0  !supdense
      return
c name      =               ! problem name, max. 36 characters
c maxmn     =  250000.      ! upper bound for m+n
c minmax    =  1.           !  =1. (mininmize)        =-1. (maximize)
c output    =  no           ! output level 0 :only statistic, 1 :solution
c objnam    =               ! objective function name  (first n type row)
c rhsnam    =               ! rhs   name   (first)
c bndnam    =               ! bound name   (first)
c rngnam    =               ! range name   (first)
c bigbound  = 1.0d+15       ! skip bounds and ranges exceeding this limit
c iterlog   =  3.           ! 0:no report, 1:only display, 2:only logfile, 3:bot
c maxdense  = 0.15          ! maximal dense columns rate
c densgap   = 3.00          ! density gap parameter
c denslen   = 10.           ! value for the possibily dense columns
c supdens   = 300.          ! 'super' dense column length
c setlam    = 0.            !  0 : density gap heuristic, ( aat )
c                          ! -1 : use lam, ( aat )
c                          !  1 : density gap heuristic, factorize
c ! supernode parameters
c psupnode  =  4.           ! primer supernode length
c ssupnode  =  4.           ! secunder supernode length
c maxsnz    = 99999999.     ! maximum nonzeros in one supernode
c ! pivot and factorization parameters
c tpiv1     = 1.0d-03       ! first  threshold pivot tolerance
c tpiv2     = 1.0d-08       ! second threshold pivot tolerance
c tabs      = 1.0d-12       ! absolute pivot tolerance for the first factorizati
c trabs     = 1.0d-15       ! absolute pivot tolerance during the algorithm
c lam       = 1.1d-05       ! minimum value of lambda
c tfind     = 25.           ! pivot search loop count
c ordering  = 2.0           !  1.0 : minimum degree
c                           !  2.0 : minimum fill-in
c                           !  3.0 : minimum fill-in for augmented system
c                           ! other: no ordering
c ! stopping criterion parameters
c topt1     = 1.0d-08       ! relative duality gap tolerance
c topt2     = 1.0d-16       ! average complementary gap tolerance
c tfeas1    = 1.0d-07       ! relative primal feasibility tolerance
c tfeas2    = 1.0d-07       ! relative dual   feasibility tolerance
c feas1     = 1.0d-02       ! absolute primal feasibility tolerance
c feas2     = 1.0d-02       ! absolute dual   feasibility tolerance
c pinfs     = 1.0d-06       ! minimum primal  infeasibility reduction
c dinfs     = 1.0d-06       ! minimal dual    infeasibility reduction
c inftol    = 1.0d+04
c tsdir     = 1.0d-16       ! search direction maximum norm tolerance
c maxiter   = 99.           ! iteration limit
c ! numerical tolerances
c tplus     = 1.0d-10       ! relative addition tolerance
c tzer      = 1.0d-35       ! relative zero tolerance
c ! iterative refinement tolerances
c tresx     = 1.0d-09       ! acceptable residual in the primal space
c tresy     = 1.0d-09       ! acceptable residual in the dual space
c maxref    = 5.            ! maximal number of refinement
c ! scaling parameters      ! scaling methods:
c                           !  0. : no scaling
c                           !  1. : simple scaling to rowmax=1, colmax=1
c                           !  2. : geometric mean scaling + simple scaling
c                           !  3. : curtis-reid's algorithm + simple scaling
c                           !  4. : geometric mean scaling only
c                           !  5. : curtis-reid's algorithm only
c objnorm   = 1.0d+02       ! scaling the objective to this max. norm
c rhsnorm   = 0.0d+00       ! scaling the rhs to this max. norm
c mindiff   = 1.0d+00       ! minimum treshold for exit from scaling
c                           ! before aggregator
c spasses1   = 5.           ! maximum number of passes ( <128 )
c smethod1   = 2.           ! scaling method
c                           ! after aggregator
c spasses2   = 0.           ! maximum number of passes ( <128 )
c smethod2   = 0.           ! scaling method
c ! complementary gap improwing  (use it in panic situation only)
c complimit = 1.0d+00       ! improve if compl. < complimit
c comppar   = 1.0d-05       ! improve if compl. < comppar * average compl.
c ! predictor-corrector and barrier parameters
c stopcor   = 1.01d+00      ! correction stop parameter
c barset    = 2.50d-01      ! barrier set-up limit
c bargrow   = 1.00d+02      ! barrier grow controll
c barmin    = 1.00d-10      ! minimum barrier threshold
c mincorr   = 1.            ! number of the minimum corrections
c maxcorr   = 1.            ! number of the maximum corrections
c inibarr   = 0.            ! use initial barrier parameter
c  centrality corrections parameters
c target    = 9.0d-02       ! trial steplength improvement
c tsmall    = 2.0d-01       ! small complementarity bound
c tlarge    = 2.0d+01       ! large compelmentarity bound
c center    = 5.0d+00       ! centrality force
c corstop   = 1.01d+00      ! correction stop parameter
c minccorr  = 0.            ! number of the minimum corrections
c maxccorr  = 9.            ! number of the maximum corrections
c ! steplenth parameters
c prdare    = 0.999d+00     ! maximal primal steplength
c dudare    = 0.999d+00     ! maximal dual   steplength
c ! variable fixing tolerances
c tfixvar   = 1.0d-16       ! variable reset parameter
c tfixslack = 1.0d-16       ! slack reset parameter
c dslacklim = 1.0d-16       ! dual slack variable limit
c ! starting point paramerers
c prmin     =   100.00      ! minimum initial variable value
c upmax     = 50000.00      ! maximum initial variable value
c dumin     =   100.00      ! minimum initial slack value
c smethod   =  2.           ! starting method  (1. or 2.)
c safemet   = -3.           ! safe method  (1. 2. or 3.)
c regularize=  0.           ! regularization (introducing dummy ranges)
c explslack =  0.           ! include slack as variables
c presolv   = 511.          !   1 : singleton row check
c                           !   2 : singleton column check
c                           !   4 : min/max row value check
c                           !   8 : cheap dual test
c                           !  16 : dual check
c                           !  32 : primal bound check and relaxation
c                           !  64 : search identical variables
c                           ! 128 : elimination of free variables
c                           ! 256 : sparser
c                           ! 512 : extended dual test
c varadd    = 1.0d-12
c slackadd  = 1.0d+16
c scfree    = 1.0d-06
      end subroutine lpreadpar
