c***************************************************
      subroutine nknewt(nry,nrx,ntf,nig,ifunc,
     *                  igt,lgp,ids,ipa,ib,izx,ixz,tp,tf,ex,sg,par,bnd,
     *                  gtor,y0,alpha,x1,y1w,z1w,dgx,gam,avec,gdgx,
     *                  rr1,gg1,rr2,dgdun,ind1,rr,isflg,indx,neval)
c***********************beta,root****************************
      implicit   none

      include   'file.h'

      integer (kind=4) :: nry,nrx,ntf,nig,ifunc,indx
      real    (kind=4) :: x1(nrx),y1w(nry),z1w(nry),y0(nry)
      real    (kind=4) :: alpha(nry),dgx(*),gam(*)
      real    (kind=4) :: tf(ntf),ex(nrx),sg(nrx),par(4,nrx)
      real    (kind=4) :: bnd(2,nrx),avec(*)
      integer (kind=4) :: igt(nig),lgp(3,nig),ids(nrx),ipa(4,nrx)
      integer (kind=4) :: ib(nrx),izx(nry)
      integer (kind=4) :: isflg,neval,ixz(nrx)
      real    (kind=4) :: rr,tp(*),gtor,dgdun,gdgx(nrx)
      real    (kind=4) :: rr1,rr2,gg1,sdgz,ppp,fai,dbeta,dgfun
      integer (kind=4) :: ind1
c
      real    (kind=4) :: rmax,rmin,r1,r2,r3,g1,g3,r4,cdot
      integer (kind=4) :: kp,i
c
      rmax = max(rr1,rr2)
      rmin = min(rr1,rr2)
      r1 = rr1
      r2 = rr2
      g1 = gg1
c
      kp = 0
  201 continue
      kp = kp+1
      r3 = r1-g1/dgdun
      if(r3.lt.rmin.or.r3.gt.rmax) then
        if(g1*g3.lt.0.0d0) then
          r3 = (r1+r3)/2.0d0
        endif
      else
      endif
      do i = 1,nry
        y1w(i) = y0(i)+r3*alpha(i)
      enddo
      call cytox(x1,y1w,z1w,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call ugfun(g3,x1,tp,ifunc)
      neval = neval+1
      call dnorm(-r3,ppp)
      fai = exp(-0.5d0*r3*r3)/2.506628275d0
      dbeta = 0.001d0*ppp/fai
      dgfun = dbeta/abs(dgdun)
      if(abs(g3).le.dgfun.or.kp.ge.100) then
        r4 = r3
        rmax = max(r2,r3)
        rmin = min(r2,r3)
        if(r4.lt.rmin.or.r4.gt.rmax) r4 = (r2+r3)/2.d0
        rr = r4
        if(indx.eq.0) then
          if(ind1.eq.1) then
            isflg = 1
          else
            isflg = -1
          endif
        else
          if(ind1.eq.1) then
            isflg = -1
          else
            isflg = 1
          endif
        endif
        if(kp.ge.100) write(not,309)
 309    format('  warning: root is not solved within 100 steps')
        return
      endif
      if(g1*g3.lt.0.0d0) then
        r2 = r1
      endif
      r1 = r3
      g1 = g3

      call fdirct(x1,y1w,tp,ex,sg,dgx,gam,avec,tf,par,bnd,igt,lgp,
     *           ids,ipa,izx,ixz,sdgz,gdgx,ifunc,0)
      neval = neval+1
      do i = 1,nry
        avec(i) = -avec(i)*sdgz
      enddo
      dgdun = cdot(avec,alpha,nry,nry,nry)
      go to 201

      end subroutine nknewt
