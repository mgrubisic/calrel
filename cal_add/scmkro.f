c******************************************************
      subroutine scmkro(not,nset,iset,nry,alpha,ndro,ro,igfx,
     *                  nind,iord)
c******************************************************
      implicit   none

      integer*4,allocatable::iprint(:)
      integer*4 nind,not,nset,nry,iset(*),ndro,igfx(*),iord(*)
      real*8 alpha(nry,*),ro(ndro,*)

c
      integer*4 i,j,k,ii,jj
      real*8 ysum,ai,aj
      allocate(iprint(nset))
c
c      do i=2,nset
      do i=2,nind
c        ii=iset(i)
        ii=iset(iord(i))
        do j=1,nind-1
          ysum=0.0d0
c          jj=iset(j)
          jj=iset(iord(j))
          do k=1,nry
            ai=alpha(k,iabs(ii))
            if(ii.lt.0) ai=-ai
            aj=alpha(k,iabs(jj))
            if(jj.lt.0) aj=-aj
            ysum=ysum+ai*aj
          enddo
          ro(i,j)=ysum
        enddo
      enddo
      do i=1,nset
        ro(i,i)=1.0d0
c+1.0d-8
      enddo
      do i=1,nind-1
      do j=i+1,nind
        ro(i,j)=ro(j,i)
      enddo
      enddo
c
      do i=1,nind
        ii=iord(i)
        ii=iset(ii)
        if(ii.gt.0) then
          iprint(i)=igfx(ii)
        else
          iprint(i)=-igfx(iabs(ii))
        endif
      enddo
c
      write(not,'(1h ,''--------------- correlation matrix'',
     *                '' ---------------'')')
      write(not,'(1h ,5x,20i15)')(iprint(i),i=1,nind)
      do i=1,nind
      write(not,'(1h ,i5,1p20e15.5)') iprint(i),(ro(i,j),j=1,nind)
      enddo
cc
c      call covinv(ndro,nset,ro)
c
      deallocate(iprint)

      end subroutine scmkro
c********************************************************************
      subroutine covinv(nd,n,a)
c********************************************************************
      implicit   none

      real*8,allocatable::b(:,:)
      integer*4 nd,n
      real*8 a(nd,*)
      integer*4 i,j,k,l
      real*8 ysum
c
      allocate(b(n,n))
c
      do i=1,n
        ysum=a(i,i)
        do k=1,i-1
          ysum=ysum-a(k,i)**2
        enddo
        a(i,i)=sqrt(ysum)
        do j=i+1,n
          ysum=a(i,j)
          do k=1,i-1
            ysum=ysum-a(k,i)*a(k,j)
          enddo
          a(i,j)=ysum/a(i,i)
        enddo
      enddo
c
      do i=2,n
      do j=1,i-1
        a(i,j)=0.0d0
      enddo
      enddo
c
      do i=1,n
      do j=1,n
        b(i,j)=a(i,j)
      enddo
      enddo
c
      do j=n,1,-1
        b(j,j)=1.0d0/a(j,j)
        do k=j-1,1,-1
          ysum=0.0d0
          do l=k+1,n
            ysum=ysum+a(k,l)*b(l,j)
          enddo
          b(k,j)=-ysum/a(k,k)
        enddo
      enddo
      do i=2,n
      do j=1,i-1
        b(i,j)=0.0d0
      enddo
      enddo
      do i=1,n
      do j=1,n
        ysum=0.0d0
        do k=1,n
          ysum=ysum+b(i,k)*b(j,k)
        enddo
        a(i,j)=ysum
      enddo
      enddo
      deallocate(b)

      end subroutine covinv
