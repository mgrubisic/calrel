c***************************************************************8
      subroutine opnsqp(not,nry,nset,ncset,ngfc,icomps,ncomp,
     *                  ids,ib,ipa,izx,ixz,lgp,igt,
     *                  zzz,yyy,bnd,tf,par,sg,ex,tp,alp,gam,dgx,gdgx,
     *                  ydesw,btdesw,alphaw)
c***************************************************************8
      implicit   none

      include 'o8comm.h'
      integer*4,allocatable::ifunc1(:),ifunc2(:)
      integer*4 not,nry,nset,ncset(*),ngfc,icomps(2,ngfc,*),ncomp
      integer*4 ids(*),ib(*),ipa(4,*),izx(*),ixz(*),lgp(3,*),igt(*)
      real*8 zzz(*),yyy(*),bnd(2,*),tf(*),par(4,*),sg(*),ex(*),tp(*)
      real*8 alp(*),gam(*),dgx(*),gdgx(*),anorm
      real*8 ydesw(nry,*),btdesw(*),alphaw(nry,*),cdot
c
      integer*4 i,j,mconst
c
c
      allocate(ifunc1(ngfc))
      allocate(ifunc2(ngfc))
c
      write(not,'(1h ,''   '')')
      write(not,'(1h ,''  solve design point by sqp  '')')
      write(not,'(1h ,''   '')')
      do i=1,nset
         mconst=ncset(i)
         do j=1,mconst
           ifunc1(j)=icomps(1,j,i)
           ifunc2(j)=icomps(2,j,i)
         enddo
         call donlp2(nry,mconst,ifunc1,ifunc2,ids,ib,ipa,izx,ixz,
     *               lgp,igt,zzz,yyy,bnd,tf,par,sg,ex,tp,
     *               alp,gam,dgx,gdgx)
         do j=1,nry
           ydesw(j,i)=x(j)
         enddo
      enddo
      ncomp=nset
      do i=1,nset
        anorm=cdot(ydesw(1,i),ydesw(1,i),nry,nry,nry)
        btdesw(i)=dsqrt(anorm)
        if(btdesw(i).lt.1.0d-8) then
        do j=1,nry
          alphaw(j,i)=1.0d0/dsqrt(dfloat(nry))
        enddo
        else
        do j=1,nry
          alphaw(j,i)=ydesw(j,i)/btdesw(i)
        enddo
        endif
      enddo

      deallocate(ifunc1)
      deallocate(ifunc2)

      end subroutine opnsqp
