c****************************************************
      subroutine scsizero(amat,amat2,izero,nind,nry)
c****************************************************
      implicit   none

      integer*4 nind,nry,izero(*)
      real*8 amat(nind,nry),amat2(nind,nry)
c
      integer (kind=4) :: i,iizero, j, nnn,nzero

      do j=1,nry
        izero(j)=0
      enddo
c
!WIP set nzero - replace zero by 0.0d0
      nzero = 0
      do j=1,nry
        iizero=0
        do i=1,nind
          amat2(i,j)=amat(i,j)
          if(abs(amat(i,j)).gt.0.0d0) then
           iizero=1
          endif
        enddo
        if(iizero.eq.0) then
          nzero=nzero+1
          izero(j)=1
        endif
      enddo
c
      if(nzero.eq.0) return
c
      nnn=0
      do j=1,nry
        if(izero(j).eq.0) then
          nnn=nnn+1
          do i=1,nind
            amat(i,nnn)=amat2(i,j)
          enddo
        endif
      enddo
c
      do j=nnn+1,nry
      do i=1,nind
         amat(i,j)=0.0d0
      enddo
      enddo

      end subroutine scsizero
