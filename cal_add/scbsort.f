c**************************************************************
      subroutine scbsort(not,ncset,ndp,ptable,min,max,next)
c**************************************************************
c
c     preparation for bi-modal bounds for system problem
c     make order information for each cutset
c
c     input     ncset  :  number of cutset
c               ndp    :  normal size of ptable
c               ptable :  prob. table for bi-modal joint cut set
c
c     output    min    :  seq id of cut set with minimum prob
c               max    :  seq id of cut set with maximum prob
c               next(i):  seq id of cut set with probability
c                         next to cut set i
c
c**************************************************************
      implicit   none

      integer*4 not,ncset,ndp,min,max,next(*)
      real*8 ptable(ndp,*)
c
      integer*4 k1,k2,i,ki,jp,j,nord,ic,inext
c
      k1=1
      k2=2
      if(ptable(k1,k1).ge.ptable(k2,k2)) go to 10
      min=2
      max=1
      next(2)=1
      next(1)=0
      go to 20
c
  10  min=1
      max=2
      next(1)=2
      next(2)=0
c
  20  if(ncset.eq.2) return
c
      do 60 i=3,ncset
      ki=i
      if(ptable(ki,ki).ge.ptable(min,min))go to 40
      if(ptable(ki,ki).le.ptable(max,max))go to 50
      jp=min
  35  j=next(jp)
      if(ptable(ki,ki).ge.ptable(j,j))go to 45
      jp=j
      go to 35
c
  45  next(jp)=i
      next(i)=j
      go to 60
c
  40  next(i)=min
      min=i
      go to 60
c
  50  next(max)=i
      next(i)=0
      max=i
c
  60  continue
c
      if(not.ne.0) then
      write(not,'(1h ,''  '')')
      write(not,'(1h ,''  ---- ordering probability of cutset -----'')')
      write(not,'(1h ,''  '')')
      write(not,'(1h ,''  ===== probability table ===== '')')
      write(not,'(1h ,''  '')')
      write(not,'(1h ,5x,15i15)') (i,i=1,ncset)
      do i=1,ncset
      write(not,'(1h ,i5,1p15e15.5)') i,(ptable(i,j),j=1,ncset)
      enddo
      write(not,'(1h ,''  ==== order of cutset ===== '')')
      write(not,'(1h ,''  max....................'',i5)') max
      write(not,'(1h ,''  min....................'',i5)') min
      nord=1
      write(not,'(1h ,2i10)') nord,min
      ic=min
   90 continue
      nord=nord+1
      inext=next(ic)
      ic=inext
      if(inext.eq.0) goto 99
      write(not,'(1h ,2i10)') nord,inext
      goto 90
   99 continue
      endif

      end subroutine scbsort
