c*****************************************************************
      subroutine scindvec(not,nry,mcomp,icomp,alpha,iord,nind,
     *                    isys,btdes)
c*****************************************************************
      implicit   none

      integer*4,allocatable::iflag(:),iflag2(:),iord2(:),izeroc(:)
      real*8,allocatable::amat(:,:),atemp(:),amat2(:,:)
      real*8 eps
      integer*4 not,nry,mcomp,iord(*),nind,icomp(*),isys
      real*8 alpha(nry,*),btdes(*)

      integer*4 i,j,ic,jc,ii,iii,iimax,imax,nord,inext,itemp,num,k,jj
      real*8 vmax,ysum,piv,com,coef,bti,btj,bi,bj
      allocate(iflag(mcomp))
      allocate(iflag2(mcomp))
      allocate(iord2(mcomp))
      allocate(izeroc(mcomp))

c     *** initialize iflag ***

      eps=1.0d-10

c$$$$$$$$$$$$$$$$$$$$$$4c      if(not.ne.0) then
      write(not,'(1h ,'' ### check for scindvec ### '')')
      write(not,'(1h ,'' nry.................'',i10)') nry
      write(not,'(1h ,'' mcomp...............'',i10)') mcomp
      do i=1,mcomp
      write(not,'(1h ,''  '')')
      write(not,'(1h ,'' component '',i5)') i
      write(not,'((1h ,'' alpha '',5e15.5))')
     *   (alpha(j,abs(icomp(i))),j=1,nry)
      enddo
c$$$$$$$$$$$$$$$$$c      endif

      do i=1,mcomp
        iflag(i)=0
        izeroc(i)=0
      enddo

c     *** eliminate one by one ***

c$$$$$$$$$$$$      if(not.ne.0) then
      write(not,'(1h ,'' '')')
      write(not,'(1h ,'' bi comp combination '')')
c$$$$$$$$$$$$      endif
      do i=1,mcomp-1
        if(iflag(i).eq.1) goto 10
        ic=abs(icomp(i))
        if(isys.eq.1) then
          if(icomp(i).gt.0) then
            bti=btdes(ic)
          else
            bti=-btdes(ic)
          endif
        else
          if(icomp(i).gt.0) then
            bti=-btdes(ic)
          else
            bti=btdes(ic)
          endif
        endif
        do j=i+1,mcomp
          jc=abs(icomp(j))
          if(isys.eq.1) then
            if(icomp(j).gt.0) then
              btj=btdes(jc)
            else
              btj=-btdes(jc)
            endif
          else
            if(icomp(j).gt.0) then
              btj=-btdes(jc)
            else
              btj=btdes(jc)
            endif
          endif
          ysum=0.0d0
          do k=1,nry
            ysum=ysum+alpha(k,ic)*alpha(k,jc)
          enddo
c$$$$$$$$$$$$$$c          if(not.ne.0) then
          write(not,'(1h ,'' '')')
          write(not,'(1h ,'' comp '',i5,'' and'',i5,'' prod'',e15.5)')
     *    ic,jc,ysum
c$$$$$$$$$$$$$c          endif
          ysum=abs(ysum)
          if(abs(ysum-1.0d0).lt.eps) then
            if(btj.le.bti) then
              iflag(i)=1
            else
              iflag(j)=1
            endif
c$$$$$$$$$$$$$$           if(not.ne.0) then
            write(not,'(1h ,'' '')')
            write(not,'(1h ,'' prod == 1 '',i5,'' is dependent'')') j
c$$$$$$$$$$$$$$$$$$$$c            endif
          endif
        enddo
   10   continue
      enddo
c     *** num of independent ***
c$$$$$$$$$$$$$$$      if(not.ne.0) then
      write(not,'(1h ,'' '')')
      write(not,'(1h ,'' independet flag after bi component '')')
      write(not,'((1h ,'' iflag...'',5i10))')(iflag(i),i=1,mcomp)
c$$$$$$$$$$$$$$$$c      endif

      nind=0
      do i=1,mcomp
        if(iflag(i).eq.0) then
          nind=nind+1
          iflag2(nind)=i
        endif
      enddo

c$$$$$$$$$$$$$$$$$      if(not.ne.0) then
      write(not,'(1h ,'' '')')
      write(not,'(1h ,'' num of indep after bi component '',i5)')nind
      write(not,'((1h ,'' inds...'',5i10))')(iflag2(i),i=1,nind)
      do i=1,nind
      write(not,'(1h ,''  '')')
      write(not,'(1h ,'' component '',i5)') iflag2(i)
      write(not,'((1h ,''alpha'',5e15.5))')
     *       (alpha(j,iabs(icomp(iflag2(i)))),j=1,nry)
      enddo
c$$$$$$$$$$$$$$$$$      endif

      allocate(amat(nind,nry))
      allocate(amat2(nind,nry))
      allocate(atemp(nry))

      if(nind.le.2) then
        do i=1,nind
          iord(i)=iflag2(i)
        enddo
        goto 100
      endif

c     *** find out first comp ***

c$$$$$$$$$$c      if(not.ne.0) then
      write(not,'(1h ,'' '')')
      write(not,'(1h ,'' find out first component '')')
c$$$$$$$$$$$$      endif
      vmax=0.0d0
      do k=1,nry
        do i=1,nind
          ii=iflag2(i)
          iii=abs(icomp(ii))
          if(abs(alpha(k,ii)).gt.vmax) then
            vmax=abs(alpha(k,iii))
            iimax=ii
            imax=i
          endif
        enddo
        if(vmax.gt.0.0d0) goto 20
      enddo
c     ---- all vectors are zero
      write(not,'(1h ,'' !! fatal error in scindvec !!'')')
      write(not,'(1h ,'' !! all ind vectors are zero!!'')')
      stop

   20 continue
c$$$$$$$$      if(not.ne.0) then
      write(not,'(1h ,'' first comp '',i5)')iimax,imax
c$$$$$$$$$      endif

c     *** buid initial matrix ***

      do j=1,nry
        amat(1,j)=alpha(j,abs(icomp(iimax)))
      enddo
      iord(1)=iimax

      nord=1
      do i=1,nind
        if(i.ne.imax) then
          ii=iflag2(i)
          iii=abs(icomp(ii))
          nord=nord+1
          do j=1,nry
            amat(nord,j)=alpha(j,iii)
          enddo
          iord(nord)=ii
        endif
      enddo
c$$$$$$$$$$$c      if(not.ne.0) then
      write(not,'(1h ,''  '')')
      write(not,'(1h ,'' ### build initial amat ###'')')
      write(not,'(1h ,''  '')')
      write(not,'((1h ,'' order of comp '',10i5))')(iord(i),i=1,nind)
      write(not,'(1h ,''  '')')
      write(not,'(1h ,'' amat '')')
      write(not,'((1h ,10e15.5))')((amat(i,j),j=1,nry),i=1,nind)
      write(not,'(1h ,''  '')')
      write(not,'(1h ,'' operation on amat '')')
c$$$$$$$$$$$c      endif


c===== remove zero column =====

      call scsizero(amat,amat2,izeroc,nind,nry)
      write(not,'(1h ,''  '')')
      write(not,'(1h ,'' after remove zero'')')
      write(not,'((1h ,10e15.5))')((amat(i,j),j=1,nry),i=1,nind)

      vmax=0.0d0
      do j=1,nind
        if(abs(amat(j,1)).gt.vmax) then
           vmax=abs(amat(j,i+1))
           inext=j
        endif
      enddo
      if(inext.ne.1) then
        do k=1,nry
          atemp(k)=amat(inext,k)
        enddo
        itemp=iord(inext)
        iord(inext)=iord(1)
        iord(1)=itemp
        do k=1,nry
          amat(inext,k)=amat(1,k)
          amat(1,k)=atemp(k)
        enddo
      endif
      write(not,'(1h ,''  '')')
      write(not,'(1h ,'' after reordering befreo pivotting zero'')')
      do ii=1,nind
        write(not,'((1h ,i5,20e15.5))')iord(ii),(amat(ii,j),j=1,nry)
      enddo

      do i=1,nind-1
        piv=amat(i,i)
c$$$$$        if(not.ne.0) then
        write(not,'(1h ,''  '')')
        write(not,'(1h ,''row'',i5,'' piv='',e15.5)') i,piv
c$$$$$$$4        endif
        if(abs(piv).lt.eps) then
c$$$$$$$4          if(not.ne.0) then
          write(not,'(1h ,''  '')')
          write(not,'(1h ,''piv =0 next row'')')
c$$$$$$$4          endif
          goto 45
        endif
        do j=i+1,nind
          com=amat(j,i)
          coef=com/piv
          do k=i,nry
            amat(j,k)=amat(j,k)-amat(i,k)*coef
          enddo
        enddo
c$$$$$$$$$4        if(not.ne.0) then
        write(not,'(1h ,'' after modification '')')
        write(not,'(1h ,'' amat '')')
        do ii=1,nind
        write(not,'((1h ,i5,20e15.5))')iord(ii),(amat(ii,j),j=1,nry)
        enddo
        call scsizero(amat,amat2,izeroc,nind,nry)
        write(not,'(1h ,'' after remove zero '')')
        write(not,'(1h ,'' amat '')')
        do ii=1,nind
        write(not,'((1h ,i5,20e15.5))')iord(ii),(amat(ii,j),j=1,nry)
        enddo
c$$$$$$$$$$$$        endif
   45   continue
        inext=i+1
        vmax=0.0d0
        do j=i+1,nind
          if(abs(amat(j,i+1)).gt.vmax) then
             vmax=abs(amat(j,i+1))
             inext=j
          endif
        enddo
        if(vmax.gt.eps) goto 30
c ------- fine other possbility ---

   30   continue
c$$$$$$$$$        if(not.ne.0) then
        write(not,'(1h ,'' next maximum pivot row'',i5)')inext
c$$$$$$$$c        endif
        if(inext.ne.i+1) then
          do k=1,nry
            atemp(k)=amat(inext,k)
          enddo
          itemp=iord(inext)
          iord(inext)=iord(i+1)
          iord(i+1)=itemp
          do k=1,nry
            amat(inext,k)=amat(i+1,k)
            amat(i+1,k)=atemp(k)
          enddo
        endif
c$$$$$$$$$$4        if(not.ne.0) then
        write(not,'(1h ,'' after reordering '')')
        write(not,'(1h ,'' amat '')')
        do ii=1,nind
        write(not,'((1h ,i5,20e15.5))')iord(ii),(amat(ii,j),j=1,nry)
        enddo
c$$$$$$$$$$$$        endif
  
      enddo


c     ----- count number of independent vector

      do i=1,nind
        iord2(i)=iord(i)
      enddo

      num=0
      do i=1,nind
        ysum=0.0d0
        do k=1,nry
          ysum=ysum+amat(i,k)**2
        enddo
        ysum=sqrt(ysum)
        if(ysum.gt.eps) then
          num=num+1
          iord(num)=iord2(i)
        endif
      enddo

c      num=nind
c      do i=nind,1,-1
c        ysum=0.0d0
c        do k=1,nry
c          ysum=ysum+amat(i,k)**2
c        enddo
c        ysum=sqrt(ysum)
c        if(ysum.lt.eps) num=num-1
c      enddo

      nind=num
  100 continue

c     ---- reordering -----

      do i=1,nind-1
        ii=icomp(iord(i))
        if(isys.eq.1) then
          if(ii.gt.0) then
            bi=btdes(ii)
          else
            bi=-btdes(-ii)
          endif
        else
          if(ii.gt.0) then
            bi=-btdes(ii)
          else
            bi=btdes(-ii)
          endif
        endif
        do j=i+1,nind
          jj=icomp(iord(j))
          if(isys.eq.1) then
            if(jj.gt.0) then
              bj=btdes(jj)
            else
              bj=-btdes(-jj)
            endif
          else
            if(ii.gt.0) then
              bj=-btdes(jj)
            else
              bj=btdes(-jj)
            endif
          endif
          if(bj.lt.bi) then
c          if(bj.gt.bi) then
             bi=bj
             itemp=iord(i)
             iord(i)=iord(j)
             iord(j)=itemp
          endif
        enddo
      enddo

      do i=1,mcomp
        iflag(i)=0
      enddo
      do i=1,nind
        iflag(iord(i))=1
      enddo
      num=nind
      do i=1,mcomp
        if(iflag(i).eq.0) then
           num=num+1
           iord(num)=i
        endif
      enddo

c      if(not.ne.0) then
c      write(not,'(1h ,'' '')')
c      write(not,'(1h ,'' final result '')')
c      write(not,'(1h ,'' num of independent '',i5)') nind
c      write(not,'(1h ,'' seq id '',10i5)')(iord(i),i=1,nind)
c      write(not,'(1h ,'' dependent '')')
c      write(not,'(1h ,'' seq id '',10i5)')(iord(i),i=nind+1,mcomp)
c      endif

      deallocate(iflag)
      deallocate(iflag2)
      deallocate(amat)
      deallocate(amat2)
      deallocate(izeroc)
      deallocate(atemp)
      deallocate(iord2)

      end subroutine scindvec
