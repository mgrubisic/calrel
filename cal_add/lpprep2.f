      subroutine lpprep2(not,ncomp,nlevel,ntotal,nry,icomp,ibtable,
     *                  beta,alph,bl,bu,ind,dattrv,ptabin,inp,imode)

      implicit   none

      real*4,allocatable::pprint(:)
      integer*4 not,ncomp,nlevel,ntotal,nry,icomp(*),ind(*)
      integer*4 ibtable(nlevel,*),inp
      real*8 beta(*),alph(nry,*)
      real*8 bt,pf,ro,cdot,ptabin(*)
      real*4 bl(*),bu(*),dattrv(*)
      integer*4 k,i,j,m,ij,nrow,ii,jj,imode
      integer (kind=4) ::  kk, nlevt,nlevw, ncount
c     ----- construct dattrv -----
      if(imode.eq.1) then
        nlevw=ncomp
        write(not,'(1h ,''  '')')
        write(not,'(1h ,'' --- uni-component probability ---'')')
      elseif(imode.eq.2) then
        nlevw=ncomp*(ncomp+1)/2
        write(not,'(1h ,''  '')')
        write(not,'(1h ,'' --- bi -component probability ---'')')
      else
        nlevw=ncomp*(ncomp+1)/2+ncomp*(ncomp-1)*(ncomp-2)/6
        write(not,'(1h ,''  '')')
        write(not,'(1h ,'' --- tri -component probability ---'')')
      endif
      nlevt=nlevel
c
      k=0
      do i=1,ntotal
        k=k+1
        dattrv(k)=-i
        do j=1,nlevw
          if(ibtable(j,i).eq.1) then
            k=k+1
            dattrv(k)=j
            k=k+1
            dattrv(k)=1.0e0
          endif
        enddo
        if(ibtable(nlevt,i).eq.1) then
          k=k+1
          dattrv(k)=nlevw+1
          k=k+1
          dattrv(k)=1.0e0
        endif
      enddo
      k=k+1
      dattrv(k)=0
c
      if(not.ne.0) then
      allocate(pprint(2*nlevel))
      write(not,'(1h ,'' '')')
      write(not,'(1h ,'' === dattrv === '')')
      write(not,'(1h ,'' '')')
      k=1
   10 continue
      if(dattrv(k).eq.0.0d0) goto 19
      if(dattrv(k).lt.0.0d0) then
        write(not,'(1h ,'' --- column '',i5,'' ---'')') int(dattrv(k))
        m=0
   15   continue
        k=k+1
        if(dattrv(k).le.0) then
          write(not,'(1h ,1p10e10.3)')(pprint(ij),ij=1,m)
          goto 10
        else
          m=m+1
          pprint(m)=dattrv(k)
          goto 15
        endif
      endif
   19 continue
      deallocate(pprint)
      endif
c
c     ----- construct bounds for disjoint comp -----
c
      nrow=0
      do i=1,ntotal
        bl(i)=0.0e0
        bu(i)=0.0e0
        ind(i)=1
        nrow=nrow+1
      enddo
c
      ncount=0
c     ----- construct bounds for each event-----
      do i=1,ncomp
        ncount=ncount+1
        if(inp.eq.0) then
          bt=-beta(icomp(i))
          call dnorm(bt,pf)
        else
          pf=ptabin(i)
        endif
        bl(i+ntotal)=pf*1.0e7
        bu(i+ntotal)=pf*1.0e7
        ind(i+ntotal)=3
        nrow=nrow+1
      enddo
c
      if(imode.eq.1) goto 100
c
      do i=1,ncomp-1
        ii=icomp(i)
        do j=i+1,ncomp
           nrow=nrow+1
           ncount=ncount+1
           jj=icomp(j)
           if(inp.eq.0) then
             ro=cdot(alph(1,ii),alph(1,jj),1,1,nry)
             call bpfij(pf,beta(ii),beta(jj),ro)
           else
             pf=ptabin(ncount)
           endif
           bl(nrow)=pf*1.0e7
           bu(nrow)=pf*1.0e7
           ind(nrow)=3
        enddo
      enddo
c
      if(imode.eq.2) goto 100
c
      do i=1,ncomp-2
        ii=icomp(i)
        do j=i+1,ncomp-1
           jj=icomp(j)
           do k=j+1,ncomp
             kk=icomp(k)
             ncount=ncount+1
             nrow=nrow+1
             if(inp.eq.0) then
               pf=0.0d0
             else
               pf=ptabin(ncount)
             endif
             bl(nrow)=pf
             bu(nrow)=pf
            ind(nrow)=3
          enddo
        enddo
      enddo
c
  100 continue
      nrow=nrow+1
      bl(nrow)=1.0e0*1.0e7
      bu(nrow)=1.0e0*1.0e7
      ind(nrow)=3
c
      if(not.ne.0) then
      write(not,'(1h ,''  '')')
      write(not,'(1h ,'' --- component bounds ----- '')')
      write(not,'(1h ,''  '')')
      write(not,'(1h ,3x2hid,13x2hbl,13x2hbu,2x3hind)')
      do i=1,ntotal
      write(not,'(1h ,i5,1p2e15.5,i5)') i,bl(i),bu(i),ind(i)
      enddo
      write(not,'(1h ,''  '')')
      write(not,'(1h ,'' --- set bounds ----- '')')
      write(not,'(1h ,''  '')')
      write(not,'(1h ,3x2hid,13x2hbl,13x2hbu,2x3hind)')
      do i=ntotal+1,nrow
      write(not,'(1h ,i5,1p2e15.5,i5)')
     *  i,bl(i),bu(i),ind(i)
      enddo
      endif

      end subroutine lpprep2
