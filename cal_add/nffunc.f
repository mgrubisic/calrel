!***************************************************************
      subroutine nffunc(nry,y1,ffunc)
!***************************************************************
      implicit   none

      integer (kind=4) :: nry
      real    (kind=8) :: y1(nry)

      real    (kind=8) :: yy,ffunc,fai,pi
      integer (kind=4) :: i

      pi = sqrt(2.0d0*acos(-1.0d0))
      ffunc = 1.0d0
      do i = 1,nry
        yy    = -0.5d0*(y1(i)**2)
        fai   =  exp(yy)/pi
        ffunc =  ffunc*fai
      enddo

      end subroutine nffunc
