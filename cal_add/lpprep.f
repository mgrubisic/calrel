      subroutine lpprep(not,ncomp,nlevel,ntotal,nry,icomp,ibtable,
     *                  beta,alph,cost,mtdat,igfx,ngf,rhsin,
     *                  ptabin,ind,imode,ptabin2,inequality)
      implicit   none

      character*8,allocatable::moji(:)
      real*8,allocatable::val(:)
      integer*4 not,ncomp,nlevel,ntotal,nry,icomp(*),ngf
      integer*4 ibtable(nlevel,*),mtdat,igfx(ngf,3)
      real*8 beta(*),alph(nry,*),cost(*),rhsin(*)
      real*8 bt,pf,ro,cdot,ptabin(*),ptabin2(*)
      integer*4 k,i,j,ii,jj,nalloc,nw,js,je,ind,imode
      integer*4 nlevw,nlevt,kk,inequality
      character*8 dump,head
      character*12 headd

      integer (kind=4) ::  nw2, iloc

      if(imode.eq.1) then
        nlevw=ncomp
        write(not,'(1h ,''  '')')
        write(not,'(1h ,'' --- uni-component probability ---'')')
      elseif(imode.eq.2) then
        nlevw=ncomp*(ncomp+1)/2
        write(not,'(1h ,''  '')')
        write(not,'(1h ,'' --- bi -component probability ---'')')
      else
        nlevw=ncomp*(ncomp+1)/2+ncomp*(ncomp-1)*(ncomp-2)/6
        write(not,'(1h ,''  '')')
        write(not,'(1h ,'' --- tri -component probability ---'')')
      endif
      nlevt=nlevel

      if(inequality.eq.0) then
      rewind mtdat
      write(mtdat,'(''name          lpbounds'')')
      write(mtdat,'(''rows'')')
      write(mtdat,'('' n  cost'')')
      do i=1,nlevw+1
      if(i.le.9) then
      write(mtdat,'('' e  lim'',i1)') i
      elseif(i.le.99) then
      write(mtdat,'('' e  lim'',i2)') i
      elseif(i.le.999) then
      write(mtdat,'('' e  lim'',i3)') i
      elseif(i.le.9999) then
      write(mtdat,'('' e  lim'',i4)') i
      elseif(i.le.99999) then
      write(mtdat,'('' e  lim'',i5)') i
      else
      write(not,'(1h ,'' too many constraints '')')
      stop
      endif
      enddo
      write(mtdat,'(''columns'')')
      nalloc=max(nlevw+2,ntotal)
      allocate(moji(nalloc))
      allocate(val(nalloc))
      do i=1,ntotal
        nw=0
        if(cost(i).ne.0) then
          nw=nw+1
          moji(nw)='cost    '
          val(nw)=cost(i)
        endif
        do j=1,nlevw
          if(ibtable(j,i).ne.0) then
            nw=nw+1
            dump='lim     '
            if(j.le.9) then
              write(dump(4:4),'(i1)')j
            elseif(j.le.99) then
              write(dump(4:5),'(i2)')j
            elseif(j.le.999) then
              write(dump(4:6),'(i3)')j
            elseif(j.le.9999) then
              write(dump(4:7),'(i4)')j
            elseif(j.le.99999) then
              write(dump(4:8),'(i5)')j
            else
                  write(not,'(1h ,'' too many constraints '')')
              stop
            endif
            moji(nw)=dump
            val(nw)=1.0d0
          endif
        enddo
c       ----- total probabiliyt -----
        if(ibtable(nlevt,i).ne.0) then
          nw=nw+1
          dump='lim     '
          if(nlevw+1.le.9) then
            write(dump(4:4),'(i1)')nlevw+1
          elseif(nlevw+1.le.99) then
              write(dump(4:5),'(i2)')nlevw+1
          elseif(nlevw+1.le.999) then
              write(dump(4:6),'(i3)')nlevw+1
          elseif(nlevw+1.le.9999) then
              write(dump(4:7),'(i4)')nlevw+1
          elseif(nlevw+1.le.99999) then
              write(dump(4:8),'(i5)')nlevw+1
          else
            write(not,'(1h ,'' too many constraints '')')
            stop
          endif
          moji(nw)=dump
          val(nw)=1.0d0
        endif

        headd='   x        '
        if(i.le.9) then
          write(headd(5:5),'(i1)') i
        elseif(i.le.99) then
          write(headd(5:6),'(i2)') i
        elseif(i.le.999) then
          write(headd(5:7),'(i3)') i
        elseif(i.le.9999) then
          write(headd(5:8),'(i4)') i
        elseif(i.le.99999) then
          write(headd(5:9),'(i5)') i
        elseif(i.le.999999) then
          write(headd(5:10),'(i6)') i
        elseif(i.le.9999999) then
          write(headd(5:11),'(i7)') i
        elseif(i.le.99999999) then
          write(headd(5:12),'(i8)') i
        else
            write(not,'(1h ,'' too many variables '')')
          stop
        endif
        do j=1,nw,2
          js=j
          je=min(j+1,nw)
          write(mtdat,'(a12,2x,a8,1pe14.5,3x,a8,1pe14.5)')
     *     headd,(moji(k),val(k),k=js,je)
        enddo
      enddo
c     ----- construct bounds for each event-----
      write(mtdat,'(''rhs'')')
c     ----- unimodal -----
      do i=1,ncomp
        if(ind.eq.0) then
          bt=-beta(icomp(i))
          call dnorm(bt,pf)
        else
c          pf=ptabin(i)
          pf=ptabin(icomp(i))
        endif
        write(not,'(1h ,i5,10x,1pe15.5)') igfx(icomp(i),1),pf
        dump='lim     '
        if(i.le.9) then
          write(dump(4:4),'(i1)')i
        elseif(i.le.99) then
          write(dump(4:5),'(i2)')i
        elseif(i.le.999) then
          write(dump(4:6),'(i3)')i
        elseif(i.le.9999) then
          write(dump(4:7),'(i4)')i
        elseif(i.le.99999) then
          write(dump(4:8),'(i5)')i
        else
          write(not,'(1h ,'' too many constraint '')')
          stop
        endif
        moji(i)=dump
        val(i)=pf
        rhsin(i)=pf
      enddo
      nw=ncomp
      if(imode.eq.1) goto 200
c     ----- bi modal -----
      do i=1,ncomp-1
        ii=icomp(i)
        do j=i+1,ncomp
           nw=nw+1
           jj=icomp(j)
           if(ind.eq.0) then
             ro=cdot(alph(1,ii),alph(1,jj),1,1,nry)
             call bpfij(pf,beta(ii),beta(jj),ro)
           else
             call location2(ii,jj,ncomp,iloc)
             pf=ptabin(iloc)
c             pf=ptabin(nw)
           endif
           dump='lim     '
           write(not,'(1h ,2i5,5x,1pe15.5)')igfx(ii,1),igfx(jj,1),pf
           if(nw.le.9) then
             write(dump(4:4),'(i1)')nw
           elseif(nw.le.99) then
             write(dump(4:5),'(i2)')nw
           elseif(nw.le.999) then
             write(dump(4:6),'(i3)')nw
           elseif(nw.le.9999) then
             write(dump(4:7),'(i4)')nw
           elseif(nw.le.99999) then
             write(dump(4:8),'(i5)')nw
           else
             write(not,'(1h ,'' too many constraints '')')
             stop
           endif
           moji(nw)=dump
           val(nw)=pf
           rhsin(nw)=pf
        enddo
      enddo
      if(imode.eq.2) goto 200
c     ----- tri modal -----
      do i=1,ncomp-2
        ii=icomp(i)
        do j=i+1,ncomp-1
          jj=icomp(j)
          do k=j+1,ncomp
             kk=icomp(k)
             nw=nw+1
             if(ind.eq.0) then
c               ro=cdot(alph(1,ii),alph(1,jj),1,1,nry)
c               call bpfij(pf,beta(ii),beta(jj),ro)
             else
               call location3(ii,jj,kk,ncomp,iloc)
               pf=ptabin(iloc)
c               pf=ptabin(nw)
             endif
             dump='lim     '
             write(not,'(1h ,3i5,1pe15.5)')igfx(ii,1),igfx(jj,1),
     *          igfx(kk,1),pf
             if(nw.le.9) then
               write(dump(4:4),'(i1)')nw
             elseif(nw.le.99) then
               write(dump(4:5),'(i2)')nw
             elseif(nw.le.999) then
               write(dump(4:6),'(i3)')nw
             elseif(nw.le.9999) then
               write(dump(4:7),'(i4)')nw
             elseif(nw.le.99999) then
               write(dump(4:8),'(i5)')nw
             else
               write(not,'(1h ,'' too many constraints '')')
               stop
             endif
             moji(nw)=dump
             val(nw)=pf
             rhsin(nw)=pf
          enddo
        enddo
      enddo
c     ----- total probability -----
 200  continue
      nw=nw+1
      dump='lim     '
      if(nw.le.9) then
        write(dump(4:4),'(i1)')nw
      elseif(nw.le.99) then
        write(dump(4:5),'(i2)')nw
      elseif(nw.le.999) then
        write(dump(4:6),'(i3)')nw
      elseif(nw.le.9999) then
        write(dump(4:7),'(i4)')nw
      elseif(nw.le.99999) then
        write(dump(4:8),'(i5)')nw
      else
        write(not,'(1h ,'' too many constraints '')')
        stop
      endif
      moji(nw)=dump
      val(nw)=1.0d0
      rhsin(nw)=1.0d0
      head='    rhs1'
      do j=1,nw,2
        js=j
        je=min(j+1,nw)
        write(mtdat,'(a8,6x,a8,1pe14.5,3x,a8,1pe14.5)')
     *   head,(moji(k),val(k),k=js,je)
      enddo
      write(mtdat,'(''bounds'')')
      do i=1,ntotal
      if(i.le.9) then
      write(mtdat,'('' lo bnd1     x'',i1,7x,1pe14.5)') i,0.0d0
      elseif(i.le.99) then
      write(mtdat,'('' lo bnd1     x'',i2,6x,1pe14.5)') i,0.0d0
      elseif(i.le.999) then
      write(mtdat,'('' lo bnd1     x'',i3,5x,1pe14.5)') i,0.0d0
      elseif(i.le.9999) then
      write(mtdat,'('' lo bnd1     x'',i4,4x,1pe14.5)') i,0.0d0
      elseif(i.le.99999) then
      write(mtdat,'('' lo bnd1     x'',i5,3x,1pe14.5)') i,0.0d0
      elseif(i.le.999999) then
      write(mtdat,'('' lo bnd1     x'',i6,2x,1pe14.5)') i,0.0d0
      elseif(i.le.9999999) then
      write(mtdat,'('' lo bnd1     x'',i7,1x,1pe14.5)') i,0.0d0
      elseif(i.le.99999999) then
      write(mtdat,'('' lo bnd1     x'',i8,1pe14.5)') i,0.0d0
      else
      write(not,'(1h ,'' too many parameters for lp '')')
      stop
      endif
      enddo
      write(mtdat,'(''enddata'')')
      else


c    ========================= inequality constraints ============
      rewind mtdat
      write(mtdat,'(''name          lpbounds'')')
      write(mtdat,'(''rows'')')
      write(mtdat,'('' n  cost'')')
c     ===============================================
      do i=1,nlevw
      if(i.le.9) then
      write(mtdat,'('' g  lim'',i1)') i
      elseif(i.le.99) then
      write(mtdat,'('' g  lim'',i2)') i
      elseif(i.le.999) then
      write(mtdat,'('' g  lim'',i3)') i
      elseif(i.le.9999) then
      write(mtdat,'('' g  lim'',i4)') i
      elseif(i.le.99999) then
      write(mtdat,'('' g  lim'',i5)') i
      else
      write(not,'(1h ,'' too many constraints '')')
      stop
      endif
      enddo
      do i=nlevw+1,2*nlevw
      if(i.le.9) then
      write(mtdat,'('' l  lim'',i1)') i
      elseif(i.le.99) then
      write(mtdat,'('' l  lim'',i2)') i
      elseif(i.le.999) then
      write(mtdat,'('' l  lim'',i3)') i
      elseif(i.le.9999) then
      write(mtdat,'('' l  lim'',i4)') i
      elseif(i.le.99999) then
      write(mtdat,'('' l  lim'',i5)') i
      else
      write(not,'(1h ,'' too many constraints '')')
      stop
      endif
      enddo
      i=2*nlevw+1
      if(i.le.9) then
      write(mtdat,'('' e  lim'',i1)') i
      elseif(i.le.99) then
      write(mtdat,'('' e  lim'',i2)') i
      elseif(i.le.999) then
      write(mtdat,'('' e  lim'',i3)') i
      elseif(i.le.9999) then
      write(mtdat,'('' e  lim'',i4)') i
      elseif(i.le.99999) then
      write(mtdat,'('' e  lim'',i5)') i
      else
      write(not,'(1h ,'' too many constraints '')')
      stop
      endif

      write(mtdat,'(''columns'')')
      nalloc=max(2*nlevw+2,ntotal)
      allocate(moji(nalloc))
      allocate(val(nalloc))

      do i=1,ntotal
        nw=0
        if(cost(i).ne.0) then
          nw=nw+1
          moji(nw)='cost    '
          val(nw)=cost(i)
        endif
        do j=1,nlevw
          if(ibtable(j,i).ne.0) then
            nw=nw+1
            dump='lim     '
            if(j.le.9) then
              write(dump(4:4),'(i1)')j
            elseif(j.le.99) then
              write(dump(4:5),'(i2)')j
            elseif(j.le.999) then
              write(dump(4:6),'(i3)')j
            elseif(j.le.9999) then
              write(dump(4:7),'(i4)')j
            elseif(j.le.99999) then
              write(dump(4:8),'(i5)')j
            else
                  write(not,'(1h ,'' too many constraints '')')
              stop
            endif
            moji(nw)=dump
            val(nw)=1.0d0
          endif
        enddo

        do j=1,nlevw
          jj=j+nlevw
          if(ibtable(j,i).ne.0) then
            nw=nw+1
            dump='lim     '
            if(jj.le.9) then
              write(dump(4:4),'(i1)')jj
            elseif(jj.le.99) then
              write(dump(4:5),'(i2)')jj
            elseif(jj.le.999) then
              write(dump(4:6),'(i3)')jj
            elseif(jj.le.9999) then
              write(dump(4:7),'(i4)')jj
            elseif(jj.le.99999) then
              write(dump(4:8),'(i5)')jj
            else
                  write(not,'(1h ,'' too many constraints '')')
              stop
            endif
            moji(nw)=dump
            val(nw)=1.0d0
          endif
        enddo
c       ----- total probabiliyt -----
        if(ibtable(nlevt,i).ne.0) then
          nw=nw+1
          dump='lim     '
          if(2*nlevw+1.le.9) then
            write(dump(4:4),'(i1)')2*nlevw+1
          elseif(2*nlevw+1.le.99) then
              write(dump(4:5),'(i2)')2*nlevw+1
          elseif(2*nlevw+1.le.999) then
              write(dump(4:6),'(i3)')2*nlevw+1
          elseif(2*nlevw+1.le.9999) then
              write(dump(4:7),'(i4)')2*nlevw+1
          elseif(2*nlevw+1.le.99999) then
              write(dump(4:8),'(i5)')2*nlevw+1
          else
            write(not,'(1h ,'' too many constraints '')')
            stop
          endif
          moji(nw)=dump
          val(nw)=1.0d0
        endif

        headd='   x        '
        if(i.le.9) then
          write(headd(5:5),'(i1)') i
        elseif(i.le.99) then
          write(headd(5:6),'(i2)') i
        elseif(i.le.999) then
          write(headd(5:7),'(i3)') i
        elseif(i.le.9999) then
          write(headd(5:8),'(i4)') i
        elseif(i.le.99999) then
          write(headd(5:9),'(i5)') i
        elseif(i.le.999999) then
          write(headd(5:10),'(i6)') i
        elseif(i.le.9999999) then
          write(headd(5:11),'(i7)') i
        elseif(i.le.99999999) then
          write(headd(5:12),'(i8)') i
        else
            write(not,'(1h ,'' too many variables '')')
          stop
        endif
        do j=1,nw,2
          js=j
          je=min(j+1,nw)
          write(mtdat,'(a12,2x,a8,1pe14.5,3x,a8,1pe14.5)')
     *     headd,(moji(k),val(k),k=js,je)
        enddo
      enddo

c     ----- construct bounds for each event-----
      write(mtdat,'(''rhs'')')
      nw=0
c     ----- unimodal -----
      do i=1,ncomp
        nw=nw+1
        if(ind.eq.0) then
          bt=-beta(icomp(i))
          call dnorm(bt,pf)
        else
c         pf=ptabin(i)
          pf=ptabin(icomp(i))
        endif
        write(not,'(1h ,i5,10x,1pe15.5)') igfx(icomp(i),1),pf
        dump='lim     '
        if(nw.le.9) then
          write(dump(4:4),'(i1)')nw
        elseif(nw.le.99) then
          write(dump(4:5),'(i2)')nw
        elseif(nw.le.999) then
          write(dump(4:6),'(i3)')nw
        elseif(nw.le.9999) then
          write(dump(4:7),'(i4)')nw
        elseif(nw.le.99999) then
          write(dump(4:8),'(i5)')nw
        else
          write(not,'(1h ,'' too many constraint '')')
          stop
        endif
        moji(nw)=dump
        val(nw)=pf
        rhsin(nw)=pf
      enddo
      if(imode.eq.1) goto 210
c     ----- bi modal -----
      do i=1,ncomp-1
        ii=icomp(i)
        do j=i+1,ncomp
           nw=nw+1
           jj=icomp(j)
           if(ind.eq.0) then
             ro=cdot(alph(1,ii),alph(1,jj),1,1,nry)
             call bpfij(pf,beta(ii),beta(jj),ro)
           else
             call location2(ii,jj,ncomp,iloc)
             pf=ptabin(iloc)
c             pf=ptabin(nw)
           endif
           dump='lim     '
           write(not,'(1h ,2i5,5x,1pe15.5)')igfx(ii,1),igfx(jj,1),pf
           if(nw.le.9) then
             write(dump(4:4),'(i1)')nw
           elseif(nw.le.99) then
             write(dump(4:5),'(i2)')nw
           elseif(nw.le.999) then
             write(dump(4:6),'(i3)')nw
           elseif(nw.le.9999) then
             write(dump(4:7),'(i4)')nw
           elseif(nw.le.99999) then
             write(dump(4:8),'(i5)')nw
           else
             write(not,'(1h ,'' too many constraints '')')
             stop
           endif
           moji(nw)=dump
           val(nw)=pf
           rhsin(nw)=pf
        enddo
      enddo
      if(imode.eq.2) goto 210
c     ----- tri modal -----
      do i=1,ncomp-2
        ii=icomp(i)
        do j=i+1,ncomp-1
          jj=icomp(j)
          do k=j+1,ncomp
             kk=icomp(k)
             nw=nw+1
             if(ind.eq.0) then
c               ro=cdot(alph(1,ii),alph(1,jj),1,1,nry)
c               call bpfij(pf,beta(ii),beta(jj),ro)
             else
               call location3(ii,jj,kk,ncomp,iloc)
               pf=ptabin(iloc)
c               pf=ptabin(nw)
             endif
             dump='lim     '
             write(not,'(1h ,3i5,1pe15.5)')igfx(ii,1),igfx(jj,1),
     *          igfx(kk,1),pf
             if(nw.le.9) then
               write(dump(4:4),'(i1)')nw
             elseif(nw.le.99) then
               write(dump(4:5),'(i2)')nw
             elseif(nw.le.999) then
               write(dump(4:6),'(i3)')nw
             elseif(nw.le.9999) then
               write(dump(4:7),'(i4)')nw
             elseif(nw.le.99999) then
               write(dump(4:8),'(i5)')nw
             else
               write(not,'(1h ,'' too many constraints '')')
               stop
             endif
             moji(nw)=dump
             val(nw)=pf
             rhsin(nw)=pf
          enddo
        enddo
      enddo
 210  continue
c     ----- unimodal -----
      nw2=0
      do i=1,ncomp
        nw=nw+1
        nw2=nw2+1
        if(ind.eq.0) then
          bt=-beta(icomp(i))
          call dnorm(bt,pf)
        else
c          pf=ptabin2(nw2)
          pf=ptabin2(icomp(i))
        endif
        write(not,'(1h ,i5,10x,1pe15.5)') igfx(icomp(i),1),pf
        dump='lim     '
        if(nw.le.9) then
          write(dump(4:4),'(i1)')nw
        elseif(nw.le.99) then
          write(dump(4:5),'(i2)')nw
        elseif(nw.le.999) then
          write(dump(4:6),'(i3)')nw
        elseif(nw.le.9999) then
          write(dump(4:7),'(i4)')nw
        elseif(nw.le.99999) then
          write(dump(4:8),'(i5)')nw
        else
          write(not,'(1h ,'' too many constraint '')')
          stop
        endif
        moji(nw)=dump
        val(nw)=pf
        rhsin(nw)=pf
      enddo
      if(imode.eq.1) goto 220
c     ----- bi modal -----
      do i=1,ncomp-1
        ii=icomp(i)
        do j=i+1,ncomp
           nw=nw+1
           nw2=nw2+1
           jj=icomp(j)
           if(ind.eq.0) then
             ro=cdot(alph(1,ii),alph(1,jj),1,1,nry)
             call bpfij(pf,beta(ii),beta(jj),ro)
           else
c             pf=ptabin2(nw2)
             call location2(ii,jj,ncomp,iloc)
             pf=ptabin2(iloc)
           endif
           dump='lim     '
           write(not,'(1h ,2i5,5x,1pe15.5)')igfx(ii,1),igfx(jj,1),pf
           if(nw.le.9) then
             write(dump(4:4),'(i1)')nw
           elseif(nw.le.99) then
             write(dump(4:5),'(i2)')nw
           elseif(nw.le.999) then
             write(dump(4:6),'(i3)')nw
           elseif(nw.le.9999) then
             write(dump(4:7),'(i4)')nw
           elseif(nw.le.99999) then
             write(dump(4:8),'(i5)')nw
           else
             write(not,'(1h ,'' too many constraints '')')
             stop
           endif
           moji(nw)=dump
           val(nw)=pf
           rhsin(nw)=pf
        enddo
      enddo
      if(imode.eq.2) goto 220
c     ----- tri modal -----
      do i=1,ncomp-2
        ii=icomp(i)
        do j=i+1,ncomp-1
          jj=icomp(j)
          do k=j+1,ncomp
             kk=icomp(k)
             nw=nw+1
             nw2=nw2+1
             if(ind.eq.0) then
c               ro=cdot(alph(1,ii),alph(1,jj),1,1,nry)
c               call bpfij(pf,beta(ii),beta(jj),ro)
             else
               call location3(ii,jj,kk,ncomp,iloc)
               pf=ptabin2(iloc)
c               pf=ptabin(nw2)
             endif
             dump='lim     '
             write(not,'(1h ,3i5,1pe15.5)')igfx(ii,1),igfx(jj,1),
     *          igfx(kk,1),pf
             if(nw.le.9) then
               write(dump(4:4),'(i1)')nw
             elseif(nw.le.99) then
               write(dump(4:5),'(i2)')nw
             elseif(nw.le.999) then
               write(dump(4:6),'(i3)')nw
             elseif(nw.le.9999) then
               write(dump(4:7),'(i4)')nw
             elseif(nw.le.99999) then
               write(dump(4:8),'(i5)')nw
             else
               write(not,'(1h ,'' too many constraints '')')
               stop
             endif
             moji(nw)=dump
             val(nw)=pf
             rhsin(nw)=pf
          enddo
        enddo
      enddo
c     ====== total =====
  220 continue
      nw=nw+1
      dump='lim     '
      if(nw.le.9) then
        write(dump(4:4),'(i1)')nw
      elseif(nw.le.99) then
        write(dump(4:5),'(i2)')nw
      elseif(nw.le.999) then
        write(dump(4:6),'(i3)')nw
      elseif(nw.le.9999) then
        write(dump(4:7),'(i4)')nw
      elseif(nw.le.99999) then
        write(dump(4:8),'(i5)')nw
      else
        write(not,'(1h ,'' too many constraints '')')
        stop
      endif
      moji(nw)=dump
      val(nw)=1.0d0
      rhsin(nw)=1.0d0
      head='    rhs1'
      do j=1,nw,2
        js=j
        je=min(j+1,nw)
        write(mtdat,'(a8,6x,a8,1pe14.5,3x,a8,1pe14.5)')
     *   head,(moji(k),val(k),k=js,je)
      enddo
      write(mtdat,'(''bounds'')')
      do i=1,ntotal
      if(i.le.9) then
      write(mtdat,'('' lo bnd1     x'',i1,7x,1pe14.5)') i,0.0d0
      elseif(i.le.99) then
      write(mtdat,'('' lo bnd1     x'',i2,6x,1pe14.5)') i,0.0d0
      elseif(i.le.999) then
      write(mtdat,'('' lo bnd1     x'',i3,5x,1pe14.5)') i,0.0d0
      elseif(i.le.9999) then
      write(mtdat,'('' lo bnd1     x'',i4,4x,1pe14.5)') i,0.0d0
      elseif(i.le.99999) then
      write(mtdat,'('' lo bnd1     x'',i5,3x,1pe14.5)') i,0.0d0
      elseif(i.le.999999) then
      write(mtdat,'('' lo bnd1     x'',i6,2x,1pe14.5)') i,0.0d0
      elseif(i.le.9999999) then
      write(mtdat,'('' lo bnd1     x'',i7,1x,1pe14.5)') i,0.0d0
      elseif(i.le.99999999) then
      write(mtdat,'('' lo bnd1     x'',i8,1pe14.5)') i,0.0d0
      else
      write(not,'(1h ,'' too many parameters for lp '')')
      stop
      endif
      enddo
      write(mtdat,'(''enddata'')')
      endif

      deallocate(moji)
      deallocate(val)

      end subroutine lpprep
c***********************************************
      subroutine location2(iad,jad,ncomp,iloc)
c***********************************************
      implicit   none
      integer*4 iad,jad,iloc,ncomp
      integer*4 i,j, imin,imax

      if(iad.lt.jad) then
        imin=iad
        imax=jad
      else
        imin=jad
        imax=iad
      endif
      iloc=ncomp
      do i=1,imin-1
        do j=i+1,ncomp
          iloc=iloc+1
        enddo
      enddo
      iloc=iloc+(imax-imin)

      end subroutine location2
c***********************************************
      subroutine location3(iad,jad,kad,ncomp,iloc)
c***********************************************
      implicit   none
      integer*4 iad,jad,kad,iloc,ncomp
      integer*4 i,j,k, imin,imid,imax

      if(iad.lt.jad) then
        if(iad.lt.kad) then
          imin=iad
          if(kad.lt.jad) then
            imid=kad
            imax=jad
          else
            imid=jad
            imax=kad
          endif
        else
          imin=kad
          imid=iad
          imax=jad
        endif
      else
        if(jad.lt.kad) then
          imin=jad
          if(kad.lt.iad) then
            imid=kad
            imax=iad
          else
            imid=iad
            imax=kad
          endif
        else
          imin=kad
          imid=jad
          imax=iad
        endif
      endif
c     upto bimodal
      iloc=ncomp
      do i=1,ncomp-1
        do j=i+1,ncomp
          iloc=iloc+1
        enddo
      enddo

      do i=1,imin-1
        do j=i+1,ncomp-1
          do k=j+1,ncomp
            iloc=iloc+1
          enddo
        enddo
      enddo

      i=imin
      do j=i+1,imid-1
        do k=j+1,ncomp
          iloc=iloc+1
        enddo
      enddo

      iloc=iloc+(imax-imid)

      end subroutine location3
