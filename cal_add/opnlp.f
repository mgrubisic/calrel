      subroutine donlp2(nry,mconst,ifunc1,ifunc2,ids,ib,ipa,izx,ixz,
     *                  lgp,igt,zzz,yyy,bnd,tf,par,sg,ex,tp,
     *                  alp,gam,dgx,gdgx)

      implicit   none

      include 'o8comm.h'
      include 'o8fint.h'
      include 'o8cons.h'
      integer*4 nry,mconst,ifunc1(*),ifunc2(*),ids(*),ipa(4,*)
      integer*4 izx(*),ixz(*),lgp(3,*),igt(*),ib(*)
      real*8 zzz(*),bnd(2,*),tf(*),par(4,*),sg(*),ex(*),tp(*),yyy(*)
      real*8 alp(*),gam(*),dgx(*),gdgx(*)
      double precision yy(nx),o8vecn
      integer i,j,idum
      real o8cpu
      save
!  default settings of new parameters
      bloc=.false.
      analyt=.true.
      valid=.false.
      epsfcn=1.d-16
      difftype=3
      taubnd=1.d0
      do i=1,nx
        xsc(i)=one
        xtr(i)=zero
      enddo
      epsdif=tm8
      do i=0,nresm
!*** presently, the test-cases in the testenvironment do
!    not differentiate
!    between linear and
!*** nonlinear constraints. for later extensions or users
!*** functions this:
        gconst(i)=.false.
        val(i)=.false.
        if ( i .gt. 0 ) gresn(i)=one
      enddo
      do i=1,nresm
        cfuerr(i)=.false.
!*** the error indicator of the function evaluation
      enddo
      ffuerr=.false.


!     some standard initialization which may be overwritten by
!     setup0 or setup

      inx=.false.
      silent=.false.
!***** the interactive input feature is no longer supported here.
!***** for the sake
!***** of easy revision the variable is supplied here however
!***** if intakt is set true, output to protocol file is copied to
!***** stdout in addition
      intakt=.false.
      te0=.false.
      te1=.false.
      te2=.false.
      te3=.false.
      cold=.true.
      prou=10
      meu=20
!***  setup0 must initialize  analyt, epsdif, del0, tau0 , n , nh ,
!     ng , gunit
!     gconst , epsfcn , taubnd , analyt , bloc , difftype
!     and the initial value for x
!     (might be also done in block data of course)
!     may also change settings of all variables initialized above
!     or via blockdata
!***
      call setup0(nry,mconst)

      call o8st

!***
!***
!***  the default for numsm
      numsm=max(n,10)
      do i=1,n
        xst(i)=x(i)
        if ( xsc(i) .eq. zero ) then
          write(*,*)  'scaling variable ',i,' is zero'
          stop
        endif
        x(i)=x(i)/xsc(i)
      enddo
      nreset=n
!     default for epsphi
      epsphi=tp3*epsmac
!***  setup may change standard setttings of parameters
!     and add some computations
!     in the user environment

      do i=1,n
!**** ug and og have been evaluted for the original variables
!**** here we use them internally for the scaled ones
        if ( llow(i) ) ug(i)=ug(i)/xsc(i)
        if ( lup(i) ) og(i)=og(i)/xsc(i)
      enddo
      call setup
!*** preevaluation of gradients of linear functions
!    done only once

      do i=0,nres
        if ( gunit(1,i) .ne. 1 .and. gconst(i) ) then
!****** evaluate gradient once
          if ( i .eq. 0 ) then
            val(0)=.true.
            call esgradf(x,gradf)
          else
            val(i)=.true.
            if ( i .le. nh ) then
              call esgradh(i,x,yy)
            else
              call esgradg(i-nh,x,yy,ifunc1,ifunc2,yyy,zzz,ex,tf,tp,sg,
     *        par,bnd,gam,dgx,gdgx,alp,igt,lgp,ids,ipa,ib,izx,ixz)
!              call esgradg(i-nh,x,yy)
            endif
            do j=1,n
              gres(j,i)=yy(j)
            enddo
            gresn(i)=max(one,o8vecn(1,n,yy))
          endif
        endif
      enddo
      idum=0
      runtim=o8cpu(idum)
!***** call the optimizer
      call o8opti(ifunc1,ifunc2,yyy,zzz,ex,tf,tp,sg,par,bnd,
     *            gam,dgx,gdgx,alp,igt,lgp,ids,ipa,ib,izx,ixz)
      runtim=o8cpu(idum)-runtim
!*****
!***** do final solution check and output
      call o8fin

      end subroutine donlp2
!*******************************************************************
!     scalar product of two vectors or parts of vectors
!*******************************************************************
      double precision function o8sc1(i,j,a,b)
!     mulpily two vectors
      implicit none
      include 'o8cons.h'
      integer i,j,k
      double precision a(*),b(*)
      double precision s
      save
        if ( i .gt. j ) then
          o8sc1=zero
          return
        else
          s=zero
          do   k=i,j
            s=s+a(k)*b(k)
          enddo
          o8sc1=s
          return
        endif
      end function o8sc1
      double precision function o8sc2(n,m,j,a,lda,b)
!     multiply row j of matrix a with vector b
      implicit none
      include 'o8cons.h'
      double precision a,b,s
      integer n,m,i,j,lda
      dimension a(lda,*),b(*)
      save
      s=zero
      do  i=n,m
        s=s+a(j,i)*b(i)
      enddo
      o8sc2=s
      return
      end function o8sc2
      double precision function o8sc3(n,m,j,a,lda,b)
!     multiply column j section (n to m) of matrix a with vector b
      implicit none
      include 'o8cons.h'
      double precision a,b,s
      integer n,m,i,j,lda
      dimension a(lda,*),b(*)
      save
      s=zero
      do  i=n,m
        s=s+a(i,j)*b(i)
      enddo
      o8sc3=s
      return
      end function o8sc3
!*****************************************************************
!     subprogram for structured output of a submatrix a(ma,na)
!     externally declared a(me,ne) on channel lognum in fix or
!     float format
!     with heading "head".
!     uses a fixed format string with 70 print columns
!*****************************************************************
      subroutine o8mdru(a,me,ne,ma,na,head,lognum,fix)
      implicit none
      integer me,ne,ma,na,lognum,i,j,jo,ju
      double precision a(me,ne)
      logical fix
      character*40 head
      integer anz
      save
      write(lognum,fmt='(/a40)')  head
      anz=4
      jo=0
      do while ( jo .lt. na )
        ju=jo+1
        jo=min(ju+anz-1,na)
        write(lognum,fmt='(/''row/column'',4(6x,i3,6x))')
     *   (j,j=ju,jo)
        do i=1,ma
          if ( fix ) then
            write(lognum,fmt='(3x,i4,3x,4(g14.7,1x))')
     *         i,(a(i,j),j=ju,jo)
          else
            write(lognum,fmt='(3x,i4,3x,4(d14.7,1x))')
     *         i,(a(i,j),j=ju,jo)
          endif
        enddo
      enddo

      end subroutine o8mdru
!**********************************************************************
!********************************************************************
      subroutine o8egph(gphi)
      include 'o8comm.h'
      include 'o8cons.h'
      double precision gphi(nx)
      integer i,j,l
      save
!**** compute gradient of lagrangian
      do  i=1,n
         gphi(i)=gradf(i) * scf
         do    j=1,nh
           gphi(i)=gphi(i)-u(j)*gres(i,j)
         enddo
         do    j=nh+1,alist(0)
           l=alist(j)
           if( u(l) .gt. zero )
!**** include constraints, whose multipliers are of correct sign only
     f     gphi(i)=gphi(i)-gres(i,l)*u(l)
         enddo
      enddo

      end subroutine o8egph
!******************************************************************
      subroutine o8ht(id,incr,is1,is2,m,a,beta,b,c)
!     application of householder transformations stored
!     in the lower or strict lower (if incr=0 or 1 resp.)
!     triangle of a and in the vector beta on b giving c.
!     only columns is1 to is2 are used in forward manner
!     if id > 0,backwards otherwise.
!     rows is1 to m of c are changed only
      implicit none
      double precision a,beta,b,c,ysum,o8sc3
      integer id,incr,is1,is2,m,i,j,k,it
      include 'o8para.h'
      include 'o8cons.h'
      dimension a(nx,*),beta(*),b(*),c(*)
      save
      do  i=1,m
        c(i)=b(i)
      enddo
      if(is1 .gt. m)       return
      if(is2 .lt. is1)      return
      do    i=is1,is2
        it=i
        if(id .lt. 0)         it=is2-it+is1
!       it=index of transformation
        j=it+incr
        ysum=beta(it)*o8sc3(j,m,it,a,nx,c)
        do  k=j,m
          c(k)=c(k)-ysum*a(k,it)
        enddo
      enddo

      end subroutine o8ht
!*********************************************************************
!     solve triangular system r*x=b, r defined by householder-qr-
!     decomposition decomp (with column scaling)
!*********************************************************************
      subroutine o8sol(nlow,nup,b,x)
      implicit none
      integer nlow,nup
      include 'o8para.h'
      include 'o8cons.h'
      include 'o8rdat.h'
      double precision b(*),x(*),xl(nx)
      save
!*******
      double precision ysum
      integer i,j
      do i=nup,nlow,-1
        ysum=zero
        do j=i+1,nup
          ysum=ysum+qr(i,j)*xl(j)
        enddo
        xl(i)=(b(i)-ysum)/diag(i)
      enddo
      do  i=nlow,nup
        x(i)=xl(i)*cscal(colno(i))
      enddo
!*** there must follow interchange of x as given by colno
!    e.g. xx(colno(i))=x(i)
!***

      end subroutine o8sol
!*********************************************************************
!     solve triangular system r(transpose)*x=b, r defined by
!     householder-qr-decomposition decomp (with column scaling)
!*********************************************************************
      subroutine o8solt(nlow,nup,b,x)
      implicit none
      integer nlow,nup
      include 'o8para.h'
      include 'o8cons.h'
      double precision b(*),x(*)
      include 'o8rdat.h'
!********
      integer i,j
      double precision ysum
      save
      do i=nlow,nup
!*** b has been permuted already !
        x(i)=b(i)*cscal(colno(i))
      enddo
      do i=nlow,nup
        ysum=zero
        do j=nlow,i-1
          ysum=ysum+qr(j,i)*x(j)
        enddo
        x(i)=(x(i)-ysum)/diag(i)
      enddo

      end subroutine o8solt
!***************************************************************
!     lenght of vector (a,b). numerically stable version with
!     overflow / underflow saveguard
!*****************************************************************
      double precision function  o8dsq1(a,b)
      implicit none
      double precision a,b,a1,b1
      include 'o8cons.h'
      save
      a1=abs(a)
      b1=abs(b)
      if ( a1 .gt. b1 ) then
        o8dsq1=a1*sqrt(one+(b1/a1)**2)
      else
        if ( b1 .gt. a1 ) then
          o8dsq1=b1*sqrt(one+(a1/b1)**2)
        else
          o8dsq1=a1*sqrt(two)
        endif
      endif

      end function  o8dsq1
!***********************************************************************
      subroutine o8rght(a,b,y,yl,n)
      implicit none
      include 'o8para.h'
      include 'o8cons.h'
      integer n,i,j
      double precision a(nx,*),b(*),y(*),yl,h
      save
!     o8rght assumes that the cholesky-factor of a
!     a=r(transpose)*r is stored in the upper half of a.
!     b is a right hand side. o8rght solves
!         r*y = b
!     yl=norm(y)**2
      yl=zero
      do   i=n,1,-1
        h=b(i)
        do    j=i+1,n
          h = h - a(i,j)*y(j)
        enddo
        h=h/a(i,i)
        y(i)=h
        yl = h**2 + yl
      enddo

      end subroutine o8rght
!**********************************************************************
      subroutine o8left(a,b,y,yl,n)
      implicit none
      include 'o8para.h'
      include 'o8cons.h'
      integer n,i,j
      double precision a(nx,*),b(*),y(*),yl,h
      save
!     o8left assumes that the cholesky-factor of a
!     a=r(transpose)*r is stored in the upper half of a.
!     b is a right hand side. o8left solves
!         r(transpose)*y = b
!     yl=norm(y)**2
      yl=zero
      do   i=1,n
        h=b(i)
        do    j=1,i-1
          h = h - a(j,i)*y(j)
        enddo
        h=h/a(i,i)
        y(i)=h
        yl = h**2 + yl
      enddo
      return
      end subroutine o8left
!******************************************************************
      double precision function o8vecn(nl,nm,x)
      implicit none
      include 'o8cons.h'
!*** euclidean norm of x , avoid overflow
      integer nl,nm,i
      double precision x(*)
      double precision xm,h
      intrinsic max,abs,sqrt
      save
      if ( nm .lt. nl ) then
        o8vecn=zero
        return
      endif
      xm=abs(x(nl))
      do i=nl+1,nm
        xm=max(xm,abs(x(i)))
      enddo
      if ( xm .eq. zero ) then
        o8vecn=zero
        return
      else
        h=zero
        do i=nl,nm
          h=h+(x(i)/xm)**2
        enddo
        o8vecn=xm*sqrt(h)
        return
      endif
      end function o8vecn
!*****************************************************
      subroutine o8zup(z)
!********** compute updated projected gradient (primal)
      include 'o8comm.h'
      include 'o8cons.h'
      integer ndualm,mdualm,ndual,mi,me,iq
      parameter (ndualm=nx+nresm,mdualm=nresm*2)
      double precision np,rnorm,rlow,xj,ddual,r,ud,ud1
      common /o8dupa/rnorm,rlow,ndual,mi,me,iq
      common /o8qpup/xj(ndualm,ndualm),ddual(ndualm),r(ndualm,ndualm),
     f                np(ndualm),ud(mdualm),ud1(mdualm)
      double precision z(ndualm)
      integer i,j
      double precision su
      save
!******      d = j(trans) *np
      do i=1,ndual
        su=zero
        do      j=1,ndual
          su=su+xj(j,i)*np(j)
        enddo
        ddual(i)=su
      enddo
!****** computation of z
      do      i=1,ndual
        z(i)=zero
        do      j=iq+1,ndual
          z(i)=z(i)+xj(i,j)*ddual(j)
        enddo
      enddo

      end subroutine o8zup
!*******************************************************************
      subroutine o8rup(rv)
!******* compute correction of dual multipliers
      include 'o8comm.h'
      include 'o8cons.h'
      integer ndualm,mdualm,ndual,mi,me,iq
      double precision np,rnorm,rlow,xj,ddual,r,ud,ud1
      parameter (ndualm=nx+nresm,mdualm=nresm*2)
      common /o8dupa/rnorm,rlow,ndual,mi,me,iq
      common /o8qpup/xj(ndualm,ndualm),ddual(ndualm),r(ndualm,ndualm),
     f                np(ndualm),ud(mdualm),ud1(mdualm)
      double precision rv(mdualm),s
      integer i,j
      save
      do      i=iq,1,-1
        s=zero
        do        j=i+1,iq
          s=s+r(i,j)*rv(j)
        enddo
        rv(i)=(ddual(i)-s)/r(i,i)
      enddo

      end subroutine o8rup
!*********************************************************************
      subroutine o8dlcd(ai,l)
!************** delete constraint nr. l
      implicit   none
      include 'o8comm.h'
      include 'o8cons.h'
      double precision np,rnorm,rlow,xj,ddual,r,ud,ud1
      integer ndualm,mdualm,ndual,mi,me,iq
      parameter (ndualm=nx+nresm,mdualm=nresm*2)
      common /o8dupa/rnorm,rlow,ndual,mi,me,iq
      common /o8qpup/xj(ndualm,ndualm),ddual(ndualm),r(ndualm,ndualm),
     f                np(ndualm),ud(mdualm),ud1(mdualm)
      integer ai(mdualm),qq,l,i,j,k
      double precision t1,t2,cc,ss,h,c1,s1,xny,o8dsq1
      save
      do i=1,iq
        if(ai(i) .eq. l)      then
          qq=i
          goto 10
         endif
      enddo
   10 continue
      do i=qq,iq-1
        ai(i)=ai(i+1)
        ud1(i)=ud1(i+1)
        do j=1,ndual
          r(j,i)=r(j,i+1)
        enddo
      enddo
!  20 continue
      ai(iq)=ai(iq+1)
      ud1(iq)=ud1(iq+1)
      ai(iq+1)=0
      ud1(iq+1)=zero
      do  j=1,iq
        r(j,iq)=zero
      enddo
      iq=iq-1
      if(iq .eq. 0)     goto 100
      do j=qq,iq
        cc=r(j,j)
        ss=r(j+1,j)
        h=o8dsq1(cc,ss)
        if(h .eq. zero)            goto 90
        c1=cc/h
        s1=ss/h
        r(j+1,j)=zero
        if ( c1 .lt. zero ) then
          r(j,j)=-h
          c1=-c1
          s1=-s1
        else
          r(j,j)=h
        endif
        xny=s1/(one+c1)
        do k=j+1,iq
          t1=r(j,k)
          t2=r(j+1,k)
          r(j,k)=t1*c1+t2*s1
          r(j+1,k)=xny*(t1+r(j,k))-t2
        enddo
        do k=1,ndual
          t1=xj(k,j)
          t2=xj(k,j+1)
          xj(k,j)=t1*c1+t2*s1
          xj(k,j+1)=xny*(xj(k,j)+t1)-t2
        enddo
   90 continue
      enddo
  100 continue
      rnorm=one
      rlow=one
!*** in order to avoid a compiler error of hp in +op3 mode
      if ( iq .ge. 1 ) then
        rnorm=abs(r(1,1))
        rlow=abs(r(1,1))
        i=1
        do while ( i .lt. iq )
          i=i+1
          rnorm=max(rnorm,abs(r(i,i)))
          rlow=min(rlow,abs(r(i,i)))
        enddo
      endif

      end subroutine o8dlcd
!***********************************************************************
      subroutine o8adcd
!********** add constraint whose gradient is given by np
      implicit   none
      include 'o8comm.h'
      include 'o8cons.h'
      double precision np,rnorm,rlow,xj,ddual,r,ud,ud1
      integer ndualm,mdualm,ndual,mi,me,iq
      parameter (ndualm=nx+nresm,mdualm=nresm*2)
      common /o8dupa/rnorm,rlow,ndual,mi,me,iq
      common /o8qpup/xj(ndualm,ndualm),ddual(ndualm),r(ndualm,ndualm),
     f                np(ndualm),ud(mdualm),ud1(mdualm)
      integer i,j,k
      double precision cc,ss,h,s1,c1,t1,t2,o8dsq1,xny
      save
      do j=ndual,iq+2,-1
        cc=ddual(j-1)
        ss=ddual(j)
        h=o8dsq1(cc,ss)
        if(h .eq. zero) goto 20
        ddual(j)=zero
        s1=ss/h
        c1=cc/h
        if ( c1 .lt. zero ) then
          c1=-c1
          s1=-s1
          ddual(j-1)=-h
        else
          ddual(j-1)=h
        endif
        xny=s1/(one+c1)
        do k=1,ndual
          t1=xj(k,j-1)
          t2=xj(k,j)
          xj(k,j-1)=t1*c1+t2*s1
          xj(k,j)=xny*(t1+xj(k,j-1))-t2
        enddo
   20 continue
      enddo
      iq=iq+1
      do i=1,iq
        r(i,iq)=ddual(i)
      enddo
      rnorm=one
      rlow=one
!*** in order to avoid a compiler error of hp in +op3 mode
      if ( iq .ge. 1 ) then
        rnorm=abs(r(1,1))
        rlow=abs(r(1,1))
        i=1
        do while ( i .lt. iq )
          i=i+1
          rnorm=max(rnorm,abs(r(i,i)))
          rlow=min(rlow,abs(r(i,i)))
        enddo
      endif
      return
      end subroutine o8adcd
!****************************************************************
      subroutine o8rinv(ndim,n,a,ndualm,ndual,x)
!******* computes the inverse of the upper triangular matrix part
!******* of a and stores it in the upper triangle of the
!******* right lower minor of x
!******* declaration dimension of a is ndim, of  x is ndualm
!******* actual dimension of a is n and of x ndual
      implicit none
      include 'o8cons.h'
      integer ndim,ndualm,n,ndual,l,j,k,incr
      double precision a(ndim,*),x(ndualm,ndualm),su
      save
      incr=ndual-n
!***  incr=nr
      do j=n,1,-1
!*** we assume a being sufficiently regular here.
!*** given in this application
!*** see top of o8qpdu
        x(j+incr,j+incr)=one/a(j,j)
        do k=j-1,1,-1
          su=zero
          do l=k+1,j
            su=su+a(k,l)*x(l+incr,j+incr)
          enddo
          x(k+incr,j+incr)=-su/a(k,k)
        enddo
      enddo

      end subroutine o8rinv
!*******************************************************************
      subroutine o8shms
! write short information on standard out
      include 'o8comm.h'
      double precision umin
      save
      if ( te0 .and. .not. silent ) then
        umin=accinf(itstep,11)
        write(*,fmt='(i5,'' fx='',d15.7,'' upsi='',d9.2,
     f  '' b2n='',d9.2,'' umi='',d9.2,'' nr'',i4,'' si'',i2)')
     f   itstep,fx,upsi,b2n,umin,nr,int(accinf(itstep,10))
        write(prou,fmt='(i5,'' fx='',d15.7,'' upsi='',d9.2,
     f  '' b2n='',d9.2,'' umi='',d9.2,'' nr'',i4,'' si'',i2)')
     f   itstep,fx,upsi,b2n,umin,nr,int(accinf(itstep,10))

      endif

      end subroutine o8shms
!************************************************************
      subroutine o8st
!************************************************************
!    initialization program , standard parameter settings done here
!************************************************************
      include 'o8comm.h'
      include 'o8cons.h'
      include 'o8fint.h'
      integer i,j,k,iz
      double precision tol1 ,xnull(nx),bd0, infiny,term
!     double precision gxi,hxi
      character*8 fil,xxx,name1
      save
      data xxx/'xxxxxxxx'/
      epsmac = two**(-20)
100   continue
      epsmac=epsmac/two
      term=one+epsmac
      if ( term .ne. one ) goto 100
      epsmac=epsmac+epsmac
      tolmac=epsmac
200   continue
      tol1=tolmac
      tolmac=tolmac/twop4
      if ( tolmac .ne. zero ) goto 200
      tolmac=tol1
!******** epsmac machine precision, tolmac smallest machine number
!******** larger than zero (approximately , base 16 for exponent
!******** therefore division by 16 assumed)

!***** warning
!      on some machines the computation of tolmac may result in an
!      error
!      because underflow is not accepted as zero as is assumed here

!*****
      if ( n .gt. nx ) stop 'donlp2: n too large/recompile'
      if ( nh+ng .gt. nresm )stop 'donlp2:nh or ng too large/recompile'
      if ( tau0 .eq. zero ) tau0=one
      if ( del0 .eq. zero ) del0=tau0*p5
!      if ( del0 .gt. tau0 ) del0=tau0*p5
      if ( nreset .gt. n )  nreset=n
      if ( nreset .le. 4  ) nreset=4
!****** standard initialization
      lastch=0
      lastdw=0
      lastup=0
      level=one
      tau=tm1
      iterma=maxit
      epsx=tm5
      sigsm=sqrt(epsmac)
      smalld=tm1
! formerly tm2. smalld has much influence on the maratos-effect
      smallw=exp(two*log(epsmac)/three)
      rho=tm6
      rho1=tm10
      del01=del0/tp1
      delmin=min(del01,max(tm6*del0,smallw))
      if ( .not. analyt ) delmin=min(del01,max(epsdif,delmin))
      c1d=tm2
      scfmax=tp4
      taufac=tp1
      taumax=scfmax**2
      updmy0=tm1
!     take del0 and tau0 from block data or setup0 in function
!     definition
!     may be modified by subsequent call of setup
        j=1
        do while ( index(name(j:40),' ') .eq. 1 )
          j=j+1
        enddo
        if ( j .gt. 40 ) then
          fil=xxx
        else
          k=index(name(j:40),' ')
          k=k+j-1
          if ( k .gt. j+7 ) k=j+7
          if ( k .lt. j ) k=j
          if ( k .gt. 40 ) k=40
          name1=name(j:k)
          do i=1,k-j+1
            iz=ichar(name1(i:i))
            if ( iz .lt. 48 .or. ( iz .gt. 57 .and. iz .lt. 65 )
     f       .or. ( iz .gt. 90 .and. iz .lt. 97 ) .or. iz .gt. 122 )
     f      name1(i:i)='x'
          enddo
          if ( k .lt. j+7 ) then
            fil=name1(1:k-j+1)//xxx(1:j+7-k)
          else
            fil=name1(1:8)
          endif
        endif
      if ( .not. silent ) open(meu,file=fil//'.mes',status='unknown')
      if ( .not. silent ) open(prou,file=fil//'.pro',status='unknown')
      infiny=epsmac/tolmac
      fx=zero
      b2n=zero
      b2n0=zero
      nres=ng+nh
      if ( cold ) then
        do i=1,nx
          do j=1,nx
            a(j,i)=zero
          enddo
          a(i,i)=one
          diag0(i)=one
        enddo
      endif
      do i=1,nresm
        diag(i)=zero
        do j=1,nx
          qr(j,i)=zero
          gres(j,i)=zero
        enddo
      enddo
      do i=1,nx
        xnull(i)=zero
        ug(i)=-infiny
        og(i)=infiny
        llow(i)=.false.
        lup(i)=.false.
      enddo
      do i=1,nh
        delfac(i)=one
      enddo
!      if ( bloc ) call user_eval(xnull,0)
      do i=nh+1,nres
        delfac(i)=one
!***** scan for real lower or upper bounds
        if ( gunit(1,i) .eq. 1 ) then
!           guniot(1,i) must be -1
!          call esg(i-nh,xnull,gxi)
!          if ( gunit(3,i) .gt. 0 ) then
!            llow(gunit(2,i))=.true.
!            ug(gunit(2,i))=-gxi/gunit(3,i)
!          else
!            og(gunit(2,i))=-gxi/gunit(3,i)
!            lup(gunit(2,i))=.true.
!          endif
        endif
      enddo
      do i=nh+1,nres
!**** modify del0, such that lower and upper bound never become
!     binding simultaneously
        if ( gunit(1,i) .eq. 1 ) then
!          j=gunit(2,i)
!          if ( og(j) .lt. infiny .and. ug(j) .gt. -infiny ) then
!            del0=min(del0,(og(j)-ug(j))*tm1*abs(gunit(3,i)))
!          endif
        endif
      enddo
      do i=nh+1,nres
!**** delfac corresponds to an indirect primal scaling
        if ( gunit(1,i) .eq. 1 ) then
!          j=gunit(2,i)
!          if ( gunit(3,i) .gt. 0 ) then
!            delfac(i)=max(delfac(i),abs(ug(j))*tm1)
!            if ( og(j) .lt. infiny )
!     f        delfac(i)=min(delfac(i),(og(j)-ug(j))/(tp1*del0))
!          else
!            delfac(i)=max(delfac(i),abs(og(j))*tm1)
!            if ( ug(j) .gt. -infiny )
!     f        delfac(i)=min(delfac(i),(og(j)-ug(j))/(tp1*del0))
!          endif
        endif
      enddo
      bd0=infiny
      do i=1,n
        if ( ug(i) .gt. zero ) bd0=min(bd0,og(i))
        if ( og(i) .lt. zero ) bd0=min(bd0,-ug(i))
      enddo
!**************** change x if necessary , such that bounds not violated
      corr=.false.
!** evaluate gradients of supersimple functions only once
!** a function is said to be supersimple iff it is of the form a*x(j)+b
      if ( gunit(1,0) .eq. 1 ) then
!        gconst(0)=.true.
!        val(0)=.true.
!        do i=1,n
!          gradf(i)=zero
!        enddo
!        gradf(gunit(2,0))=gunit(3,0)*xsc(gunit(2,0))
!        gfn=abs(gunit(3,0))
      else
        val(0)=.false.
        do i=1,n
          gradf(i)=zero
        enddo
      endif
      do i=1,nh
        if ( gunit(1,i) .eq. 1 ) then
!*** a fixed variable. corrected if necessary
!         val(i)=.true.
!         gconst(i)=.true.
!          gres(gunit(2,i),i)=gunit(3,i)*xsc(gunit(2,i))
!          gresn(i)=abs(gunit(3,i))*xsc(gunit(2,i))
!          if ( gresn(i) .eq. zero ) then
!            if ( .not. silent )
!     f         write(meu,*) gunit(2,i), ' fixed variable/zero gradient'
!            close(meu)
!            close(prou)
!            stop
!          endif
!          call esh(i,xnull,hxi)
!          term=-hxi/gunit(3,i)
!          if ( term .ne. x(gunit(2,i)) ) corr=.true.
!          x(gunit(2,i))=term
        endif
      enddo
!      if ( bloc ) call user_eval(x,0)
      do i=nh+1,nres
        if ( gunit(1,i) .eq. 1 ) then
!          if ( gunit(3,i) .eq. 0 ) then
!            if ( .not. silent ) write(meu,*)
!     f        gunit(2,i),' bounded variable, zero gradient'
!            close(meu)
!            close(prou)
!            stop
!          endif
!          call esg(i-nh,x,gxi)
!          gxi=two*delmin-gxi
!          if ( gxi .gt. zero ) then
!            corr=.true.
!            x(gunit(2,i))=x(gunit(2,i))+gxi/gunit(3,i)
!          endif
!          gres(gunit(2,i),i)=gunit(3,i)*xsc(gunit(2,i))
!          gresn(i)=abs(gunit(3,i))*xsc(gunit(2,i))
!          val(i)=.true.
!          gconst(i)=.true.
        endif
      enddo
!***
      if ( corr .and. .not. silent ) call o8msg(13)
!***
!     remember to reevaluate the functions if corr=.true.
!     and bloc=.true.
!      if ( bloc ) call user_eval(x,1)
!***
      do i = 1,nres
        bind(i)=0
        bind0(i)=0
        u(i)=zero
        u0(i)=zero
        cres(i) = 0
        cgres(i) = 0
! initial weights of the penalty-term
        if ( cold ) w(i)=one
        sort(i)=i
      enddo
      clow=nint(one)
      ny=two

!********* scf = weight factor for objective function
!********* scf0 = damping factor for tangential direction
      scf=one
      scf0=one
      sigla=twop11
      beta=four
! formerly two
      alpha=tm1
! delta =tm2 formerly
      delta1=p9
      delta=tm3
      theta=p9
! theta=0.99 formerly
      icf=0
      icgf=0
      if ( .not. silent ) then
        write(prou,*) 'donlp2, v3, 05/29/98, copyright p. spellucci '
        call o8tida (meu)
        write(prou,*) name
        write(meu,*)  'donlp2, v3, 05/29/98, copyright p. spellucci '
        call o8tida (prou)
        write(meu,*) name
      endif

      end subroutine o8st
!***********************************************************************
      subroutine o8info(icase)
      implicit   none
      include 'o8comm.h'
      include 'o8cons.h'
      integer icase,i,j,l,k
      double precision y,phih
      character*40 head
      save
      if(.not.te2)      return
      goto (1000,2000,3000,4000,5000,6000,7000,8000,9000,10000,
     f      11000,12000,13000,14000,15000,16000,17000,18000,
     f      19000,20000,21000,22000),icase
 1000 continue
      write(prou,1100) itstep,scf,psist,psi,upsi,fxst,fx,
     f             (x(i),i=1,n)
      write(prou,*) ' valid permutation of x'
      write(prou,1200) (perm(i),i=1,n)
 1200 format((/20(i3,1x)))
        if ( phase .ge. 0 .and. te3 ) then
          head='quasi-newton-matrix full update'
          call o8mdru(a,nx,nx,n,n,head,prou,.false.)
        endif
 1100 format(///80(1h=),/10x,i4,'-th iteration step',/
     f       '   scf=',d12.5,' psist=',d12.5,
     f       '   psi=',d12.5,'  upsi=',d12.5,
     f      /'  fxst=',d12.5,'    fx=',
     f       d12.5,/'  x=',/6(1x,d12.5))
      if ( intakt ) then
        write(*,1100) itstep,scf,psist,psi,upsi,fxst,fx,
     f             (x(i),i=1,n)
        write(*,*) ' valid permutation of x'
        write(*,1200) (perm(i),i=1,n)
      endif
      return
 2000 continue
      write(prou,2100) del,b2n0,b2n,gfn
      if(alist(0).ne. 0)
     f write(prou,2200) (alist(i),res(alist(i)),gresn(alist(i)),
     f                i=1,alist(0))
      if(alist(0) .ne. 0 .and. .not. singul )
     f write(prou,2400)(diag(i),i=1,alist(0))
      if(alist(0).ne. 0 .and. te3 )  then
        do i=1,alist(0)
          l=alist(i)
          write(prou,2300) l, (gres(j,l),j=1,n)
        enddo
      endif
      if ( intakt ) then
        write(*,2100) del,b2n0,b2n,gfn
        if(alist(0).ne. 0)
     f    write(*,2200) (alist(i),res(alist(i)),gresn(alist(i)),
     f                i=1,alist(0))
        if(alist(0) .ne. 0 .and. .not. singul )
     f      write(*,2400)(diag(i),i=1,alist(0))
        if(alist(0).ne. 0 .and. te3 )  then
          do i=1,alist(0)
            l=alist(i)
            write(*,2300) l, (gres(j,l),j=1,n)
          enddo
        endif
      endif
      return
 2100 format(//'  del=',d13.6,'  b2n0=',d13.6,'   b2n=',d13.6,
     f       '   gfn=',d13.6)
 2200 format(//1x,'values of restrictions',
     f       (/1x,2(:,'(',i4,2x,d12.5,2x,d12.5,')',2x)))
 2300 format(//1x,'gradient of restriction nr.',i4,/
     f       (1x,5(d12.5,2x)))
 2400 format(//1x,'  diag(r)=',/6(1x,d12.5))
 3000 continue
      if( .not. (nr .eq. 0 .or. phase .eq. -1) )
     f  write(prou,3100)   (alist(k),u(alist(k)),k=1,nr)
      if( .not. (nr .eq. 0 .or. phase .eq. -1)  .and. intakt )
     f  write(*,3100)   (alist(k),u(alist(k)),k=1,nr)
 3100 format(/'  multipliers: first estimate'/,'  u =',
     f        /4(1x,i4,1x,d12.5))
      return
 4000 continue
      if( .not. (nr .eq. 0 .or. phase .eq. -1) )
     f  write(prou,4100)   (alist(k),u(alist(k)),k=1,nr)
      if( .not. (nr .eq. 0 .or. phase .eq. -1)  .and. intakt )
     f  write(*,4100)   (alist(k),u(alist(k)),k=1,nr)
 4100 format(/'  multipliers: second estimate'/,
     f  '  u =',/4(1x,i4,1x,d12.5))
      return
 5000 continue
      if ( intakt )
     f  write(*,*) '  condition number of r     ',accinf(itstep,13)
      write(prou,*) '  condition number of r     ',accinf(itstep,13)
      if ( phase .eq. -1 ) then
        return
      else
        write(prou,*)
     f    '  condition number of a     ',accinf(itstep,14)
        if ( intakt ) then
          write(*,*)
     f    '  condition number of a     ',accinf(itstep,14)
        endif
        return
      endif
 6000 continue
      return
 7000 continue
      write(prou,7100) phase,scf0,(d(i),i=1,n)
      if(phase .eq. 2)    write(prou,7200)  (dd(i),i=1,n)
      if ( intakt ) then
        write(*,7100) phase,scf0,(d(i),i=1,n)
        if(phase .eq. 2)    write(*,7200)  (dd(i),i=1,n)
      endif
      return
 7100 format(//1x,' phase=',i3,'  scf0=',d12.5/1x,' d ='/6(1x,d12.5))
 7200 format(//1x,' dd='/6(1x,d12.5))
 8000 continue
      y=tau0*p5
      phih=fx*scf+psi
      if ( intakt )write(*,8100)phih,dirder,psi,y,fx,dscal,scf,upsi
      write(prou,8100) phih,dirder,psi,y,fx,dscal,scf,upsi
      return
 8100 format(//1x,'start unimin',//'    phi=',d12.5,'   dphi=',d12.5,
     f '    psi=',d12.5,' tau0/2=',d12.5,
     f       /'     fx=',d12.5,'  dscal=',
     f       d12.5,'    scf=',d12.5,'   upsi=',d12.5)
 9000 continue
      write(prou,9100)   sig,fx1,psi1,upsi1
      if ( intakt ) write(*,9100)   sig,fx1,psi1,upsi1
 9100 format('    sig=',d12.5,'     fx=',d12.5,
     f       '    psi=',d12.5,'   upsi=',d12.5 )
      return
10000 continue
      write(prou,10100) sig,cfincr,(violis(i),i=1,violis(0))
      if ( violis(0) .eq. 0 ) write(prou,*) 'none'
      if ( intakt ) then
        write(*,10100) sig,cfincr,(violis(i),i=1,violis(0))
        if ( violis(0) .eq. 0 ) write(*,*) 'none'
      endif
      return
10100 format(//1x,'end unimin',//1x,'sig=',d12.5,
     f       '  num. f-evaluations',i2,
     f       /1x,'list of inactive hit constraints',/(13(i4,2x)))
11000 continue
      if ( intakt )
     f write(*,*) 'additional increase of eta due to large clow'
      write(prou,*) 'additional increase of eta due to large clow'
      return
12000 continue
      write(prou,12100)   scf,clow,eta
      if(nres .ne. 0)    write(prou,12200)   (w(i),i=1,nres)
      if ( intakt ) then
        write(*,12100)   scf,clow,eta
        if(nres .ne. 0)  write(*,12200) (w(i),i=1,nres)
      endif
      return
12100 format(//2x,'current scaling,  scf = ',d12.5,' clow = ',i12,
     f   ' eta = ',d12.5)
12200 format(//2x,'scalres='/6(1x,d12.5))
13000 continue
      if ( accinf(itstep,27) .eq. zero ) then
        if ( intakt ) write(*,*) 'update suppressed'
        write(prou,*) 'update suppressed'
      elseif ( accinf(itstep,27) .eq. -one ) then
        write(prou,*) 'restart with scaled unit matrix'
        if ( intakt )
     f    write(*,*) 'restart with scaled unit matrix'
      else
        write(prou,*) 'bfgs-update'
        write(prou,fmt='('' type ='',d15.7)') accinf(itstep,27)
        write(prou,fmt='(''  ny  ='',d15.7)') accinf(itstep,28)
        write(prou,fmt='('' thet ='',d15.7)') accinf(itstep,29)
        if ( intakt ) then
          write(*,*) 'bfgs-update'
          write(*,fmt='('' type ='',d15.7)') accinf(itstep,27)
          write(*,fmt='(''  ny  ='',d15.7)') accinf(itstep,28)
          write(*,fmt='('' thet ='',d15.7)') accinf(itstep,29)
        endif
      endif
      return
14000 continue
      if ( accinf(itstep,27) .eq. zero ) then
        if ( intakt ) write(*,*) 'update suppressed'
        write(prou,*) 'update suppressed'
      elseif ( accinf(itstep,27) .eq. one ) then
        write(prou,*) 'bfgs-update as in pantoja and mayne'
        write(prou,fmt='(''  tk  ='',d15.7)') accinf(itstep,28)
        write(prou,fmt='('' xsik ='',d15.7)') accinf(itstep,29)
        if ( intakt ) then
          write(*,*) 'bfgs-update'
          write(*,fmt='(''  tk  ='',d15.7)') accinf(itstep,28)
          write(*,fmt='('' xsik ='',d15.7)') accinf(itstep,29)
        endif
      else
        if ( intakt ) write(*,*) 'restart with scaled unit matrix'
        write(prou,*) 'restart with scaled unit matrix'
      endif
      return
15000 continue
      if ( intakt ) write(*,15100)
      write(prou,15100)
15100 format(///' singular case : full regularized sqp')
      if ( intakt ) write(*,*) '  del =',del
      write(prou,*) '  del =',del
      if ( intakt )
     f  write(*,15200) (alist(i),res(alist(i)),gresn(alist(i)),
     f                i=1,alist(0))
      write(prou,15200)  (alist(i),res(alist(i)),gresn(alist(i)),
     f                i=1,alist(0))
15200 format(//2x,'values of restrictions',
     f       (/2x,2(:,'(',i4,2x,d12.5,2x,d12.5,')',2x)))
      if ( intakt ) write(*,12200) (w(i),i=1,nres)
      write(prou,12200) (w(i),i=1,nres)
      if (  te3 ) then
        head='gradients of constraints'
        call o8mdru(gres,nx,nresm,n,nres,head,prou,.false.)
      endif
      return
16000 continue
      write(prou,*) 'exit from full sqp'
      write(prou,16100) (accinf(itstep,i),i=30,32)
16100 format('            termination reason ',e8.1,/
     f       '          final value of tauqp ',e11.4,/
     f       '      sum norm of slack vector ',e11.4)
      write(prou,7100) phase,scf0,(d(i),i=1,n)
      if ( nres .ne. 0 ) write(prou,3100) (k,u(k),k=1,nres)
      if ( intakt ) then
        write(*,*) 'exit from full sqp'
        write(*,16100) (accinf(itstep,i),i=30,32)
        write(*,7100) phase,scf0,(d(i),i=1,n)
        if ( nres .ne. 0 ) write(*,3100) (k,u(k),k=1,nres)
      endif
      return
17000 continue
      write(prou,*) 'small directional derivative ',dirder,': finish'
      if ( intakt )
     f  write(*,*) 'small directional derivative ',dirder,': finish'
      return
18000 continue
      if ( intakt )
     f write(*,*) 'small correction from full regularized sqp,finish'
      write(prou,*) 'small correction from full regularized sqp,finish'
      return
19000 continue
      write(prou,*) 'qp-solver terminated unsuccessfully'
      if ( intakt ) write(*,*) 'qp-solver terminated unsuccessfully'
      return
20000 continue
      if ( intakt ) write(*,*) 'restart with scaled unit matrix'
      write(prou,*) 'restart with scaled unit matrix'
      return
21000 continue
      return
22000 continue

      end subroutine o8info
!***********************************************************************
!     qr-decomposition of matrix of gradients of binding constraints
!     this set may be expanded using multiple calls to o8dec.
!     no exit on singular r-factor here. information on
!     the decompostion is stored in betaq and in and below the
!     diagonal of qr. r-factor is stored in diag (diagonal ) and
!     above the diagonal of qr. cscal is the column scaling of the
!     original matrix. column pivoting is done here  and is stored
!     in colno
!***********************************************************************
      subroutine o8dec(nlow,nrl)
      implicit   none
      include 'o8comm.h'
      include 'o8cons.h'
      integer nlow,nrl,n1,n2
      integer i,j,k,l,i1,i2,ipiv
      real    (kind=8) :: ysum,term,dalpha,ddbeta,qrii,dl,o8sc3
      real    (kind=8) :: qri(nx),qri0(nx),o8vecn,curle
      save
      external o8vecn,o8sc3
      if ( nlow .gt. nrl ) return
      if ( nlow .eq. 1 ) rank=0
      dl=one/(n+n+n)
      do  i=nlow,nrl
        diag(i)=zero
        betaq(i)=zero
        colno(i)=i
        do j=1,n
          qri(j)=gres(j,alist(i))
        enddo
        call o8left(a,qri,qri0,ysum,n)
        if ( ysum .eq. zero ) then
          cscal(i)=one
          colle(i)=zero
          do j=1,n
            qr(j,i)=zero
          enddo
        else
          do j=1,n
            qri(j)=qri0(perm(j))
          enddo
          term=one/sqrt(max(ysum,rho**2))
          cscal(i)=term
          if ( nlow .gt. 1 ) then
            call o8ht(1,0,1,rank,n,qr,betaq,qri,qri0)
            do j=1,n
              qri(j)=qri0(j)
            enddo
          endif
          do j=1,n
            qr(j,i)=qri(j)*term
          enddo
!*** colle : length of remaining column  squared
          colle(i)=(o8vecn(rank+1,n,qri)*term)**2
        endif
      enddo
      if ( nlow .gt. 1 .and. rank .lt. nlow-1 ) then
!   shift zero block to the right
         i1=nlow-1-rank
         i2=nrl-nlow+1
         do i=1,min(i1,i2)
           ipiv=rank+i
           k=nrl-i+1
           term=betaq(k)
           betaq(k)=betaq(ipiv)
           betaq(ipiv)=term
           j=colno(k)
           colno(k)=colno(ipiv)
           colno(ipiv)=j
           term=colle(k)
           colle(k)=colle(ipiv)
           colle(ipiv)=term
           do j=1,n
             term=qr(j,k)
             qr(j,k)=qr(j,ipiv)
             qr(j,ipiv)=term
           enddo
         enddo
      endif
      if ( nlow .gt. 1 ) then
        n1=rank+1
        n2=n1+nrl-nlow
      else
        n1=nlow
        n2=nrl
      endif
      do  i=n1,n2
!  search for pivot column
        ipiv=i
        curle=colle(i)
        do j=i+1,n2
          if ( colle(j) .gt. curle ) then
            curle=colle(j)
          endif
        enddo
        do j=n2,i,-1
          if ( colle(j) .ge. curle/three ) ipiv=j
        enddo
!  interchange columns explicitly, if necessary
!  make interchanges continuous with respect to x
        if ( ipiv .ne. i ) then
          j=colno(i)
          colno(i)=colno(ipiv)
          colno(ipiv)=j
          term=colle(i)
          colle(i)=colle(ipiv)
          colle(ipiv)=term
          do k=1,n
            term=qr(k,i)
            qr(k,i)=qr(k,ipiv)
            qr(k,ipiv)=term
          enddo
        endif

        ysum=zero
        do   j=i,n
          term=qr(j,i)
          qri(j)=term
          ysum=term*term+ysum
        enddo
        if(ysum .le. rho**2 ) then
!   set tiny values to zero
          do j=i,n2
            colle(j)=zero
            do k=i,n
              qr(k,j)=zero
            enddo
          enddo
          rank=i-1
          return
        endif
        qrii=qri(i)
        dalpha=-sqrt(ysum)
        if ( abs(qrii) .le. -dalpha*dl ) then
          term=zero
          do j=i+1,n
            if ( abs(qri(j)) .gt. term ) then
              term=abs(qri(j))
              l=j
            endif
          enddo
          k=perm1(i)
          perm1(i)=perm1(l)
          perm1(l)=k
        endif
        if(qrii .lt. zero)      dalpha=-dalpha
        ddbeta =one/(ysum-qrii*dalpha)
        diag(i)=dalpha
        betaq(i)=ddbeta
        qri(i)=qrii-dalpha
        qr(i,i)=qri(i)
        rank=i
        i1=i+1
        do  j=i1,n2
          ysum=ddbeta*o8sc3(i,n,j,qr,nx,qri)
          do   k=i,n
            qr(k,j)=qr(k,j)-ysum*qri(k)
          enddo
          colle(j)=colle(j)-qr(i,j)**2
        enddo
      enddo

      end subroutine o8dec
!*******************************************************************
      subroutine o8sce
!  computation of new scaling factors for l1-penalty-function
      implicit   none
      include 'o8comm.h'
      include 'o8cons.h'
      integer i
      double precision term,s1,s2,diff0
      logical wlow
      save
      wlow=.false.
      do i=1,nres
!***** w1 tentative new weights
        term=ny*abs(u(i))+tau
        if ( term .gt. w(i) ) then
          w1(i)=term+tau
        else
          w1(i)=w(i)
          if ( term .lt. w(i)*p5 .and.
     f         bind(i) .eq. 1  ) w1(i)=(term+w(i))*p5
        endif
        if ( w1(i) .lt. w(i)) wlow=.true.
      enddo
!  wlow equals true if one tentative weight at least has been decreased
      s1=zero
      s2=zero
      do i=1,nres
        if ( i .le. nh ) then
          s1=s1+w1(i)*abs(resst(i))
          s2=s2+w1(i)*abs(res(i))
        else
          s1=s1-min(zero,resst(i))*w1(i)
          s2=s2-min(zero,res(i))*w1(i)
        endif
      enddo
      diff0=(fxst-fx)*scf+(s1-s2)
      if ( wlow .and. diff0 .ge. eta*clow
     f    .and. itstep-lastdw .gt. max(5,min(20,n/10)) ) then
!***** accept new (diminished ) weights
        if ( clow .gt. itstep/10 ) then
          eta=onep3*eta
          if ( .not. silent ) call o8info(11)
        endif
        lastch=itstep
        lastdw=itstep
        level=diff0/iterma
        psist=s1
        psi=s2
        do i=1,nres
          w(i)=w1(i)
        enddo
        clow=clow+nint(one)
      else
!***** increase individual weights if necessary. let weigths unchanged
!***** otherwise
        s1=zero
        s2=zero
        do i=1,nres
          if ( w1(i) .gt. w(i) ) then
            lastup=itstep
            lastch=itstep
          endif
          w(i)=max(w(i),w1(i))
          if ( i.le. nh ) then
            s1=s1+w(i)*abs(resst(i))
            s2=s2+w(i)*abs(res(i))
          else
            s1=s1-w(i)*min(zero,resst(i))
            s2=s2-w(i)*min(zero,res(i))
          endif
        enddo
        psist=s1
        psi=s2
      endif
      term=zero
      if ( nres .ge. 1 ) term=w(1)
      do i=2,nres
        term=max(term,w(i))
      enddo
      accinf(itstep,20)=term
!****** maximum of weights
      accinf(itstep,19)=clow
      if ( .not. silent ) call o8info(12)
      return
      end subroutine o8sce
!********************************************************************
      subroutine o8cutd
!**** cut d if appropriate and rescale
      implicit   none
      include 'o8comm.h'
      include 'o8cons.h'
      integer i
      double precision term,term1,o8vecn,o8sc1
      external o8vecn,o8sc1
      save
      xnorm=o8vecn(1,n,x)
      term=beta*(xnorm+one)
      dnorm=o8vecn(1,n,d)
      d0norm=o8vecn(1,n,d0)
      dscal=one
      if ( dnorm*d0norm .ne. zero ) then
        cosphi=o8sc1(1,n,d,d0)/(d0norm*dnorm)
      else
        cosphi=zero
      endif
      if ( dnorm .gt. term ) then
!*****  d too long:  rescale
        term1=term/dnorm
        dnorm=term
        dscal=term1
        do i= 1,n
          d(i)=d(i)*term1
          dd(i)=dd(i)*term1**2
        enddo
      endif
!*** since we project the ray with respect to the bounds, be sure
!*** to compute the directional derivative correctly
!*** therefore correct d and dd appropriately
      do i=1,n
        if ( llow(i) .and. x(i)+sigsm*d(i) .le. ug(i)
     f     ) then
          d(i)=zero
          dd(i)=max(zero,dd(i))
        endif
        if ( lup(i) .and. x(i)+sigsm*d(i) .ge. og(i)
     f     ) then
         d(i)=zero
         dd(i)=min(zero,dd(i))
        endif
      enddo
      dnorm=o8vecn(1,n,d)

      end subroutine o8cutd
!****************************************************************
      subroutine o8inim
!***  initialize the quasi newton update with a multiple of the
!     identity
      implicit   none
      include 'o8comm.h'
      include 'o8cons.h'
      integer i,j
      save
          do i=1,n
            do j=1,n
              a(j,i)=zero
            enddo
            a(i,i)=matsc
            diag0(i)=matsc
          enddo
          accinf(itstep,27)=-one
          accinf(itstep,14)=one
          if ( .not. silent ) call o8info(20)

      end subroutine o8inim
!****************************************************************
!     compute the directional derivative of the l1-penalty-function
!****************************************************************
      subroutine o8dird
      implicit   none
      include 'o8comm.h'
      include 'o8cons.h'
      integer i
      double precision term,term1,o8sc1,o8sc3
      external o8sc1,o8sc3
      save

!***** compute directional derivative of zangwill function

      dirder=o8sc1(1,n,gradf,d)*scf
      do  i=1,nres
        term=o8sc3(1,n,i,gres,nx,d)*w(i)
        term1=res(i)
        if ( i .le. nh ) then
          if ( term1/gresn(i) . lt. -tp3*epsmac ) then
            dirder=dirder-term
          else
            if ( term1/gresn(i) .gt. tp3*epsmac ) then
              dirder=dirder+term
            else
              dirder=dirder+abs(term)
            endif
          endif
        else
          if ( bind(i) .eq. 1 ) then
            if ( abs(term1)/gresn(i) .le. tp3*epsmac ) then
              dirder=dirder-min(zero,term)
            else
              if ( term1/gresn(i) .lt. -tp3*epsmac ) then
                if ( term .gt. zero ) term=min(term,-res(i)*w(i))
!*****  only negative values of the constraints contribute to the
!*****  zangwill function
                dirder=dirder-term
              endif
            endif
          endif
        endif
      enddo

      end subroutine o8dird
!********************************************************************
      subroutine o8smax
      implicit   none
      include 'o8comm.h'
      include 'o8cons.h'
      integer i
      logical exis
      save
!****
!***** compute maximum stepsize stmaxl such that projection
!***** on the box of lower and upper bounds changes
!***** for sig in [0,stmaxl],if such exists
      exis=.true.
      do i=1,n
        exis= exis .and.( ( d(i) .eq. zero )
     f        .or. ( lup(i) .and. d(i) .gt. zero )
     f        .or. ( llow(i) .and. d(i) .lt. zero ) )
      enddo
      if ( exis ) then
        stmaxl=sigsm
        do  i=1,n
          if (llow(i) .and. d(i) .lt. zero) then
            if ( -d(i)*sigla .ge. x(i)-ug(i) ) then
              stmaxl=max(stmaxl,(x(i)-ug(i))/(-d(i)))
            else
              stmaxl=sigla
            endif
          endif
          if ( lup(i) .and. d(i) .gt. zero) then
            if ( d(i)*sigla .ge. og(i)-x(i) ) then
              stmaxl=max(stmaxl,(og(i)-x(i))/d(i))
            else
              stmaxl=sigla
            endif
          endif
        enddo
      else
        stmaxl=sigla
      endif
!***** but never use stepsize larger than sigla
      stmaxl=min(sigla,stmaxl)

      end subroutine o8smax
!********************************************************************
      subroutine o8rest
!**** restore the best point found so far to be the current new point
      implicit   none
      include 'o8comm.h'
      integer j
      save
        phi1=phimin
        psi1=psimin
        upsi1=upsim
        sig=sigmin
        fx1=fmin
        do j=1,n
          x1(j)=xmin(j)
        enddo
        do j=1,nres
          res1(j)=resmin(j)
        enddo

      end subroutine o8rest
!********************************************************************
      subroutine o8save
!**** save the best point found so far in ...min variables
      implicit   none
      include 'o8comm.h'
      integer i
      save
        phimin=phi1
        upsim=upsi1
        psimin=psi1
        fmin = fx1
        sigmin = sig
        do  i=1,n
          xmin(i)=x1(i)
        enddo
        do  i=1,nres
          resmin(i)=res1(i)
        enddo

      end subroutine o8save
!*******************************************************************
      subroutine o8upd(r,z,y,n,fail)
      implicit none
      include 'o8para.h'
      include 'o8cons.h'
      integer n,i,j,i1
      logical fail
      double precision r(nx,*),z(*),y(*)
      double precision sdiag(nx),rn1(nx),w(nx)
      double precision yl,zl,wl,wn1,ai,bi,h,o8dsq1
      intrinsic abs,sqrt
      save
      external o8dsq1
!     o8upd  computes  the upper triangular cholesky-factor
!     r1 of
!               r(transpose)*r+z*z(transpose)-y*y(transpose)
!     and restores it in r. the strict lower triangle of r re-
!     mains unchanged.
!     fail=true if the decomposition does'nt exist, stop on dimension
!     error, fail=false on success.
      if ( n .gt. nx ) then
        stop 'o8upd wrong call'
      endif
      fail=.false.
!     save subdiagonal
      do    i=1,n-1
      sdiag(i)=r(i+1,i)
      r(i+1,i)=zero
      enddo
!     step one: include z*z(transpose)
      zl=zero
      do   i=1,n
        zl = zl + z(i)**2
      enddo
      if ( zl .ne. zero ) then
!     solve w(transpose)*r=z(transpose)
      call o8left(r,z,w,wl,n)
      wl=sqrt(wl+one)
!     u(2)*u(3)*...*u(n)*w = ( norm(w),0,..,0)(transpose)
!     u(i) rotations
      do   i=n,2,-1
      if ( w(i) .ne. zero ) then
        i1=i-1
        ai=w(i1)
        bi=w(i)
        w(i1)=o8dsq1(ai,bi)
        ai=ai/w(i1)
        bi=-bi/w(i1)
        r(i,i1)=bi*r(i1,i1)
        r(i1,i1)=ai*r(i1,i1)
        do   j=i,n
          h = ai*r(i1,j) - bi*r(i,j)
          r(i,j) = bi*r(i1,j) + ai*r(i,j)
          r(i1,j) = h
        enddo
      endif
      enddo
!     r=d*r, d=diag(wl,1,...,1), r now hessenberg
      do    i=1,n
      r(1,i)=r(1,i)*wl
      enddo
!     r=u(n-1)*...*u(1)*r now upper triangular,
!     u(i)  givens-rotations
      do    i=1,n-1
        i1=i+1
        ai=r(i,i)
        bi=-r(i1,i)
        h=o8dsq1(ai,bi)
        if ( h .ne. zero ) then
          ai=ai/h
          bi=bi/h
          r(i,i)=h
          r(i1,i)=zero
          do   j=i+1,n
            h = ai*r(i,j) - bi*r(i1,j)
            r(i1,j) = bi*r(i,j) + ai*r(i1,j)
            r(i,j) = h
          enddo
        endif
      enddo
      endif
!     step two :  include -y*y(transpose)
      yl=zero
      do   i=1,n
        yl = yl + y(i)**2
      enddo
      if ( yl .ne. zero ) then
        call o8left(r,y,w,wl,n)
        if ( wl .ge. one ) then
          fail=.true.
        else
          wl=sqrt(abs(one-wl))
          wn1=wl
!******************************************************
!      ( r(new) ,0 )                (    r  , w )
!      (-----------) = u(1)*...u(n)*(-----------)
!      (y(transp),1)                ((0,..,0),wl)
!******************************************************
          do    i=n,1,-1
            ai=wn1
            bi=w(i)
            wn1=o8dsq1(ai,bi)
            if ( wn1 .ne. zero ) then
              ai=ai/wn1
              bi=bi/wn1
              rn1(i)=bi*r(i,i)
              r(i,i)=ai*r(i,i)
              do    j=i+1,n
                h = ai*r(i,j) - bi*rn1(j)
                rn1(j) = bi*r(i,j) + ai*rn1(j)
                r(i,j) = h
              enddo
            endif
          enddo
        endif
      endif
!     restore subdiagonal
      do   i=1,n-1
        r(i+1,i)=sdiag(i)
      enddo

      end subroutine o8upd
!**********************************************************************
      subroutine o8bfgs
! computation of the pantoja-mayne bfgs-update of hessian
      implicit   none
      include 'o8comm.h'
      include 'o8cons.h'
      integer i,j
      double precision dg(nx),adx(nx),den1,den2,den3,
     *   th,tk,xsik,ltdx(nx),gtdx(nresm),updx(nx),updz(nx),
     *   term,term1,anorm,acond,ndx,ngtdx,den21
      double precision o8sc1,o8sc2,o8sc3,o8vecn
      external o8sc1,o8sc2,o8sc3,o8vecn
      logical fail
      save
      do   i=1,n
!****** multiply dx= (s in the usual notation) by cholesky-factor
!       stored in the upper half of a
       ltdx(i)=o8sc2(i,n,i,a,nx,difx)
       dg(i)=gphi1(i)-gphi0(i)
      enddo
      if ( o8vecn(1,n,dg) .eq. zero ) then
!****** suppress update
        accinf(itstep,27)=zero
        accinf(itstep,28)=zero
        accinf(itstep,29)=zero
        if ( .not. silent ) call o8msg(21)
        return
      endif
      do i=1,n
        adx(i)=o8sc3(1,i,i,a,nx,ltdx)
      enddo
!*** adx = a * ( x-x0), x-x0=difx
      do i=1,alist(0)
        gtdx(i)=o8sc3(1,n,alist(i),gres,nx,difx)
        gtdx(i)=gtdx(i)/gresn(alist(i))
      enddo
!*** gtdx= grad(res)(transp)*(x-x0)
      ndx=o8vecn(1,n,difx)
      tk=min(p5,dnorm**2)
      anorm=zero
      term1=abs(a(1,1))
      anorm=zero
      do i=1,n
        do j=i,n
          anorm=anorm+a(i,j)**2
        enddo
        term1=min(term1,abs(a(i,i)))
      enddo
      if ( term1 .ne. zero ) then
        acond=anorm/term1**2
      else
        acond=epsmac/tolmac
      endif
      den1=o8vecn(1,n,ltdx)**2
      den2=o8sc1(1,n,dg,difx)
      if ( den1 .le. rho1*anorm*ndx**2 .or. acond .ge. one/rho1 ) then
!*** take a restart step
        call o8inim
        return
      endif
      if ( nres .eq. 0 ) then
! in the unconstrained case we take the powell update
        th=one
        if ( den2 .lt. p2*den1 ) then
          th=p8*den1/(den1-den2)
          do i=1,n
            dg(i)=th*dg(i)+(one-th)*adx(i)
          enddo
          den2=o8sc1(1,n,dg,difx)
        endif
        term=one/sqrt(den2)
        do i=1,n
          dg(i)=dg(i)*term
          updz(i)=dg(i)
        enddo
        term=one/sqrt(den1)
        do i=1,n
          updx(i)=adx(i)*term
        enddo
        accinf(itstep,28)=den2/den1
        accinf(itstep,29)=th
        accinf(itstep,27)=two
        if ( th .ne. one ) accinf(itstep,27)=three
      else
        ngtdx=o8vecn(1,alist(0),gtdx)
        term=one/sqrt(den1)
        do i=1,n
          updx(i)=adx(i)*term
        enddo
        if ( den2 .ge. rho1*o8sc1(1,n,dg,dg)
     f      .and. o8vecn(1,n,dg) .ge. sqrt(epsmac)*ndx ) then
          xsik=zero
          do i=1,n
            updz(i)=dg(i)
          enddo
          den21=den2
        else
!*** try pantoja-mayne modification
          den3=tk*ndx**2+ngtdx**2
          if ( den2 .ge. rho1*o8sc1(1,n,dg,dg) ) then
            xsik=one
          else
            xsik=one+(tk*ndx**2+abs(den2) )/den3
          endif
          do i=1,n
            term=zero
            do j=1,alist(0)
              term1=gres(i,alist(j))*gtdx(j)
              term1=term1/gresn(alist(j))
              term=term+term1
            enddo
            updz(i)=dg(i)+xsik*(tk*difx(i)+term)
          enddo
          den21=o8sc1(1,n,updz,difx)
        endif
        term=one/sqrt(den21)
        do i=1,n
          updz(i)=updz(i)*term
        enddo
        th=one
        if ( den2 .lt. p2*den1 ) then
          th=p8*den1/(den1-den2)
          do i=1,n
            dg(i)=th*dg(i)+(one-th)*adx(i)
          enddo
          den2=o8sc1(1,n,dg,difx)
        endif
        term=one/sqrt(den2)
        do i=1,n
          dg(i)=dg(i)*term
        enddo
        if ( o8vecn(1,n,dg) .le. tm3*o8vecn(1,n,updz) ) then
!***** the powell update produces a smaller growth
          do i=1,n
            updz(i)=dg(i)
          enddo
          accinf(itstep,28)=den2/den1
          accinf(itstep,29)=th
          accinf(itstep,27)=two
          if ( th .ne. one ) accinf(itstep,27)=three
        else
!*** no update if strongly irregular
          accinf(itstep,27)=one
          accinf(itstep,28)=tk
          accinf(itstep,29)=xsik
        endif
      endif
      call o8upd(a,updz,updx,n,fail)
!****** check illconditioning after updating
      term=abs(a(1,1))
      term1=term
      i=1
!***in order to overcome a curious error in hp's
!*** f77compiler this kind of loop
      do while ( i .lt. n )
        i=i+1
        term=max(term,abs(a(i,i)))
        term1=min(term1,abs(a(i,i)))
      enddo
      if ( fail  .or. term1**2 .le. rho1*term**2 ) then
!****** reset
        call o8inim
      endif

      end subroutine o8bfgs
!*************************************************************
      subroutine o8msg(num)
      implicit   none
! write messages on "special events"
      include 'o8comm.h'
      include 'o8qpdu.h'
      integer mdualm,i
      parameter (mdualm=2*nresm)
      integer iptr,iqtr,aitr(mdualm)
      double precision sstr,riitr
      common/o8qptr/sstr,riitr,iptr,iqtr,aitr
      integer num
      save
      if ( num .le. 0 .or. num .gt. 25 ) return
      goto (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,
     f    18,19,20,21,22,23,24,25) num
    1 continue
      write(meu,*) 'step=',itstep
      write(meu,*) 'rankdeficiency of grad''s of active constr.!'
      write(meu,*) 'on the basis of delmin!'
      return
    2 continue
      write(meu,*) 'step=',itstep
      write(meu,*) 'rescaling of objective function scf=',scf
      return
    3 continue
      write(meu,*) 'step=',itstep
      write(meu,*) 'rankdeficiency of grad''s of active constr.!'
      write(meu,*) ' del=',del
      return
    4 continue
      write(meu,*) 'step=',itstep
      write(meu,*) 'rankdeficiency of grad''s of active constr.!'
      write(meu,*) ' del=',del
      return
    5 continue
      write(meu,*) 'qpterm<0: no dir. of. desc., tauqp max'
      return
    6 continue
      write(meu,*) 'step=',itstep
      write(meu,*) 'second order correction suppressed! '
      return
    7 continue
      write(meu,*) 'step=',itstep
      write(meu,*) 'retry next step with a=id '
      return
    8 continue
      write(meu,*) 'step=',itstep
      write(meu,*) 'some constraint has gradient equal to zero '
      write(meu,*) 'resulting d may be no direction of descent'
      return
    9 continue
      write(meu,*) 'step=',itstep
      write(meu,*) 'try regularized sqp with increased weights'
      write(meu,*) 'since dnorm small or infeasibility large'
      return
   10 continue
      write(meu,*) 'step=',itstep
      write(meu,*) 'qpsolver did not complete, try d anyway, may fail'
      return
   11 continue
      write(meu,*) 'step=',itstep
      write(meu,*) 'direct. deriv. positive! may be due to inaccurate'
      write(meu,*) 'gradients or extreme illconditioning'
      write(meu,*) 'dirder= ',dirder
      return
   12 continue
      write(meu,*) 'step=',itstep
      write(meu,*) 'call of matdru suppressed, mat too large'
      return
   13 continue
      write(meu,*) 'step=',itstep
      write(meu,*) 'startvalue corrected in order to fit bounds'
      return
   14 continue
      write(meu,*) 'step=',itstep
      write(meu,*) 'try full sqp due to slow progress in x '
      return
   15 continue
      write(meu,*) 'step=',itstep
      write(meu,*) 'try full sqp due to small stepsizes while'
      write(meu,*) 'infeasibility large'
      return
   16 continue
      write(meu,*) 'step=',itstep
      write(meu,*) 'on exit from o8qpdu dir. deriv. positive!'
      write(meu,*) 'try increased tauqp'
      return
   17 continue
      write(meu,*) 'step=',itstep
      write(meu,*) 'try regularized sqp with increased weights'
      write(meu,*) 'no decrease of weights possible'
      write(meu,*) 'and incompatibility large'
      return
   18 continue
      write(meu,*) 'step=',itstep
      write(meu,*) 'try regularized sqp with increased weights'
      write(meu,*) 'since no direction of descent has been obtained'
      return
   19 continue
      write(meu,*) 'step=',itstep
      write(meu,*) 'degeneracy in dual qp'
      write(meu,*) 'restr. ',iptr,' to be added'
      write(meu,*) 'new rii=',riitr
      write(meu,*) 'length of current working set=',iqtr
      write(meu,*) 'working set'
      write(meu,fmt='(15(i4,1x))') (aitr(i),i=1,iqtr)
      write(meu,*) 'primal feasibility violation is=',sstr
      return
   20 continue
      write(meu,*) 'step=',itstep
      write(meu,*) 'dual qp seemingly infeasible '
      write(meu,*) 'theoretically impossible'
      return
   21 continue
      write(meu,*) 'step=',itstep
      write(meu,*) 'no update since dg=0'
      return
   22 continue
      write(meu,*) 'step',itstep
      write(meu,*) 'function evaluation returns err=.true.'
      return
   23 continue
      write(meu,*) 'step',itstep
      write(meu,*) 'contraint evaluation returns err=.true.'
      return
   24 continue
      write(meu,*) 'step',itstep
      write(meu,*) 'current point cannot be changed in current'
      write(meu,*) 'direction due to error-message from function'
      write(meu,*) 'evaluation'
      return
   25 continue
      write(meu,*) 'step',itstep
      write(meu,*) 'reduce stepsize due to error-indicator set'
      write(meu,*) 'by users function evaluation'

      end subroutine o8msg
!***************************************************************
!********************************************************************
!     solution of extended quadratic program
!     scf*gradf(x)*d+(1/2)*d*a*d+summe( tauqp*d(i) +( my/2)*d(i)**2)
!     minimal subject to
!     d(i) .ge. 0, i=1,...,nr
!     (gres(.,j)*d+res(j))+vz*d(j)=0, j=1,...,nh vz=-sign(res(j))
!     (gres(.,alist(j)))*d+res(alist(j)))+d(j) .ge. 0, j=nh+1,....,nr
!     the weight tauqp is adapted during solution
!     the quasi-newton-matrix a is taken from o8comm.h
!     a is regularized if not sufficiently well conditioned
!     the resulting d(1+nr),...,d(n+nr) is a direction of descent for
!     the zangwill function of the corresponding nonlinear
!     optimization problem
!     f(x)=min, res(j)=0, j=1,..nh, res(j) .ge. 0 , j=nh+1,nres
!     at the currrent point x if the weight tauqp is chosen
!     appropriately
!     the quadratic programming problem is solved using the method
!     of goldfarb and idnani
!     variables are stored in xd (solution) and ud (multipliers)
!     in the following order xd=( d(j), j=1,nr; d=direct. of desc.)
!     ud = ( multipliers for d(i) .ge. 0 , i=1,..,nr ;
!     multipliers for for equality constraints ,
!     multipliers for the general inequality constraints )
!**********************************************************************
      subroutine o8qpdu
      implicit   none
      include 'o8comm.h'
      include 'o8cons.h'
      include 'o8qpdu.h'
      integer ndualm,mdualm,ndual,mi,me,iq
      parameter (ndualm=nx+nresm,mdualm=nresm*2)
      double precision np,rnorm,rlow,xj,ddual,r,ud
      double precision g0(ndualm),ci0(mdualm),cii(mdualm),
     f       cei(ndualm),
     f       xd(ndualm),s(mdualm),z(ndualm),vr(mdualm)
      double precision xdold(ndualm),udold(mdualm),ud1
      integer ai(mdualm),iai(mdualm),iaexcl(mdualm),aiold(mdualm)
      double precision y(ndualm),mult(nresm)
      common /o8dupa/rnorm,rlow,ndual,mi,me,iq
      common /o8qpup/xj(ndualm,ndualm),ddual(ndualm),r(ndualm,ndualm),
     f                np(ndualm),ud(mdualm),ud1(mdualm)
      double precision infe1,s1,s2,tiny,
     f    my,zz,ss,su,t,t1,t2,f,psid,c1,c2,cdiag,o8sc1,term,
     f       su1,su2,condr,infiny,term1,o8vecn,
     f       diff0
      integer i,j,k,ip,l,qpdel(0:mdualm),incr
      logical wlow
      integer iptr,iqtr,aitr(mdualm)
      double precision sstr,riitr
      common/o8qptr/sstr,riitr,iptr,iqtr,aitr
      save
      ndual=n+nr
!*** number of equality constraints in qp-problem
      me=nh
!*** qp inequality constraints = active constraints -
!*** equality-constraints + slack's
      mi=2*nr-nh
      infiny=epsmac/tolmac
      if ( analyt ) then
        tiny=2*nr*epsmac*tp3
      else
        tiny=2*nr*max(epsdif,epsmac*tp3)
      endif
      qpterm=0
      do i=1,nr
!*** check gradients of active constraints against zero
        do j=1,n
          y(j)=gres(j,alist(i))
        enddo
        mult(i)=one
        if ( o8vecn(1,n,y) .eq. zero ) then
          mult(i)=zero
          if ( .not. silent ) call o8msg(8)
        endif
      enddo
!*** restart point in case of increase of tauqp
   10 continue
!*******  initialize matrices j and r
      do i=1,ndual
        ddual(i)=zero
        do j=1,ndual
          r(j,i)=zero
          xj(j,i)=zero
        enddo
      enddo
      rnorm=one
      rlow=one
      term1=zero
      do i=1,nres
        u(i)=zero
        if ( w(i) .gt. term1 ) term1=w(i)
      enddo
      accinf(itstep,19)=clow
      accinf(itstep,20)=term1
      accinf(itstep,31)=tauqp
      do i=1,me+mi
        ud(i)=zero
      enddo
      c1=abs(a(1,1))
      do i=1,n
        c1=max(c1,abs(a(i,i)))
      enddo
      c1=c1*tp1
!**** we require much more regularity of a in the singular case
      do i=1,n
        if ( abs(a(i,i)) .lt. sqrt(rho1)*c1 ) a(i,i)=sqrt(rho1)*c1
      enddo
!**** invert the cholesky-factor and store in xj ( idnanis j-matrix)
      call o8rinv(nx,n,a,ndualm,ndual,xj)
      c1=abs(a(1,1))
      incr=nr
      c2=abs(xj(1+incr,1+incr))
      do i=1,n
        c1=max(c1,abs(a(i,i)))
        c2=max(c2,abs(xj(i+incr,i+incr)))
      enddo
      my=zero
      do i=1,n
        do j=i,n
           my=my+a(i,j)**2
        enddo
      enddo
      my=my/n
      cdiag=one/sqrt(my)
      do  i=1,incr
        xj(i,i)=cdiag
      enddo
      do i=1,ndual
        if ( i .gt. incr )  g0(i)=gradf(i-incr)*scf
        if ( i .le. incr )  g0(i)=tauqp
      enddo
!*** compute unconstrained solution
!*** the cholesky-factor of a is stored in the upper triangle
      do i=1,n
        su=zero
        do j=1,i-1
          su=su+a(j,i)*y(j+incr)
        enddo
        y(i+incr)=(g0(i+incr)-su)/a(i,i)
      enddo
      do i=n,1,-1
        su=zero
        do j=i+1,n
          su=su+a(i,j)*y(j+incr)
        enddo
        y(i+incr)=(y(i+incr)-su)/a(i,i)
      enddo
      do i=1,incr
!****** initially assume the slacks being zero
        y(i)=zero
      enddo
      do i=1,ndual
        xd(i)=-y(i)
      enddo
!****** unconstrained minimizer of the qp: slacks come first
      f=p5*o8sc1(1,ndual,g0,xd)
!****** define the initial working set: all slacks are at their
!****** lower bounds
      iq=nr
      do i=1,iq
        ai(i)=i
        r(i,i)=one
        ud(i)=tauqp
      enddo
      rnorm=one
      rlow=one
!****** introduction of equality constraints
      do i=1,me
        do  j =1,iq
          ud1(j)=ud(j)
        enddo
   20   continue
        ud1(iq+1)=zero
!***** an equality constraint is indicated by the negative index
        ai(iq+1)=-nr-i
        do  j=1,n
!*** me=nh and alist(i)=i for i=1,..,nh
          cei(j+incr)=gres(j,i)
        enddo
        do j=1,incr
          cei(j)=zero
        enddo
        cei(i)=one
        if ( res(i) .gt. zero ) cei(i)=-one
        do  j=1,ndual
          np(j)=cei(j)
        enddo
        call o8zup(z)
        if ( iq .ne. 0 ) call o8rup(vr)
!*******|z|=0?
!old        zz=o8sc1(1,ndual,z,z)
        zz=o8vecn(1,ndual,z)
        term=o8sc1(1,ndual,z,np)
!old        if  (zz .ne. zero .and. term .gt. zero )         then
        if  (zz .ge. tiny*rnorm .and. term .gt. zero )         then
          t2=(-o8sc1(1,ndual,np,xd)-res(i))/term
        elseif ( (-o8sc1(1,ndual,np,xd)-res(i)) .ge. zero ) then
          t2=infiny
        else
          t2=-infiny
        endif
!***** in addition of an equality constraint, t2 may be
!***** positive or negative
        if ( iq .ne. 0 ) call o8rup(vr)
        l=0
        if ( t2 .gt. zero ) then
          t1=infiny
          do k=1,iq
            if(vr(k) .gt. zero .and. ai(k) .gt. 0 )     then
              if(ud1(k)/vr(k) .lt. t1)      then
                t1=ud1(k)/vr(k)
              endif
            endif
          enddo
          t=min(t1,t2)
        else
          t1=infiny
          do k=1,iq
            if(vr(k) .lt. zero .and. ai(k) .gt. 0 )     then
              if(ud1(k)/abs(vr(k)) .lt. t1)      then
                t1=ud1(k)/abs(vr(k))
              endif
            endif
          enddo
          t1=-t1
          t=max(t1,t2)
!**** t now negative
        endif
!*** add constraint , otherwise we must first delete some
!*** inequality constraint with negative multiplier
!*** first delete then add!
        if ( abs(t) .ge. infiny ) goto 2000
        if(abs(t2) .ge. infiny )       then
!***** purely dual step
          do      k=1,iq
            ud1(k)=ud1(k)+t*(-vr(k))
            if ( ud1(k) .lt. zero .and. ai(k) .gt. 0 ) ud1(k)=zero
          enddo
          ud1(iq+1)=ud1(iq+1)+t
          qpdel(0)=0
          do j=1,iq
            if ( ud1(j) .le. tiny .and. ai(j) .gt. 0 ) then
              qpdel(0)=qpdel(0)+1
              qpdel(qpdel(0))=ai(j)
            endif
          enddo
          do k=1,qpdel(0)
            l=qpdel(k)
            iai(l)=l
            call o8dlcd(ai,l)
          enddo
          goto 20
        endif
        do k=1,ndual
          xd(k)=xd(k)+t*z(k)
        enddo
        do k=1,iq
          ud1(k)=ud1(k)+t*(-vr(k))
          if ( ud1(k) .lt. zero .and. ai(k) .gt. 0 ) ud1(k)=zero
        enddo
        ud1(iq+1)=ud1(iq+1)+t
        f=f+t*o8sc1(1,ndual,z,np)*(p5*t+ud1(iq+1))
        if ( abs(t2-t1) .le. tiny ) then
          qpdel(0)=0
          do j=1,iq
            if ( ud1(j) .le. tiny .and. ai(j) .gt. 0 ) then
              qpdel(0)=qpdel(0)+1
              qpdel(qpdel(0))=ai(j)
            endif
          enddo
          do k=1,qpdel(0)
            l=qpdel(k)
            iai(l)=l
            call o8dlcd(ai,l)
          enddo
          ai(iq+1)=-i-nr
          call o8adcd
        elseif ( t .eq. t2 ) then
          ai(iq+1)=-i-nr
          call o8adcd
        else
          qpdel(0)=0
          do j=1,iq
            if ( ud1(j) .le. tiny .and. ai(j) .gt. 0 ) then
              qpdel(0)=qpdel(0)+1
              qpdel(qpdel(0))=ai(j)
            endif
          enddo
          do k=1,qpdel(0)
            l=qpdel(k)
            iai(l)=l
            call o8dlcd(ai,l)
          enddo
          goto 20
        endif
        do j=1,iq
          ud(j)=ud1(j)
        enddo
      enddo
!****** set iai=k\ai
      do i=1,mi
        iai(i)=i
      enddo
!****** step 1
   50 continue
!***  ai = qp - working set , iai(i)=0 if i in ai
      do i=1,iq
        ip=ai(i)
        if ( ip .gt. 0 ) iai(ip)=0
      enddo
!****** s(xd)=ci(trans)*xd+ci0 >= 0 ?
      psid=zero
!*** psid : the measure of infeasibility
      do i=1,mi
!*** iaexcl: if = 0, exclude from addition in this cycle
        iaexcl(i)=1
        su=zero
!*** numbers of inequality constraints:
!*** i=1,...,nr corresponds to the constraints v>=0, u_a>=0
!*** i=nr+1,....,mi to the regularized general inequalities
        if ( i .gt. nr ) then
          k=alist(i+nh-incr)
          do j=1,n
            cii(j+incr)=gres(j,k)
          enddo
          do j=1,incr
            cii(j)=zero
          enddo
          cii(nh+i-incr)=one
          ci0(i)=res(k)
        else
          do j=1,ndual
            cii(j)=zero
          enddo
          ci0(i)=zero
          cii(i)=one
        endif
        su=o8sc1(1,ndual,cii,xd)+ci0(i)
        s(i)=su
        psid=psid+min(zero,su)
      enddo
      do i=1,iq
        udold(i)=ud(i)
        aiold(i)=ai(i)
      enddo
      do  i=1,ndual
        xdold(i)=xd(i)
      enddo
   60 continue
      ss=zero
      ip=0
!*** introduce most violated inequality constraint
      do i=1,mi
        if(s(i) .lt. ss .and. iai(i) .ne. 0 .and. iaexcl(i) .ne.0)then
          ss=s(i)
          ip=i
        endif
      enddo
      if ( iq .gt. 1 ) then
        condr=rnorm/rlow
      else
        condr=one
      endif
!***
!***
      if (abs(psid) .le. tiny*(c1*c2+condr) .or. ip .eq. 0) then
!****   successful termination of qp-solver for current tauqp
        if ( abs(psid) .gt. tiny*(c1*c2+condr) .and. .not. silent )
     f    call o8msg(10)
        qpterm=1
        accinf(itstep,30)=one
        accinf(itstep,13)=condr
        accinf(itstep,14)=c1*c2
        do i=1,n
          d(i)=xd(i+incr)
        enddo
!new : dnorm added
        dnorm=o8vecn(1,n,d)
        infeas=zero
        do i=1,incr
          infeas=infeas+abs(xd(i))
        enddo
!******* l1-norm of slack variables
        accinf(itstep,31)=tauqp
        accinf(itstep,32)=infeas
        wlow=.false.
        su1=zero
        su2=zero
        do i=1,iq
          if ( ai(i) .lt. 0 ) then
            u(-(ai(i)+nr))=ud(i)
          else
           if ( ai(i) .gt. nr ) u(alist(ai(i)+nh-nr))=ud(i)
          endif
        enddo
        term1=zero
        do j=1,n
          np(j)=gradf(j)*scf
        enddo
        do i=1,nres
          do j=1,n
            np(j)=np(j)-gres(j,i)*u(i)
          enddo
        enddo
        b2n=o8vecn(1,n,np)
        if ( scf .ne. zero ) b2n=b2n/scf
!*** correction in the original variables
        infe1=zero
        do i=1,nr
          infe1=infe1+abs(xd(i))*mult(i)
        enddo
!old        if ( upsi .le. delmin*nres .and. upsi0 .le. delmin*nres
        if ( upsi .le. delmin*nres
     f   .and.  b2n .le. (gfn+one)*epsx*tp2 .and. phase .ge. 0
     f   .and. infeas .le. delmin*nres
     f      )  then
!****** since multipliers may be incorrect for infeas .ne. zero
!****** be careful. if multipliers are important check in solchk
!****** and possibly compute them separately given x and the active
!****** set e.g. by using nnls from netlib
!****** we consider the problem as successfully solved with reduced
!****** requirements
          do i=1,n
            d(i)=zero
          enddo
          dnorm=zero
          qpterm=nint(one)
          dirder=zero
          optite=nint(three)
          return
        endif
!*** there may be an additional increase of tauqp necessary again
!***
        if ( infe1 .gt. (one-delta1/tauqp)*upsi .and.
     f       ( o8vecn(1,n,d) .le. min(infe1,infe1**2)*tp1
     f        .or. upsi .gt. tau0*p5 ) ) then
!*** further increase tauqp !
          do i=1,nres
            u(i)=zero
            slack(i)=zero
          enddo
          if ( .not. silent ) call o8msg(17)
          if ( tauqp*taufac .gt. taumax ) then
            if ( .not. silent ) call o8msg(5)
            qpterm=-1
            accinf(itstep,30)=qpterm
            accinf(itstep,31)=tauqp
            accinf(itstep,32)=infeas
            do i=1,n
              d(i)=zero
            enddo
            dnorm=zero
            return
          else
            tauqp=tauqp*taufac
            goto 10
          endif
        endif

!*** compute new weights for the penalty-function

        do i=1,nres
          slack(i)=zero
        enddo
        do i=1,nr
          slack(alist(i))=xd(i)
        enddo
        wlow=.false.
        do i=1,nres
          w1(i)=w(i)
          if ( i .le. nh ) then
            if ( abs(slack(i)) .gt. abs(res(i))+tiny ) then
              w1(i)=abs(u(i))
            else
              w1(i)=ny*abs(u(i))+tau
            endif
          else
            if ( bind(i)  .eq. 0 ) then
              w1(i)=max(w(i)*p8,tau)
            else
              if ( res(i) .ge. zero .and. slack(i) .le. tiny)
     *            w1(i)=max(ny*abs(u(i))+tau,(abs(u(i))+w1(i))*p5)
              if ( res(i) .ge. zero .and. slack(i) .gt. tiny )
     *            w1(i)=abs(u(i))
              if (res(i) .lt. zero .and. slack(i) .le. -res(i)+tiny)
     *            w1(i)=max(ny*abs(u(i))+tau,(abs(u(i))+w1(i))*p5)
              if (res(i) .lt. zero .and. slack(i) .gt. -res(i)+tiny)
     *            w1(i)=abs(u(i))

            endif
          endif
          if ( w1(i) .lt. w(i) ) wlow=.true.
        enddo
        if ( wlow  ) then
          s1=zero
          s2=zero
          do i=1,nres
            if ( i .le. nh ) then
              s1=s1+w1(i)*abs(resst(i))
              s2=s2+w1(i)*abs(res(i))
            else
              s1=s1-min(zero,resst(i))*w1(i)
              s2=s2-min(zero,res(i))*w1(i)
            endif
          enddo
          diff0=(fxst-fx)*scf+(s1-s2)
          if (  diff0 .ge. eta*clow
     f    .and. itstep-lastdw .ge. max(5,min(n/10,20))  ) then
!***** accept new (diminished ) weights
            lastdw=itstep
            lastch=itstep
            level=diff0/iterma
            psist=s1
            psi=s2
            do i=1,nres
              if (w1(i) .ne. w(i)) lastch=itstep
              w(i)=w1(i)
            enddo
            clow=clow+nint(one)
            if ( clow .gt. itstep/10 ) then
!*** additional increase of eta
              eta=eta*onep3
              if ( .not. silent )  call o8info(11)
            endif
            if ( .not. silent ) call o8info(12)
            goto 1000
          endif
        endif
!***  we cannot accept new weights
!***  reset weights
        do i=1,nres
          w1(i)=w(i)
          if ( i .le. nh ) then
            if ( slack(i) .gt. abs(res(i)) )
     *        w1(i)=abs(u(i))
            if ( slack(i) .le. abs(res(i)) ) then
              if ( w(i) .le. abs(u(i)) .and. abs(u(i)) .le.
     *             w(i)+tau ) then
                   w1(i)=w(i)+two*tau
              else
                   w1(i)=max(w(i),ny*abs(u(i))+tau)
              endif
            endif
          else
            if ( slack(i) .gt. -min(-tiny,res(i))
     *        .and. bind(i) .eq. 1 ) then
              w1(i)=abs(u(i))
            elseif ( bind(i) .eq. 1 .and. slack(i) .le.
     *           -min(-tiny,res(i)) .and. u(i) .le. w(i)+tau
     *           .and. w(i) .ge. u(i) )
     *        then
              w1(i)=w(i)+two*tau
            elseif ( bind(i) .eq. 1 ) then
              w1(i)=max(w(i),ny*abs(u(i))+tau)
            endif
          endif
        enddo
        term1=zero
        do i=1,nres
          if ( w1(i) .gt. w(i) .or. w1(i) .lt. w(i) )
     *       lastch=itstep
          if ( w1(i) .gt. w(i) ) lastup=itstep
          if ( w1(i) .lt. w(i) ) lastdw=itstep
          w(i)=w1(i)
          term1=max(term1,w(i))
        enddo
        s1=zero
        s2=zero
        do i=1,nres
          if ( i.le. nh ) then
            s1=s1+w(i)*abs(resst(i))
            s2=s2+w(i)*abs(res(i))
          else
            s1=s1-w(i)*min(zero,resst(i))
            s2=s2-w(i)*min(zero,res(i))
          endif
        enddo
        psist=s1
        psi=s2
        if ( .not. silent ) call o8info(12)
        accinf(itstep,20)=term1
        accinf(itstep,19)=clow
        goto 1000
!***
      endif
      if ( ip .gt. nr ) then
        k=alist(ip+nh-nr)
        do j=1,n
          cii(j+incr)=gres(j,k)
        enddo
        do j=1,incr
          cii(j)=zero
        enddo
        cii(nh+ip-nr)=one
        ci0(ip)=res(k)
      else
        do j=1,ndual
          cii(j)=zero
        enddo
        ci0(ip)=zero
        cii(ip)=one
      endif
      do i=1,ndual
        np(i)=cii(i)
      enddo
      do  i =1,iq
        ud1(i)=ud(i)
      enddo
      ud1(iq+1)=zero
      ai(iq+1)=ip
  100 continue
!********   step 2a
      call o8zup(z)
      if(iq .ne. 0)     call o8rup(vr)
      l=0
      t1=infiny
      do k=1,iq
        if(ai(k) .gt. 0 .and. vr(k) .gt. zero)     then
          if(ud1(k)/vr(k) .lt. t1)      then
            t1=ud1(k)/vr(k)
          endif
        endif
      enddo
!*******|z|=0?
!old      zz=o8sc1(1,ndual,z,z)
      zz=o8vecn(1,ndual,z)
      term=o8sc1(1,ndual,z,np)
!old      if  (zz .ne. zero .and. term .gt. zero )         then
      if ( zz .ge. tiny*rnorm .and. term .gt. zero ) then
        t2=-s(ip)/term
      else
        t2=infiny
      endif
      t=min(t1,t2)
      if(t .ge. infiny ) goto 2000
!**********
      if(t2 .ge. infiny )       then
        do      k=1,iq
          ud1(k)=ud1(k)+t*(-vr(k))
          if ( ud1(k) .lt. zero .and. ai(k) .gt. 0 ) ud1(k)=zero
        enddo
        ud1(iq+1)=ud1(iq+1)+t
        qpdel(0)=0
        do i=1,iq
          if ( ud1(i) .le. tiny .and. ai(i) .gt. 0 ) then
            qpdel(0)=qpdel(0)+1
            qpdel(qpdel(0))=ai(i)
          endif
        enddo
        do k=1,qpdel(0)
          l=qpdel(k)
          iai(l)=l
          call o8dlcd(ai,l)
        enddo
        goto 100
      endif
      do k=1,ndual
        xd(k)=xd(k)+t*z(k)
      enddo
      do k=1,iq
        ud1(k)=ud1(k)+t*(-vr(k))
        if ( ud1(k) .lt. zero .and. ai(k) .gt. 0 ) ud1(k)=zero
      enddo
      ud1(iq+1)=ud1(iq+1)+t
      f=f+t*o8sc1(1,ndual,z,np)*(p5*t+ud1(iq+1))
      if (t2 .le.t1-tiny  )      then
!**** ddual is computed by o8zup
        if ( o8vecn(iq+1,ndual,ddual) .lt. epsmac*rnorm )
     f    then
!**** degeneracy: adding this constraint gives a singular working set
!**** theoretically impossible, but due to roundoff this may occur
!**** mark this constraint and try to add another one
          iptr=ip
          iqtr=iq
          do i=1,iq
            aitr(i)=ai(i)
          enddo
          sstr=ss
          riitr=o8vecn(iq+1,ndual,ddual)
          if ( .not. silent ) call o8msg(19)
          iaexcl(ip)=0
          do  i=1,mi
            iai(i)=i
          enddo
          do  i=1,iq
            ai(i)=aiold(i)
            if (ai(i) .gt. 0 ) iai(ai(i))=0
            ud1(i)=udold(i)
          enddo
          do  i=1,ndual
            xd(i)=xdold(i)
          enddo
          goto 60
        endif
!*** add constraint, l-pair
        call o8adcd
        iai(ip)=0
        do i=1,iq
          ud(i)=ud1(i)
        enddo
        goto 50
      endif
      su=zero
      if ( ip .gt. nr ) then
!*** a general linear inequality constraint
        k=alist(ip+nh-nr)
        do j=1,n
          cii(j+incr)=gres(j,k)
        enddo
        do j=1,incr
          cii(j)=zero
        enddo
        cii(nh+ip-nr)=one
        ci0(ip)=res(k)
        s(ip)=o8sc1(1,ndual,cii,xd)+ci0(ip)
      else
!*** a slack constraint
        s(ip)=xd(ip)
      endif
!*** now t=t1
      qpdel(0)=0
      do i=1,iq
        if ( ud1(i) .le. tiny .and. ai(i) .gt. 0 ) then
          qpdel(0)=qpdel(0)+1
          qpdel(qpdel(0))=ai(i)
        endif
      enddo
      do k=1,qpdel(0)
        l=qpdel(k)
        iai(l)=l
        call o8dlcd(ai,l)
      enddo
      if ( t2 .le. t1+tiny ) then
        if ( o8vecn(iq+1,ndual,ddual) .lt. epsmac*rnorm )
     f    then
!**** degeneracy
          iptr=ip
          iqtr=iq
          do i=1,iq
            aitr(i)=ai(i)
          enddo
          sstr=ss
          riitr=o8vecn(iq+1,ndual,ddual)
           if ( .not. silent ) call o8msg(19)
          iaexcl(ip)=0
          do  i=1,mi
            iai(i)=i
          enddo
          do  i=1,iq
            ai(i)=aiold(i)
            if ( ai(i) .gt. 0 ) iai(ai(i))=0
            ud1(i)=udold(i)
          enddo
          do  i=1,ndual
            xd(i)=xdold(i)
          enddo
          goto 60
        endif
!*** add constraint, l-pair
        call o8adcd
        iai(ip)=0
        do i=1,iq
          ud(i)=ud1(i)
        enddo
        goto 50
      else
        goto 100
      endif
!**** this is the exit point of o8qpdu
!**** we either may have successful or unsuccessful termination here
!**** the latter with qpterm=-2 or -3, in which case it may nevertheless
!**** be possible to use the computed d. -2 or -3 exit is theoretically
!**** impossible but may occur due to roundoff effects.
!**** we check the directional derivative of the penalty-function now
 1000 continue
!*** cut and rescale d if appropriate
      call o8cutd
!*** compute the directional derivative dirder
      call o8dird
      if ( dirder .ge. zero  .or.
     f    ( -dirder .le. epsmac*tp2*(scf*abs(fx)+psi+one) .and.
     f      infeas .gt. max(upsi,nres*delmin) ) ) then
        if ( .not. silent ) call o8msg(18)
        if ( tauqp .le. taumax/taufac ) then
          tauqp=tauqp*taufac
          goto 10
        else
          if ( .not. silent ) call o8msg(5)
          qpterm=-1
          accinf(itstep,30)=qpterm
          accinf(itstep,31)=tauqp
          accinf(itstep,32)=infeas
          do i=1,n
            d(i)=zero
          enddo
          dnorm=zero
          dirder=zero
        endif
      endif
      return
 2000 continue
!*** qp infeasible ( in this application impossible , theoretically)
        if ( .not. silent ) call o8msg(20)
        qpterm=-2
        accinf(itstep,30)=-two
        accinf(itstep,13)=condr
        accinf(itstep,14)=c1*c2
        do i=1,n
          d(i)=xd(i+incr)
        enddo
        dnorm=o8vecn(1,n,d)
        su1=zero
        do i=1,incr
          su1=su1+abs(xd(i))
        enddo
!******* l1-norm of slack variables
        accinf(itstep,32)=su1
        infeas=su1
        wlow=.false.
        su1=zero
        su2=zero
        do i=1,iq
          if ( ai(i) .lt. 0 ) then
            u(-(ai(i)+nr))=ud(i)
          else
            if ( ai(i) .gt. nr ) u(alist(ai(i)-nr+nh))=ud(i)
          endif
        enddo
        term1=zero
        do j=1,n
          np(j)=gradf(j)*scf
        enddo
        do i=1,nres
          do j=1,n
            np(j)=np(j)-gres(j,i)*u(i)
          enddo
          w1(i)=w(i)
          if ( i .le. nh ) then
            if ( slack(i) .gt. abs(res(i)) )
     *        w1(i)=abs(u(i))
            if ( slack(i) .le. abs(res(i)) ) then
              if ( w(i) .le. abs(u(i)) .and. abs(u(i)) .le.
     *             w(i)+tau ) then
                w1(i)=w(i)+two*tau
              else
                w1(i)=max(w(i),ny*abs(u(i))+tau)
              endif
            endif
            su1=su1+abs(res(i))*w1(i)
            su2=su2+abs(resst(i))*w1(i)
          else
            if ( slack(i) .gt. -min(-tiny,res(i))
     *        .and. bind(i) .eq. 1 ) then
              w1(i)=abs(u(i))
            elseif ( bind(i) .eq. 1 .and. slack(i) .le.
     *           -min(-tiny,res(i)) .and. u(i) .le. w(i)+tau
     *           .and. w(i) .ge. u(i) )
     *        then
              w1(i)=w(i)+two*tau
            elseif ( bind(i) .eq. 1 ) then
              w1(i)=max(w(i),ny*abs(u(i))+tau)
            endif
            su1=su1-w1(i)*min(zero,res(i))
            su2=su2-w1(i)*min(zero,resst(i))
          endif
          if ( w(i) .ne. w1(i) ) lastch=itstep
          w(i)=w1(i)
          term1=max(term1,w(i))
        enddo
        psist=su2
        psi=su1
        b2n=sqrt(o8sc1(1,n,np,np))
        if ( scf .ne. zero ) then
            b2n=b2n/scf
        else
            b2n = -one
        endif
        if ( wlow ) then
           clow=clow+nint(one)
           lastch=itstep
           lastdw=itstep
        endif
        if ( .not. silent ) call o8info(12)
        accinf(itstep,19)=clow
        accinf(itstep,20)=term1
        accinf(itstep,31)=tauqp
        goto 1000
      end subroutine o8qpdu
!************************************************************
      subroutine o8fin

      implicit   none

      include 'o8comm.h'
      include 'o8cons.h'
      include 'o8fint.h'
      integer i,j,k,ih1,ih2,ih3,ih5,ih6,ih7,ih8,ih9
      double precision umin,term
      integer nsing,crtot,cgrtot,nupreg,nbfgs1,nresta
      character*64 messag(18),line
      save
!**** termination reason + 11 = message number
      data messag/
     f'constraint evaluation returns error with current point',
     f'objective evaluation returns error with current point',
     f'qpsolver: working set singular in dual extended qp ',
     f'qpsolver: extended qp-problem seemingly infeasible ',
     f'qpsolver: no descent direction from qp for tau=tau_max',
     f'qpsolver: on exit correction small, infeasible point',
     f'stepsizeselection: computed d not a direction of descent',
     f'more than maxit iteration steps',
     f'stepsizeselection: no acceptable stepsize in [sigsm,sigla]',
     f'stepsizeselection: directional deriv. very small, infeasible',
     f'kt-conditions satisfied, no further correction computed',
     f'kt-conditions satisfied, computed correction small',
     f'stepsizeselection: x (almost) feasible, dir. deriv. very small',
     f'kt-conditions (relaxed) satisfied, singular point',
     f'very slow primal progress, singular or illconditoned problem',
     f'very slow progress in x, singular problem',
     f'correction very small, almost feasible but singular point',
     f'numsm  small differences in penalty function,terminate'/
      if ( scf .ne. zero ) then
        do i=1,nres
          u(i)=u(i)/scf
        enddo
      endif
!***  in solchk the user might add some additional checks and/or output
!      call solchk
!***
      if ( silent .and. .not. intakt ) return

      if ( intakt .and. .not. silent ) write(*,*) name
      if (  .not. silent ) then
        if ( intakt  )
     f     write(*,400) n,nh,ng,epsx,sigsm,(xst(i),i=1,n)
        write(prou,400) n,nh,ng,epsx,sigsm,(xst(i),i=1,n)
  400   format(/'     n=',i10, '    nh=',i10,'    ng=',i10/
     1       /'  epsx=',1p,d10.3,' sigsm=',1p,d10.3/
     2       /'startvalue'/(5(1p,d15.7,1x)))
      endif
      if ( intakt .and. .not. silent )
     1write(*,500)epsmac,tolmac,del0,delmin,tau0,tau,smalld,smallw,
     2               rho,rho1
      if ( .not. silent )
     1 write(prou,500)epsmac,tolmac,del0,delmin,tau0,tau,smalld,
     2           smallw,rho,rho1
  500 format(/'  eps=',1p,d10.3,'  tol=',1p,d10.3,' del0=',1p,d10.3,
     1        ' delm=',1p,d10.3,' tau0=',1p,d10.3/'  tau=',1p,d10.3,
     2        '   sd=',1p,d10.3,'   sw=',1p,d10.3,'  rho=',1p,d10.3,
     3        ' rho1=',1p,d10.3)
      if ( .not. silent ) then
        write(prou,
     f        fmt='('' scfm='',d10.3,''  c1d='',d10.3,
     f        '' epdi='',d10.3,/''  nre='',i10,'' anal='',l10
     f        )') scfmax,c1d,epsdif,nreset,analyt
        if ( intakt )
     f   write(*,
     f        fmt='('' scfm='',d10.3,''  c1d='',d10.3,
     f        '' epdi='',d10.3,/''  nre='',i10,'' anal='',l10
     f        )') scfmax,c1d,epsdif,nreset,analyt
      endif
      if ( .not. silent .and. .not. analyt ) then
        write(prou,
     f        fmt='('' vbnd='',d10.3,'' efcn='',d10.3,
     f        '' diff='',i1)') taubnd,epsfcn,difftype
        if ( intakt )
     f         write(*,
     f        fmt='(''taubnd='',d10.3,'' epsfcn='',d10.3,
     f        '' difftype='',i1)') taubnd,epsfcn,difftype
      endif
      i=0
      j=0
      umin=zero
      do k=1,nres
        i=i+cres(k)
        j=j+cgres(k)
        if ( k .gt. nh ) umin=min(umin,u(k))
      enddo
      crtot=i
      cgrtot=j
      nsing=0
      nresta=0
      nupreg=0
      nbfgs1=0
      do k=1,itstep
        if ( accinf(k,10) .eq. one ) nsing=nsing+1
        if ( accinf(k,27) .eq. one ) nbfgs1=nbfgs1+1
!*** for the pantoja mayne update
        if ( accinf(k,29) .eq. zero .and. accinf(k,27) .eq. one )
     f              nupreg=nupreg+1
        if ( accinf(k,27) .eq. -one ) nresta=nresta+1
      enddo
      k=int(optite)+11
      if ( k .ge. 1 .and. k .le. 18 ) then
        line=messag(k)
      else
        line='variable optite undefined on exit'
      endif
      if ( intakt .and. .not. silent  )
     f  write(*,510) line,icf,icgf,crtot,cgrtot,scf,gfn,b2n,upsi,umin,
     f               runtim
      if ( .not. silent )
     f   write(prou,510) line,icf,
     f                   icgf,crtot,cgrtot,scf,gfn,b2n,upsi,umin,
     f                   runtim
  510 format(/1x,'termination reason:',/1x,a62,
     f       /1x,'evaluations of f                   ',i10
     f       /1x,'evaluations of grad f              ',i10/1x,
     f           'evaluations of constraints         ',i10/1x,
     f           'evaluations of grads of constraints',i10/1x,
     f           'final scaling of objective         ',d14.7/1x,
     f           'norm of grad(f)                    ',d14.7/1x,
     f           'lagrangian violation               ',d14.7,/1x,
     f           'feasibility violation              ',d14.7,/1x,
     f           'dual feasibility violation         ',d14.7,/1x,
     f           'optimizer runtime sec''s            ',e14.7)
      if ( intakt .and. .not. silent ) write(*,550) fx
      if ( .not. silent ) write(prou,550)     fx
  550 format(//1x,'optimal value of f =  ',1p,d21.14)
      if ( intakt .and. .not. silent ) write(*,560) (x(i),i=1,n)
      if ( .not. silent ) write(prou,560) (x(i),i=1,n)
  560 format(/1x,'optimal solution  x =  '/(1x,3(1p,d21.14,1x)))
      if ( nres .ne. 0 .and. .not. silent ) then
        write(prou,520) (i,res(i),gresn(i),u(i),i=1,nres)
        if ( intakt )
     f    write(*,520) (i,res(i),gresn(i),u(i),i=1,nres)
      endif
  520 format(/2x,'multipliers are relativ to scf=1'
     1    /2x,'nr.    constraint      normgrad (or 1)   multiplier'/
     2       (1x,i4,1x,3(d15.8,2x)))
      if ( nres .ne. 0 .and. .not. silent  ) then
        write(prou,600)  (cres(i),cgres(i),i=1,nres)
        if ( intakt )
     f    write(*,600)  (cres(i),cgres(i),i=1,nres)
  600 format(/1x,'evaluations of restrictions and their gradients'/
     1      (5(:,' (',i6,',',i6,')')))
      endif
      if ( itstep .gt. 1 .and. optite .eq. 0 ) itstep=itstep-1
      if ( .not. silent )
     f    write(prou,
     f    fmt='(''last estimate of condition of active gradients'',
     f   e11.4)')       accinf(itstep,13)
      term=accinf(itstep,14)
      i=itstep
      do while ( i .gt. 1 .and. term .eq. zero )
        i=i-1
        term=accinf(i,14)
      enddo
      if ( .not. silent ) then
        write(prou,
     f    fmt='(''last estimate of condition of approx. hessian '',
     f            e11.4)') term
        write(prou,fmt='(''iterative steps total          '',i5)')
     f     itstep
        write(prou,fmt='(''# of restarts                  '',i5)')
     f     nresta
        write(prou,fmt='(''# of full regular updates      '',i5)')
     f     nupreg
        write(prou,fmt='(''# of updates                   '',i5)')
     f     nbfgs1
        write(prou,fmt='(''# of full regularized sqp-steps'',i5)')
     f     nsing

        if ( intakt ) then
      write(*,fmt='(''last estimate of cond.nr. of active gradients '',
     f   e11.4)')       accinf(itstep,13)
      write(*,fmt='(''last estimate of cond.nr. of approx.  hessian '',
     f            e11.4)') accinf(itstep,1 4)
      write(*,fmt='(''iterative steps total           '',i5)') itstep
      write(*,fmt='(''# of restarts                   '',i5)') nresta
      write(*,fmt='(''# of full regular updates       '',i5)') nupreg
      write(*,fmt='(''# of updates                    '',i5)') nbfgs1
      write(*,fmt='(''# of regularized full sqp-steps '',i5)') nsing

        endif
      endif
      if ( optite .lt. zero ) te1=.true.
      if ( silent ) te1=.false.
      if ( te1 ) then
        do i=1 ,itstep
          ih1 = int(accinf(i,1))
          ih2 = int(accinf(i,9))
          ih3 = int(accinf(i,10))
          ih5 = int(accinf(i,18))
          ih6 = int(accinf(i,19))
          ih7 = int(accinf(i,22))
          ih8 = int(accinf(i,26))
          ih9 = int(accinf(i,27))
          write(prou,1000) ih1,(accinf(i,j),j=2,8),ih2,ih3,
     f    accinf(i,11),(accinf(i,j),j=13,14),(accinf(i,j),j=16,17),
     f           ih5,ih6,accinf(i,20),
     f           accinf(i,21),ih7,(accinf(i,j),j=23,25),ih8,ih9,
     f           accinf(i,28),accinf(i,29)
          if ( accinf(i,10) .eq. 1. ) then
            write(prou,2000) (accinf(i,j),j=30,32)
          endif
        enddo
      endif
!****
!   accinf a c c u m u l a t e d   i n f o r m a t i o n
!   on iteration sequence
!   1: step-nr
!   2: f(x-k) current value of objective (zero in feasibility
!      improvement  phase (-1)  )
!   3: scf    internal scaling of objective (zero in phase -1)
!   4: psi    the weighted penalty-term
!   5: upsi   the unweighted penalty-term (l1-norm of constraint vector)
!   6: del_k_1 bound for currently active constraints
!   7: b2n0   l2-norm of projected gradient, based on constraints
!             in level delmin
!             and below, measured in the norm induced by the
!             inverse hessian
!             estimate
!   8: b2n    l2-norm of projected gradient based on del_k_1
!   9: nr     number of binding constraints
!  10: sing   if 1, the binding constraints don't satisfy the
!             regularity condition
!  11: umin   infinity norm of negative part of multiplier
!  12: -------------
!  13: cond_r condition number of diagonal part of qr-decomp.
!      of normalized   gradients of binding constraints
!  14: cond_h condition number of diagonal of cholesky-factor
!      of updated full hessian
!  15: scf0   the relative damping of tangential component
!             if upsi>tau0/2
!  16: xnorm  l2-norm of x
!  17: dnorm  l2-norm of d (correction from qp -subproblem, unscaled)
!  18: phase  -1 : infeasibility improvement phase,
!             0: initial optimization
!             1: binding constraints unchanged ,
!             2: d small, maratos correction
!             in use
!  19: c_k    number of decreases of penalty weights
!  20: wmax   infinity norm of weights
!  21: sig_k  stepsize from unidimensional minimization (backtracking)
!  22: cfincr number of objective evaluations for stepsize-algorithm
!  23: dirder directional derivative of penalty-function along d
!             (scaled)
!  24: dscal  scaling factor for d
!  25: cosphi cos of arc between d and d_previous. if larger theta ,
!             sig larger than  one (up to sigla) is tried
!  26: violis(0) number of constraints not binding at x but hit
!             during line search
!  27:        type of update for h:
!             1 normal p&m-bfgs-update,
!             0 update suppressed,
!             -1 restart with scaled unit matrix ,
!             2 standard bfgs,
!             3 bfgs modified (powell)

!  28: ny_k/tk   modification factor for damping the projector
!                in bfgs/pantoja-mayne-term respectively
!  29: 1-my_k/xsik modification factor for damping
!                the quasi-newton-relation in bfgs
!                for unmodified bfgs ny_k should be larger than
!                updmy0 (near one)
!                and 1-my_k equal one./pantoja-mayne term respectively
!  30: qpterm 0, if sing=-1, termination indicator of
!                qp-solver otherwise
!             1: successful,
!             -1: tau becomes larger than tauqp without slack-
!                 variables becoming sufficiently small .
!             -3: working set of qp-solver becomes linearly dependent
!             -2: infeasible qp-problem (theoretically impossible)
!  31: tauqp: weight of slack-variables in qp-solver
!  32: infeas l1-norm of slack-variables in qp-solver

 1000 format(i4,'  fx=',e14.7,' scf=',e14.7,' psi=',
     f        e14.7,   ' ups=',e14.7,/4x,
     f        ' del=',e14.7,' b20=',e14.7,' b2n=',e14.7,'  nr=',
     f        i5,9x,/4x    ,'  si=',i4,10x,
     f        '  u-=',e14.7,
     f        ' c-r=',e14.7,' c-d=',e14.7,/4x,
     f        '  xn=',e14.7,'  dn=',e14.7,' pha=',i4,10x,
     f        '  cl=',i14,/4x,' skm=',e14.7,
     f        ' sig=',e14.7,' cf+=',i5,9x,' dir=',e14.7,/4x,
     f        ' dsc=',e14.7,' cos=',e14.7,' vio=',i5,  /4x,
     f        ' upd=',i5,9x,'  tk=',e14.7,' xsi=',e14.7)
 2000 format(4x,' qpt=',e14.1,' tqp=',e14.7,' sla=',e14.7)
      if ( .not. silent ) close(prou)
      if ( .not. silent ) close(meu)

      end subroutine o8fin
