c  compute the gradient of the i-th equality constraint
      subroutine esgradh(i,x,gradhi)

      implicit   none

      include 'o8fuco.h'
      include 'o8fint.h'
      include 'o8cons.h'
      double precision x(*),gradhi(*)
      integer i,j
      double precision d1,d2,d3,sd1,sd2,sd3,fhelp,fhelp1,fhelp2,
     *   fhelp3,fhelp4,fhelp5,fhelp6,xincr,xhelp,hixloc
      save
      if ( gunit(1,i) .eq. 1 ) then
        do j=1,n
          gradhi(j)=zero
        enddo
        gradhi(gunit(2,i))=gunit(3,i)*xsc(gunit(2,i))
        return
      endif
      if ( bloc ) then
        if ( valid ) then
          cgres(i)=cgres(i)+1
          do j=1,n
            gradhi(j)=xsc(j)*fugrad(j,i)
          enddo
          return
        else
          stop 'donlp2: bloc call with function info invalid'
        endif
      else
        do j=1,n
          xtr(j)=xsc(j)*x(j)
        enddo
        if ( analyt ) then
          call egradh(i,xtr,gradhi)
        else
        if ( difftype .eq. 1 ) then
          deldif=min(tm1*sqrt(epsfcn),tm2)
          call eh(i,xtr,hixloc)
          do j=1,n
            xhelp=xtr(j)
            xincr=min(tm2,deldif*abs(xhelp)+deldif,taubnd)
            if ( xhelp .ge. zero ) then
              xtr(j)=xhelp+xincr
            else
              xtr(j)=xhelp-xincr
            endif
            call eh(i,xtr,fhelp)
            gradhi(j)=(fhelp-hixloc)/(xtr(j)-xhelp)
            xtr(j)=xhelp
          enddo
        elseif ( difftype .eq. 2 ) then
          deldif=min(tm1*epsfcn**(one/three),tm2)
          do j=1,n
            xhelp=xtr(j)
            xincr=min(tm2,deldif*abs(xhelp)+deldif,taubnd)
            xtr(j)=xhelp+xincr
            call eh(i,xtr,fhelp1)
            xtr(j)=xhelp-xincr
            call eh(i,xtr,fhelp2)
            gradhi(j)=(fhelp1-fhelp2)/(xincr+xincr)
            xtr(j)=xhelp
          enddo
        else
          deldif=min(tm1*epsfcn**(one/seven),tm2)
          do j=1,n
            xhelp=xtr(j)
            xincr=min(tm2,deldif*abs(xhelp)+deldif,taubnd/four)
            xtr(j)=xhelp-xincr
            call eh(i,xtr,fhelp1)
            xtr(j)=xhelp+xincr
            call eh(i,xtr,fhelp2)
            xincr=xincr+xincr
            d1=xincr
            xtr(j)=xhelp-xincr
            call eh(i,xtr,fhelp3)
            xtr(j)=xhelp+xincr
            call eh(i,xtr,fhelp4)
            xincr=xincr+xincr
            d2=xincr
            xtr(j)=xhelp-xincr
            call eh(i,xtr,fhelp5)
            xtr(j)=xhelp+xincr
            call eh(i,xtr,fhelp6)
            xtr(j)=xhelp
            d3=xincr+xincr
            sd1=(fhelp2-fhelp1)/d1
            sd2=(fhelp4-fhelp3)/d2
            sd3=(fhelp6-fhelp5)/d3
            sd3=sd2-sd3
            sd2=sd1-sd2
            sd3=sd2-sd3
            gradhi(j)=sd1+p4*sd2+sd3/c45
          enddo
        endif

        endif
        do j=1,n
          gradhi(j)=xsc(j)*gradhi(j)
        enddo
        return
      endif

      end subroutine esgradh
