c*******************************************************
      subroutine ngenorm(num,stddev,amvec,uvec,stp,iopt,ind)
c*******************************************************
c
c
c     generate uncorrelated normal r.v. vector
c     with mean amvec
c          variance sgvec
c
c     if ind = 1 standard normal
c            = 0 specified by input
c
c********************************************************
      implicit   none

      integer*4 num,ind,iopt
      real*8 stddev,amvec(*),uvec(*),stp
c
      integer*4 i,ier
      real*8 rn
c
      call gguw(stp,num,iopt,uvec)
      iopt = 0
      do i=1,num
        rn=uvec(i)
        call dnormi(uvec(i),rn,ier)
        if(ind.eq.0) then
          uvec(i)=uvec(i)*stddev+amvec(i)
        endif
      enddo

      end subroutine ngenorm
