      subroutine nclength(mcon,nvars,mequa,mwlast,milast)

      implicit   none

      integer*4 npmax,nall,mcon,nvars,mxb,mb,mbb,mblb,mbub
      integer*4 mzp,mxp,mequa,mqc,mwj,mpj,mgr,mdxl,mwa,mwlast,mindb
      integer*4 mind,milast
c
      npmax = 05
      nall = mcon + 2*nvars + npmax + 1
      mxb= 1 + nall + 2+nall+2
      mb = mxb + nvars
      mbb = mb + nvars
      mblb = mbb + nvars
      mblb=mblb+nall
      mbub = mblb + nall
      mbub=mbub+nall
      mzp = mbub + nall
      mxp = mzp + mequa*npmax
      mqc = mxp + nvars*npmax
      mwj = mqc + max(mequa,nvars)*npmax
      mpj = mwj + (nall)* (nall+1)
      mgr = mpj + nvars + 1
      mdxl = mgr + nvars
      mwa = mdxl + nvars + nvars
      mwlast = mwa + 9* (mcon+1) + 13* (2*nvars+npmax+1)
c
      mindb = 3
      mind = mindb + nall + nvars
      milast = mind + 3* (mcon+1) + 4* (2*nvars+npmax+1)

      end subroutine nclength
