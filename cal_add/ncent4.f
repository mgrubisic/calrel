c************************************************************
      subroutine ncent(not,icl,ntl,mc,nrx,nry,nig,ngf,ntf,ngfc,
     *                 ids,ipa,izx,ixz,ib,lgp,igt,
     *                 xdes,btdes,zzz,bnd,tf,tp,par,sg,ex,ydes,alpha,
     *                 weight,wlim,rdev,dist,
     *                 nc,ycent,alcent,btcent,sgcent,p0,ndesign,
     *                 alph,fltf,igfx1,isampl)
c************************************************************
c
c     make design point y in u space
c          normal vector a in u space
c
c************************************************************
      implicit   none

      include   'nlpflag.h'

      integer*4,allocatable::idel(:),ncset(:)
      real*8,allocatable::yyy(:),alp(:),gam(:),dgx(:),gdgx(:)
      integer*4,allocatable::ialpha(:,:),iaflag(:),icomps(:,:,:)
      real*8,allocatable::ydesw(:,:),btdesw(:),alphaw(:,:)
      integer*4 nrx,nry,nig,ngf,ntf,ngfc,nc,not
      real*8 xdes(nrx,ngf),ydes(nry,ngf),zzz(nry),bnd(2,nrx),tf(ntf)
      real*8 par(4,nrx),sg(nrx),ex(nrx),alpha(nry,ngf),btdes(ngf)
      real*8 weight(ngf),wlim(ngf),rdev,dist,sgcent(ngf),ycent(nry,ngf)
      real*8 alcent(nry,ngf),btcent(ngf),p0,tp(*),alph(nry,ngf)
      integer*4 ids(nrx),ipa(4,nrx),izx(nrx),lgp(3,nig),igt(nig)
      integer*4 ixz(*),ib(*),igfx1(*)
      integer*4 ntl,mc(ntl,2),icl
      logical fltf(ngf)
c
      integer*4 i,j,k,ndesign
      real*8 ptotal,p,rrdev,ddd,dcr,bdum
      real*8 betai,betaj,btemp,aitemp,yitemp,tolalpha
      integer*4 ncomp,maxset,iset,icset,icomp,ifunc
      integer*4 nset,isampl
      allocate(idel(ngf))
      allocate(ialpha(nry,ngfc))
      allocate(iaflag(nry))
      allocate(icomps(2,ngfc,ngfc))
      allocate(ncset(ngfc))
      allocate(ydesw(nry,ngfc))
      allocate(btdesw(ngfc))
      allocate(alphaw(nry,ngfc))
      allocate(yyy(nrx))
      allocate(alp(nrx))
      allocate(gam(nrx))
      allocate(dgx(nrx))
      allocate(gdgx(nrx))
c
c     ----- reproduce alpha and beta from x-design -----
c
      tolalpha=1.0d0/dfloat(nry)*1.0d-3
c
c      write(not,'(1h ,'' '')')
c      write(not,'(1h ,'' ***** subroutine ncent *****'')')
c      write(not,'(1h ,'' '')')
c      write(not,'(1h ,'' make important points of sampling '')')
c      write(not,'(1h ,'' '')')
c      write(not,'(1h ,'' results of form analysis '')')
c      write(not,'(1h ,'' '')')
c      write(not,'(1h ,'' number of components,,,,,,,,,,,,'',i5)')ngfc
c      write(not,'(1h ,''           variables,,,,,,,,,,,,,'',i5)')nry
      do i=1,ngfc
        if(.not.fltf(i)) then
        write(not,'(1h ,'' fatal error in making center points '')')
        write(not,'(1h ,'' form has not been done for lsf'',i5)')
     *              igfx1(i)
        stop
        endif
        call cxtoy(xdes(1,i),ydes(1,i),zzz,tf,ex,sg,par,bnd,
     *             igt,lgp,ids,ipa,izx,bdum)
c        anorm=cdot(ydes(1,i),ydes(1,i),nry,nry,nry)
        do j=1,nry
c          alpha(j,i)=ydes(j,i)/btdes(i)
          alpha(j,i)=alph(j,i)
          if(dabs(alpha(j,i)).lt.tolalpha) then
            ialpha(j,i)=1
          else
            ialpha(j,i)=0
          endif
        enddo
      enddo
c
c      write(not,'(1h ,'' '')')
c      write(not,'(1h ,'' desing points'')')
c      write(not,'(1h ,'' '')')
c      write(not,'(1h ,10x,20(10x4hcomp,i1))')(i,i=1,20)
c      do i=1,nry
c      write(not,'(1h,i10,20e15.5)')
c     *   i,(ydes(i,j),j=1,ngfc)
c      enddo
c      write(not,'(1h ,'' '')')
c      write(not,'(1h ,'' alpha vector & beta'')')
c      write(not,'(1h ,'' '')')
c      write(not,'(1h ,10x,20(10x4hcomp,i1))')(i,i=1,20)
c      do i=1,nry
c      write(not,'(1h,i10,20e15.5)')
c     *   i,(alpha(i,j),j=1,ngfc)
c      enddo
c      write(not,'(1h ,6x4hbeta,20e15.5)') (btdes(i),i=1,ngfc)
c
      if(ngfc.eq.1) then
c       ----- component problem -----
        nc=1
        btcent(1)=btdes(1)
        if(rdev.gt.0.0d0) then
          sgcent(1)=rdev
        else
          rrdev=-rdev
          sgcent(1)=btdes(1)/rrdev+dsqrt(((btdes(i)/rrdev)**2)+1.0d0)
        endif
        wlim(1)=1.0d0
        weight(1)=1.0d0-p0
        do i=1,nry
          ycent(i,1)=ydes(i,1)
          alcent(i,1)=alpha(i,1)
        enddo
        write(not,'(1h ,'' '')')
        write(not,'(1h ,'' component problem nc=1'')')
        write(not,'(1h ,'' '')')
        write(not,'(1h ,10x,6x9hdesign pt,10x5halpha)')
        write(not,'((1h ,i10,2e15.5))')
     *         (i,ycent(i,1),alcent(i,1),i=1,nry)
c        write(not,'(1h ,''beta,,,,,,,,,,,,'',e15.5)') btcent(1)
c        write(not,'(1h ,''sgcent,,,,,,,,,,'',e15.5)') sgcent(1)
        write(not,'(1h ,'' '')')
      else
        if(ngfc.ne.1.and.ndesign.eq.0) then
           ndesign=ngfc
        endif
        write(not,'(1h ,'' '')')
        write(not,'(1h ,'' system problem '')')
        write(not,'(1h ,'' '')')
        write(not,'(1h ,''num of use design points,,,,,,'',i10)')ndesign
c
        ncomp=ngfc
c
        do i=1,ncomp
        btdesw(i)=btdes(i)
        do j=1,nry
          ydesw(j,i)=ydes(j,i)
          alphaw(j,i)=alpha(j,i)
        enddo
        enddo
c
        if(icl.eq.3.and.isampl.ne.0) then
c       ----- parallel system problem -----
c       ------count #of set and #of comp in each set -----
          write(not,'(1h ,'' '')')
          write(not,'(1h ,'' general system problem '')')
          write(not,'(1h ,'' '')')
c
c         ------ prepartaion for optimization problem -----
c
          maxset=0
          iset=1
          icset=0
          do i=1,ntl
            icomp=mc(i,2)
            ifunc=mc(i,1)
            if(icomp.ne.0) then
              icset=icset+1
              icomps(1,icset,iset)=icomp
              icomps(2,icset,iset)=ifunc
            else
              ncset(iset)=icset
              iset=iset+1
              maxset=max(maxset,icset)
              icset=0
            endif
          enddo
          nset=iset-1
          write(not,'(1h ,'' '')')
          write(not,'(1h ,'' number of cutset,,,,,,,,'',i5)')nset
          write(not,'(1h ,'' '')')
          do i=1,nset
          write(not,'(1h ,'' set,,,,,,,,,,,,,,,,,,,,,'',i5)')i
          write(not,'(1h ,'' number of components,,,,'',i5)')ncset(i)
          write(not,'(1h ,'' components '')')
c          write(not,'(1h ,5i10)') (icomps(1,j,i),j=1,ncset(i))
          write(not,'(1h ,5i10)') (icomps(2,j,i),j=1,ncset(i))
          enddo
c		
c         ----- linearized limit state surface -----
c
          if(isampl.ge.0) then
          call optwol(not,nry,nset,ncset,ngfc,ialpha,icomps,
     *                alpha,btdes,ncomp,ydesw,btdesw,alphaw)
          else
          iswnlp=0
          call opnsqp(not,nry,nset,ncset,ngfc,icomps,ncomp,
     *                ids,ib,ipa,izx,ixz,lgp,igt,
     *                zzz,yyy,bnd,tf,par,sg,ex,tp,alp,gam,dgx,gdgx,
     *                ydesw,btdesw,alphaw)
          endif
        endif
c
c       ----- eliminate close points -----
c
c        write(not,'(1h ,''  '')')
c        write(not,'(1h ,''  eliminate close points '')')
c        write(not,'(1h ,''  '')')
c        write(not,'(1h ,'' '')')
c        write(not,'(1h ,'' desing points'')')
c        write(not,'(1h ,'' '')')
c        write(not,'(1h ,10x,20(10x4hcomp,i1))')(j,j=1,20)
c        do j=1,nry
c        write(not,'(1h,i10,20e15.5)')
c     *     j,(ydesw(j,i),i=1,ncomp)
c        enddo
c        write(not,'(1h ,'' '')')
c        write(not,'(1h ,'' alpha vector & beta'')')
c        write(not,'(1h ,'' '')')
c        write(not,'(1h ,10x,20(10x4hcomp,i1))')(i,i=1,20)
c        do i=1,nry
c        write(not,'(1h,i10,20e15.5)')
c     *     i,(alphaw(i,j),j=1,ncomp)
c        enddo
c        write(not,'(1h ,6x4hbeta,20e15.5)') (btdesw(i),i=1,ncomp)
c
        do i=1,ncomp
          idel(i)=0
        enddo
        do i=1,ncomp-1
          if(idel(i).eq.1) goto 100
          do j=i+1,ncomp
            if(idel(j).eq.1) goto 110
            ddd=0.0d0
            do k=1,nry
              ddd=ddd+(ydesw(k,i)-ydesw(k,j))**2
            enddo
            ddd=dsqrt(ddd)
            dcr=(sgcent(i)+sgcent(j))*dist/2.0d0
            dcr=0.0d0
            if(ddd.lt.dcr) then
              if(btdesw(i).ge.btdesw(j)) then
                idel(i)=1
                goto 100
              else
                idel(j)=1
              endif
            endif
  110       continue
          enddo
  100     continue
        enddo
c       ----- delete points -----
        nc=0
        do i=1,ncomp
          if(idel(i).eq.0) then
            nc=nc+1
            btcent(nc)=btdesw(i)
            do j=1,nry
              ycent(j,nc)=ydesw(j,i)
              alcent(j,nc)=alphaw(j,i)
            enddo
          endif
        enddo

c       ----- ordering -----

        if((icl.eq.2).or.(icl.eq.3.and.isampl.ne.0)) then
        do i=1,nc-1
          betai=dabs(btcent(i))
          do j=i+1,nc
            betaj=dabs(btcent(j))
            if(betai.gt.betaj) then
               btemp=betaj
               betaj=betai
               btcent(j)=betai
               betai=btemp
               btcent(i)=btemp
               do k=1,nry
                 aitemp=alcent(k,i)
                 alcent(k,i)=alcent(k,j)
                 alcent(k,j)=aitemp
                 yitemp=ycent(k,i)
                 ycent(k,i)=ycent(k,j)
                 ycent(k,j)=yitemp
               enddo
             endif
           enddo
        enddo
        else
c       ----- ordering -----
        do i=1,nc-1
          betai=dabs(btcent(i))
          do j=i+1,nc
            betaj=dabs(btcent(j))
            if(betai.lt.betaj) then
               btemp=betaj
               betaj=betai
               btcent(j)=betai
               betai=btemp
               btcent(i)=btemp
               do k=1,nry
                 aitemp=alcent(k,i)
                 alcent(k,i)=alcent(k,j)
                 alcent(k,j)=aitemp
                 yitemp=ycent(k,i)
                 ycent(k,i)=ycent(k,j)
                 ycent(k,j)=yitemp
               enddo
             endif
           enddo
        enddo
        endif
        nc=min(ndesign,ncomp)
        do i=1,nc
        sgcent(i)=1.0d0
        enddo
        write(not,'(1h ,''  '')')
        write(not,'(1h ,''  after deletion and ordering '')')
        write(not,'(1h ,''  '')')
        write(not,'(1h ,'' '')')
        write(not,'(1h ,'' desing points'')')
        write(not,'(1h ,'' '')')
        write(not,'(1h ,10x,20(10x4hcomp,i1))')(j,j=1,20)
        do j=1,nry
        write(not,'(1h,i10,20e15.5)')
     *     j,(ycent(j,i),i=1,nc)
        enddo
        write(not,'(1h ,'' '')')
        write(not,'(1h ,'' alpha vector & beta'')')
        write(not,'(1h ,'' '')')
        write(not,'(1h ,10x,20(10x4hcomp,i1))')(i,i=1,20)
        do i=1,nry
        write(not,'(1h,i10,20e15.5)')
     *     i,(alcent(i,j),j=1,nc)
        enddo
        write(not,'(1h ,6x4hbeta,20e15.5)') (btcent(i),i=1,nc)
c       ----- weight -----
        ptotal=0.0d0
        do i=1,nc
          call dnorm(-dabs(btcent(i)),p)
          ptotal=ptotal+p
          weight(i)=p
        enddo
        do i=1,nc
          weight(i)=weight(i)/ptotal*(1-p0)
          if(i.eq.1) then
            wlim(i)=p0+weight(i)
          else
            wlim(i)=wlim(i-1)+weight(i)
          endif
        enddo
        write(not,'(1h ,6x4hwww ,20e15.5)') (weight(i),i=1,nc)
        write(not,'(1h ,6x4hwlim,20e15.5)') (wlim(i),i=1,nc)
      endif
c
      deallocate(idel)
      deallocate(ialpha)
      deallocate(iaflag)
      deallocate(icomps)
      deallocate(ncset)
      deallocate(ydesw)
      deallocate(btdesw)
      deallocate(alphaw)
      deallocate(yyy)
      deallocate(alp)
      deallocate(gam)
      deallocate(dgx)
      deallocate(gdgx)

      end subroutine ncent
