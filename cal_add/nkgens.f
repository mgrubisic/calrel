c******************************************************************
      subroutine nkgens(nrx,nry,ntf,ngf,nig,ntl,nrmx,ngfc,igfx,igt,
     *                  lgp,ids,ipa,ib,izx,mcst,mcend,mc,
     *                  x1,y1,z,tf,ex,sg,par,
     *                  bnd,tp,alpha,y0,ir,rr,troot,xpp,btlim)
c******************************************************************
      implicit   none

      integer*4 nrx,nry,ntf,ngf,nig,nrmx,ntl,ngfc,igfx(ngf,3),igt(nig)
      integer*4 lgp(3,nig),ids(nrx),ipa(4,nrx),ib(nrx),izx(nry)
      real*8 x1(nrx),y0(nry),tf(ntf),ex(nrx),sg(nrx),par(4,nrx)
      real*8 bnd(2,nrx),tp(*),alpha(nry),y1(nry)
      real*8 z(nry),g0(ngf)
      integer*4 ir(ngf),mcst,mcend,mc(ntl,2)
      real*8 rr(nrmx,ngf),troot(nrmx*ngf),xpp,btlim
c
      integer*4 nr,i,j,idg
      real*8 wrj,lroot,rroot,diff,r,p1,p2
c
c     ----- count num of roots and order them -----
c
c      nr=1
c      troot(1)=-btlim
      nr=0
      do i=1,ngfc
        if(ir(i).ne.0) then
          do j=1,ir(i)
            nr=nr+1
            troot(nr)=rr(j,i)
          enddo
        endif
      enddo
c
      do i=1,nr-1
        do j=i+1,nr
          if(troot(j).lt.troot(i)) then
            wrj=troot(j)
            troot(j)=troot(i)
            troot(i)=wrj
          endif
        enddo
      enddo
c
c     ----- compute probability -----
c
      if(nr.le.1) then
        do i=1,ngfc
          if(g0(i).gt.0.0d0) then
            xpp=0.0d0
            return
          endif
        enddo
        xpp=-1.0d0
      else
        xpp=0.0d0
        do 120 i=1,nr-1
          lroot=troot(i)
          rroot=troot(i+1)
          diff=rroot-lroot
          if(dabs(diff).le.1.0d-4) goto 120
          r=(lroot+rroot)/2.0d0
          call nkgenr(nrx,nry,ntf,ngf,nig,ntl,ngfc,igfx,igt,
     *                lgp,ids,ipa,ib,izx,ir,mcst,mcend,mc,
     *                x1,y0,tf,ex,sg,par,bnd,tp,alpha,y1,z,
     *                r,idg)
          if(idg.eq.1) then
            call dnorm(rroot,p1)
            call dnorm(lroot,p2)
            xpp=xpp+p1-p2
          endif
  120   continue
c
c       ----- check for both ends
c
        r=troot(nr)+1.0d0
        call nkgenr(nrx,nry,ntf,ngf,nig,ntl,ngfc,igfx,igt,
     *              lgp,ids,ipa,ib,izx,ir,mcst,mcend,mc,
     *              x1,y0,tf,ex,sg,par,bnd,tp,alpha,y1,z,
     *              r,idg)
        if(idg.eq.1) then
          call dnorm(-troot(nr),p1)
          xpp=xpp+p1
        endif
        r=troot(1)-1.0d0
        call nkgenr(nrx,nry,ntf,ngf,nig,ntl,ngfc,igfx,igt,
     *              lgp,ids,ipa,ib,izx,ir,mcst,mcend,mc,
     *              x1,y0,tf,ex,sg,par,bnd,tp,alpha,y1,z,
     *              r,idg)
        if(idg.eq.1) then
          call dnorm(troot(1),p1)
          xpp=xpp+p1
        endif
      endif

      end subroutine nkgens
