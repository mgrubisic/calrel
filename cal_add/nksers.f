c***********************************************************
      subroutine nksers(nrmx,ngf,ngfc,ir,isflg,rr,xpp)
c***********************************************************
      implicit   none

      integer*4 nrmx,ngf,ngfc,ir(ngf),isflg(nrmx,ngf)
      real*8 rr(nrmx,ngf),xpp
c
      integer*4 nr,nzero,iimin,iimax,i,j
      real*8 rpmin,rnmax,p1,p2
c
      rpmin= 1.0e+10
      iimin= 0
      rnmax=-1.0e+10
      iimax= 0
      nzero=0
      do i=1,ngfc
        nr=ir(i)
        do j=1,nr
          if(isflg(j,i).eq.1) then
            nzero=1
            if(rr(j,i).lt.rpmin) then
              rpmin=rr(j,i)
              iimin=1
            endif
          elseif(isflg(j,i).eq.-1) then
            nzero=1
            if(rr(j,i).gt.rnmax) then
              rnmax=rr(j,i)
              iimax=1
            endif
          endif
        enddo
      enddo
      if(nzero.eq.0) then
        xpp=0.0d0
        return
      endif
c
      if(iimax.eq.1.and.iimin.eq.1) then
        if(rnmax.gt.rpmin) then
          call dnorm(rnmax,p1)
          call dnorm(rpmin,p2)
          xpp=p2-p1
        else
          xpp=0.0d0
        endif
      elseif(iimax.eq.1) then
        call dnorm(rnmax,xpp)
      else
        call dnorm(-rpmin,xpp)
      endif

      end subroutine nksers
