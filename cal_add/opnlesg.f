c compute the i-th inequality constaint, bounds included
      subroutine esg(i,x,gxi,isfunc,idfunc,yyy,zzz,tf,tp,ex,sg,par,
     *               bnd,igt,lgp,ids,ipa,ib,izx)

      implicit   none

      include 'o8fuco.h'
      include 'o8fint.h'
      double precision x(*),gxi
      real*8 yyy(*),zzz(*),tf(*),ex(*),sg(*),par(4,*),bnd(2,*),tp(*)
      integer*4 idfunc(*),igt(*),lgp(3,*),ids(*),ipa(4,*),ib(*),izx(*)
      integer*4 isfunc(*)

      integer i
      integer j
      save
      if ( bloc ) then
        if ( valid ) then
          if ( gunit(1,i+nh) .ne. 1 ) cres(i+nh)=cres(i+nh)+1
          gxi=fu(i+nh)
        else
          stop 'donlp2: bloc call with function info invalid'
        endif
      else
        do j=1,n
          xtr(j)=x(j)*xsc(j)
        enddo
        call eg(i,xtr,gxi,isfunc,idfunc,yyy,zzz,tf,tp,ex,sg,par,bnd,
     *         igt,lgp,ids,ipa,ib,izx)
c        call eg(i,xtr,gxi)
      endif

      end subroutine esg
