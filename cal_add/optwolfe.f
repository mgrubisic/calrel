      subroutine wolfe(ndm,m,pmat,istrt,s,ncor,icor,iwork,liwrk,work,
     &                 lwrk,r,coef,ptnr,pmat1,nparm,numgr,wcoef,wpt,
     &                 wdist,nmaj,nmin,jflag)

      implicit   none

      real    (kind=8) :: s,wdist
      integer (kind=4) :: ndm,m,istrt 
      integer (kind=4) :: ncor,nparm,liwrk,lwrk,numgr,nmaj,nmin,jflag
      integer (kind=4) :: icor(nparm+1),iwork(liwrk)
      real    (kind=8) :: pmat(nparm+1,numgr),wcoef(numgr)
      real    (kind=8) :: wpt(nparm),r(nparm+1),coef(numgr)
      real    (kind=8) :: pmat1(nparm+1,numgr),work(lwrk),ptnr(nparm+1)

      real    (kind=8) :: spcmn 

      integer (kind=4) :: iloc 
      real    (kind=8) :: d1mach
c this program was developed by ed kaufman, david leeming, and jerry
c taylor.  the method used is an enhanced version of the method described
c in (wolfe, philip, finding the nearest point in a polytope, mathematical
c programming 11 (1976), 128-149).

c***the next group of comments is for the case where the user wishes to
c run wolfe by itself rather than as a part of conmax.

c to run the program, first set the three machine and precision dependent
c constants in function subprograms i1mach and d1mach, write a driver
c program which dimensions the arrays in the calling sequence for wolfe
c and sets the input variables as specified in the list below, then call
c subroutine wolfe.  the only subprograms needed are i1mach, d1mach,
c wolfe, iloc, conenr, house, dotprd, and refwl.  no subroutine libraries
c (such as imsl) are needed.

c the variables, in the order of their appearance in the argument list of
c subroutine wolfe, are as follows.

c ndm  (input)  this is the number of variables.  it must be less than or
c    equal to nparm.

c m  (input)  this is the number of inequalities defining the polytope.  it
c    must be less than or equal to numgr.

c pmat  (input)  this is an array whose kth column contains the vector
c    (a(k),b(k)) for k = 1,...,m, where the m inequalities a(k).x + b(k)
c    .le. 0.0 define the polytope whose nearest point to the origin we
c    seek.  the first dimension of pmat in the driver program must be
c    exactly nparm+1, while the second dimension of pmat in the driver
c    program must be at least numgr.
c    if we actually want the nearest point in the polytope to some point
c    y other than the origin, we translate y to the origin before calling
c    wolfe, that is, call wolfe to find the nearest point z to the origin
c    in the polytope defined by a(k).z + (b(k) + a(k).y) .le. 0.0, then
c    compute x  =  y + z.

c istrt  (input)  set this equal to zero unless a hot start is desired--
c    see next paragraph of comments for more details.  if istrt is set
c    equal to 1, then s, wcoef, ncor, and icor must also be assigned
c    values initially.

c s  (output)  you may ignore this scale factor unless you want to use
c    the hot start option.

c ncor  (output)  this is the number of vectors (i.e. colunns of pmat) in
c    the final corral.

c icor  (output)  this array contains the ncor indices of the vectors in
c    the final corral.  its dimension in the driver program must be at
c    least nparm+1.

c iwork  (work array)  its dimension in the driver program must be liwrk.

c liwrk  (input)  this is the dimension of iwork.  it must be at least
c    7*nparm + 7*numgr + 3.

c work  (work array)  its dimension in the driver program must be lwrk.

c lwrk  (input)  this is the dimension of work.  it must be at least
c    2*nparm**2 + 4*numgr*nparm + 11*numgr + 27*nparm + 13.
c    note that some storage could be saved by rewriting function
c    subprogram iloc to take out all but the arrays needed (namely 1, 3,
c    4, 9, 28, 32, 34, 39 for work, 18, 23 for iwork) and scrunching
c    work and iwork in iloc so the remaining arrays follow one after
c    another.

c r  (work array)  its dimension in the driver program must be at least
c    nparm+1.

c coef  (work array)  its dimension in the driver program must be at least
c    numgr.

c ptnr  (work array)  its dimension in the driver program must be at least
c    nparm+1.

c pmat1  (work array)  its dimension in the driver program should be the
c    same as the dimension of pmat.

c nparm  (input)  this is basically a dimension parameter here.  it must
c    be greater than or equal to ndm.

c numgr  (input)  this is basically a dimension parameter here.  it must
c    be greater than or equal to m.

c wcoef  (output)  this will give the coefficients of the vectors a(k)
c    needed to form a linear combination equal to the solution in wpt.
c    its dimension in the driver program must be at least numgr.
c    wcoef may not be accurate if it was necessary to call refwl to
c    refine wpt, which rarely happens.

c wpt  (output)  this will give the coordinates of the point we are seeking,
c    namely the nearest point in the polytope to the origin.  its dimension
c    in the driver program must be at least nparm.

c wdist  (output)  this will be the (minimized) euclidean distance of wpt
c    from the origin.

c nmaj  (output)  this will be the number of major cycles used in wolfe.

c nmin  (output)  this will be the number of minor cycles used in wolfe.

c jflag  (output)  this is a flag variable which is 0 in case of a normal
c    solution and is positive otherwise (in which case the returned
c    solution may be no good).

c***end of comments for running wolfe by itself rather than as a part of
c conmax.

c given m inequalities of the form a(k).x + b(k) .le. 0.0 for k = 1,
c ...,m, where a(k) and x are ndm dimensional vectors and b(k)
c are numbers, this subroutine returns the nearest point to the
c origin in the polytope defined by these inequalities (unless
c jflag .gt. 0, which indicates failure).  the user should put
c the mdm+1 dimensional vectors (a(k),b(k)) in the columns of pmat.
c the solution point will be returned in wpt, and will also be a
c linear combination of the a(k) vectors with (nonpositive)
c coefficients in the m dimensional vector wcoef.  wcoef may not be
c accurate if refwl was used to refine wpt, which rarely happens. the
c number of vectors in the final corral will be returned in ncor with
c their indices in icor, and all entries of wcoef not corresponding to
c indices in icor will be zero.  the distance will be returned in
c wdist, and the numbers of major and minor cycles in the cone
c subproblem will be returned in nmaj and nmin respectively.
c if the user sets istrt = 0 the program will start from scratch, but
c the user can set istrt = 1 (hot start) and specify ncor, icor, wcoef,
c and the factor s.  (see later comments; set s = 1.0 if no better value is
c available.  set wcoef(j) = 0.0 if icor(i) .ne. j for i=1,...,ncor.)  (if
c inaccurate wcoef or s is used in a hot start attempt little will be
c lost, since ncor and icor are more important for a successful hot start
c than wcoef and s.)  we must always have ncor .le. ndm+1 in theory since
c the ncor ndm+1 dimensional vectors in a corral should be linearly
c independent, and in practice we will always require ncor .le. ndm+1.
c if the user sets istrt = 1 but the program fails, it will
c automatically try from scratch before giving up.

c set machine and precision dependent constants for wolfe.
c     nwrit = i1mach(2)

      real    (kind=8) :: ab
      real    (kind=8) :: bk
      real    (kind=8) :: dist
      real    (kind=8) :: fackp,facsc,fact,four
      integer (kind=4) :: i,ilc18,ilc28,ilc32,ilc34,ilc39
      integer (kind=4) :: ind,iref,istrt1,itcon,iup
      integer (kind=4) :: j,jmax
      integer (kind=4) :: k
      integer (kind=4) :: l,lmcon
      integer (kind=4) :: n 
      real    (kind=8) :: one
      real    (kind=8) :: quot
      real    (kind=8) :: s1,s1hi,s1low,scl,scl1,scl1a
      real    (kind=8) :: ten,three,tol,tol1,tols,two
      real    (kind=8) :: v1,violm,vmax
      real    (kind=8) :: zero 

      real    (kind=8) :: dotprd
      one = 1.0d0
      zero = one-one
      two = one+one
      three = one+two
      four = two+two
      ten = four+four+two
      spcmn = d1mach(3)
      tol = ten*ten*spcmn
      tol1 = (ten**4)*spcmn
      tols = sqrt(spcmn)
      iref = 0
      violm = one/two
      lmcon = 3
      itcon = 0
      iup = 0
      s1low = ten*ten*ten*spcmn
      s1hi = one-s1low
c make sure s1low .le. one third and s1hi .ge. two thirds to avoid
c squeezing the allowable region for s1 too tightly (or even making it
c empty).
      if(s1low-one/three)40,40,30
  30  s1low = one/three
  40  if(s1hi-two/three)50,60,60
  50  s1hi = two/three
  60  facsc = ten*ten*ten*ten
      fackp = facsc
c end of setting machine and precision dependent constants for wolfe.
      ilc18 = iloc(18,nparm,numgr)
      ilc28 = iloc(28,nparm,numgr)
      ilc32 = iloc(32,nparm,numgr)
      ilc34 = iloc(34,nparm,numgr)
      ilc39 = iloc(39,nparm,numgr)
      n = ndm+1
      istrt1 = istrt
      do 100 i = 1,ndm
        r(i) = zero
  100   continue
      r(n) = one

c now compute the scale factor scl, whose main purpose is to avoid
c having all vectors in pmat with positive last component form an angle
c close to 90 degrees with r  =  (0...0 1), which can cause numerical
c problems.  we will compute scl  =  min(max(abs(a(i,k)): 1 .le. i .le.
c ndm)/b(k), b(k) .ge. tols, 1 .le. k .le. m) unless no b(k) is .ge.
c tols, in which case we set scl = 1.0, or some b(k) is .ge. tols but
c scl would be .lt. tol, in which case we set scl  =  tol.
  105 scl = one
      ind = 0
      do 150 k = 1,m
        bk = pmat(n,k)
        if(bk-tols)150,110,110
  110   quot = zero
        do 120 i = 1,ndm
          ab = abs(pmat(i,k))
          if(ab-quot)120,120,115
  115     quot = ab
  120     continue
        quot = quot/bk
        if(ind)140,140,130
  130   if(quot-scl)140,150,150
  140   ind = 1
        scl = quot
  150   continue
  155 if(scl-tol)160,170,170
  160 scl = tol
c put scaled pmat into pmat1 for use in conenr.  pmat itself will remain
c unchanged.
  170 do 180 j = 1,m
        do 175 i = 1,ndm
          pmat1(i,j) = pmat(i,j)/scl
  175     continue
        pmat1(n,j) = pmat(n,j)
  180   continue
c now do a normal scaling on each column of pmat1 which has an element
c with absolute value .ge. tol1.
      do 190 j = 1,m
        scl1 = zero
        do 184 i = 1,n
          ab = abs(pmat1(i,j))
          if(ab-scl1)184,184,182
  182     scl1 = ab
  184     continue
        if(scl1-tol1)185,187,187
c also put a scaled version of wcoef into coef if istrt1 = 1.
  185   if(istrt1)190,190,186
  186   coef(j) = wcoef(j)
        go to 190
  187   do 188 i = 1,n
          pmat1(i,j) = pmat1(i,j)/scl1
  188     continue
        if(istrt1)190,190,189
  189   coef(j) = wcoef(j)*scl1
  190   continue

c if istrt1 = 1, for use in conenr set coef = (-s1*scl**2)*coef, where
c s1  =  s/(s + (1.0-s)*scl**2) is the s value in the scaled situation.
c note that a partly scaled version of wcoef (see loop ending with the
c statement numbered 190 above) is already in coef if istrt1 = 1.
      if(istrt1)400,400,200
c if we had ncor .gt. n, reset ncor to n.
  200 if(ncor-n)275,275,225
  225 ncor = n
  275 fact = -(s/(s+(one-s)*scl**2))*scl**2
      do 300 j = 1,m
        coef(j) = fact*coef(j)
  300   continue

c call conenr to compute the nearest point to r in the cone of
c nonnegative linear combinations of columns of pmat1.
  400 call conenr(n,m,pmat1,r,istrt1,ncor,icor,tol,iwork,liwrk,
     *work,lwrk,work(ilc39),work(ilc32),work(ilc28),nparm,numgr,coef,
     *ptnr,dist,nmaj,nmin,jflag)

c if jflag = 3 then conenr has failed, possibly because scl was too large.
      if(jflag-3)420,440,420
c here jflag .ne. 3 and we compute s1  =  1.0 - ptnr(n).
  420 s1 = one-ptnr(n)
      if(s1-s1low)440,580,580
c here jflag = 3 or s1 .lt. s1low, so if itcon .lt. lmcon we try again with
c smaller scl.
  440 if(itcon-lmcon)480,460,460

c here we were unable to get an acceptable s1 from conenr so we set
c jflag = 4 as a warning and return.  first try again from scratch if this
c has not been done.
  460 if(istrt1)470,470,465
  465 istrt1 = 0
      itcon = 0
      iref = 0
      iup = 0
      facsc = fackp
      go to 105

  470 jflag = 4
      return

c here we increment itcon and if scl was not already very small we
c decrease it and try conenr again.
  480 itcon = itcon+1
      if(iup)540,520,500
c here iup = 1 and we have oscillation in the search for a usable scl so
c we replace the correction factor by its square root and reset iup to
c 0 to indicate oscillation.
  500 iup = 0
  510 facsc = sqrt(facsc)
      go to 540

c here iup = 0 so either we are just starting (in which case we set iup=-1
c to indicate we are in a phase of decreasing scl) or we are oscillating.
  520 if(itcon-1)530,530,510
  530 iup = -1
c here we decrease scl if it was not already very small.
  540 if(scl-(one+one/ten)*tol)460,560,560
  560 scl = scl/facsc
      go to 155

c here jflag .ne. 3 and s1 .ge. s1low, so if also s1 .le. s1hi we accept
c the result from conenr and move on.
  580 if(s1-s1hi)680,680,600

c here jflag .ne. 3 and s1 .gt. s1hi, so if itcon .lt. lmcon we try
c again with larger scl.
c if here jflag = 0 and ncor=0 the nearest point to the origin in the
c polytope appears to be the origin so we forego adjusting scl.
  600 if(jflag)608,603,608
  603 if(ncor)680,680,608
  608 if(itcon-lmcon)610,460,460
  610 itcon = itcon+1
      if(iup)620,640,660
c here iup = -1 and we have oscillation in the search for a usable scl so
c we replace the correction factor by its square root and set iup = 0
c to indicate oscillation.
  620 iup = 0
  630 facsc = sqrt(facsc)
      go to 660
c here iup = 0 so either we are just starting (in which case we set iup=1
c to indicate we are in a phase of increasing scl) or we are oscillating.
  640 if(itcon-1)650,650,630
  650 iup = 1
  660 scl = scl*facsc
      go to 170

c here conenr may have succeeded and we compute the nearest point
c (wpt,s1) = r-ptnr to r from the dual of the cone described earlier.
c this new cone is the set of (x,t) such that (a(k)/scl,b(k)).(x,t) .le.
c 0.0 for k = 1,...,m.
  680 do 700 i = 1,ndm
        wpt(i) = -ptnr(i)
  700   continue
c divide wpt by s1*scl.
      do 1000 i = 1,ndm
        wpt(i) = wpt(i)/(s1*scl)
 1000   continue
c compute the maximum wolfe constraint violation as a check.
 1010 do 1080 j = 1,m
        v1 = pmat(n,j)
        do 1020 i = 1,ndm
          v1 = v1+pmat(i,j)*wpt(i)
 1020     continue
        if(j-1)1060,1060,1040
 1040   if(v1-vmax)1080,1080,1060
 1060   jmax = j
        vmax = v1
 1080   continue
c if vmax .le. violm we reset jflag to 0 and accept the result.
      if(vmax-violm)1082,1082,1084
 1082 jflag = 0
      go to 1099

c here vmax is too large.
 1084 if(iref)1094,1094,1086
c here we have unsuccessfully tried to refine wpt with refwl at least
c once.  if ncor .lt. ndm and the worst violation occurred outside
c icor we will put it in icor and try refwl again, otherwise we will
c set jflag = 7 and return (first trying froom scratch if this has not
c been done).
 1086 if(ncor-ndm)1088,1096,1096
 1088 if(ncor)1093,1093,1090
 1090 do 1092 l = 1,ncor
        if(jmax-icor(l))1092,1096,1092
 1092   continue
 1093 ncor = ncor+1
      icor(ncor) = jmax

c increment iref and call refwl to attempt to refine wpt, then go back
c and recheck the maximum constraint violation.
 1094 iref = iref+1
      call refwl(ndm,ncor,icor,pmat,pmat1,nparm,numgr,iwork(ilc18),
     *work(ilc34),wpt)
      go to 1010

 1096 if(istrt1)1098,1098,1097
 1097 istrt1 = 0
      itcon = 0
      iref = 0
      iup = 0
      facsc = fackp
      go to 105

 1098 jflag = 7
      return

c divide the coefficients by -s1*scl**2.
 1099 do 1100 j = 1,m
        wcoef(j) = -coef(j)/(s1*scl**2)
 1100   continue

c we now reconstruct the normal scaling factors computed in the loop
c ending with the statement labelled 190 in this subroutine.  in a later
c version of this subroutine an array may be created to store these in
c that loop, but for now we avoid the extra storage and programming work
c of fiddling with the variable dimensioning.  to recreate the factor
c scl1 corresponding to column j, we compute the maximum absolute value
c of the first ndm elements of pmat in this column, divide it by scl, take
c the maximum of this and abs(pmat(ndm+1,j)), and take scl1 to be this
c value unless it is less than tol1, in which we (in effect) take scl1 = 1.0.
c finally, since wcoef(j) was computed with the jth column of pmat divided
c by scl1 it contains a hidden factor of scl1, which we divide out.
      do 1170 j = 1,m
        scl1a = zero
        do 1130 i = 1,ndm
          ab = abs(pmat(i,j))
          if(ab-scl1a)1130,1130,1120
 1120     scl1a = ab
 1130     continue
        scl1 = scl1a/scl
        ab = abs(pmat(ndm+1,j))
        if(ab-scl1)1150,1150,1140
 1140   scl1 = ab
 1150   if(scl1-tol1)1170,1160,1160
 1160   wcoef(j) = wcoef(j)/scl1
 1170   continue

c compute the s value for the unscaled situation.
      s = s1/(s1+(one-s1)/scl**2)
c copy wpt into ptnr to get the right dimension for dotprd and compute
c the distance.
      do 1200 i = 1,ndm
        ptnr(i) = wpt(i)
 1200   continue
      ptnr(n) = zero
      wdist = sqrt(dotprd(ndm,ptnr,ptnr,nparm))
      return
      end

      double precision function d1mach(i)

      integer small(4)
      integer large(4)
      integer right(4)
      integer diver(4)
      integer log10(4)

      double precision dmach(5)

      equivalence (dmach(1),small(1))
      equivalence (dmach(2),large(1))
      equivalence (dmach(3),right(1))
      equivalence (dmach(4),diver(1))
      equivalence (dmach(5),log10(1))

c     machine constants for the burroughs 1700 system.

c     data small(1) / zc00800000 /
c     data small(2) / z000000000 /

c     data large(1) / zdffffffff /
c     data large(2) / zfffffffff /

c     data right(1) / zcc5800000 /
c     data right(2) / z000000000 /

c     data diver(1) / zcc6800000 /
c     data diver(2) / z000000000 /

c     data log10(1) / zd00e730e7 /
c     data log10(2) / zc77800dc0 /

c     machine constants for the burroughs 5700 system.

c     data small(1) / o1771000000000000 /
c     data small(2) / o0000000000000000 /

c     data large(1) / o0777777777777777 /
c     data large(2) / o0007777777777777 /

c     data right(1) / o1461000000000000 /
c     data right(2) / o0000000000000000 /

c     data diver(1) / o1451000000000000 /
c     data diver(2) / o0000000000000000 /

c     data log10(1) / o1157163034761674 /
c     data log10(2) / o0006677466732724 /

c     machine constants for the burroughs 6700/7700 systems.

c     data small(1) / o1771000000000000 /
c     data small(2) / o7770000000000000 /

c     data large(1) / o0777777777777777 /
c     data large(2) / o7777777777777777 /

c     data right(1) / o1461000000000000 /
c     data right(2) / o0000000000000000 /

c     data diver(1) / o1451000000000000 /
c     data diver(2) / o0000000000000000 /

c     data log10(1) / o1157163034761674 /
c     data log10(2) / o0006677466732724 /

c     machine constants for the cdc 6000/7000 series.

c     data small(1) / 00564000000000000000b /
c     data small(2) / 00000000000000000000b /

c     data large(1) / 37757777777777777777b /
c     data large(2) / 37157777777777777777b /

c     data right(1) / 15624000000000000000b /
c     data right(2) / 00000000000000000000b /

c     data diver(1) / 15634000000000000000b /
c     data diver(2) / 00000000000000000000b /

c     data log10(1) / 17164642023241175717b /
c     data log10(2) / 16367571421742254654b /

c     machine constants for the cray 1

c     data small(1) / 201354000000000000000b /
c     data small(2) / 000000000000000000000b /

c     data large(1) / 577767777777777777777b /
c     data large(2) / 000007777777777777774b /

c     data right(1) / 376434000000000000000b /
c     data right(2) / 000000000000000000000b /

c     data diver(1) / 376444000000000000000b /
c     data diver(2) / 000000000000000000000b /

c     data log10(1) / 377774642023241175717b /
c     data log10(2) / 000007571421742254654b /

c     machine constants for the data general eclipse s/200

c     note - it may be appropriate to include the following card -
c     static dmach(5)

c     data small/20k,3*0/,large/77777k,3*177777k/
c     data right/31420k,3*0/,diver/32020k,3*0/
c     data log10/40423k,42023k,50237k,74776k/

c     machine constants for the harris 220

c     data small(1),small(2) / '20000000, '00000201 /
c     data large(1),large(2) / '37777777, '37777577 /
c     data right(1),right(2) / '20000000, '00000333 /
c     data diver(1),diver(2) / '20000000, '00000334 /
c     data log10(1),log10(2) / '23210115, '10237777 /

c     machine constants for the honeywell 600/6000 series.

c     data small(1),small(2) / o402400000000, o000000000000 /
c     data large(1),large(2) / o376777777777, o777777777777 /
c     data right(1),right(2) / o604400000000, o000000000000 /
c     data diver(1),diver(2) / o606400000000, o000000000000 /
c     data log10(1),log10(2) / o776464202324, o117571775714 /

c      machine constants for the hp 2100
c      three word double precision option with ftn4

c      data small(1), small(2), small(3) / 40000b,       0,       1 /
c      data large(1), large(2), large(3) / 77777b, 177777b, 177776b /
c      data right(1), right(2), right(3) / 40000b,       0,    265b /
c      data diver(1), diver(2), diver(3) / 40000b,       0,    276b /
c      data log10(1), log10(2), log10(3) / 46420b,  46502b,  77777b /


c      machine constants for the hp 2100
c      four word double precision option with ftn4

c      data small(1), small(2) /  40000b,       0 /
c      data small(3), small(4) /       0,       1 /
c      data large(1), large(2) /  77777b, 177777b /
c      data large(3), large(4) / 177777b, 177776b /
c      data right(1), right(2) /  40000b,       0 /
c      data right(3), right(4) /       0,    225b /
c      data diver(1), diver(2) /  40000b,       0 /
c      data diver(3), diver(4) /       0,    227b /
c      data log10(1), log10(2) /  46420b,  46502b /
c      data log10(3), log10(4) /  76747b, 176377b /


c     machine constants for the ibm 360/370 series,
c     the xerox sigma 5/7/9, the sel systems 85/86, and
c     the perkin elmer (interdata) 7/32.

c     data small(1),small(2) / z00100000, z00000000 /
c     data large(1),large(2) / z7fffffff, zffffffff /
c     data right(1),right(2) / z33100000, z00000000 /
c     data diver(1),diver(2) / z34100000, z00000000 /
c     data log10(1),log10(2) / z41134413, z509f79ff /

c     machine constants for the pdp-10 (ka processor).

c     data small(1),small(2) / "033400000000, "000000000000 /
c     data large(1),large(2) / "377777777777, "344777777777 /
c     data right(1),right(2) / "113400000000, "000000000000 /
c     data diver(1),diver(2) / "114400000000, "000000000000 /
c     data log10(1),log10(2) / "177464202324, "144117571776 /

c     machine constants for the pdp-10 (ki processor).

c     data small(1),small(2) / "000400000000, "000000000000 /
c     data large(1),large(2) / "377777777777, "377777777777 /
c     data right(1),right(2) / "103400000000, "000000000000 /
c     data diver(1),diver(2) / "104400000000, "000000000000 /
c     data log10(1),log10(2) / "177464202324, "476747767461 /

c     machine constants for pdp-11 fortran supporting
c     32-bit integers (expressed in integer and octal).

c     data small(1),small(2) /    8388608,           0 /
c     data large(1),large(2) / 2147483647,          -1 /
c     data right(1),right(2) /  612368384,           0 /
c     data diver(1),diver(2) /  620756992,           0 /
c     data log10(1),log10(2) / 1067065498, -2063872008 /

c     data small(1),small(2) / o00040000000, o00000000000 /
c     data large(1),large(2) / o17777777777, o37777777777 /
c     data right(1),right(2) / o04440000000, o00000000000 /
c     data diver(1),diver(2) / o04500000000, o00000000000 /
c     data log10(1),log10(2) / o07746420232, o20476747770 /

c     machine constants for pdp-11 fortran supporting
c     16-bit integers (expressed in integer and octal).

c     data small(1),small(2) /    128,      0 /
c     data small(3),small(4) /      0,      0 /

c     data large(1),large(2) /  32767,     -1 /
c     data large(3),large(4) /     -1,     -1 /

c     data right(1),right(2) /   9344,      0 /
c     data right(3),right(4) /      0,      0 /

c     data diver(1),diver(2) /   9472,      0 /
c     data diver(3),diver(4) /      0,      0 /

c     data log10(1),log10(2) /  16282,   8346 /
c     data log10(3),log10(4) / -31493, -12296 /

c     data small(1),small(2) / o000200, o000000 /
c     data small(3),small(4) / o000000, o000000 /

c     data large(1),large(2) / o077777, o177777 /
c     data large(3),large(4) / o177777, o177777 /

c     data right(1),right(2) / o022200, o000000 /
c     data right(3),right(4) / o000000, o000000 /

c     data diver(1),diver(2) / o022400, o000000 /
c     data diver(3),diver(4) / o000000, o000000 /

c     data log10(1),log10(2) / o037632, o020232 /
c     data log10(3),log10(4) / o102373, o147770 /

c     machine constants for the univac 1100 series. ftn compiler

c     data small(1),small(2) / o000040000000, o000000000000 /
c     data large(1),large(2) / o377777777777, o777777777777 /
c     data right(1),right(2) / o170540000000, o000000000000 /
c     data diver(1),diver(2) / o170640000000, o000000000000 /
c     data log10(1),log10(2) / o177746420232, o411757177572 /


c     machine constants for vax 11/780
c     (expressed in integer and hexadecimal)
c    ***the hex format below may not be suitable for unix sysyems***
c    *** the integer format should be ok for unix systems***

c     data small(1), small(2) /        128,           0 /
c     data large(1), large(2) /     -32769,          -1 /
c     data right(1), right(2) /       9344,           0 /
c     data diver(1), diver(2) /       9472,           0 /
c     data log10(1), log10(2) /  546979738,  -805796613 /

c     data small(1), small(2) / z00000080, z00000000 /
c     data large(1), large(2) / zffff7fff, zffffffff /
c     data right(1), right(2) / z00002480, z00000000 /
c     data diver(1), diver(2) / z00002500, z00000000 /
c     data log10(1), log10(2) / z209a3f9a, zcff884fb /

c   machine constants for vax 11/780 (g-floating)
c     (expressed in integer and hexadecimal)
c    ***the hex format below may not be suitable for unix sysyems***
c    *** the integer format should be ok for unix systems***

c     data small(1), small(2) /         16,           0 /
c     data large(1), large(2) /     -32769,          -1 /
c     data right(1), right(2) /      15552,           0 /
c     data diver(1), diver(2) /      15568,           0 /
c     data log10(1), log10(2) /  1142112243, 2046775455 /

c     data small(1), small(2) / z00000010, z00000000 /
c     data large(1), large(2) / zffff7fff, zffffffff /
c     data right(1), right(2) / z00003cc0, z00000000 /
c     data diver(1), diver(2) / z00003cd0, z00000000 /
c     data log10(1), log10(2) / z44133ff3, z79ff509f /

c     machine constants for the elxsi 6400
c     (assuming real*8 is the default double precision)

c      data small(1), small(2) / '00100000'x,'00000000'x/
c      data large(1), large(2) / '7fefffff'x,'ffffffff'x/
c      data right(1), right(2) / '3cb00000'x,'00000000'x/
c      data diver(1), diver(2) / '3cc00000'x,'00000000'x/
c      data log10(1), diver(2) / '3fd34413'x,'509f79ff'x/

c     machine constants for the ibm pc - microsoft fortran

c     data small(1), small(2) / #00000000, #00100000 /
c     data large(1), large(2) / #ffffffff, #7fefffff /
c     data right(1), right(2) / #00000000, #3ca00000 /
c     data diver(1), diver(2) / #00000000, #3cb00000 /
c     data log10(1), log10(2) / #509f79ff, #3fd34413 /

c     machine constants for the ibm pc - professional fortran
c                       for the lahey f77l compiler.

      data small(1), small(2) / z'00000000', z'00100000' /
      data large(1), large(2) / z'ffffffff', z'7fefffff' /
      data right(1), right(2) / z'00000000', z'3ca00000' /
      data diver(1), diver(2) / z'00000000', z'3cb00000' /
      data log10(1), log10(2) / z'509f79ff', z'3fd34413' /

c***first executable statement  d1mach
      if (i .lt. 1  .or.  i .gt. 5)
     1   call xerror( 'd1mach -- i out of bounds',25,1,2)

      d1mach  =  dmach(i)

      end

      double precision function dqf4(y,d,a,gx,sdgy,nry)

      implicit   none

      include   'para.h'

      integer (kind=4) :: nry
      real    (kind=8) :: gx, sdgy
      real    (kind=8) :: y(nry),d(nry),a(nry)

      real    (kind=8) :: cdot

      dqf4 = cdot(y,d,1,1,nry)-sign(u,gx)*sdgy*cdot(a,d,1,1,nry)

      end function dqf4

      subroutine xerror (xmess, nmess, nerr, level)

      implicit   none

c     replacement subroutine for the slatec xerror package subroutine.

!     character (len=120) :: xmess
      character           :: xmess*(*)
      integer (kind=4) :: nmess,nerr,level
      integer (kind=4) :: ierr

      integer (kind=4) :: i1mach

      if (level.ge.1) then
      ierr = i1mach(3)
      write(ierr,'(1x,a)') xmess(1:nmess)
      write(ierr,'('' error number = '',i5,'', message level = '',i5)')
     &      nerr,level
      end if

      end subroutine xerror

      subroutine xerrwv(xmess,nmess,nerr,level,ni,i1,i2,
     &           nr,r1,r2)

      implicit   none

c     replacement subroutine for the slatec xerrwv package subroutine.

      character (len=120) :: xmess 
      integer (kind=4) :: nmess,nerr,level,ni,i1,i2,nr
      real    (kind=8) :: r1,r2

      integer (kind=4) :: ierr, igoipr

      integer (kind=4) :: i1mach
      real    (kind=8) :: cdot

      if(level.lt.1) return
      ierr = i1mach(3)
      write(ierr,'(1x,a)') xmess(1:nmess)
      write(ierr,'('' error number = '',i5,'', message level = '',i5)')
     .      nerr,level
      assign 80 to igoipr
      go to (10,20),ni
      go to 80
   10 continue

c     write one integer value.
      write(ierr,'('' i1  =  '',i8)') i1
      go to igoipr
   20 assign 30 to igoipr
      go to 10
   30 continue

c     write one integer value.
      write(ierr,'('' i2  =  '',i8)') i2
   80 continue
      assign 40 to igoipr
      go to (50,60),nr
   40 return
   50 continue

c     write one real value.
      write(ierr,'('' r1  =  '',1pe15.7)') r1
      go to igoipr
   60 assign 70 to igoipr
      go to 50
   70 continue

c     write real value.
      write(ierr,'('' r2  =  '',1pe15.7)') r2
      go to 40

      end subroutine xerrwv

      integer function i1mach(i)

      implicit   none

      integer (kind=4) :: imach(16),output, i

      equivalence (imach(4),output)

c     machine constants for the burroughs 1700 system.

c     data imach( 1) /    7 /
c     data imach( 2) /    2 /
c     data imach( 3) /    2 /
c     data imach( 4) /    2 /
c     data imach( 5) /   36 /
c     data imach( 6) /    4 /
c     data imach( 7) /    2 /
c     data imach( 8) /   33 /
c     data imach( 9) / z1ffffffff /
c     data imach(10) /    2 /
c     data imach(11) /   24 /
c     data imach(12) / -256 /
c     data imach(13) /  255 /
c     data imach(14) /   60 /
c     data imach(15) / -256 /
c     data imach(16) /  255 /

c     machine constants for the burroughs 5700 system.

c     data imach( 1) /   5 /
c     data imach( 2) /   6 /
c     data imach( 3) /   7 /
c     data imach( 4) /   6 /
c     data imach( 5) /  48 /
c     data imach( 6) /   6 /
c     data imach( 7) /   2 /
c     data imach( 8) /  39 /
c     data imach( 9) / o0007777777777777 /
c     data imach(10) /   8 /
c     data imach(11) /  13 /
c     data imach(12) / -50 /
c     data imach(13) /  76 /
c     data imach(14) /  26 /
c     data imach(15) / -50 /
c     data imach(16) /  76 /

c     machine constants for the burroughs 6700/7700 systems.

c     data imach( 1) /   5 /
c     data imach( 2) /   6 /
c     data imach( 3) /   7 /
c     data imach( 4) /   6 /
c     data imach( 5) /  48 /
c     data imach( 6) /   6 /
c     data imach( 7) /   2 /
c     data imach( 8) /  39 /
c     data imach( 9) / o0007777777777777 /
c     data imach(10) /   8 /
c     data imach(11) /  13 /
c     data imach(12) / -50 /
c     data imach(13) /  76 /
c     data imach(14) /  26 /
c     data imach(15) / -32754 /
c     data imach(16) /  32780 /

c     machine constants for the cdc 6000/7000 series.

c     data imach( 1) /    5 /
c     data imach( 2) /    6 /
c     data imach( 3) /    7 /
c     data imach( 4) /6loutput/
c     data imach( 5) /   60 /
c     data imach( 6) /   10 /
c     data imach( 7) /    2 /
c     data imach( 8) /   48 /
c     data imach( 9) / 00007777777777777777b /
c     data imach(10) /    2 /
c     data imach(11) /   47 /
c     data imach(12) / -929 /
c     data imach(13) / 1070 /
c     data imach(14) /   94 /
c     data imach(15) / -929 /
c     data imach(16) / 1069 /

c     machine constants for the cray 1

c     data imach( 1) /   100 /
c     data imach( 2) /   101 /
c     data imach( 3) /   102 /
c     data imach( 4) /   101 /
c     data imach( 5) /    64 /
c     data imach( 6) /     8 /
c     data imach( 7) /     2 /
c     data imach( 8) /    63 /
c     data imach( 9) /  777777777777777777777b /
c     data imach(10) /     2 /
c     data imach(11) /    47 /
c     data imach(12) / -8189 /
c     data imach(13) /  8190 /
c     machine constants for the data general eclipse s/200

c     data imach( 1) /   11 /
c     data imach( 2) /   12 /
c     data imach( 3) /    8 /
c     data imach( 4) /   10 /
c     data imach( 5) /   16 /
c     data imach( 6) /    2 /
c     data imach( 7) /    2 /
c     data imach( 8) /   15 /
c     data imach( 9) /32767 /
c     data imach(10) /   16 /
c     data imach(11) /    6 /
c     data imach(12) /  -64 /
c     data imach(13) /   63 /
c     data imach(14) /   14 /
c     data imach(15) /  -64 /
c     data imach(16) /   63 /

c     machine constants for the harris 220

c     data imach( 1) /       5 /
c     data imach( 2) /       6 /
c     data imach( 3) /       0 /
c     data imach( 4) /       6 /
c     data imach( 5) /      24 /
c     data imach( 6) /       3 /
c     data imach( 7) /       2 /
c     data imach( 8) /      23 /
c     data imach( 9) / 8388607 /
c     data imach(10) /       2 /
c     data imach(11) /      23 /
c     data imach(12) /    -127 /
c     data imach(13) /     127 /
c     data imach(14) /      38 /
c     data imach(15) /    -127 /
c     data imach(16) /     127 /

c     machine constants for the honeywell 600/6000 series.

c     data imach( 1) /    5 /
c     data imach( 2) /    6 /
c     data imach( 3) /   43 /
c     data imach( 4) /    6 /
c     data imach( 5) /   36 /
c     data imach( 6) /    6 /
c     data imach( 7) /    2 /
c     data imach( 8) /   35 /
c     data imach( 9) / o377777777777 /
c     data imach(10) /    2 /
c     data imach(11) /   27 /
c     data imach(12) / -127 /
c     data imach(13) /  127 /
c     data imach(14) /   63 /
c     data imach(15) / -127 /
c     data imach(16) /  127 /

c     machine constants for the hp 2100
c     3 word double precision option with ftn4

c     data imach(1) /      5/
c     data imach(2) /      6 /
c     data imach(3) /      4 /
c     data imach(4) /      1 /
c     data imach(5) /     16 /
c     data imach(6) /      2 /
c     data imach(7) /      2 /
c     data imach(8) /     15 /
c     data imach(9) /  32767 /
c     data imach(10)/      2 /
c     data imach(11)/     23 /
c     data imach(12)/   -128 /
c     data imach(13)/    127 /
c     data imach(14)/     39 /
c     data imach(15)/   -128 /
c     data imach(16)/    127 /

c     machine constants for the hp 2100
c     4 word double precision option with ftn4

c     data imach(1) /      5 /
c     data imach(2) /      6 /
c     data imach(3) /      4 /
c     data imach(4) /      1 /
c     data imach(5) /     16 /
c     data imach(6) /      2 /
c     data imach(7) /      2 /
c     data imach(8) /     15 /
c     data imach(9) /  32767 /
c     data imach(10)/      2 /
c     data imach(11)/     23 /
c     data imach(12)/   -128 /
c     data imach(13)/    127 /
c     data imach(14)/     55 /
c     data imach(15)/   -128 /
c     data imach(16)/    127 /

c     machine constants for the ibm 360/370 series,
c     the xerox sigma 5/7/9, the sel systems 85/86, and
c     the perkin elmer (interdata) 7/32.

c     data imach( 1) /   5 /
c     data imach( 2) /   6 /
c     data imach( 3) /   7 /
c     data imach( 4) /   6 /
c     data imach( 5) /  32 /
c     data imach( 6) /   4 /
c     data imach( 7) /  16 /
c     data imach( 8) /  31 /
c     data imach( 9) / z7fffffff /
c     data imach(10) /  16 /
c     data imach(11) /   6 /
c     data imach(12) / -64 /
c     data imach(13) /  63 /
c     data imach(14) /  14 /
c     data imach(15) / -64 /
c     data imach(16) /  63 /

c     machine constants for the pdp-10 (ka processor).

c     data imach( 1) /    5 /
c     data imach( 2) /    6 /
c     data imach( 3) /    5 /
c     data imach( 4) /    6 /
c     data imach( 5) /   36 /
c     data imach( 6) /    5 /
c     data imach( 7) /    2 /
c     data imach( 8) /   35 /
c     data imach( 9) / "377777777777 /
c     data imach(10) /    2 /
c     data imach(11) /   27 /
c     data imach(12) / -128 /
c     data imach(13) /  127 /
c     data imach(14) /   54 /
c     data imach(15) / -101 /
c     data imach(16) /  127 /

c     machine constants for the pdp-10 (ki processor).

c     data imach( 1) /    5 /
c     data imach( 2) /    6 /
c     data imach( 3) /    5 /
c     data imach( 4) /    6 /
c     data imach( 5) /   36 /
c     data imach( 6) /    5 /
c     data imach( 7) /    2 /
c     data imach( 8) /   35 /
c     data imach( 9) / "377777777777 /
c     data imach(10) /    2 /
c     data imach(11) /   27 /
c     data imach(12) / -128 /
c     data imach(13) /  127 /
c     data imach(14) /   62 /
c     data imach(15) / -128 /
c     data imach(16) /  127 /

c     machine constants for pdp-11 fortran supporting
c     32-bit integer arithmetic.

c     data imach( 1) /    5 /
c     data imach( 2) /    6 /
c     data imach( 3) /    5 /
c     data imach( 4) /    6 /
c     data imach( 5) /   32 /
c     data imach( 6) /    4 /
c     data imach( 7) /    2 /
c     data imach( 8) /   31 /
c     data imach( 9) / 2147483647 /
c     data imach(10) /    2 /
c     data imach(11) /   24 /
c     data imach(12) / -127 /
c     data imach(13) /  127 /
c     data imach(14) /   56 /
c     data imach(15) / -127 /
c     data imach(16) /  127 /

c     machine constants for pdp-11 fortran supporting
c     16-bit integer arithmetic.

c     data imach( 1) /    5 /
c     data imach( 2) /    6 /
c     data imach( 3) /    5 /
c     data imach( 4) /    6 /
c     data imach( 5) /   16 /
c     data imach( 6) /    2 /
c     data imach( 7) /    2 /
c     data imach( 8) /   15 /
c     data imach( 9) / 32767 /
c     data imach(10) /    2 /
c     data imach(11) /   24 /
c     data imach(12) / -127 /
c     data imach(13) /  127 /
c     data imach(14) /   56 /
c     data imach(15) / -127 /
c     data imach(16) /  127 /

c     machine constants for the univac 1100 series. ftn compiler


c     data imach( 1) /    5 /
c     data imach( 2) /    6 /
c     data imach( 3) /    1 /
c     data imach( 4) /    6 /
c     data imach( 5) /   36 /
c     data imach( 6) /    4 /
c     data imach( 7) /    2 /
c     data imach( 8) /   35 /
c     data imach( 9) / o377777777777 /
c     data imach(10) /    2 /
c     data imach(11) /   27 /
c     data imach(12) / -128 /
c     data imach(13) /  127 /
c     data imach(14) /   60 /
c     data imach(15) /-1024 /
c     data imach(16) / 1023 /


c     machine constants for the vax 11/780

c     data imach(1) /    5 /
c     data imach(2) /    6 /
c     data imach(3) /    5 /
c     data imach(4) /    6 /
c     data imach(5) /   32 /
c     data imach(6) /    4 /
c     data imach(7) /    2 /
c     data imach(8) /   31 /
c     data imach(9) /2147483647 /
c     data imach(10)/    2 /
c     data imach(11)/   24 /
c     data imach(12)/ -127 /
c     data imach(13)/  127 /
c     data imach(14)/   56 /
c     data imach(15)/ -127 /
c     data imach(16)/  127 /

c     machine constants for the elxsi 6400

c      data imach( 1) /     5/
c      data imach( 2) /     6/
c      data imach( 3) /     6/
c      data imach( 4) /     6/
c      data imach( 5) /    32/
c      data imach( 6) /     4/
c      data imach( 7) /     2/
c      data imach( 8) /    32/
c      data imach( 9) /2147483647/
c      data imach(10) /     2/
c      data imach(11) /    24/
c      data imach(12) /  -126/
c      data imach(13) /   127/
c      data imach(14) /    53/
c      data imach(15) / -1022/
c      data imach(16) /  1023/

c     machine constants for the z80 microprocessor

c     data imach( 1) /     1/
c     data imach( 2) /     1/
c     data imach( 3) /     0/
c     data imach( 4) /     1/
c     data imach( 5) /    16/
c     data imach( 6) /     2/
c     data imach( 7) /     2/
c     data imach( 8) /    15/
c     data imach( 9) / 32767/
c     data imach(10) /     2/
c     data imach(11) /    24/
c     data imach(12) /  -127/
c     data imach(13) /   127/
c     data imach(14) /    56/
c     data imach(15) /  -127/
c     data imach(16) /   127/

c     machine constants for the ibm pc - microsoft fortran

      data imach( 1) /     5/
      data imach( 2) /     6/
      data imach( 3) /     6/
      data imach( 4) /     0/
      data imach( 5) /    32/
      data imach( 6) /     4/
      data imach( 7) /     2/
      data imach( 8) /    31/
      data imach( 9) / 2147483647/
      data imach(10) /     2/
      data imach(11) /    24/
      data imach(12) /  -126/
      data imach(13) /   127/
      data imach(14) /    53/
      data imach(15) / -1022/
      data imach(16) /  1023/

c     machine constants for the ibm pc - professional fortran

c     data imach( 1) /     4/
c     data imach( 2) /     7/
c     data imach( 3) /     7/
c     data imach( 4) /     0/
c     data imach( 5) /    32/
c     data imach( 6) /     4/
c     data imach( 7) /     2/
c     data imach( 8) /    31/
c     data imach( 9) / 2147483647/
c     data imach(10) /     2/
c     data imach(11) /    24/
c     data imach(12) /  -126/
c     data imach(13) /   127/
c     data imach(14) /    53/
c     data imach(15) / -1022/
c     data imach(16) /  1023/

c***first executable statement  i1mach
      if (i .lt. 1  .or.  i .gt. 16) go to 10

      i1mach = imach(i)
      return

   10 continue
      write(output,9000)
9000  format('1error    1 in i1mach - i out of bounds ')

      stop
      end function i1mach

      integer function iloc(iarr,nparm,numgr)

      implicit   none

      integer (kind=4) :: iarr,nparm,numgr

c this function subprogram returns the subscript of the first element of
c array iarr relative to iwork (if the array is integer, i.e. 13 .le.
c iarr .le. 23) or relative to work (if the array is floating point, i.e.
c 1 .le. iarr .le. 12 or 24 .le. iarr .le. 48).

      go to (10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,
     *170,180,190,200,210,220,230,240,250,260,270,280,290,300,310,320,
     *330,340,350,360,370,380,390,400,410,420,430,440,450,460,470,
     *480),iarr

c   1  aa(nparm+1,nparm+1)  (opposite v, y; starts at v starting point)
   10 iloc = 3*numgr*nparm+6*numgr+11*nparm+8
      return

c   2  actdif(numgr)
   20 iloc = 1
      return

c   3  b(nparm+1)  (opposite v, y;  follows aa)
   30 iloc = nparm**2+3*numgr*nparm+6*numgr+13*nparm+9
      return

c   4  beta(nparm+1)  (opposite v, y;  follows b)
   40 iloc = nparm**2+3*numgr*nparm+6*numgr+14*nparm+10
      return

c   5  bndkp(nparm) (follows actdif)
   50 iloc = numgr+1
      return

c   6  coef(numgr)
   60 iloc = numgr+nparm+1
      return

c   7  cofbnd(nparm)
   70 iloc = 2*numgr+nparm+1
      return

c   8  confun(numgr,nparm+1)  (opposite pmat1)
   80 iloc = 2*numgr+2*nparm+1
      return

c   9  d(nparm+1)  (opposite v, y;  follows beta)
   90 iloc = nparm**2+3*numgr*nparm+6*numgr+15*nparm+11
      return

c  10  dvec(nparm) (follows confun)
  100 iloc = numgr*nparm+3*numgr+2*nparm+1
      return

c  11  err1(numgr+3)
  110 iloc = numgr*nparm+3*numgr+3*nparm+1
      return

c  12  funtbl(numgr,nparm+1)
  120 iloc = numgr*nparm+4*numgr+3*nparm+4
      return

c  13  iact(numgr)
  130 iloc = 1
      return

c  14  iact1(numgr)
  140 iloc = numgr+1
      return

c  15  icor(nparm+1)
  150 iloc = 2*numgr+1
      return

c  16  icor1(nparm+1)  (does not appear in program by name)
  160 iloc = 2*numgr+nparm+2
      return

c  17  icntyp(numgr)
  170 iloc = 2*numgr+2*nparm+3
      return

c  18  ixrct(numgr+2*nparm)
  180 iloc = 3*numgr+2*nparm+3
      return

c  19  iycct(nparm+1) (opposite kpivot)
  190 iloc = 4*numgr+4*nparm+3
      return

c  20  iyrct(numgr+2*nparm)
  200 iloc = 4*numgr+5*nparm+4
      return

c  21  jcntyp(numgr)
  210 iloc = 5*numgr+7*nparm+4
      return

c  22  kcntyp(numgr)
  220 iloc = 6*numgr+7*nparm+4
      return

c  23  kpivot(nparm+1)  (opposite iycct)
  230 iloc = 4*numgr+4*nparm+3
      return

c  24  param1(nparm) (follows funtbl)
  240 iloc = 2*numgr*nparm+5*numgr+3*nparm+4
      return

c  25  parprj(nparm)
  250 iloc = 2*numgr*nparm+5*numgr+4*nparm+4
      return

c  26  parser(nparm)
  260 iloc = 2*numgr*nparm+5*numgr+5*nparm+4
      return

c  27  parwrk(nparm)
  270 iloc = 2*numgr*nparm+5*numgr+6*nparm+4
      return

c  28  picor(nparm+1,nparm+1)  (opposite v, y;  follows d)
  280 iloc = nparm**2+3*numgr*nparm+6*numgr+16*nparm+12
      return

c  29  pmat(nparm+1,numgr) (follows parwrk)
  290 iloc = 2*numgr*nparm+5*numgr+7*nparm+4
      return

c  30  pmat1(nparm+1,numgr)  (opposite confun)
  300 iloc = 2*numgr+2*nparm+1
      return

c  31  ptnr(nparm+1) (follows pmat)
  310 iloc = 3*numgr*nparm+6*numgr+7*nparm+4
      return

c  32  ptnrr(nparm+1)
  320 iloc = 3*numgr*nparm+6*numgr+8*nparm+5
      return

c  33  r(nparm+1)
  330 iloc = 3*numgr*nparm+6*numgr+9*nparm+6
      return

c  34  save(nparm+1)
  340 iloc = 3*numgr*nparm+6*numgr+10*nparm+7
      return

c  35  v(numgr+2*nparm+1,nparm+2)  (with y, opposite aa, b, beta, d,
c      picor, zwork)
  350 iloc = 3*numgr*nparm+6*numgr+11*nparm+8
      return

c  36  vder(nparm) (follows y)
  360 iloc = 2*nparm**2+4*numgr*nparm+9*numgr+18*nparm+10
      return

c  37  vdern(nparm)
  370 iloc = 2*nparm**2+4*numgr*nparm+9*numgr+19*nparm+10
      return

c  38  vders(nparm)
  380 iloc = 2*nparm**2+4*numgr*nparm+9*numgr+20*nparm+10
      return

c  39  vec(nparm+1)
  390 iloc = 2*nparm**2+4*numgr*nparm+9*numgr+21*nparm+10
      return

c  40  wcoef(numgr)
  400 iloc = 2*nparm**2+4*numgr*nparm+9*numgr+22*nparm+11
      return

c  41  wcoef1(numgr)  (does not appear in the program by name)
  410 iloc = 2*nparm**2+4*numgr*nparm+10*numgr+22*nparm+11
      return

c  42  wpt(nparm)
  420 iloc = 2*nparm**2+4*numgr*nparm+11*numgr+22*nparm+11
      return

c  43  wvec(nparm)
  430 iloc = 2*nparm**2+4*numgr*nparm+11*numgr+23*nparm+11
      return

c  44  x(nparm+1)
  440 iloc = 2*nparm**2+4*numgr*nparm+11*numgr+24*nparm+11
      return

c  45  xkeep(nparm+1)
  450 iloc = 2*nparm**2+4*numgr*nparm+11*numgr+25*nparm+12
      return

c  46  xrk(nparm+1)
  460 iloc = 2*nparm**2+4*numgr*nparm+11*numgr+26*nparm+13
      return

c  47  y(numgr+2*nparm)  (with v, opposite aa, b, beta, d, picor,
c      zwork;  follows v)
  470 iloc = 2*nparm**2+4*numgr*nparm+8*numgr+16*nparm+10
      return

c  48  zwork(nparm)  (opposite v, y;  follows picor)
  480 iloc = 2*nparm**2+3*numgr*nparm+6*numgr+18*nparm+13

      end
