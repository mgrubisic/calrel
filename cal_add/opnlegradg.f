      subroutine egradg(i,x,gradgi,isfunc,idfunc,yyy,zzz,ex,tf,tp,
     * sg,par,bnd,gam,dgx,gdgx,alp,igt,lgp,ids,ipa,ib,izx,ixz)

      implicit   none

      include 'o8fuco.h'

      integer*4 idfunc(*),ids(*),ipa(4,*),izx(*),lgp(3,*)
      integer*4 igt(*),ib(*),ixz(*),isfunc(*)
      real*8 zzz(*),bnd(2,*),tf(*),par(4,*),sg(*),ex(*),tp(*),yyy(*)
      real*8 gam(*),dgx(*),alp(*),sdgy,gdgx(*)
      double precision x(*) ,gradgi(*)
      integer i,j,ifc,ii
c
c     ----- input check -----
c
      write(60,'(1h ,''========================'')')
      write(60,'(1h ,'' input check for egradg '')')
      write(60,'(1h ,''========================'')')
c
      write(60,'(1h ,'' idfunc '')')
      write(60,'((1h ,10i5))') (idfunc(ii),ii=1,ng)
      write(60,'(1h ,'' isfunc '')')
      write(60,'((1h ,10i5))') (isfunc(ii),ii=1,ng)
      write(60,'(1h ,'' ex '')')
      write(60,'((1h ,10e15.5))') (ex(ii),ii=1,n)
      write(60,'(1h ,'' sg '')')
      write(60,'((1h ,10e15.5))') (sg(ii),ii=1,n)
      write(60,'(1h ,'' ids '')')
      write(60,'((1h ,10i5))') (ids(ii),ii=1,n)
      write(60,'(1h ,'' izx '')')
      write(60,'((1h ,10i5))') (izx(ii),ii=1,n)
      write(60,'(1h ,'' ixz '')')
      write(60,'((1h ,10i5))') (ixz(ii),ii=1,n)
c
      if ( gunit(1,i+nh) .ne. 1 ) cgres(i+nh)=cgres(i+nh)+1
      do  j=1,nx
        gradgi(j)=0.d0
      enddo
      ifc=iabs(idfunc(i))
      call cytox(yyy,x,zzz,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call fdirct(yyy,x,tp,ex,sg,dgx,gam,alp,tf,par,bnd,igt,lgp,
     *          ids,ipa,izx,ixz,sdgy,gdgx,ifc,0)
      if(idfunc(i).gt.0) then
        do i=1,n
          gradgi(i)=alp(i)*sdgy
        enddo
      else
        do i=1,n
          gradgi(i)=-alp(i)*sdgy
        enddo
      endif
c
      write(60,'(1h ,'' resultant gradgi '')')
      write(60,'((1h ,10e15.5))') (gradgi(ii),ii=1,n)
c
      return
      end subroutine egradg
