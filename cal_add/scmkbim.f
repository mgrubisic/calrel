c*****************************************************************
      subroutine scmkbim(not,ngf,nset1,icset1,nset2,icset2,nset,iset,
     *                   ifail)
c*****************************************************************
c
c     construct set data for paralell of 2 cut set
c
c     input   nset1 : # of comp. in cutset 1
c             icset1: seq id of comp. in cutset 1
c             nset2 : # of comp. in cutset 2
c             icset2: seq id of comp. in cutset 2
c             ngf   : total # of components
c                     (as the maximum # of components)
c
c     output  nset  : # of comp. in joint set
c             iset  : seq id of comp. in joint set
c
c*****************************************************************
      implicit   none

      integer*4,allocatable::iflag(:)
      integer*4 not,ngf,nset1,icset1(nset1),nset2,icset2(nset2),
     *          nset,iset(*)
c
      integer*4 i,ii,ifail
c
      allocate(iflag(ngf))
c
      do i=1,ngf
        iflag(i)=0
      enddo
c
      do i=1,nset1
        ii=icset1(i)
        if(ii.gt.0) then
          iflag(ii)=1
        else
          iflag(iabs(ii))=-1
        endif
      enddo
c
      ifail=0
      do i=1,nset2
        ii=icset2(i)
        if(ii.gt.0) then
          if(iflag(iabs(ii)).eq.0) then
             iflag(ii)=1
          elseif(iflag(iabs(ii)).eq.1) then
             iflag(ii)=1
          else
             ifail=1
          endif
        else
          if(iflag(iabs(ii)).eq.0) then
             iflag(iabs(ii))=-1
          elseif(iflag(iabs(ii)).eq.1) then
             ifail=1
          else
             iflag(iabs(ii))=-1
          endif
        endif
      enddo
c
      nset=0
      if(ifail.eq.0) then
      do i=1,ngf
        if(iflag(i).gt.0) then
          nset=nset+1
          iset(nset)=i
        elseif(iflag(i).lt.0) then
          nset=nset+1
          iset(nset)=-i
        endif
      enddo
      endif
c
      if(not.ne.0) then
      write(not,'(1h ,''  '')')
      write(not,'(1h ,'' # of comp. in the 1st cutset......'',i5)')nset1
      write(not,'(1h ,'' components......'',5i10)')(icset1(i),i=1,nset1)
      write(not,'(1h ,''  '')')
      write(not,'(1h ,'' # of comp. in the 2nd cutset......'',i5)')nset2
      write(not,'(1h ,'' components......'',5i10)')(icset2(i),i=1,nset2)
      write(not,'(1h ,''  '')')
      write(not,'(1h ,'' # of comp. in the joint...........'',i5)')nset
      if(nset.ne.0) then
      write(not,'(1h ,'' components......'',5i10)')(iset(i),i=1,nset)
      endif
      endif
c
      deallocate(iflag)

      end subroutine scmkbim
