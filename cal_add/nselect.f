c******************************************
      subroutine nselect(ngfc,wlim,stp,iopt,ifunc)
c******************************************
c
c     ----- select the func # -----
c
c******************************************
      implicit   none

      integer*4 ngfc,ifunc,iopt,jfunc
      real*8 wlim(*),stp,rn
c
      call gguw(stp,1,iopt,rn)
      iopt = 0
      do jfunc=1,ngfc-1
        if(rn.le.wlim(jfunc)) goto 10
      enddo
      ifunc=ngfc
      goto 20
   10 ifunc=jfunc
   20 continue

      end subroutine nselect
