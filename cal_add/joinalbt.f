!*******************************************************************
      subroutine joinalbt(nry,x1,y1,tp,ex,sg,dgx,gam,alp,tf,par,bnd,igt,
     *                    lgp,ids,ipa,izx,ixz,sdgy,gdgx,gdes,
     *                    ifunc1,ifunc2,alpnew,btnew)
!*******************************************************************
      implicit   none

      real    (kind=8) :: x1(*),y1(*),tp(*),ex(*),sg(*),dgx(8)
      real    (kind=8) :: par(4,*),bnd(2,*),sdgy,gdgx(*)
      real    (kind=8) :: gam(*),alp(*),tf(*),alpnew(*),btnew,gdes
      integer (kind=4) :: igt(*),lgp(3,*),ids(*),ipa(*),izx(*),ixz(*)
      integer (kind=4) :: ifunc1,ifunc2,nry

      integer (kind=4) :: k
      real    (kind=8) :: ysum

      call fdirct(x1,y1,tp,ex,sg,dgx,gam,alp,tf,par,bnd,igt,
     *           lgp,ids,ipa,izx,ixz,sdgy,gdgx,abs(ifunc2),0)
      ysum = 0.0d0
      do k = 1,nry
        alpnew(k) = alp(k)
        ysum = ysum+y1(k)*alp(k)
      enddo
      btnew = ysum+gdes/sdgy
      if(ifunc1.lt.0) then
        do k = 1,nry
          alpnew(k) = -alpnew(k)
        enddo
        btnew = -btnew
      endif

      end subroutine joinalbt
