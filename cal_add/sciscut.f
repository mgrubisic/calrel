c******************************************************************
      subroutine sciscut(not,ngf,igfx,icl,ntl,mc,
     *                   isys,ncset,nset,ndic,icset,fltf)
c*******************************************************************

c     construct cutset information

c     input    icl  : problem =2 series system
c                              3 general system

c              ntl  : total number of cutset data
c              mc   : cutset data

c     output   isys : system   =1 series
c                              =2 parallel
c                              =3 general

c              ncset      : number of cutset
c              nset(i)    : number of components in set i
c              icset(j,i) : component id in ith cut-set

c********************************************************************
      implicit   none

      integer*4,allocatable::iprint(:)
      integer*4 icl,ntl,mc(ntl,2),ndic,ngf,not,igfx(ngf,3)
      integer*4 isys,ncset,nset(*),icset(ndic,*)
      logical fltf(*)

      integer*4 i,iim,isq,nsett,j,npr

      if(icl.eq.2) then
        isys=1
        ncset=1
        nsett=0
        do i=1,ntl
          iim=mc(i,1)
          if(iim.ne.0) then
            nsett=nsett+1
            isq=mc(i,2)
c  added
            isq=igfx(isq,2)
            if(.not.fltf(isq)) then
              write(not,'(1h ,'' fatal error in scis '')')
              write(not,'(1h ,'' form is not performed'',
     *        '' for l.s.f. '',i5)') igfx(isq,1)
              stop
            endif
            if(iim.gt.0) then
              icset(nsett,1)=isq
            else
              icset(nsett,1)=-isq
            endif
          endif
        enddo
        nset(1)=nsett
      else
        ncset=1
        nsett=0
        do i=1,ntl
          iim=mc(i,1)  ! function id
          if(iim.ne.0) then
            nsett=nsett+1
            isq=mc(i,2)  ! function seq id
            isq=igfx(isq,2)
            if(.not.fltf(isq)) then
              write(not,'(1h ,'' fatal error in scis '')')
              write(not,'(1h ,'' form is not performed'',
     *        '' for l.s.f. '',i5)') igfx(isq,1)
              stop
            endif
            if(iim.gt.0) then
              icset(nsett,ncset)=isq
            else
              icset(nsett,ncset)=-isq
            endif
          else
            nset(ncset)=nsett
            ncset=ncset+1
            nsett=0
          endif
        enddo
        ncset=ncset-1

        if(ncset.eq.1) then
          isys=2
        else
          isys=3
        endif
      endif

      allocate(iprint(ndic))
      if(isys.eq.1) then
      write(not,'(1h ,'' '')')
      write(not,'(1h ,''===== series system problem  ===== '')')
      write(not,'(1h ,'' '')')
      npr=0
      do i=1,ncset
      do j=1,nset(i)
        if(icset(j,i).gt.0) then
          npr=npr+1
          iprint(npr)=igfx(icset(j,i),1)
        else
          npr=npr+1
          iprint(npr)=-igfx(-icset(j,i),1)
        endif
      enddo
      enddo
      write(not,'(1h ,''components...........................'',10i5)')
     *   (iprint(j),j=1,npr)
      elseif(isys.eq.2) then
      write(not,'(1h ,'' '')')
      write(not,'(1h ,''===== parallel system problem ===== '')')
      write(not,'(1h ,'' '')')
      do i=1,ncset
      do j=1,nset(i)
        if(icset(j,i).gt.0) then
          iprint(j)=igfx(icset(j,i),1)
        else
          iprint(j)=-igfx(-icset(j,i),1)
        endif
      enddo
      write(not,'(1h ,''number of components.................'',i5)')
     *     nset(i)
      write(not,'(1h ,''components...........................'',10i5)')
     *   (iprint(j),j=1,nset(i))
      enddo
      else
      write(not,'(1h ,'' '')')
      write(not,'(1h ,''===== general system problem ===== '')')
      write(not,'(1h ,'' '')')
      write(not,'(1h ,''number of cut sets...................'',i5)')
     * ncset
      do i=1,ncset
      do j=1,nset(i)
        if(icset(j,i).gt.0) then
          iprint(j)=igfx(icset(j,i),1)
        else
          iprint(j)=-igfx(-icset(j,i),1)
        endif
      enddo
      write(not,'(1h ,''----- cutset'',i5,''  -----'')') i
      write(not,'(1h ,''number of components.................'',i5)')
     *     nset(i)
      write(not,'(1h ,''components...........................'',10i5)')
     *   (iprint(j),j=1,nset(i))
      enddo
      endif

      deallocate(iprint)

      end subroutine sciscut
