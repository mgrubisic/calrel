      subroutine house(n,ncor,picor,r,kpivot,nparm,aa,beta,d,asave,b,
     *                 vec,ihouse)

      implicit   none

      integer (kind=4) :: n,ncor,nparm,ihouse
      integer (kind=4) :: kpivot(nparm+1)
      real    (kind=8) :: vec(nparm+1),aa(nparm+1,nparm+1),beta(nparm+1)
      real    (kind=8) :: d(nparm+1),asave(nparm+1),b(nparm+1)
      real    (kind=8) :: picor(nparm+1,nparm+1),r(nparm+1)

! given ncor n dimensional vectors as columns of the n by ncor
! matrix picor and an n dimensional vector r, this subroutine uses
! householder transformations to find the best least squares solution
! vec to the linear system of equations picor*vec = r, where vec
! is an ncor dimensional vector.  if the rank of picor is
! (computationally) 0, the subroutine will return with the failure
! warning ihouse=1, otherwise it will return with ihouse=0.  if the
! rank is .gt. 0 but .lt. ncor, then (ncor - rank) of the components
! of vec will be set to 0.0.  the arays picor and r will not be
! changed by this subroutine.  the subroutine will attempt up to
! numref iterative refinements of the solution, where the user can
! set numref as any nonnegative integer, but to get the most out of
! the iterative refinement process, the computation of the residual
! summ near the end of this subroutine should be done in higher
! precision than the other computations in the subroutine.

! compute machine and precision dependent constants.
!     nwrit=i1mach(2)

      real    (kind=8) :: aakk,amax
      real    (kind=8) :: d1mach
      real    (kind=8) :: four
      integer (kind=4) :: i,ia,icount,ii
      integer (kind=4) :: j,jj 
      integer (kind=4) :: k,kchnge,kk,kp,krank,kt
      integer (kind=4) :: nmref1,nmref2,numref
      real    (kind=8) :: one
      real    (kind=8) :: spcmn,sqdk,ysum,summ,store
      real    (kind=8) :: ten,test,testt,tolsq,two
      real    (kind=8) :: zero
      one=1.0d0
      zero=one-one
      two=one+one
      four=two+two
      ten=four+four+two
      spcmn=d1mach(3)
      tolsq=(ten*ten*spcmn)**2
      ihouse=0
! set numref = the limit on the number of iterative refinement steps.
      numref=1
      nmref1=numref+1
      nmref2=numref+2
! set krank = min(n,ncor).  this may be reduced later.
      krank=ncor
      if(n-ncor)6,8,8
    6 krank=n
! initially set kpivot.  after all column interchanges are done
! kpivot(j) will be the original position of the column where the
! jth pivot was done.  this column will be moved to column j.
    8 do 10 j=1,ncor
         kpivot(j)=j
   10 continue
! copy r into b and picor into aa, but in the process replace any numbers
! with absolute value less than spcmn by zero to avoid underflows.
      do 18 i=1,n
        if(abs(r(i))-spcmn)14,16,16
   14   b(i)=zero
        go to 18
   16   b(i)=r(i)
   18   continue
      do 23 j=1,ncor
        do 22 i=1,n
          if(abs(picor(i,j))-spcmn)20,21,21
   20     aa(i,j)=zero
          go to 22
   21     aa(i,j)=picor(i,j)
   22     continue
   23   continue
      do 130 k=1,ncor
         if(k-n)24,24,140
   24    d(k)=zero
         kchnge=k
         do 40 jj=k,ncor
            ysum=zero
            do 30 ia=k,n
              if(abs(aa(ia,jj))-spcmn)30,30,25
   25         ysum=ysum+aa(ia,jj)*aa(ia,jj)
   30         continue
            if(d(k)-ysum)35,40,40
   35       kchnge=jj
            d(k)=ysum
   40    continue

!  kchnge contains the index of the column of greatest
!  length between k and ncor (from position k to the bottom).
! if k=1 and d(k) .lt. tolsq we return with the failure warning
! ihouse=1.
        if(k-1)42,42,48
   42   if(d(k)-tolsq)44,48,48
   44   ihouse=1
        return

   48    if(kchnge-k)49,60,49

!  start column interchange.

   49    do 50 i=1,n
            store=aa(i,kchnge)
            aa(i,kchnge)=aa(i,k)
            aa(i,k)=store
   50    continue
         kk=kpivot(k)
         kpivot(k)=kpivot(kchnge)
         kpivot(kchnge)=kk
   60    continue
         if(k-1)65,70,65
   65    amax=abs(d(1))
         test=(float(n-k+1)*(ten*ten*spcmn)**2)*(amax*amax)
         if(abs(d(k))-test)67,67,70

! here the length of the best of columns k through ncor (from k down)
! was too small, and we reduce krank to k-1 and leave this loop.
   67    d(k)=sqrt(d(k))
         krank=k-1
         go to 140

   70    continue

! now compute the scalar beta(k) and the n-k+1 dimensional vector
! gnu(k) (to be placed in aa(k,k),...,aa(n,k)) for i(k) - beta(k)*
! gnu(k)*(gnu(k) transpose), which is the active part of the
! householder transformation ph(k) = diag(i(k-1), active part).  this
! is a symmetric orthogonal matrix which when multiplied times aa will
! zero out aa(k+1,k),...,aa(n,k) and change aa(k,k) to -sgn(old
! aa(k,k))*sqdk, where sqdk = length of old (aa(k,k),...,aa(n,k)) and
! we redefine the sgn function to have value 1.0 if its argument is
! 0.0.  we will have beta(k) = 1.0/(sqdk**2 + abs(old aa(k,k))*sqdk)
! and gnu(k) = (old aa(k,k) + sgn(old aa(k,k))*sqdk, old aa(k+1,k),...,
! old aa(n,k)).  we will also replace d(k) by the new aa(k,k) (which
! will not actually be written into aa) for later use.
         aakk=aa(k,k)
         sqdk=sqrt(d(k))
         if(aakk-zero)80,75,75
   75    beta(k)=one/(d(k)+aakk*sqdk)
         aa(k,k)=sqdk+aakk
         d(k)=-sqdk
         go to 90
   80    continue
         beta(k)=one/(d(k)-aakk*sqdk)
         aa(k,k)=-sqdk+aakk
         d(k)=sqdk
   90    continue
         kt=k+1
         if(k-ncor)95,120,95

! here k .lt. ncor and we multiply columns k+1,...,ncor of aa by the
! householder transformation ph(k), which will change only positions
! k through the bottom of these columns.  this is done by, for j =
! k+1,...,ncor, replacing column j (from k down) by column j (from k down)
! - gnu(k)*(gnu(k).column j (from k down))*beta(k).
   95    do 110 j=kt,ncor
            asave(j)=zero
            do 100 ia=k,n
  100       asave(j)=asave(j)+aa(ia,k)*aa(ia,j)
            do 110 i=k,n
               aa(i,j)=aa(i,j)-aa(i,k)*asave(j)*beta(k)
  110    continue
  120    continue
  130 continue
  140 continue

      do 150 i=1,krank
! if i .le. min(krank,ncor-1), divide row i of aa from column i+1
! through column ncor by the new aa(i,i) (which is not actually
! written into aa(i,i), but is stored in d(i)).
         ii=i+1
         if(i-ncor)145,160,145
  145    do 150 j=ii,ncor
            aa(i,j)=aa(i,j)/d(i)
  150 continue
  160 continue

! now all the diagonal elements of aa (although not written in)
! are 1.0 and all off diagonal elements of aa are less than or
! equal to 1.0.

! initialize the iterative refinement counter icount and zero out vec
! initially.  the vec values not corresponding to the first krank
! columns (modulo earlier column interchanges) will remain at 0.0.
      icount=1
      do 250 i=1,ncor
  250 vec(i)=zero
  260 continue

! premultiply b by the householder transformations ph(1),...,
! ph(krank).  recall that gnu(i) is still in aa(i,i),...,aa(n,i)
! for i=1,...,krank.

      do 290 i=1,krank
         ysum=zero
         do 270 ia=i,n
  270    ysum=ysum+aa(ia,i)*b(ia)
         ysum=ysum*beta(i)
         do 280 j=i,n
            b(j)=b(j)-aa(j,i)*ysum
  280    continue
  290 continue

! now only use the first krank terms of b, as we cant do anything about
! the others, whose square root of sum of squares will give the least
! squares distance.
! divide b(i) by d(i) for i=1,...,krank as we did this to row i of aa.

      do 300 i=1,krank
         b(i)=b(i)/d(i)
  300 continue

! the problem has now been reduced to solving (upper left krank by
! krank part of aa)*(first krank terms of vec, modulo column
! interchange unscrambling) = (first krank terms of b).  although the
! diagonal and below diagonal terms of the coefficient matrix have not
! been written in, the system is upper triangular with diagonal elements
! all equal to 1.0, so we solve by back substitution.  we first put
! the solution to this system in b(1),...,b(krank) and sort it out
! later.  if icount .gt. 1 the solution is an iterative correction to
! vec rather than vec itself.
      do 320 ii=1,krank
         i=krank+1-ii
         kk=i-1
         if(i-1)305,320,305
! here we already have b(i) (where i  .gt. 1) and we subtract aa(j,i)*
! b(i) from b(j) for j = 1,...,i-1.
  305    do 310 j=1,kk
            b(j)=b(j)-aa(j,i)*b(i)
  310    continue
  320 continue

!  test for convergence.
!  first test, too many iterations.
!  second test, see if vec is decreasing.

! compute the length squared of the first top 1 through krank part of
! b, which will be the residual vector if icount .gt. 1.
      ysum=zero
      do 390 i=1,krank
        if(abs(b(i))-spcmn)390,390,385
  385   ysum=ysum+b(i)*b(i)
  390   continue
      if(icount-1)395,400,395
  395 if(ysum-test/two)410,410,397
  397 icount=nmref2
      go to 410
  400 testt=ysum
  410 test=ysum

! compute the vec values, which will be actual vec values if icount=1
! and corrections to vec values if icount .gt. 1.  we get these by
! unscrambling the b values and adding them to the appropriate old vec
! values (which will be 0.0 if icount=1).
      do 420 i=1,krank
         kp=kpivot(i)
         vec(kp)=b(i)+vec(kp)
  420 continue

! calculate the residual r - acoef*vec.  recall that acoef and r
! contain the original coefficient and right side arrays respectively.
! to get the most out of iterative refinement this computation should
! probably be done in higher precision, in which case it may be
! fruitful to also set numref larger at the beginning of this
! subroutine.
      do 440 i=1,n
         summ=zero
         do 430 j=1,ncor
           if(abs(picor(i,j))-spcmn)430,425,425
  425    summ=summ+picor(i,j)*vec(j)
  430    continue
  440 b(i)=r(i)-summ

!  third test, was the correction significant.

      if(test-spcmn*testt)450,442,442
  442 if(icount-nmref1)444,450,444
  444 if(icount-nmref2)446,450,450
  446 icount=icount+1
      go to 260
  450 continue

      end
