c evaluation of gradient of i-th inequality
c not necessary for bounds, but constant gradients must be set
c here e.g. using dcopy from a data-field
      subroutine esgradg(i,x,gradgi,isfunc,idfunc,yyy,zzz,ex,tf,tp,
     *           sg,par,bnd,gam,dgx,gdgx,alp,igt,lgp,ids,ipa,ib,izx,ixz)

      implicit   none

      include 'o8fuco.h'
      include 'o8cons.h'
      include 'o8fint.h'
      double precision x(*) ,gradgi(*)
      integer i,j
      integer*4 idfunc(*),ids(*),ipa(4,*),izx(*),lgp(3,*),igt(*),ib(*)
      integer*4 ixz(*),isfunc(i)
      real*8 zzz(*),bnd(2,*),tf(*),par(4,*),sg(*),ex(*),tp(*),yyy(*)
      real*8 gam(*),dgx(*),gdgx(*),alp(*)
c     double precision d1,d2,d3,sd1,sd2,sd3,fhelp,fhelp1,fhelp2,
c    *   fhelp3,fhelp4,fhelp5,fhelp6,xincr,xhelp,gixloc
      save
      if ( gunit(1,i+nh) .eq. 1 ) then
        do  j=1,n
          gradgi(j)=zero
        enddo
        gradgi(gunit(2,i+nh))=gunit(3,i+nh)*xsc(gunit(2,i+nh))
        return
      endif
      if ( bloc ) then
        if ( valid ) then
          if ( gunit(1,i+nh) .ne. 1 ) cgres(i+nh)=cgres(i+nh)+1
          do j=1,n
            gradgi(j)=xsc(j)*fugrad(j,i+nh)
          enddo
          return
        else
          stop 'donlp2: bloc call with function info invalid'
        endif
      else
        do j=1,n
          xtr(j)=x(j)*xsc(j)
        enddo
        if ( analyt ) then
            call egradg(i,xtr,gradgi,isfunc,idfunc,yyy,zzz,ex,tf,tp,
     *      sg,par,bnd,gam,dgx,gdgx,alp,igt,lgp,ids,ipa,ib,izx,ixz)
        else
c        if ( difftype .eq. 1 ) then
c          deldif=min(tm1*sqrt(epsfcn),tm2)
c          call eg(i,xtr,gixloc)
c          call eg(i,xtr,gixloc,idfunc,yyy,zzz,tf,sg,par,bnd,
c     *              igt,lgp,ids,ipa,ib,izx)
c          do j=1,n
c            xhelp=xtr(j)
c            xincr=min(tm2,deldif*abs(xhelp)+deldif,taubnd)
c            if ( xhelp .ge. zero ) then
c              xtr(j)=xhelp+xincr
c            else
c              xtr(j)=xhelp-xincr
c            endif
cc            call eg(i,xtr,fhelp)
c          call eg(i,xtr,fhelp,idfunc,yyy,zzz,tf,sg,par,bnd,
c     *              igt,lgp,ids,ipa,ib,izx)
c            gradgi(j)=(fhelp-gixloc)/(xtr(j)-xhelp)
c            xtr(j)=xhelp
c          enddo
c        elseif ( difftype .eq. 2 ) then
c          deldif=min(tm1*epsfcn**(one/three),tm2)
c          do j=1,n
c            xhelp=xtr(j)
c            xincr=min(tm2,deldif*abs(xhelp)+deldif,taubnd)
c            xtr(j)=xhelp+xincr
cc            call eg(i,xtr,fhelp1)
c          call eg(i,xtr,fhelp1,idfunc,yyy,zzz,tf,sg,par,bnd,
c     *              igt,lgp,ids,ipa,ib,izx)
c            xtr(j)=xhelp-xincr
cc            call eg(i,xtr,fhelp2)
c          call eg(i,xtr,fhelp2,idfunc,yyy,zzz,tf,sg,par,bnd,
c     *              igt,lgp,ids,ipa,ib,izx)
c            gradgi(j)=(fhelp1-fhelp2)/(xincr+xincr)
c            xtr(j)=xhelp
c          enddo
c        else
c          deldif=min(tm1*epsfcn**(one/seven),tm2)
c          do j=1,n
c            xhelp=xtr(j)
c            xincr=min(tm2,deldif*abs(xhelp)+deldif,taubnd/four)
c            xtr(j)=xhelp-xincr
cc            call eg(i,xtr,fhelp1)
c          call eg(i,xtr,fhelp1,idfunc,yyy,zzz,tf,sg,par,bnd,
c     *              igt,lgp,ids,ipa,ib,izx)
c            xtr(j)=xhelp+xincr
cc            call eg(i,xtr,fhelp2)
c          call eg(i,xtr,fhelp2,idfunc,yyy,zzz,tf,sg,par,bnd,
c     *              igt,lgp,ids,ipa,ib,izx)
c            xincr=xincr+xincr
c            d1=xincr
c            xtr(j)=xhelp-xincr
cc            call eg(i,xtr,fhelp3)
c          call eg(i,xtr,fhelp3,idfunc,yyy,zzz,tf,sg,par,bnd,
c     *              igt,lgp,ids,ipa,ib,izx)
c            xtr(j)=xhelp+xincr
cc            call eg(i,xtr,fhelp4)
c          call eg(i,xtr,fhelp4,idfunc,yyy,zzz,tf,sg,par,bnd,
c     *              igt,lgp,ids,ipa,ib,izx)
c            xincr=xincr+xincr
c            call eg(i,xtr,fhelp5)
c          call eg(i,xtr,fhelp5,idfunc,yyy,zzz,tf,sg,par,bnd,
c     *              igt,lgp,ids,ipa,ib,izx)
c            xtr(j)=xhelp+xincr
cc            call eg(i,xtr,fhelp6)
c          call eg(i,xtr,fhelp6,idfunc,yyy,zzz,tf,sg,par,bnd,
c     *              igt,lgp,ids,ipa,ib,izx)
c            xtr(j)=xhelp
c            d3=xincr+xincr
c            sd1=(fhelp2-fhelp1)/d1
c            sd2=(fhelp4-fhelp3)/d2
c            sd3=(fhelp6-fhelp5)/d3
c            sd3=sd2-sd3
c            sd2=sd1-sd2
c            sd3=sd2-sd3
c            gradgi(j)=sd1+p4*sd2+sd3/c45
c          enddo
c        endif

        endif
        do j=1,n
          gradgi(j)=xsc(j)*gradgi(j)
        enddo
      endif

      end subroutine esgradg

      real function o8cpu(idum)

      implicit   none

      integer idum

      o8cpu=0.

      end function o8cpu

      subroutine o8tida(chan)

      implicit   none

      integer chan

      end subroutine o8tida
