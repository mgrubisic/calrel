c***************************************************************************
      subroutine lpboun(beta,alph,igfx,mc,fltf,imxx,maxset,ibt,ind,
     *                 imeth,icheck,iclin,ncompr,ntlr,mcr,nptab,ptabin,
     *                 ptabin2,inequality,incost,ibtnegative)
c***************************************************************************

      implicit   none

      include    'cuts.h'
      include    'file.h'
      include    'filebound.h'
      include    'flag.h'
      include    'prob.h'

      integer (kind=4) :: maxset
      integer (kind=4),allocatable :: icset(:,:)
      integer (kind=4),allocatable :: nset(:)
      integer (kind=4),allocatable :: ibtable(:,:)
      integer (kind=4),allocatable :: icomp(:)
      integer (kind=4),allocatable :: igfxe(:,:)
      integer (kind=4),allocatable :: mce(:,:)
      logical         ,allocatable :: fltfe(:)
      real    (kind=8),allocatable :: cost(:)
      real    (kind=8),allocatable :: rhsin(:)
      real    (kind=8),allocatable :: primal8(:)
      real    (kind=8),allocatable :: costimax(:)
      real    (kind=8),allocatable :: costimin(:)
      real    (kind=4),allocatable :: dattrv(:)
      real    (kind=4),allocatable :: bl(:)
      real    (kind=4),allocatable :: bu(:)
      real    (kind=4),allocatable :: prgopt(:)
      real    (kind=4),allocatable :: work(:)
      real    (kind=4),allocatable :: primal(:)
      real    (kind=4),allocatable :: duals(:)
      real    (kind=4),allocatable :: cost4(:)
      integer (kind=4),allocatable :: indd(:)
      integer (kind=4),allocatable :: iwork(:)
      integer (kind=4),allocatable :: ibasis(:)

      integer (kind=4) :: imxx,icheck,npr,inequality,incost
      integer (kind=4) :: ncompr,ntlr,mcr(*),nptab,ibt,ind,imode,imeth
      real    (kind=8) :: ptabin(nptab),ptabin2(nptab)

      real    (kind=8) :: beta(ngf),alph(nry,ngf)
      integer (kind=4) :: igfx(ngf,3),mc(ntl,2)
      logical          :: fltf(ngf)

      external lpusrmat

c---  index failure modes in order of decreasing failure probability

      integer (kind=4) :: ntotal,ncomp,ncset,nlevel,ngfe,j,i,ntle,info
      integer (kind=4) :: imxx2,mtdat,minmax,icode,ier,iclin,nonzero
      integer (kind=4) :: lamat,lw,liw,lbm,mtinp,ibtnegative
      real    (kind=8) :: pfmin,pfmax,yl1,yu1,cdot
      character (len=80)  :: filname
      character (len=100) :: data 
      integer (kind=4) :: mtjh1, mtjh2

      integer (kind=4) :: is,ie, iii, iline
      integer (kind=4) :: mtdat2, nlev
      mtdat=52
c
      allocate(nset(maxset))
c
      mtjh1=91
      mtjh2=92
c      open(mtjh1,file=filnam2,form='formatted')
c      open(mtjh2,file=filnam3,form='formatted')
      open(mtjh1,file='',form='formatted')
      open(mtjh2,file='',form='formatted')

      if(ind.eq.0) then
        allocate(igfxe(ngf,3))
        allocate(mce(ntl,2))
        allocate(fltfe(ngf))
        allocate(icset(maxset,maxset))
        allocate(icomp(ngf))
        ngfe=ngf
        do j=1,3
        do i=1,ngf
          igfxe(i,j)=igfx(i,j)
        enddo
        enddo
        ntle=ntl
        do j=1,2
        do i=1,ntl
          mce(i,j)=mc(i,j)
        enddo
        enddo
        do i=1,ngf
          fltfe(i)=fltf(i)
        enddo
      else
        allocate(igfxe(ncompr,3))
        allocate(mce(ntlr,2))
        allocate(fltfe(ncompr))
        allocate(icset(maxset,maxset))
        allocate(icomp(ncompr))
        ngfe=ncompr
        do j=1,3
        do i=1,ncompr
          igfxe(i,j)=i
        enddo
        enddo
        ntle=ntlr
        do j=1,2
        do i=1,ntlr
          mce(i,j)=mcr(i)
        enddo
        enddo
        do i=1,ncompr
          fltfe(i)=.true.
        enddo
      endif
c
      npr=0
      if(icheck.ne.0) npr=not
c
      if(ind.eq.0.and.icl.eq.1) return
      call lpcnsets(not,ngfe,igfxe,ntle,mce,ncomp,icomp,
     *              ncset,nset,maxset,icset,fltfe,iclin)
      ntotal=2**ncomp
      nlevel=(ncomp+1)*ncomp/2
      nlevel=nlevel+ncomp*(ncomp-1)*(ncomp-2)/6
      nlevel=nlevel+1
c
      allocate(ibtable(nlevel,ntotal))
      allocate(cost(ntotal))
c
      call lpmksets(npr,ngfe,igfxe,ntotal,ncomp,icomp,nlevel,ibtable,
     *              ncset,nset,maxset,icset,cost,nonzero)
c
      if(incost.ne.0) then
        allocate(costimin(ntotal))
        allocate(costimax(ntotal))
        read(5,*) (costimin(i),i=1,ntotal)
        read(5,*) (costimax(i),i=1,ntotal)
        write(not,'(1x,'' '')')
        write(not,'(1x,''===== user input cost function (min)====='')')
        write(not,'(1x,'' '')')
        write(not,'(1x,10x,10i10)') (i,i=1,10)
        iline=-1
        do i=1,ntotal,10
        iline=iline+1
        is=i
        ie=i+9
        if(ie.gt.ntotal) ie=ntotal
        write(not,'(1x,i10,1p,10e10.3)')
     *   iline*10, (costimin(j),j=is,ie)
        enddo
        write(not,'(1x,'' '')')
        write(not,'(1x,''===== user input cost function (max)====='')')
        write(not,'(1x,'' '')')
        write(not,'(1x,10x,10i10)') (i,i=1,10)
        iline=-1
        do i=1,ntotal,10
        iline=iline+1
        is=i
        ie=i+9
        if(ie.gt.ntotal) ie=ntotal
        write(not,'(1x,i10,1p,10e10.3)')
     *  iline*10, (costimax(j),j=is,ie)
        enddo
      endif

      if(imeth.eq.0) then
c     ----- inner point method -----
        allocate(rhsin(nlevel*2))
        do imode=1,ibt
c         ----- minimize -----
          if(ibtnegative.eq.1.and.imode.ne.ibt) goto 9876
          if(imode.eq.1) then
            mtdat=61
            open(unit = mtdat,file='lp1.txt',form='formatted',
     &           status='unknown')
          elseif(imode.eq.2) then
            mtdat=62
            open(unit = mtdat,file='lp2.txt',form='formatted',
     &           status='unknown')
          else
            mtdat=63
            open(unit = mtdat,file='lp3.txt',form='formatted',
     &           status='unknown')
          endif
          if(incost.ne.0) then
            do iii=1,ntotal
              cost(iii)=costimin(iii)
            enddo
          endif
          call lpprep(not,ncomp,nlevel,ntotal,nry,icomp,ibtable,
     *        beta,alph,cost,mtdat,igfxe,ngfe,rhsin,ptabin,ind,imode,
     *        ptabin2,inequality)
c
c         ----- minimize problem -----
c
          filname='lpboun.mps'
          mtinp=85
          open(mtinp,file=filname,form='formatted',status='unknown')
          rewind mtdat
          rewind mtinp
  100     continue
          read(mtdat,'(a100)',end=199) data
          write(mtinp,'(a100)') data
          goto 100
  199     continue
          close(mtinp)
          close(mtdat)

          imxx2=int(1.5*real(imxx)) ! WIP truncate not round with nint
          minmax=1
          call lpbpmain(filname,icode,pfmin,minmax,imxx,imxx2,rhsin,
     *                 cost,mtjh2)
          if(icode.ne.2) call lperror(not,icode)
c
c         ----- maximize problem -----
c
          if(incost.ne.0) then
            if(imode.eq.1) then
              mtdat2=71
              open(mtdat2,file='lp12.txt',form='formatted',
     *         status='unknown')
            elseif(imode.eq.2) then
              mtdat2=72
              open(mtdat2,file='lp22.txt',form='formatted',
     *       status='unknown')
            else
              mtdat2=73
              open(mtdat2,file='lp32.txt',form='formatted',
     *       status='unknown')
            endif
            do iii=1,ntotal
              cost(iii)=costimax(iii)
            enddo
            call lpprep(not,ncomp,nlevel,ntotal,nry,icomp,ibtable,
     *      beta,alph,cost,mtdat2,igfxe,ngfe,rhsin,ptabin,ind,imode,
     *      ptabin2,inequality)
          endif

          if(incost.ne.0) then
            open(mtinp,file=filname,form='formatted',status='unknown')
            rewind mtdat2
            rewind mtinp
  200       continue
            read(mtdat2,'(a100)',end=299) data
            write(mtinp,'(a100)') data
            goto 200
  299       continue
            close(mtinp)
            close(mtdat2)
          endif

          minmax=-1
          call lpbpmain(filname,icode,pfmax,minmax,imxx,imxx2,rhsin,
     *              cost,mtjh2)
          pfmax=-pfmax
          if(icode.ne.2) call lperror(not,icode)
c         ----- output -----
          call dnormi(yl1,pfmin,ier)
          call dnormi(yu1,pfmax,ier)
          if(imode.eq.1) then
            write(not,1030)pfmin,pfmax,-yl1,-yu1
            write(mtjh1,'(1p4e15.5)')pfmin,pfmax,-yl1,-yu1
          elseif(imode.eq.2) then
            write(not,1031)pfmin,pfmax,-yl1,-yu1
            write(mtjh1,'(1p4e15.5)')pfmin,pfmax,-yl1,-yu1
          elseif(imode.eq.3) then
            write(not,1032)pfmin,pfmax,-yl1,-yu1
            write(mtjh1,'(1p4e15.5)')pfmin,pfmax,-yl1,-yu1
          endif
 9876     continue
         enddo
        deallocate(rhsin)
      else
c     ----- simplex method -----
        lamat=4*ntotal+7+100000
        lbm=8*nlevel+100000
        lw=4*ntotal+8*nlevel+lamat+lbm+100000
        liw= ntotal+11*nlevel+lamat+2*lbm+100000
        allocate(dattrv(2*nonzero+ntotal+1))
        allocate(bl(ntotal+nlevel))
        allocate(bu(ntotal+nlevel))
        allocate(indd(ntotal+nlevel))
        allocate(prgopt(4))
        allocate(work(lw))
        allocate(iwork(liw))
        allocate(primal(ntotal+nlevel))
        allocate(primal8(ntotal+nlevel))
        allocate(duals(ntotal+nlevel))
        allocate(ibasis(nlevel))
        allocate(cost4(ntotal))
        do i=1,ntotal
          cost4(i)=real(cost(i))
        enddo
c
        do imode=1,3
c         ----- minimize -----
          if(imode.eq.1) nlev=ncomp+1
          if(imode.eq.2) nlev=ncomp*(ncomp+1)/2+1
          if(imode.eq.3) nlev=+ncomp*(ncomp-1)*(ncomp-2)/6
     *                   +ncomp*(ncomp+1)/2+1
          prgopt(1)=4.0e0
          prgopt(2)=51.0e0
          prgopt(3)=3.0e0
          prgopt(4)=8.0e0
          prgopt(5)=63.0e0
          prgopt(6)=1.0e0
          prgopt(7)=1.0e-3
          prgopt(8)=1.0e0
          call lpprep2(not,ncomp,nlevel,ntotal,nry,icomp,ibtable,
     *                  beta,alph,bl,bu,indd,dattrv,ptabin,ind,imode)
          call lpsplp (lpusrmat, nlev, ntotal, cost4, prgopt, dattrv,
     +                 bl, bu, indd, info, primal, duals, ibasis, work,
     *                 lw, iwork, liw)
          do j=1,ntotal
            primal8(j)=primal(j)
          enddo
          pfmin=cdot(cost,primal8,1,1,ntotal)
          if(info.ne.1) then
            write(not,1000)
            stop
          endif
c         ----- maximize -----
          prgopt(1)=4.0e0
          prgopt(2)=50.0e0
          prgopt(3)=1.0e0
          prgopt(4)=1.0e0
          call lpsplp (lpusrmat, nlev, ntotal, cost4, prgopt, dattrv,
     +                 bl, bu, indd, info, primal, duals, ibasis, work,
     +                 lw, iwork, liw)
          do j=1,ntotal
            primal8(j)=primal(j)
          enddo
          pfmax=cdot(cost,primal8,1,1,ntotal)
          if(info.ne.1) then
            write(not,1000)
            stop
          endif
          call dnormi(yl1,pfmin,ier)
          call dnormi(yu1,pfmax,ier)
          if(imode.eq.1) then
            write(not,1030)pfmin,pfmax,-yl1,-yu1
            write(mtjh1,'(1p4e15.5)')pfmin,pfmax,-yl1,-yu1
          elseif(imode.eq.2) then
            write(not,1031)pfmin,pfmax,-yl1,-yu1
            write(mtjh1,'(1p4e15.5)')pfmin,pfmax,-yl1,-yu1
          elseif(imode.eq.3) then
            write(not,1032)pfmin,pfmax,-yl1,-yu1
            write(mtjh1,'(1p4e15.5)')pfmin,pfmax,-yl1,-yu1
          endif
        enddo
        deallocate(dattrv)
        deallocate(bl)
        deallocate(bu)
        deallocate(indd)
        deallocate(prgopt)
        deallocate(work)
        deallocate(iwork)
        deallocate(primal)
        deallocate(primal8)
        deallocate(cost4)
        deallocate(duals)
        deallocate(ibasis)
      endif
c
      deallocate(icset)
      deallocate(nset)
      deallocate(icomp)
      deallocate(ibtable)
      deallocate(cost)
      deallocate(igfxe)
      deallocate(mce)
      deallocate(fltfe)
      if(incost.ne.0) then
        deallocate(costimin)
        deallocate(costimax)
      endif
c
      return
 1000 format(1x,' failure in lp sub program.'/
     *          ' check the problem')
 1030 format(/
     *' uni-component lp bounds:',16x,'lower bound',7x,'upper bound'/
     *'   probability ......................',1pe15.5,' , ',1pe15.5/
     *'   generalized reliability index.....',1pe15.5,' , ',1pe15.5)
 1031 format(/
     *' bi -component lp bounds:',16x,'lower bound',7x,'upper bound'/
     *'   probability ......................',1pe15.5,' , ',1pe15.5/
     *'   generalized reliability index.....',1pe15.5,' , ',1pe15.5)
 1032 format(/
     *' tri-component lp bounds:',16x,'lower bound',7x,'upper bound'/
     *'   probability ......................',1pe15.5,' , ',1pe15.5/
     *'   generalized reliability index.....',1pe15.5,' , ',1pe15.5)

      end subroutine lpboun

c************************************************************************
      subroutine lperror(not,icode)
c************************************************************************
      implicit   none

      integer (kind=4) :: not,icode

      write(not,'(1h ,'' !! warning in lp(minimize)!! code='',i5)')icode
      write(not,'(1h ,'' !! solution is not guranteed !!'')')
      write(not,'(1h ,'' !! see file lp.log lp.out    !! '')')
      if(icode.le.1)then
         write(not,5)
      else if(icode.eq.3)then
         write(not,7)
      else if(icode.eq.4)then
         write(not,8)
      endif

!     Formats

    5 format(1x,'execution stopped.')
    7 format(1x,'problem is dual infeasibile (or badly scaled).')
    8 format(1x,'problem is primal infeasibile (or badly scaled).')

      end subroutine lperror
